Rails.application.routes.draw do
  concern :votable do
    member do
      put :downvote, action: 'downvote'
      put :upvote, action: 'upvote'
    end
  end

  concern :favoritable do
    member do
      put :toggle_like, action: 'toggle_like'
    end
  end

  concern :commentable do
    resources :comments, only: [:index, :create]
  end

  devise_for :cenobites, skip: :invitations, controllers: {
    sessions: 'sessions/sessions',
  }

  devise_scope :cenobite do
    # Disable every invitation route except "/accept", we're managing these in
    # acquaintances
    get 'cenobites/invitations/accept', to: 'devise/invitations#edit', as: :accept_cenobite_invitation
    put 'cenobites/invitations', to: 'devise/invitations#update', as: :cenobite_invitation
  end

  root to: 'hellcome#index'

  resources :links, only: [:index]
  resources :pastes, only: [:show] do
    member do
      match :reveal, to: 'pastes#reveal', via: [:put, :patch, :get]
    end
  end

  namespace :member do
    resources :acquaintances, only: [:index, :new, :create, :destroy] do
      member do
        post :accept
        put :resend_request
      end
    end
    resources :cenobites, only: [:index] do
      member do
        post :invite
      end
    end
    get :chat, to: 'chat#index', as: :chat
    resources :chat_rooms, except: [:new, :edit] do
      resources :chat_messages, except: [:new, :edit]
      resources :chat_presences, only: [:index]
    end
    resources :chat_participants, except: [:new, :create, :edit]
    resources :comments, except: [:index, :new, :create, :edit]
    resources :dead_man_switches, constraints: { id: /\d+/ } do
      member do
        put :ping
        put :rearm
      end
      collection do
        put :ping_all
      end
    end
    resources :events, concerns: [:commentable] do
      member do
        put :attend
        delete :leave
      end
    end
    resources :fortunes, except: [:show], concerns: :votable do
      collection do
        get :random
      end
    end
    resources :invites, only: [:destroy] do
      member do
        put :resend
      end
    end
    resources :items do
      collection do
        post :bulk_create
      end
    end
    resources :likes, only: [:index, :destroy]
    resources :links, except: [:index, :show], concerns: [:votable, :favoritable]
    resources :motions, concerns: [:votable, :commentable] do
      member do
        post :subscribe
        post :unsubscribe
      end
    end
    resources :page, only: [:show]
    resources :pastes
    resource :preferences, controller: :cenobites, only: [:edit, :update]
    resources :rss_feeds, except: [:show]
    resources :rss_items, only: [:index], concerns: [:votable, :favoritable]
    get :stats, to: 'stats#show', as: :stats
    resources :subscriptions, only: [:index, :destroy]
  end

  # Styleguide routes
  mount Styleguide::Engine => '/style' unless Rails.env.production?
end
