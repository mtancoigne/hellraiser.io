require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Hellraiser
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.0

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")

    # Load defaults
    config.hellraiser = config_for(:hellraiser_defaults)

    # Instance configuration may not have overrides for current environment
    # so we load it first and only merge if it contains something
    instance_config = config_for(:hellraiser) if Rails.root.join('config', 'hellraiser.yml').exist?
    config.hellraiser.deep_merge! instance_config if instance_config

    config.action_mailer.default_url_options = config.hellraiser.default_url_options
    config.action_mailer.default_options = config.hellraiser.default_email_options

    config.generators do |g|
      g.stylesheets false
      g.helper false
    end
  end
end
