module HellRaiser
  class DeadManSwitches
    def self.notify_expired
      DeadManSwitch.expired.unsent.find_each do |dms|
        dms.notify! if dms.expired?
      end
    end
  end
end
