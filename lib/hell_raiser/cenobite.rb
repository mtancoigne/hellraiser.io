require 'digest'

require 'hell_raiser/colors'

module HellRaiser
  class Cenobite
    class << self
      def colors(entity, parent_id: nil)
        raise 'No cenobite_id in entity' unless entity.cenobite_id

        string = "#{parent_id || entity.id}_#{entity.cenobite_id}_#{Rails.application.credentials.cenobite_id_salt}"

        Colors.string_colors string
      end
    end
  end
end
