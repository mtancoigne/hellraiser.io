require 'hell_raiser/rss/entry'
require 'hell_raiser/rss/feed'

module HellRaiser
  module Rss
    class InvalidFeedError < StandardError; end

    def self.fetch(url)
      raise InvalidFeedError unless /\A#{URI::DEFAULT_PARSER.make_regexp(%w[http https])}\z/.match?(url)

      connection = Faraday.new(url: url) do |faraday|
        faraday.use FaradayMiddleware::FollowRedirects
        faraday.adapter Faraday.default_adapter
      end

      response = connection.get

      Feedjira.parse response.body
    rescue Feedjira::NoParserAvailable, Faraday::ConnectionFailed
      raise InvalidFeedError
    end
  end
end
