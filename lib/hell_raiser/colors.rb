module HellRaiser
  class Colors
    class << self
      def string_colors(string)
        color       = {}
        color[:hex] = Digest::MD5.hexdigest(string)[0...6]
        rgb         = color[:hex].scan(/../).map { |h| h.hex.to_i }
        color[:bg]  = lighten(rgb, 50)
        color[:fg]  = darken(rgb, 50)

        color
      end

      def lighten(rgb, amount)
        rgb.map do |e|
          new = e - amount
          [new, 0].max
        end
      end

      def darken(rgb, amount)
        rgb.map do |e|
          new = e + amount
          [new, 255].max
        end
      end
    end
  end
end
