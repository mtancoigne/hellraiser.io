require 'feedjira'

module HellRaiser
  module Rss
    # Responsible for fetching and converting a feed to a hash usable for RssItem
    class Feed
      def initialize(feed)
        @feed = feed
        base_hash
      end

      def to_hash
        hash = case @feed
               when Feedjira::Parser::Atom
                 from_atom
               when Feedjira::Parser::AtomYoutube
                 from_atom_youtube
               when Feedjira::Parser::RSS
                 from_rss
               end

        @hash.merge hash
      end

      private

      def base_hash
        @hash = {
          title:           @feed.title,
          link:            @feed.url,
          last_fetched_at: Time.current,
        }
      end

      def from_atom
        {
          description: @feed.description,
          language:    nil,
        }
      end

      def from_atom_youtube
        {
          description: @feed.title,
        }
      end

      def from_rss
        {
          description: @feed.description,
          language:    @feed.language,
        }
      end
    end
  end
end
