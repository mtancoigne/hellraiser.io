require 'feedjira'

module HellRaiser
  module Rss
    class Entry
      def initialize(entry)
        @entry = entry
        base_hash
      end

      def to_hash
        hash = case @entry
               when Feedjira::Parser::AtomEntry
                 from_atom
               when Feedjira::Parser::AtomYoutubeEntry
                 from_atom_youtube
               when Feedjira::Parser::RSSEntry
                 from_rss
               end

        @hash.merge hash
      end

      private

      def base_hash
        @hash = {
          description:  @entry.summary,
          link:         @entry.url,
          published_at: @entry.published,
          title:        @entry.title,
        }
      end

      def from_atom
        {
          description: @entry.content || @entry.summary,
        }
      end

      def from_atom_youtube
        {
          description: @entry.content,
        }
      end

      def from_rss
        {}
      end
    end
  end
end
