namespace :chat do
  desc 'Empties the chat participant table. Should be used on every server boot'
  task 'clear-presences' => :environment do
    ChatPresence.delete_all
  end

  desc 'Remove old messages'
  task prune: :environment do
    max = Rails.configuration.hellraiser.chat[:messages_to_keep]
    raise 'Hellraiser configuration missing: "chat.messages_to_keep"' unless max

    ChatRoom.find_each do |chat_room|
      amount = chat_room.chat_messages.count
      chat_room.chat_messages.first(amount - max).each(&:destroy) if amount > max
    end
  end
end
