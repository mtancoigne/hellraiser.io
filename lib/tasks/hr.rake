require 'hell_raiser/dead_man_switches'
namespace :hr do
  desc 'Send emails from expired dead man switches'
  task 'dms:send_emails' => :environment do
    Rails.logger = Logger.new($stdout)
    Rails.logger.info '[DMS] Running dms:send_emails'

    HellRaiser::DeadManSwitches.notify_expired
  end

  task 'feeds:fetch' => :environment do
    Rails.logger = Logger.new($stdout)
    Rails.logger.info '[DMS] Running feeds:fetch'

    RssFeed.find_each(&:fetch_items)
  end
end
