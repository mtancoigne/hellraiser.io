<!--
Thank you for your participation!

Please, fill the sections or remove if unnecessary.
Note that this formalism only helps us to manage the MRs easily.
-->

<!-- Describe your merge request here -->

## Checks

<!--
Changelog is in app/views/pages/_changelog.*.md.erb

If you're unsure ~~strike through~~ an element.
Delete useless ones if you know what you're doing
-->

- [ ] I created/updated FactoryBot factories (preferably using Faker)
- [ ] I created/updated RSpec tests:
  - [ ] Acceptance
  - [ ] Channel
  - [ ] Class (lib/...)
  - [ ] Mailer
  - [ ] Model
  - [ ] Policy
  - [ ] Request
- [ ] I created/updated Cucumber scenarios
- [ ] My commits follows the [conventional commits format](https://www.conventionalcommits.org)
- [ ] I updated the readme to reflect my changes
- [ ] I updated the changelog to reflect my changes, in a separate commit
- [ ] I updated the styleguide to reflect my changes
