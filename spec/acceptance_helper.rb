require 'rspec_rails_api'

RSpec.configure do |config|
  config.include RSpec::Rails::Api::DSL::Example
end

renderer = RSpec::Rails::Api::OpenApiRenderer.new
# Options here should be customized
renderer.api_title       = 'Hellraiser.io'
renderer.api_version     = '1'
renderer.api_description = 'Something not related to the franchise'
# Options below are optional
renderer.api_servers = [{ url: 'https://hellraiser.io' }]

RSpec.configuration.after(:context, type: :acceptance) do |context|
  renderer.merge_context context.class.metadata[:rra].to_h
end

{
  error:        {
    error: { type: :string, description: 'The error' },
  },
  vote_details: {
    score:       { type: :integer, description: 'Amount of positive votes' },
    vote:        { type: :string, description: 'Performed action: "upvote" or "downvote"' },
    votes_count: { type: :integer, description: 'New total vote count' },
  },
  link:         {
    id:          { type: :integer, description: 'Link identifier' },
    title:       { type: :string, description: 'Title' },
    url:         { type: :string, description: 'Url' },
    description: { type: :string, description: 'Description' },
    visibility:  { type: :integer, description: 'Link visibility' },
    score:       { type: :integer, description: 'Vote score' },
    votes_count: { type: :integer, description: 'Total vote amount' },
    liked:       { type: :boolean, description: 'True when current cenobite liked the link' },
    cenobite_id: { type: :integer, required: false, description: 'Cenobite identifier' },
    created_at:  { type: :datetime, description: 'Creation date' },
    updated_at:  { type: :datetime, description: 'Update date' },
    tag_list:    { type: :string, description: 'Tags list, comma separated' },
  },
}.each_pair { |name, definition| RSpec::Rails::Api::Metadata.add_entity name, definition }
