require 'rails_helper'

RSpec.describe Member::ChatRoomsController, type: :acceptance do
  resource 'Chat rooms', 'Chat rooms management'

  entity :chat_room,
         id:              { type: :integer, description: 'Room identifier' },
         name:            { type: :string, description: 'Room name' },
         description:     { type: :string, required: false, description: 'Room description' },
         cenobite_id:     { type: :integer, required: false, description: 'Creator identifier. May be null if not current cenobite' },
         acquaintance_id: { type: :integer, required: false, description: 'Acquaintance id for chats between cenobites. Null when room is public' },
         created_at:      { type: :datetime, description: 'Creation date' },
         updated_at:      { type: :datetime, description: 'Update date' }

  parameters :chat_room_path_params,
             id: { type: :integer, description: 'Target room identifier' }

  parameters :chat_room_form_params,
             chat_room:        { type: :object, description: 'The room', attributes: {
               name:        { type: :string, description: 'Name' },
               description: { type: :string, description: 'Description' },
             } },
             chat_participant: { type: :object, description: 'Parameters for the creator', attributes: {
               name:      { type: :string, description: 'Name' },
               auto_join: { type: :string, description: 'Flag to join this room automatically' },
             } }

  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:chat_room) { FactoryBot.create(:chat_room, cenobite: cenobite) }
  let(:chat_room_other) { FactoryBot.create(:chat_room) }
  let(:create_payload) do
    {
      chat_room:        FactoryBot.build(:chat_room).attributes,
      chat_participant: { name: 'Commander' },
    }
  end
  let(:update_payload) { { chat_room: { name: 'Better name' } } }
  # Needed to do manual requests
  let(:request_headers) do
    {
      'Accept'       => 'application/json',
      'Content-Type' => 'application/json',
    }
  end

  on_get '/member/chat_rooms', 'List chat rooms' do
    for_code 200, expect_many: :chat_room do |url|
      chat_room
      chat_room_other
      sign_in cenobite
      test_response_of url
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url
    end
  end

  on_post '/member/chat_rooms', 'Create a room' do
    request_params defined: :chat_room_form_params

    for_code 201, expect_one: :chat_room do |url|
      sign_in cenobite
      test_response_of url, payload: create_payload
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url
    end
  end

  on_put '/member/chat_rooms/:id', 'Update chat room' do
    path_params defined: :chat_room_path_params
    request_params defined: :chat_room_form_params

    for_code 200, expect_one: :chat_room do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: chat_room.id }, payload: update_payload
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: chat_room_other.id }, payload: update_payload
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: chat_room.id }
    end
  end

  on_get '/member/chat_rooms/:id', 'Display chat room' do
    path_params defined: :chat_room_path_params

    for_code 200, expect_one: :chat_room do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: chat_room_other.id }
    end

    for_code 200, 'show cenobite id when not owned', expect_one: :chat_room, test_only: true do |url|
      sign_in cenobite
      aggregate_failures do
        test_response_of url, path_params: { id: chat_room.id }
        expect(JSON.parse(response.body)['cenobite_id']).to eq cenobite.id
      end
    end

    for_code 200, 'hide cenobite id when not owned', expect_one: :chat_room, test_only: true do |url|
      sign_in cenobite
      aggregate_failures do
        test_response_of url, path_params: { id: chat_room_other.id }
        expect(JSON.parse(response.body)['cenobite_id']).to be_nil
      end
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: chat_room.id }
    end
  end

  on_delete '/member/chat_rooms/:id', 'Destroy one owned chat room' do
    path_params defined: :chat_room_path_params

    for_code 204 do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: chat_room.id }
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: chat_room_other.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: chat_room.id }
    end
  end
end
