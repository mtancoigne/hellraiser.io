require 'rails_helper'

RSpec.describe Member::LinksController, type: :acceptance do
  resource 'Links', 'Links management'

  parameters :link_form_params,
             link: { type: :object, description: 'The link', attributes: {
               title:       { type: :string, description: 'Link title' },
               url:         { type: :string, description: 'Link url' },
               description: { type: :string, description: 'Link description' },
               visibility:  { type: :integer, description: 'Link visibility' },
               tag_list:    { type: :string, description: 'Tags list, comma separated' },
             } }

  parameters :link_path_params,
             id: { type: :integer, description: 'Target link identifier' }

  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:cenobite_other) { FactoryBot.create(:cenobite, :active) }
  let(:link) { FactoryBot.create(:link, cenobite: cenobite) }
  let(:link_other) { FactoryBot.create(:link, cenobite: cenobite_other) }
  let(:create_payload) { { link: FactoryBot.build(:link).attributes } }
  let(:update_payload) { { link: { title: 'New link' } } }

  on_post '/member/links', 'Create one link' do
    request_params defined: :link_form_params

    for_code 201, expect_one: :link do |url|
      sign_in cenobite
      test_response_of url, payload: create_payload
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url
    end
  end

  on_put '/member/links/:id', 'Update one owned link' do
    path_params defined: :link_path_params
    request_params defined: :link_form_params

    for_code 200, expect_one: :link do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: link.id }, payload: update_payload
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: link_other.id }
    end
  end

  on_put '/member/links/:id/upvote', 'Upvote a link' do
    path_params defined: :link_path_params

    for_code 200, expect_one: :vote_details do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: link.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: link.id }
    end
  end

  on_put '/member/links/:id/downvote', 'Downvote a link' do
    path_params defined: :link_path_params

    for_code 200, expect_one: :vote_details do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: link.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: link.id }
    end
  end

  on_delete '/member/links/:id', 'Destroy one owned link' do
    path_params defined: :link_path_params

    for_code 204 do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: link.id }
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: link_other.id }
    end
  end

  on_put '/member/links/:id/toggle_like', 'Toggles the favorite state' do
    path_params defined: :link_path_params

    for_code 200, expect_one: :link do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: link.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: link.id }
    end
  end
end
