require 'rails_helper'

RSpec.describe Member::RssFeedsController, type: :acceptance do
  resource 'RSS feeds', 'Manage RSS feeds'

  entity :rss_feed,
         id:              { type: :integer, description: 'Feed identifier' },
         url:             { type: :string, description: 'URL to the feed file' },
         title:           { type: :string, description: 'Title' },
         description:     { type: :string, required: false, description: 'Description' },
         language:        { type: :string, required: false, description: 'Feed language' },
         link:            { type: :string, required: false, description: 'Link to the website' },
         last_fetched_at: { type: :datetime, required: false, description: 'Last fetch date' },
         created_at:      { type: :datetime, description: 'Entry creation date' },
         updated_at:      { type: :datetime, description: 'Last update date for the entry' },
         cenobite_id:     { type: :integer, required: false, description: 'Cenobite identifier. Nil if not the signed in cenobite' },
         tag_list:        { type: :string, description: 'Associated tags' }

  parameters :rss_feed_path_params,
             id: { type: :integer, description: 'Target feed identifier' }

  parameters :rss_feed_create_form_params,
             rss_feed: { type: :object, description: 'The feed', attributes: {
               url:      { type: :string, description: 'Feed URL' },
               tag_list: { type: :string, description: 'Tag list, comma separated' },
             } }

  parameters :rss_feed_update_form_params,
             rss_feed: { type: :object, description: 'The feed', attributes: {
               tag_list: { type: :string, description: 'Tag list, comma separated' },
             } }

  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:cenobite_other) { FactoryBot.create(:cenobite, :active) }
  let(:feed_cassette) { 'rss_feed/get_rss_feed' }
  let(:feed_url) { 'https://korben.info/feed' }
  let(:other_feed_cassette) { 'rss_feed/get_atom_feed' }
  let(:other_feed_url) { 'https://linuxfr.org/news.atom' }
  let(:rss_feed) do
    feed = nil
    VCR.use_cassette feed_cassette do
      feed = FactoryBot.create(:rss_feed, url: feed_url, cenobite: cenobite)
    end
    feed
  end
  let(:rss_feed_other) do
    feed = nil
    VCR.use_cassette other_feed_cassette do
      feed = FactoryBot.create(:rss_feed, url: other_feed_url, cenobite: cenobite_other)
    end
    feed
  end
  let(:create_payload) { { rss_feed: { url: feed_url } } }
  let(:update_payload) { { rss_feed: { tags: 'test, dev, movies' } } }

  on_get '/member/rss_feeds', 'List the available feeds' do
    for_code 200, expect_many: :rss_feed do |url|
      rss_feed
      rss_feed_other
      sign_in cenobite
      test_response_of url
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url
    end
  end

  on_post '/member/rss_feeds', 'Create a feed' do
    request_params defined: :rss_feed_create_form_params

    for_code 201, expect_one: :rss_feed do |url|
      sign_in cenobite
      VCR.use_cassette feed_cassette do
        test_response_of url, payload: create_payload
      end
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url
    end
  end

  on_put '/member/rss_feeds/:id', 'Update one owned rss feed' do
    path_params defined: :rss_feed_path_params
    request_params defined: :rss_feed_update_form_params

    for_code 200, expect_one: :rss_feed do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: rss_feed.id }, payload: update_payload
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: rss_feed_other.id }
    end
  end

  on_delete '/member/rss_feeds/:id', 'Destroy one owned feed' do
    path_params defined: :rss_feed_path_params

    for_code 204 do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: rss_feed.id }
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: rss_feed_other.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: rss_feed.id }
    end
  end
end
