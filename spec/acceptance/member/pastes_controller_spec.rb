require 'rails_helper'

RSpec.describe Member::PastesController, type: :acceptance do
  resource 'Pastes', 'Pastes management'

  entity :paste,
         id:          { type: :string, description: 'Paste identifier' },
         name:        { type: :string, description: 'File name' },
         extension:   { type: :string, description: 'Extension' },
         content:     { type: :string, description: 'File content' },
         burn:        { type: :boolean, description: 'Burnable flag' },
         password:    { type: :boolean, description: 'Password protection state' },
         cenobite_id: { type: :integer, description: 'Cenobite identifier' },
         created_at:  { type: :datetime, description: 'Creation date' },
         updated_at:  { type: :datetime, description: 'Update date' }

  parameters :paste_path_params,
             id: { type: :integer, description: 'Target paste identifier' }

  parameters :paste_form_params,
             paste: { type: :object, description: 'The paste', attributes: {
               name:      { type: :string, description: 'File name' },
               extension: { type: :string, description: 'File extension' },
               content:   { type: :string, description: 'Content' },
               burn:      { type: :boolean, description: 'Burnable pastes will be destroyed on display' },
               password:  { type: :boolean, required: false, description: 'Password, leave blank for public access' },
             } }

  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:cenobite_other) { FactoryBot.create(:cenobite, :active) }
  let(:paste) { FactoryBot.create(:paste, cenobite: cenobite) }
  let(:paste_other) { FactoryBot.create(:paste, cenobite: cenobite_other) }
  let(:create_payload) { { paste: FactoryBot.build(:paste).attributes } }
  let(:update_payload) { { paste: { name: 'New_file' } } }

  on_get '/member/pastes', 'List owned pastes' do
    for_code 200, expect_many: :paste do |url|
      FactoryBot.create_list(:paste, 2, cenobite: cenobite)
      sign_in cenobite
      test_response_of url
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url
    end
  end

  on_post '/member/pastes', 'Create one paste' do
    request_params defined: :paste_form_params

    for_code 201, expect_one: :paste do |url|
      sign_in cenobite
      test_response_of url, payload: create_payload
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url
    end
  end

  on_get '/member/pastes/:id', 'Display one owned paste' do
    path_params defined: :paste_path_params

    for_code 200, expect_one: :paste do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: paste.id }
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: paste_other.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: paste.id }
    end
  end

  on_put '/member/pastes/:id', 'Update one owned paste' do
    path_params defined: :paste_path_params
    request_params defined: :paste_form_params

    for_code 200, expect_one: :paste do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: paste.id }, payload: update_payload
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: paste_other.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: paste.id }
    end
  end

  on_delete '/member/pastes/:id', 'Destroy one owned paste' do
    path_params defined: :paste_path_params

    for_code 204 do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: paste.id }
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: paste_other.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: paste.id }
    end
  end
end
