require 'rails_helper'

RSpec.describe Member::FortunesController, type: :acceptance do
  resource 'Fortunes', 'Fortunes management'

  entity :fortune,
         id:              { type: :integer, description: 'Fortune identifier' },
         content:         { type: :string, description: 'Content' },
         source:          { type: :string, required: false, description: 'Source: may be a pseudo, an URL, ...' },
         score:           { type: :integer, description: 'Vote score' },
         votes_count:     { type: :integer, description: 'Total vote amount' },
         created_at:      { type: :datetime, description: 'Creation date' },
         updated_at:      { type: :datetime, description: 'Update date' },
         cenobite_id:     { type: :integer, required: false, description: 'Cenobite identifier' },
         cenobite_colors: { type: :object, description: 'Colors to represent cenobite', attributes: {
           hex:  { type: :string, description: 'Direct hex representation. Can be used as name' },
           fg:   { type: :array, description: 'RGB array to use as background-color' },
           bg:   { type: :array, description: 'RGB array to use as text color' },
           name: { type: :string, required: false, description: 'Name, should be null for fortunes' },
         } }

  parameters :fortune_path_params,
             id: { type: :integer, description: 'Target fortune identifier' }

  parameters :fortune_form_params,
             fortune: { type: :object, description: 'The fortune', attributes: {
               title:   { type: :string, description: 'Fortune title' },
               content: { type: :string, description: 'Fortune content' },
               source:  { type: :string, required: false, description: 'Optional source' },
             } }

  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:cenobite_other) { FactoryBot.create(:cenobite, :active) }
  let(:fortune) { FactoryBot.create(:fortune, cenobite: cenobite) }
  let(:fortune_other) { FactoryBot.create(:fortune, cenobite: cenobite_other) }
  let(:create_payload) { { fortune: FactoryBot.build(:fortune).attributes } }
  let(:update_payload) { { fortune: { content: 'Hahahahah!' } } }

  on_get '/member/fortunes', 'List current cenobite\'s fortunes' do
    for_code 200, expect_many: :fortune do |url|
      FactoryBot.create_list(:fortune, 2, cenobite: cenobite)
      sign_in cenobite
      test_response_of url
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url
    end
  end

  on_post '/member/fortunes', 'Create one fortune' do
    request_params defined: :fortune_form_params

    for_code 201, expect_one: :fortune do |url|
      sign_in cenobite
      test_response_of url, payload: create_payload
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url
    end
  end

  on_get '/member/fortunes/random', 'Display one fortune' do
    for_code 200, expect_one: :fortune do |url|
      sign_in cenobite
      fortune
      test_response_of url
    end
  end

  on_put '/member/fortunes/:id', 'Update one owned fortune' do
    path_params defined: :fortune_path_params
    request_params defined: :fortune_form_params

    for_code 200, expect_one: :fortune do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: fortune.id }, payload: update_payload
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: fortune_other.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: fortune.id }
    end
  end

  on_put '/member/fortunes/:id/upvote', 'Upvote a fortune' do
    path_params defined: :fortune_path_params

    for_code 200, expect_one: :vote_details do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: fortune.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: fortune.id }
    end
  end

  on_put '/member/fortunes/:id/downvote', 'Downvote a fortune' do
    path_params defined: :fortune_path_params

    for_code 200, expect_one: :vote_details do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: fortune.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: fortune.id }
    end
  end

  on_delete '/member/fortunes/:id', 'Destroy one owned fortune' do
    path_params defined: :fortune_path_params

    for_code 204 do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: fortune.id }
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: fortune_other.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: fortune.id }
    end
  end
end
