require 'rails_helper'

RSpec.describe Member::ChatMessagesController, type: :acceptance do
  resource 'Chat messages', 'Chat messages management'

  entity :chat_message,
         id:                  { type: :integer, description: 'Message identifier' },
         content:             { type: :string, required: false, description: 'Message content' },
         system:              { type: :boolean, description: 'Flag for system messages' },
         chat_participant_id: { type: :integer, description: 'Creator identifier' },
         chat_room_id:        { type: :integer, description: 'Chat room identifier' },
         chat_participant:    { type: :object, required: false, description: 'Participant information. Null for system messages', attributes: {
           id:   { type: :integer, description: 'Participant identifier' },
           name: { type: :string, description: 'Participant name' },
         } },
         file:                { type: :object, required: false, description: 'Attached file informations', attributes: {
           content_type: { type: :string, description: 'Mime type' },
           metadata:     { type: :object, description: 'File metadata. Depends on content_type' },
           file_name:    { type: :string, description: 'Original file name' },
           byte_size:    { type: :string, description: 'Size in bytes' },
           urls:         { type: :string, description: 'Urls to representations variants. the "file" key represents the original file' },
         } },
         created_at:          { type: :datetime, description: 'Creation date' },
         updated_at:          { type: :datetime, description: 'Update date' }

  parameters :chat_message_path_params,
             id:           { type: :integer, description: 'Target message identifier' },
             chat_room_id: { type: :integer, description: 'Target room identifier' }

  parameters :chat_messages_path_params,
             chat_room_id: { type: :integer, description: 'Target room identifier' }

  parameters :chat_message_form_params,
             chat_message: { type: :object, description: 'The message', attributes: {
               content: { type: :string, description: 'Message' },
             } }

  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  # Joined chat room
  let(:chat_room) { FactoryBot.create(:chat_room, cenobite: cenobite) }
  let(:chat_participant) { FactoryBot.create(:chat_participant, chat_room: chat_room, cenobite: cenobite) }
  let(:chat_presence) { FactoryBot.create(:chat_presence, chat_room: chat_room, chat_participant: chat_participant) }
  let(:chat_message) { FactoryBot.create(:chat_message, chat_participant: chat_participant, chat_room: chat_room) }

  # Joined room, but no presence
  let(:chat_room_other) { FactoryBot.create(:chat_room) }
  let(:chat_message_other) { FactoryBot.create(:chat_message, chat_room: chat_room_other) }

  let(:create_payload) { FactoryBot.build(:chat_message).attributes }
  let(:update_payload) { { chat_message: { content: 'Better pun' } } }

  on_get '/member/chat_rooms/:chat_room_id/chat_messages', 'List chat messages' do
    path_params defined: :chat_messages_path_params

    # List messages in a joined room
    for_code 200, expect_many: :chat_message do |url|
      chat_message
      sign_in cenobite
      test_response_of url, path_params: { chat_room_id: chat_room.id }
    end

    # List messages in a non-joined room
    for_code 403, expect_one: :error do |url|
      chat_message_other
      sign_in cenobite
      test_response_of url, path_params: { chat_room_id: chat_room_other.id }
    end

    # List messages as unauthenticated
    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { chat_room_id: chat_room.id }
    end
  end

  on_post '/member/chat_rooms/:chat_room_id/chat_messages', 'Send a message' do
    path_params defined: :chat_messages_path_params
    request_params defined: :chat_message_form_params

    # Create message in a joined room
    for_code 201, expect_one: :chat_message do |url|
      chat_presence # Join the room
      sign_in cenobite
      test_response_of url, path_params: { chat_room_id: chat_room.id }, payload: create_payload
    end

    # Create message in a non-joined room
    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { chat_room_id: chat_room_other.id }, payload: create_payload
    end

    # Create message as unauthenticated
    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { chat_room_id: chat_room.id }, payload: create_payload
    end
  end

  on_put '/member/chat_rooms/:chat_room_id/chat_messages/:id', 'Update chat message' do
    path_params defined: :chat_message_path_params
    request_params defined: :chat_message_form_params

    # Update owned message in a joined room
    for_code 200, expect_one: :chat_message do |url|
      sign_in cenobite
      test_response_of url, path_params: { chat_room_id: chat_room.id, id: chat_message.id }, payload: update_payload
    end

    # Update owned message in a room where cenobite is no more presence
    # Other tests are made in the policy spec, we only test some responses here.
    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { chat_room_id: chat_room_other.id, id: chat_message_other.id }, payload: update_payload
    end

    # Update a message, unauthenticated
    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { chat_room_id: chat_room.id, id: chat_room.id }
    end
  end

  on_get '/member/chat_rooms/:chat_room_id/chat_messages/:id', 'Display chat message' do
    path_params defined: :chat_message_path_params

    # Display message from a joined room
    for_code 200, expect_one: :chat_message do |url|
      sign_in cenobite
      test_response_of url, path_params: { chat_room_id: chat_room.id, id: chat_message.id }
    end

    # Display message in a room where cenobite is no more presence
    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { chat_room_id: chat_room_other.id, id: chat_message_other.id }
    end

    # Display message unauthenticated
    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { chat_room_id: chat_room.id, id: chat_message.id }
    end
  end

  on_delete '/member/chat_rooms/:chat_room_id/chat_messages/:id', 'Destroy one owned chat message' do
    path_params defined: :chat_message_path_params

    # Destroy owned message in a joined room
    for_code 204 do |url|
      sign_in cenobite
      test_response_of url, path_params: { chat_room_id: chat_room.id, id: chat_message.id }
    end

    # Destroy message in a room where cenobite is no more presence
    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { chat_room_id: chat_room_other.id, id: chat_message_other.id }
    end

    # Destroy a message unauthenticated
    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { chat_room_id: chat_room.id, id: chat_message.id }
    end
  end
end
