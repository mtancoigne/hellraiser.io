require 'rails_helper'

RSpec.describe Member::EventsController, type: :acceptance do
  resource 'Events', 'Events management'

  entity :event,
         id:                 { type: :integer, description: 'Event identifier' },
         name:               { type: :string, description: 'Name' },
         description:        { type: :string, required: false, description: 'Description' },
         place:              { type: :string, required: false, description: 'Optional place for the event' },
         url:                { type: :string, required: false, description: 'Optional link to something online' },
         visibility:         { type: :integer, description: 'Event visibility' },
         cenobite_id:        { type: :integer, required: false, description: 'Cenobite identifier' },
         starts_at:          { type: :datetime, description: 'Event start date and time' },
         starts_at_timezone: { type: :string, description: 'Timezone used by user for event creation' },
         ends_at:            { type: :datetime, description: 'Event end date and time' },
         ends_at_timezone:   { type: :string, description: 'Timezone used by user for event creation' },
         attendees_amount:   { type: :integer, description: 'Number of attendees for this event' },
         attendees:          { type: :array, required: false, description: 'Attendees names, only visible to event owner' },
         created_at:         { type: :datetime, description: 'Creation date' },
         updated_at:         { type: :datetime, description: 'Update date' }

  parameters :event_form_params,
             event: { type: :object, description: 'The event', attributes: {
               name:        { type: :string, description: 'Name' },
               description: { type: :string, required: false, description: 'Description' },
               place:       { type: :string, required: false, description: 'Optional place for the event' },
               url:         { type: :string, required: false, description: 'Optional link to something online' },
               starts_at:   { type: :datetime, description: 'Event start date and time' },
               ends_at:     { type: :datetime, description: 'Event end date and time' },
             } }

  parameters :event_path_params,
             id: { type: :integer, description: 'Target event identifier' }

  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:cenobite_other) { FactoryBot.create(:cenobite, :active) }
  let(:event) { FactoryBot.create(:event, cenobite: cenobite) }
  let(:event_other) { FactoryBot.create(:event, cenobite: cenobite_other) }
  let(:event_other_private) { FactoryBot.create(:event, :visible_by_owner, cenobite: cenobite_other) }
  let(:create_payload) { { event: FactoryBot.build(:event).attributes } }
  let(:update_payload) { { event: { title: 'New event' } } }

  on_post '/member/events', 'Create one event' do
    request_params defined: :event_form_params

    for_code 201, expect_one: :event do |url|
      sign_in cenobite
      test_response_of url, payload: create_payload
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url
    end
  end

  on_get '/member/events/:id', 'Display one event' do
    path_params defined: :event_path_params

    for_code 200, expect_one: :event do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: event.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: event.id }
    end
  end

  on_put '/member/events/:id', 'Update one owned event' do
    path_params defined: :event_path_params
    request_params defined: :event_form_params

    for_code 200, expect_one: :event do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: event.id }, payload: update_payload
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: event_other.id }
    end
  end

  on_delete '/member/events/:id', 'Destroy one owned event' do
    path_params defined: :event_path_params

    for_code 204 do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: event.id }
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: event_other.id }
    end
  end

  on_put '/member/events/:id/attend', 'Join an event' do
    path_params defined: :event_path_params

    for_code 201, expect_one: :event do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: event_other.id }
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: event_other_private.id }
    end
  end

  on_delete '/member/events/:id/leave', 'Leave an event' do
    path_params defined: :event_path_params

    for_code 204 do |url|
      event.add_attendee! cenobite
      sign_in cenobite
      test_response_of url, path_params: { id: event.id }
    end

    # When there is no attendance previously created
    for_code 404, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: event.id }
    end
  end
end
