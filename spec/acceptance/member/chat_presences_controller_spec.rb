require 'rails_helper'

RSpec.describe Member::ChatPresencesController, type: :acceptance do
  resource 'Chat presences', 'Chat presences management'

  entity :chat_presence,
         id:                  { type: :integer, description: 'Presence identifier' },
         content:             { type: :string, required: false, description: 'Presence content' },
         chat_participant_id: { type: :integer, description: 'Creator identifier' },
         chat_room_id:        { type: :integer, description: 'Chat room identifier' },
         chat_participant:    { type: :object, description: 'Participant information', attributes: {
           id:   { type: :integer, description: 'Participant identifier' },
           name: { type: :string, description: 'Participant name' },
         } },
         created_at:          { type: :datetime, description: 'Creation date' },
         updated_at:          { type: :datetime, description: 'Update date' }

  parameters :chat_presences_path_params,
             chat_room_id: { type: :integer, description: 'Target room identifier' }

  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  # Joined chat room
  let(:chat_room) do
    FactoryBot.create(:chat_room)
  end
  let(:chat_participant) { FactoryBot.create(:chat_participant, cenobite: cenobite, chat_room: chat_room) }
  # Chat presence in a joined room
  let(:cenobite_joined) { FactoryBot.create(:chat_presence, chat_participant: chat_participant, chat_room: chat_room) }
  # Other cenobite in the same room
  let(:other_joined) { FactoryBot.create(:chat_presence, chat_room: chat_room) }

  on_get '/member/chat_rooms/:chat_room_id/chat_presences', 'List chat presences' do
    path_params defined: :chat_presences_path_params

    # List presences in a joined room
    for_code 200, expect_many: :chat_presence do |url|
      cenobite_joined
      other_joined
      sign_in cenobite
      test_response_of url, path_params: { chat_room_id: chat_room.id }
    end

    # List presences in a non-joined room
    for_code 403, expect_one: :error do |url|
      other_joined
      sign_in cenobite
      test_response_of url, path_params: { chat_room_id: chat_room.id }
    end

    # List presences as unauthenticated
    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { chat_room_id: chat_room.id }
    end
  end
end
