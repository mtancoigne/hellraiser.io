require 'rails_helper'

RSpec.describe Member::RssItemsController, type: :acceptance do
  resource 'RSS items', 'Display item from feeds'

  entity :rss_item,
         id:           { type: :integer, description: 'RSS Item identifier' },
         title:        { type: :string, description: 'Article title' },
         link:         { type: :string, description: 'Link to the article' },
         description:  { type: :string, description: 'Article description/summary' },
         published_at: { type: :datetime, required: false, description: 'Publication date, if any' },
         score:        { type: :integer, description: 'Amount of positive votes' },
         votes_count:  { type: :integer, description: 'Total amount of votes' },
         rss_feed_id:  { type: :integer, description: 'RSS feed identifier' },
         liked:        { type: :boolean, description: 'Whether or not the item has been favorited by user' },
         created_at:   { type: :datetime, description: 'Creation date for the entry' },
         updated_at:   { type: :datetime, description: 'Update date for the entry' }

  parameters :rss_item_path_params,
             id: { type: :integer, description: 'Target item identifier' }

  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:feed_cassette) { 'rss_feed/get_atom_feed' }
  let(:feed_url) { 'https://linuxfr.org/news.atom' }

  before do
    VCR.use_cassette feed_cassette do
      FactoryBot.create(:rss_feed, url: feed_url, cenobite: cenobite)
    end
  end

  on_get '/member/rss_items', 'List all items' do
    for_code 200, expect_many: :rss_item do |url|
      sign_in cenobite
      test_response_of url
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url
    end
  end

  on_put '/member/rss_items/:id/upvote', 'Upvote a a RSS item' do
    path_params defined: :rss_item_path_params

    for_code 200, expect_one: :vote_details do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: RssItem.last.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: RssItem.last.id }
    end
  end

  on_put '/member/rss_items/:id/downvote', 'Downvote a RSS item' do
    path_params defined: :rss_item_path_params

    for_code 200, expect_one: :vote_details do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: RssItem.last.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: RssItem.last.id }
    end
  end

  on_put '/member/rss_items/:id/toggle_like', 'Toggles the favorite state' do
    path_params defined: :rss_item_path_params

    for_code 200, expect_one: :rss_item do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: RssItem.last.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: RssItem.last.id }
    end
  end
end
