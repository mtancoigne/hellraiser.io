require 'rails_helper'

RSpec.describe Member::AcquaintancesController, type: :acceptance do
  resource 'Acquaintances', 'Acquaintances management'

  entity :acquaintance,
         id:          { type: :integer, description: 'Acquaintance identifier' },
         inviter_id:  { type: :integer, description: 'Identifier of the cenobite who proposed the acquaintance' },
         invitee_id:  { type: :integer, description: 'Identifier of the targeted cenobite' },
         accepted_at: { type: :datetime, required: false, description: 'Acceptation date, if accepted' },
         created_at:  { type: :datetime, description: 'Creation date' },
         cenobite:    { type: :object, description: 'Information on the "other" cenobite (the one that\s" not you)', attributes: {
           id:    { type: :integer, required: false, description: 'Cenobite identifier' },
           name:  { type: :string, required: false, description: 'Cenobite name. Only visible if accepted' },
           email: { type: :string, required: false, description: 'Cenobite email. Only visible under some conditions.' },
         } }

  parameters :acquaintance_path_params,
             id: { type: :integer, description: 'Target acquaintance identifier' }

  parameters :acquaintance_form_params,
             acquaintance: { type: :object, description: 'The acquaintance request', attributes: {
               email: { type: :string, description: 'Email of the target cenobite' },
             } }

  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:other_pending_acquaintance) { FactoryBot.create(:acquaintance) }
  let(:other_valid_acquaintance) { FactoryBot.create(:acquaintance, :accepted) }

  on_get '/member/acquaintances', 'List acquaintances' do
    for_code 200, expect_many: :acquaintance do |url|
      FactoryBot.create(:acquaintance, inviter: cenobite)
      FactoryBot.create(:acquaintance, invitee: cenobite)
      sign_in cenobite

      test_response_of url
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url
    end
  end

  on_post '/member/acquaintances', 'Invite someone' do
    request_params defined: :acquaintance_form_params
    for_code 201, expect_one: :acquaintance do |url|
      other_cenobite = FactoryBot.create(:cenobite, :active)
      sign_in cenobite

      test_response_of url, payload: { acquaintance: { email: other_cenobite.email } }
    end
  end

  on_post '/member/acquaintances/:id/accept', 'Accept an acquaintance request' do
    path_params defined: :acquaintance_path_params

    for_code 201, expect_one: :acquaintance do |url|
      acquaintance = FactoryBot.create(:acquaintance, invitee: cenobite)
      sign_in cenobite

      test_response_of url, path_params: { id: acquaintance.id }
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: other_pending_acquaintance.id }
    end
  end

  on_delete '/member/acquaintances/:id', 'Destroy an acquaintance' do
    path_params defined: :acquaintance_path_params

    for_code 204 do |url|
      acquaintance = FactoryBot.create(:acquaintance, :accepted, invitee: cenobite)
      sign_in cenobite

      test_response_of url, path_params: { id: acquaintance.id }
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: other_valid_acquaintance.id }
    end
  end
end
