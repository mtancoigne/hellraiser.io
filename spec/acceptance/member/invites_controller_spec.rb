require 'rails_helper'

RSpec.describe Member::InvitesController, type: :acceptance do
  resource 'Invites', 'Destroy or resend invites'

  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:invited_cenobite) { Cenobite.invite!({ email: Faker::Internet.email }, cenobite) }

  on_put '/member/invites/:id/resend', 'Resend cenobite invitation' do
    path_params fields: { id: { type: :integer, description: 'Target cenobite identifier' } }

    for_code 204 do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: invited_cenobite.id }
    end

    for_code 404, expect_one: :error do |url|
      sign_in cenobite
      invited_cenobite.update!(invitation_accepted_at: Time.current)
      test_response_of url, path_params: { id: invited_cenobite.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: invited_cenobite.id }
    end
  end

  on_delete '/member/invites/:id', 'Destroy cenobite invitation' do
    path_params fields: { id: { type: :integer, description: 'Target cenobite identifier' } }

    for_code 204 do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: invited_cenobite.id }
    end

    for_code 404, expect_one: :error do |url|
      sign_in cenobite
      invited_cenobite.update!(invitation_accepted_at: Time.current)
      test_response_of url, path_params: { id: invited_cenobite.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: invited_cenobite.id }
    end
  end
end
