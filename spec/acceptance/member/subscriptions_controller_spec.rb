require 'rails_helper'

RSpec.describe Member::SubscriptionsController, type: :acceptance do
  resource 'Subscriptions', 'Subscriptions management'

  entity :subscription,
         id:          { type: :integer, description: 'Motion identifier' },
         entity_type: { type: :string, description: 'Association type' },
         entity_id:   { type: :integer, description: 'Association identifier' },
         cenobite_id: { type: :integer, description: 'Subscription holder' },
         created_at:  { type: :datetime, description: 'Creation date' },
         updated_at:  { type: :datetime, description: 'Update date' }

  parameters :subscription_path_params,
             id: { type: :integer, description: 'Target subscription identifier' }

  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:cenobite_other) { FactoryBot.create(:cenobite, :active) }
  let(:subscription) { FactoryBot.create(:subscription, :on_motion, cenobite: cenobite) }
  let(:subscription_other) { FactoryBot.create(:subscription, :on_motion, cenobite: cenobite_other) }

  on_get '/member/subscriptions', 'List subscriptions' do
    for_code 200, expect_many: :subscription do |url|
      FactoryBot.create_list(:subscription, 2, :on_motion, cenobite: cenobite)
      sign_in cenobite
      test_response_of url
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url
    end
  end

  on_delete '/member/subscriptions/:id', 'Destroy one subscription' do
    path_params defined: :subscription_path_params

    for_code 204 do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: subscription.id }
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: subscription_other.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: subscription.id }
    end
  end
end
