require 'rails_helper'

RSpec.describe Member::MotionsController, type: :acceptance do
  resource 'Motions', 'Motions management'

  entity :motion,
         id:                { type: :integer, description: 'Motion identifier' },
         title:             { type: :string, description: 'Title' },
         content:           { type: :string, description: 'Content' },
         closed:            { type: :boolean, description: 'Is this motion closed ?' },
         score:             { type: :integer, description: 'Vote score' },
         votes_count:       { type: :integer, description: 'Total vote amount' },
         has_picture:       { type: :boolean, description: 'Picture flag' },
         picture:           { type: :object, required: false, description: 'Attached file informations', attributes: {
           content_type: { type: :string, description: 'Mime type' },
           metadata:     { type: :object, description: 'File metadata. Depends on content_type' },
           file_name:    { type: :string, description: 'Original file name' },
           byte_size:    { type: :string, description: 'Size in bytes' },
           urls:         { type: :string, description: 'Urls to representations variants. the "file" key represents the original file' },
         } },
         cenobite_id:       { type: :integer, required: false, description: 'Cenobite identifier' },
         notable_change_at: { type: :datetime, description: 'Date of the last comment if any, or last update date. Use to order by activity.' },
         created_at:        { type: :datetime, description: 'Creation date' },
         updated_at:        { type: :datetime, description: 'Update date' },
         cenobite_colors:   { type: :object, description: 'Colors to represent cenobite', attributes: {
           hex:  { type: :string, description: 'Direct hex representation. Can be used as name' },
           fg:   { type: :array, description: 'RGB array to use as background-color' },
           bg:   { type: :array, description: 'RGB array to use as text color' },
           name: { type: :string, required: false, description: 'Name, for non-anonymous content' },
         } }

  parameters :motion_path_params,
             id: { type: :integer, description: 'Target motion identifier' }

  parameters :motion_create_form_params,
             motion: { type: :object, description: 'The motion', attributes: {
               title:     { type: :string, description: 'Motion title' },
               content:   { type: :string, description: 'Motion content' },
               anonymous: { type: :boolean, description: 'Make the cenobites publish their usernames' },
               picture:   { type: :string, required: false, description: 'Optional picture. Use a FormData object as payload' },
             } }

  parameters :motion_update_form_params,
             motion: { type: :object, description: 'The motion', attributes: {
               title:   { type: :string, description: 'Motion title' },
               content: { type: :string, description: 'Motion content' },
               closed:  { type: :boolean, description: 'Mark the motion as done' },
               picture: { type: :string, required: false, description: 'Optional picture. Use a FormData object as payload' },
             } }

  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:cenobite_other) { FactoryBot.create(:cenobite, :active) }
  let(:motion) { FactoryBot.create(:motion, cenobite: cenobite) }
  let(:motion_other) { FactoryBot.create(:motion, cenobite: cenobite_other) }
  let(:create_payload) { { motion: FactoryBot.build(:motion).attributes } }
  let(:update_payload) { { motion: { title: 'New motion' } } }

  on_get '/member/motions', 'List motions' do
    for_code 200, expect_many: :motion do |url|
      FactoryBot.create_list(:motion, 2)
      sign_in cenobite
      test_response_of url
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url
    end
  end

  on_post '/member/motions', 'Create one motion' do
    request_params defined: :motion_create_form_params

    for_code 201, expect_one: :motion do |url|
      sign_in cenobite
      test_response_of url, payload: create_payload
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url
    end
  end

  on_get '/member/motions/:id', 'Display one motion' do
    path_params defined: :motion_path_params

    for_code 200, expect_one: :motion do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: motion_other.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: motion.id }
    end
  end

  on_put '/member/motions/:id', 'Update one owned motion' do
    path_params defined: :motion_path_params
    request_params defined: :motion_update_form_params

    for_code 200, expect_one: :motion do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: motion.id }, payload: update_payload
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: motion_other.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: motion.id }
    end
  end

  on_put '/member/motions/:id/upvote', 'Upvote a motion' do
    path_params defined: :motion_path_params

    for_code 200, expect_one: :vote_details do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: motion.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: motion.id }
    end
  end

  on_put '/member/motions/:id/downvote', 'Downvote a motion' do
    path_params defined: :motion_path_params

    for_code 200, expect_one: :vote_details do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: motion.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: motion.id }
    end
  end

  on_delete '/member/motions/:id', 'Destroy one owned motion' do
    path_params defined: :motion_path_params

    for_code 204 do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: motion.id }
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: motion_other.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: motion.id }
    end
  end
end
