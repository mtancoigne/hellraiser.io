require 'rails_helper'

RSpec.describe Member::StatsController, type: :acceptance do
  resource 'Stats', 'Site stats'

  entity :stats_object,
         cenobites: { type: :object, description: 'Cenobites stats', attributes: {
           total:    { type: :integer, description: 'Total cenobite count' },
           active:   { type: :integer, description: 'Active cenobites' },
           inactive: { type: :integer, description: 'Inactive cenobites' },
         } },
         motions:   { type: :object, description: 'Motions stats', attributes: {
           total:          { type: :integer, description: 'Total motions count' },
           closed:         { type: :integer, description: 'Closed motions' },
           open:           { type: :integer, description: 'Open motions' },
           total_comments: { type: :integer, description: 'Total comments count' },
           graph_data:     { type: :array, description: 'Data for graphs', of: {
             # FIXME: data graph defined twice
             # This is due to an issue with rspec-rails-api not being able to use sub-objects
             name: { type: :string, description: 'Serie name' },
             data: { type: :object, description: 'Datas' },
           } },
         } },
         pastes:    { type: :object, description: 'Pastes stats', attributes: {
           total:              { type: :integer, description: 'Total pastes amount (burned pastes does not count)' },
           password_protected: { type: :integer, description: 'Password protected pastes' },
           burnable:           { type: :integer, description: 'Burnable (and not yet seen) pastes' },
         } },
         dms:       { type: :integer, description: 'Dead man switches stats' },
         links:     { type: :object, description: 'Links stats', attributes: {
           total:      { type: :integer, description: 'Total links amount' },
           public:     { type: :integer, description: 'Public links' },
           member:     { type: :integer, description: 'Member links' },
           private:    { type: :integer, description: 'Private links' },
           graph_data: { type: :array, description: 'Data for graphs', of: {
             name: { type: :string, description: 'Serie name' },
             data: { type: :object, description: 'Datas' },
           } },
         } },
         items:     { type: :object, description: 'Items stats', attributes: {
           total: { type: :integer, description: 'Total items amount' },
         } }

  let(:cenobite) { FactoryBot.create(:cenobite, :active) }

  on_get '/member/stats', 'Display stats' do
    for_code 200, expect_one: :stats_object do |url|
      sign_in cenobite
      test_response_of url
    end
  end
end
