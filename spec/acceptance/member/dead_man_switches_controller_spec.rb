require 'rails_helper'

RSpec.describe Member::DeadManSwitchesController do
  resource 'Dead man switches', 'Manage dead man switches'

  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:dead_man_switch) { FactoryBot.create(:dead_man_switch, cenobite: cenobite) }
  let(:dead_man_switch_sent) { FactoryBot.create(:dead_man_switch, :sent, cenobite: cenobite) }
  let(:other_dead_man_switch) { FactoryBot.create(:dead_man_switch) }

  entity :dead_man_switch,
         id:             { type: :integer, description: 'DMS identifier' },
         name:           { type: :string, description: 'The name' },
         recipients:     { type: :string, description: 'List of email addresses' },
         content:        { type: :string, description: 'DMS content' },
         ping_frequency: { type: :integer, description: 'Maximum time between pings' },
         last_pinged_at: { type: :datetime, required: false, description: 'Last ping date and time' },
         expires_at:     { type: :datetime, required: false, description: 'Expiration date' },
         email_sent_at:  { type: :datetime, required: false, description: 'Date of expiration' },
         cenobite_id:    { type: :integer, description: 'Ping owner' },
         created_at:     { type: :datetime, description: 'Creation date' },
         updated_at:     { type: :datetime, description: 'Update date' }

  parameters :dms_path_params,
             id: { type: :integer, description: 'Target DMS identifier' }

  parameters :dms_post_params,
             name:           { type: :string, description: 'The name' },
             recipients:     { type: :string, description: 'List of email addresses' },
             content:        { type: :string, description: 'DMS content' },
             ping_frequency: { type: :integer, description: 'Maximum time between pings' }

  parameters :dms_update_params,
             name:           { type: :string, description: 'The name' },
             recipients:     { type: :string, description: 'List of email addresses' },
             content:        { type: :string, description: 'DMS content' },
             ping_frequency: { type: :integer, description: 'Maximum time between pings' },
             update_ping:    { type: :boolean, description: 'Also ping the DMS or not' }

  on_get '/member/dead_man_switches', 'List DMS' do
    for_code 200, expect_many: :dead_man_switch do |url|
      FactoryBot.create_list(:dead_man_switch, 2, cenobite: cenobite)
      sign_in cenobite
      test_response_of url
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url
    end
  end

  on_get '/member/dead_man_switches/:id', 'Show one' do
    path_params defined: :dms_path_params

    for_code 200, expect_one: :dead_man_switch do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: dead_man_switch.id }
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: other_dead_man_switch.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: dead_man_switch.id }
    end
  end

  on_post '/member/dead_man_switches', 'Create one' do
    request_params defined: :dms_post_params

    for_code 201, expect_one: :dead_man_switch do |url|
      sign_in cenobite
      test_response_of url, payload: { dead_man_switch: FactoryBot.build(:dead_man_switch).attributes }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url
    end
  end

  on_put '/member/dead_man_switches/:id', 'Update one' do
    path_params defined: :dms_path_params
    request_params defined: :dms_update_params

    for_code 200, expect_one: :dead_man_switch do |url|
      sign_in cenobite
      payload = { name: 'New name' }
      test_response_of url, path_params: { id: dead_man_switch.id }, payload: payload
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: other_dead_man_switch.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: dead_man_switch.id }
    end
  end

  on_delete '/member/dead_man_switches/:id', 'Destroy one' do
    path_params defined: :dms_path_params

    for_code 204 do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: dead_man_switch.id }
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: other_dead_man_switch.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: dead_man_switch.id }
    end
  end

  on_put '/member/dead_man_switches/ping_all', 'Ping all DMS' do
    for_code 200, expect_many: :dead_man_switch do |url|
      FactoryBot.create_list(:dead_man_switch, 2, cenobite: cenobite)
      sign_in cenobite
      test_response_of url
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url
    end
  end

  on_put '/member/dead_man_switches/:id/ping', 'Ping one DMS' do
    path_params defined: :dms_path_params

    for_code 200, expect_one: :dead_man_switch do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: dead_man_switch.id }
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: other_dead_man_switch.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: dead_man_switch.id }
    end
  end

  on_put '/member/dead_man_switches/:id/rearm', 'Rearm an expired DMS' do
    path_params defined: :dms_path_params

    for_code 200, expect_one: :dead_man_switch do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: dead_man_switch_sent.id }
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: other_dead_man_switch.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: dead_man_switch.id }
    end
  end
end
