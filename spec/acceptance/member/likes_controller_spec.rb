require 'rails_helper'

RSpec.describe Member::LikesController, type: :acceptance do
  resource 'Likes', 'Favorites management'

  entity :likable,
         id:   { type: :integer, description: 'Item identifier' },
         name: { type: :string, description: 'Item name/title' }

  entity :like,
         id:           { type: :integer, description: 'Favorite identifier' },
         likable_type: { type: :string, description: 'Item type' },
         likable:      { type: :object, description: 'Item', attributes: :likable },
         created_at:   { type: :datetime, description: 'Creation date' },
         cenobite_id:  { type: :integer, description: 'Cenobite identifier' }

  parameters :like_path_params,
             id: { type: :integer, description: 'Target favorite identifier' }

  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:cenobite_other) { FactoryBot.create(:cenobite, :active) }
  let(:like) { FactoryBot.create(:like, cenobite: cenobite) }
  let(:like_other) { FactoryBot.create(:like) }

  on_get '/member/likes', 'List current cenobite\'s favorites' do
    for_code 200, expect_many: :like do |url|
      FactoryBot.create_list(:like, 2, cenobite: cenobite)
      sign_in cenobite
      test_response_of url
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url
    end
  end

  on_delete '/member/likes/:id', 'Destroy one owned favorite' do
    path_params defined: :like_path_params

    for_code 204 do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: like.id }
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: like_other.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: like.id }
    end
  end
end
