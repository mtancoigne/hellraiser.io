require 'rails_helper'

RSpec.describe Member::CommentsController, type: :acceptance do
  resource 'Comments', 'Comments management (motions, events)'

  entity :comment,
         id:              { type: :integer, description: 'Comment identifier' },
         content:         { type: :string, description: 'Content' },
         has_picture:     { type: :boolean, description: 'Picture flag' },
         picture:         { type: :object, required: false, description: 'Attached file informations', attributes: {
           content_type: { type: :string, description: 'Mime type' },
           metadata:     { type: :object, description: 'File metadata. Depends on content_type' },
           file_name:    { type: :string, description: 'Original file name' },
           byte_size:    { type: :string, description: 'Size in bytes' },
           urls:         { type: :string, description: 'Urls to representations variants. the "file" key represents the original file' },
         } },
         cenobite_id:     { type: :integer, required: false, description: 'Cenobite identifier' },
         created_at:      { type: :datetime, description: 'Creation date' },
         updated_at:      { type: :datetime, description: 'Update date' },
         cenobite_colors: { type: :object, description: 'Colors to represent cenobite', attributes: {
           hex:  { type: :string, description: 'Direct hex representation. Can be used as name' },
           fg:   { type: :array, description: 'RGB array to use as background-color' },
           bg:   { type: :array, description: 'RGB array to use as text color' },
           name: { type: :string, required: false, description: 'Name, for non-anonymous content' },
         } }

  parameters :comment_path_params,
             id: { type: :integer, description: 'Target comment identifier' }

  parameters :comments_path_params,
             type:    { type: :string, description: 'Parent type (i.e.: motions)' },
             type_id: { type: :integer, description: 'Parent ID' }

  parameters :comment_form_params,
             comment: { type: :object, description: 'The comment', attributes: {
               content: { type: :string, description: 'Comment content' },
               picture: { type: :string, required: false, description: 'Optional picture. Use a FormData object as payload' },
             } }

  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:cenobite_other) { FactoryBot.create(:cenobite, :active) }
  let(:commentable) { FactoryBot.create(:motion) }
  let(:comment) { FactoryBot.create(:comment, commentable: commentable, cenobite: cenobite) }
  let(:comment_other) { FactoryBot.create(:comment, commentable: commentable, cenobite: cenobite_other) }
  let(:create_payload) { { comment: FactoryBot.build(:comment).attributes } }
  let(:update_payload) { { comment: { title: 'New comment' } } }

  on_get '/member/:type/:type_id/comments', 'List comments' do
    path_params defined: :comments_path_params
    for_code 200, expect_many: :comment do |url|
      FactoryBot.create_list(:comment, 2, commentable: commentable)
      sign_in cenobite
      test_response_of url, path_params: { type: 'motions', type_id: commentable.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { type: 'motions', type_id: commentable.id }
    end
  end

  on_post '/member/:type/:type_id/comments', 'Create one comment' do
    path_params defined: :comments_path_params
    request_params defined: :comment_form_params

    for_code 201, expect_one: :comment do |url|
      sign_in cenobite
      test_response_of url, path_params: { type: 'motions', type_id: commentable.id }, payload: create_payload
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { type: 'motions', type_id: commentable.id }
    end
  end

  on_get '/member/comments/:id', 'Display one comment' do
    path_params defined: :comment_path_params

    for_code 200, expect_one: :comment do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: comment_other.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: comment.id }
    end
  end

  on_put '/member/comments/:id', 'Update one owned comment' do
    path_params defined: :comment_path_params
    request_params defined: :comment_form_params

    for_code 200, expect_one: :comment do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: comment.id }, payload: update_payload
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: comment_other.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: comment.id }
    end
  end

  on_delete '/member/comments/:id', 'Destroy one owned comment' do
    path_params defined: :comment_path_params

    for_code 204 do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: comment.id }
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: comment_other.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: comment.id }
    end
  end
end
