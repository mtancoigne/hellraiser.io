require 'rails_helper'

RSpec.describe Member::ChatParticipantsController, type: :acceptance do
  resource 'Chat participants', 'Chat room participation management'

  entity :chat_participant,
         id:           { type: :integer, description: 'Association identifier' },
         name:         { type: :string, description: 'Participant name' },
         auto_join:    { type: :boolean, description: 'Whether or not to join this room automatically when joining the chat' },
         unread_count: { type: :integer, description: 'Amount of unread messages since last connection to the room' },
         cenobite_id:  { type: :integer, required: false, description: 'Cenobite identifier' },
         chat_room_id: { type: :integer, required: false, description: 'Chat room identifier' },
         created_at:   { type: :datetime, description: 'Creation date' },
         updated_at:   { type: :datetime, description: 'Update date' }

  parameters :chat_participant_form_params,
             chat_participant: { type: :object, description: 'The association', attributes: {
               name:      { type: :string, description: 'Participant name' },
               auto_join: { type: :boolean, description: 'Flag to join the room automatically' },
             } }

  parameters :chat_participant_path_params,
             id: { type: :integer, description: 'Target chat participant association' }

  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:chat_participant) { FactoryBot.create(:chat_participant, cenobite: cenobite) }
  let(:chat_participant_other) { FactoryBot.create(:chat_participant) }
  let(:update_payload) { { chat_participant: { name: 'John' } } }

  on_get '/member/chat_participants', 'List joined rooms' do
    for_code 200, expect_many: :chat_participant do |url|
      chat_participant
      sign_in cenobite
      test_response_of url
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url
    end
  end

  on_get '/member/chat_participants/:id', 'Display an association' do
    path_params defined: :chat_participant_path_params

    for_code 200, expect_one: :chat_participant do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: chat_participant.id }
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: chat_participant_other.id }
    end
  end

  on_put '/member/chat_participants/:id', 'Update the name in a room' do
    path_params defined: :chat_participant_path_params
    request_params defined: :chat_participant_form_params

    for_code 200, expect_one: :chat_participant do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: chat_participant.id }, payload: update_payload
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: chat_participant_other.id }
    end
  end

  on_delete '/member/chat_participants/:id', 'Destroy one association' do
    path_params defined: :chat_participant_path_params

    for_code 204 do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: chat_participant.id }
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: chat_participant_other.id }
    end
  end
end
