require 'rails_helper'

RSpec.describe Member::ItemsController, type: :acceptance do
  resource 'Items', 'Items management'

  entity :item,
         id:          { type: :integer, description: 'Item identifier' },
         name:        { type: :string, description: 'Name' },
         description: { type: :string, required: false, description: 'Description' },
         category:    { type: :string, required: false, description: 'Category' },
         owner_id:    { type: :integer, description: 'Owner cenobite identifier' },
         borrower_id: { type: :integer, required: false, description: 'Borrower cenobite identifier, only for owned items or items borrowed by current user' },
         borrowed_at: { type: :datetime, required: false, description: 'Borrow date, only for owned items' },
         borrowable:  { type: :boolean, description: 'Flag for item availability. False = item is borrowed' },
         created_at:  { type: :datetime, description: 'Creation date' },
         updated_at:  { type: :datetime, description: 'Update date' }

  parameters :item_path_params,
             id: { type: :integer, description: 'Target item identifier' }

  parameters :item_form_params,
             item: { type: :object, description: 'The item', attributes: {
               name:        { type: :string, description: 'Name' },
               description: { type: :string, description: 'Description' },
               category:    { type: :string, description: 'Category' },
               borrower_id: { type: :integer, description: 'Borrower cenobite identifier' },
             } }

  let(:acquaintance) { FactoryBot.create(:acquaintance, :accepted) }
  let(:cenobite) { acquaintance.inviter }
  let(:cenobite_other) { acquaintance.invitee }
  let(:item) { FactoryBot.create(:item, owner: cenobite) }
  let(:item_other) { FactoryBot.create(:item, owner: cenobite_other) }
  let(:create_payload) { { item: FactoryBot.build(:item).attributes } }
  let(:update_payload) { { item: { name: 'New name', borrower_id: cenobite_other.id } } }

  before do
    item
    item_other
  end

  on_get '/member/items', 'List owned items' do
    for_code 200, expect_many: :item do |url|
      sign_in cenobite
      test_response_of url
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url
    end
  end

  on_get '/member/items?only=acquaintances', 'List acquaintances items' do
    for_code 200, expect_many: :item do |url|
      sign_in cenobite
      test_response_of url, path_params: { only: 'acquaintances' }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { only: 'acquaintances' }
    end
  end

  on_post '/member/items', 'Create one item' do
    request_params defined: :item_form_params

    for_code 201, expect_one: :item do |url|
      sign_in cenobite
      test_response_of url, payload: create_payload
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url
    end
  end

  on_put '/member/items/:id', 'Update an item' do
    path_params defined: :item_path_params
    request_params defined: :item_form_params

    for_code 200, expect_one: :item do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: item.id }, payload: update_payload
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: item_other.id }, payload: update_payload
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: item.id }
    end
  end

  on_get '/member/items/:id', 'Display one item' do
    path_params defined: :item_path_params

    for_code 200, expect_one: :item do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: item_other.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: item.id }
    end
  end

  on_delete '/member/items/:id', 'Destroy one owned item' do
    path_params defined: :item_path_params

    for_code 204 do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: item.id }
    end

    for_code 403, expect_one: :error do |url|
      sign_in cenobite
      test_response_of url, path_params: { id: item_other.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: item.id }
    end
  end
end
