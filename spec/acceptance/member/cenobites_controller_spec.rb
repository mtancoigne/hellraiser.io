require 'rails_helper'

RSpec.describe Member::CenobitesController, type: :acceptance do
  resource 'Cenobite', 'Motions management'

  entity :cenobite,
         id:    { type: :integer, description: 'Cenobite identifier, if visible' },
         email: { type: :string, required: false, description: 'Email, if visible' },
         name:  { type: :string, description: 'Cenobite name' }

  entity :preferences,
         email:                   { type: :string, required: false, description: 'New email address. Will need confirmation' },
         locale:                  { type: :string, required: false, description: 'Site locale. Select in fr/en' },
         name:                    { type: :string, description: 'Name for public motions' },
         notify_new_acquaintance: { type: :boolean, description: 'Acquaintance notification preference' },
         notify_new_motion:       { type: :boolean, description: 'Motion notification preference' },
         theme:                   { type: :string, required: false, description: 'Preferred theme. Select in light/dark or null to use default' },
         use_3d_view:             { type: :boolean, description: 'Use the fancy and useless 3d view. Only applies to the website' }

  parameters :preferences_form_params,
             cenobite: { type: :object, description: 'The cenobite preferences', attributes: {
               email:                   { type: :string, required: false, description: 'New email address. Will need confirmation' },
               locale:                  { type: :string, description: 'Site locale. Select in fr/en' },
               name:                    { type: :string, description: 'Name for public motions' },
               notify_new_acquaintance: { type: :boolean, description: 'Acquaintance notification preference' },
               notify_new_motion:       { type: :boolean, description: 'Motion notification preference' },
               theme:                   { type: :string, required: false, description: 'Preferred theme. Select in light/dark or null to use default' },
               use_3d_view:             { type: :boolean, description: 'Use the fancy and useless 3d view. Only applies to the website' },
             } }

  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:update_payload) { { cenobite: { notify_new_motion: false } } }

  on_get '/member/cenobites', 'Visible cenobites list' do
    for_code 200, expect_many: :cenobite do |url|
      FactoryBot.create_list(:cenobite, 3, :visible, :active)
      cenobite.update visible_by_members: true
      sign_in cenobite
      test_response_of url, payload: update_payload
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url
    end

    # When cenobite is not visible by other
    for_code 403, expect_one: :error, test_only: true do |url|
      sign_in cenobite
      test_response_of url
    end
  end

  on_patch '/member/preferences', 'Update cenobite preferences' do
    request_params defined: :preferences_form_params

    for_code 200, expect_one: :preferences do |url|
      sign_in cenobite
      test_response_of url, payload: update_payload
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url
    end
  end
end
