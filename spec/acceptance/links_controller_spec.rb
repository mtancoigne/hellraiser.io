require 'rails_helper'

RSpec.describe LinksController, type: :acceptance do
  resource 'Links', 'Links management'

  on_get '/links', 'List links' do
    for_code 200, expect_many: :link do |url|
      FactoryBot.create_list(:link, 2, :visible_by_all)
      test_response_of url
    end
  end
end
