require 'rails_helper'

RSpec.describe PastesController, type: :acceptance do
  resource 'Pastes', 'Pastes management'

  entity :public_paste,
         id:         { type: :string, description: 'Paste identifier' },
         name:       { type: :string, description: 'Name' },
         extension:  { type: :string, description: 'Extension' },
         content:    { type: :string, required: false, description: 'File content. Null in case of protected/burnable' },
         burn:       { type: :boolean, description: 'Burnable flag' },
         password:   { type: :boolean, description: 'Password protection state' },
         created_at: { type: :datetime, description: 'Creation date' },
         updated_at: { type: :datetime, description: 'Update date' }

  entity :revealed_paste,
         content: { type: :string, required: false, description: 'Paste content' }

  parameters :paste_path_params,
             id: { type: :integer, description: 'Target paste identifier' }

  parameters :paste_reveal_params,
             paste: { type: :object, description: 'A paste', attributes: {
               password: { type: :string, description: 'Password to display the paste' },
             } }

  let(:paste) { FactoryBot.create(:paste) }
  let(:paste_burnable) { FactoryBot.create(:paste, :burnable) }
  let(:paste_protected) { FactoryBot.create(:paste, :protected) }
  let(:reveal_payload) { { paste: { password: 'password' } } }

  on_get '/pastes/:id', 'Display one paste. Protected/burnable pastes comes with null content' do
    path_params defined: :paste_path_params

    for_code 200, expect_one: :public_paste do |url|
      test_response_of url, path_params: { id: paste.id }
    end

    for_code 404, expect_one: :error do |url|
      test_response_of url, path_params: { id: 'nope' }
    end
  end

  on_get '/pastes/:id/reveal', 'Display a burnable paste' do
    path_params defined: :paste_path_params

    for_code 200, expect_one: :revealed_paste do |url|
      test_response_of url, path_params: { id: paste_burnable.id }
    end

    for_code 404, expect_one: :error do |url|
      test_response_of url, path_params: { id: 'nope' }
    end
  end

  on_put '/pastes/:id/reveal', 'Display a protected (+burnable) paste' do
    path_params defined: :paste_path_params
    request_params defined: :paste_reveal_params

    for_code 200, expect_one: :revealed_paste do |url|
      test_response_of url, path_params: { id: paste_protected.id }, payload: reveal_payload
    end

    for_code 404, expect_one: :error do |url|
      test_response_of url, path_params: { id: 'nope' }, payload: reveal_payload
    end
  end
end
