require 'rails_helper'

require 'hell_raiser/rss'

RSpec.describe HellRaiser::Rss do
  describe '#fetch' do
    context 'with a valid url' do
      it 'returns an instance of Feedjira::Parser::xxx' do
        VCR.use_cassette 'rss_feed/get_rss_feed' do
          feed = described_class.fetch('https://korben.info/feed')
          expect(feed).to be_instance_of Feedjira::Parser::RSS
        end
      end
    end

    context 'with an invalid url' do
      it 'raises an error' do
        VCR.use_cassette 'rss_feed/invalid_feed' do
          expect do
            described_class.fetch 'https://example.com'
          end.to raise_error HellRaiser::Rss::InvalidFeedError
        end
      end
    end

    context 'with an invalid feed' do
      it 'raises an error' do
        # Won't create a cassette as it fails.
        VCR.use_cassette 'rss_feed/inexistant_feed' do
          expect do
            described_class.fetch 'https://inexistantwebsite'
          end.to raise_error HellRaiser::Rss::InvalidFeedError
        end
      end
    end
  end
end
