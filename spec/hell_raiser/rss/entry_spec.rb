require 'rails_helper'

require 'hell_raiser/rss'

RSpec.describe HellRaiser::Rss::Entry do
  describe '.to_hash' do
    let(:valid_keys) { [:description, :link, :published_at, :title] }
    # Both example_cassette and example_url are defined in the following context
    # blocks.
    let(:rss_entry) do
      VCR.use_cassette example_cassette do
        feed = HellRaiser::Rss.fetch example_url
        described_class.new feed.entries.first
      end
    end

    context 'with atom entry' do
      let(:example_url) { 'https://korben.info/feed' }
      let(:example_cassette) { 'rss_feed/get_rss_feed' }

      it 'returns a hash' do
        expect(rss_entry.to_hash).to be_a Hash
      end
    end

    context 'with atom-youtube entry' do
      let(:example_url) { 'https://www.youtube.com/feeds/videos.xml?user=TSPcarbonfree' }
      let(:example_cassette) { 'rss_feed/get_atom_youtube_feed' }

      it 'returns a hash' do
        expect(rss_entry.to_hash).to be_a Hash
      end
    end

    context 'with rss entry' do
      let(:example_url) { 'https://linuxfr.org/news.atom' }
      let(:example_cassette) { 'rss_feed/get_atom_feed' }

      it 'returns a hash' do
        expect(rss_entry.to_hash).to be_a Hash
      end
    end
  end
end
