require 'rails_helper'

require 'hell_raiser/rss'

RSpec.describe HellRaiser::Rss::Feed do
  let(:cenobite) { FactoryBot.create(:cenobite) }

  describe '.to_hash' do
    # Both example_cassette and example_url are defined in the following context
    # blocks.
    let(:rss_feed) do
      rss_feed          = RssFeed.new url: example_url
      rss_feed.cenobite = cenobite

      rss_feed
    end

    context 'with atom feed' do
      let(:example_url) { 'https://korben.info/feed' }
      let(:example_cassette) { 'rss_feed/get_rss_feed' }

      it 'returns a hash valid for an RssFeed' do
        VCR.use_cassette example_cassette do
          expect(rss_feed).to be_valid
        end
      end
    end

    context 'with atom-youtube feed' do
      let(:example_url) { 'https://www.youtube.com/feeds/videos.xml?user=TSPcarbonfree' }
      let(:example_cassette) { 'rss_feed/get_atom_youtube_feed' }

      it 'returns a hash valid for an RssFeed' do
        VCR.use_cassette example_cassette do
          expect(rss_feed).to be_valid
        end
      end
    end

    context 'with rss feed' do
      let(:example_url) { 'https://linuxfr.org/news.atom' }
      let(:example_cassette) { 'rss_feed/get_atom_feed' }

      it 'returns a hash valid for an RssFeed' do
        VCR.use_cassette example_cassette do
          expect(rss_feed).to be_valid
        end
      end
    end
  end
end
