require 'rails_helper'
require 'hell_raiser/dead_man_switches'

RSpec.describe HellRaiser::DeadManSwitches do
  include ActiveJob::TestHelper

  before do
    # 4 dms:
    #   - 2 active
    time = 1.hour.ago
    FactoryBot.create_list(:dead_man_switch, 2)
    #   - 1 expired and sent
    dms = FactoryBot.create(:dead_man_switch, email_sent_at: time)
    dms.update_column :expires_at, time # rubocop:disable Rails/SkipsModelValidations
    #   - 1 expired
    dms = FactoryBot.create(:dead_man_switch)
    dms.update_column :expires_at, time # rubocop:disable Rails/SkipsModelValidations
  end

  it 'sends emails' do
    expect do
      perform_enqueued_jobs do
        described_class.notify_expired
      end
    end.to change(ActionMailer::Base.deliveries, :count).by(1)
  end
end
