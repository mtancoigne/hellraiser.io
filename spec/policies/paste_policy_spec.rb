require 'rails_helper'
require 'pundit/rspec'

RSpec.describe PastePolicy, type: :policy do
  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:scope) { Pundit.policy_scope!(cenobite, Paste) }
  let(:paste) { FactoryBot.create(:paste, cenobite: cenobite) }
  let(:paste_other) { FactoryBot.create(:paste) }

  permissions '.scope' do
    it('returns all pastes') do
      paste
      paste_other

      expect(scope.count).to eq 2
    end
  end

  permissions :show?, :reveal? do
    it 'grants access' do
      expect(described_class).to permit(nil, paste)
    end
  end
end
