require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Member::MotionPolicy, type: :policy do
  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:scope) { Pundit.policy_scope!(cenobite, [:member, Motion]) }
  let(:motion) { FactoryBot.create(:motion, cenobite: cenobite) }
  let(:motion_other) { FactoryBot.create(:motion) }

  permissions '.scope' do
    it('returns all the motions') do
      motion
      motion_other

      expect(scope.count).to eq 2
    end
  end

  permissions :index?, :show?, :new?, :create?, :upvote?, :downvote? do
    it 'grants access' do
      expect(described_class).to permit(cenobite, motion)
    end
  end

  permissions :edit?, :update?, :destroy? do
    it 'denies access if motion is not owned' do
      expect(described_class).not_to permit(cenobite, motion_other)
    end

    it 'grants access if motion is owned' do
      expect(described_class).to permit(cenobite, motion)
    end
  end
end
