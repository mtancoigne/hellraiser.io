require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Member::LinkPolicy, type: :policy do
  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:scope) { Pundit.policy_scope!(cenobite, [:member, Link]) }
  let(:link) { FactoryBot.create(:link, :visible_by_owner, cenobite: cenobite) }
  let(:link_other_member) { FactoryBot.create(:link, :visible_by_members) }
  let(:link_other_private) { FactoryBot.create(:link, :visible_by_owner) }

  permissions '.scope' do
    it('filters private links') do
      link
      link_other_member
      link_other_private

      expect(scope.count).to eq 2
    end
  end

  permissions :index?, :new?, :create? do
    it 'grants access' do
      expect(described_class).to permit(cenobite, link)
    end
  end

  permissions :update?, :destroy? do
    it 'denies access if link is not owned' do
      expect(described_class).not_to permit(cenobite, link_other_member)
    end

    it 'grants access if link is owned' do
      expect(described_class).to permit(cenobite, link)
    end
  end
end
