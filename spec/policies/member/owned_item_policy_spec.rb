require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Member::OwnedItemPolicy, type: :policy do
  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:acquaintance) { FactoryBot.create(:acquaintance, :accepted, inviter: cenobite) }
  let(:acquaintance_pending) { FactoryBot.create(:acquaintance, invitee: cenobite) }

  let(:scope) { Pundit.policy_scope!(cenobite, [:member, OwnedItem]) }
  let(:item) { FactoryBot.create(:item, owner: cenobite) }
  let(:items) { FactoryBot.create_list(:item, 3, owner: cenobite) }
  let(:item_borrowed) { FactoryBot.create(:item, :borrowed, owner: acquaintance.invitee) }
  let(:item_pending_acquaintance) { FactoryBot.create(:item, owner: acquaintance_pending.inviter) }
  let(:item_other) { FactoryBot.create(:item) }

  permissions '.scope' do
    it "returns all cenobite's items" do
      items
      item_borrowed
      item_pending_acquaintance
      item_other

      expect(scope.count).to eq 3
    end
  end

  permissions :index? do
    it 'grants access' do
      expect(described_class).to permit(cenobite, OwnedItem)
    end
  end

  permissions :show? do
    it 'grants access' do
      expect(described_class).to permit(cenobite, item)
    end
  end

  permissions :create?, :update?, :destroy? do
    it 'grants access' do
      expect(described_class).to permit(cenobite, item)
    end
  end
end
