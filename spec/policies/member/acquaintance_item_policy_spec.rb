require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Member::AcquaintanceItemPolicy, type: :policy do
  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:acquaintance) { FactoryBot.create(:acquaintance, :accepted, inviter: cenobite) }
  let(:acquaintance_pending) { FactoryBot.create(:acquaintance, invitee: cenobite) }

  let(:scope) { Pundit.policy_scope!(cenobite, [:member, AcquaintanceItem]) }
  let(:item) { FactoryBot.create(:item, owner: cenobite) }
  let(:item_borrowed) { FactoryBot.create(:item, :borrowed, owner: acquaintance.invitee) }
  let(:item_pending_acquaintance) { FactoryBot.create(:item, owner: acquaintance_pending.inviter) }
  let(:item_other) { FactoryBot.create(:item) }

  permissions '.scope' do
    it "returns all the accepted acquaintances' items" do
      item
      item_borrowed
      item_pending_acquaintance
      item_other

      expect(scope.count).to eq 1
    end
  end

  permissions :index? do
    it 'grants access' do
      expect(described_class).to permit(cenobite, AcquaintanceItem)
    end
  end

  permissions :show? do
    it 'grants access to items in accepted acquaintances' do
      expect(described_class).to permit(cenobite, item_borrowed)
    end

    it 'denies access to items in non-accepted acquaintances' do
      expect(described_class).not_to permit(cenobite, item_pending_acquaintance)
    end

    it 'denies access to items of unknown cenobites' do
      expect(described_class).not_to permit(cenobite, item_other)
    end
  end

  permissions :update?, :destroy? do
    it 'denies access' do
      expect(described_class).not_to permit(cenobite, item_other)
    end
  end
end
