require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Member::DeadManSwitchPolicy, type: :policy do
  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:scope) { Pundit.policy_scope!(cenobite, [:member, DeadManSwitch]) }
  let(:dms_other) { FactoryBot.create(:dead_man_switch) }
  let(:dms) { FactoryBot.create(:dead_man_switch, cenobite: cenobite) }
  let(:dms_expired) { FactoryBot.create(:dead_man_switch, :expired, cenobite: cenobite) }
  let(:dms_sent) { FactoryBot.create(:dead_man_switch, :sent, cenobite: cenobite) }

  permissions '.scope' do
    it('filters by current cenobite') do
      FactoryBot.create(:dead_man_switch, cenobite: cenobite)
      FactoryBot.create_list(:dead_man_switch, 2)

      expect(scope.count).to eq 1
    end
  end

  permissions :show?, :edit?, :update?, :destroy? do
    it 'denies access if dms is not owned' do
      expect(described_class).not_to permit(cenobite, dms_other)
    end

    it 'grants access if dms is owned' do
      expect(described_class).to permit(cenobite, dms)
    end
  end

  permissions :ping? do
    it 'denies access if dms is not owned' do
      expect(described_class).not_to permit(cenobite, dms_other)
    end

    it 'grants access if dms is owned' do
      expect(described_class).to permit(cenobite, dms)
    end

    it 'grants access if dms is not sent' do
      expect(described_class).to permit(cenobite, dms)
    end

    # Maybe the task is running, so it's too late to ping:
    # user may think he avoided the trigger by rearming an expired
    # dms that actually is being triggered
    it 'denies access if dms is expired' do
      expect(described_class).not_to permit(cenobite, dms_expired)
    end

    it 'denies access if dms is sent' do
      expect(described_class).not_to permit(cenobite, dms_sent)
    end
  end

  permissions :rearm? do
    it 'denies access if dms is not owned' do
      expect(described_class).not_to permit(cenobite, dms_other)
    end

    it 'grants access if dms is owned, expired and sent' do
      expect(described_class).to permit(cenobite, dms_sent)
    end

    # Maybe the task is running, so it's too late to rearm:
    # user may think he avoided the trigger by rearming an expired
    # dms that actually is being triggered
    it 'denies access if dms is expired' do
      expect(described_class).not_to permit(cenobite, dms_expired)
    end

    it 'denies access if dms is not expired' do
      expect(described_class).not_to permit(cenobite, dms)
    end
  end
end
