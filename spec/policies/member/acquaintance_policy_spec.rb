require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Member::AcquaintancePolicy, type: :policy do
  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:scope) { Pundit.policy_scope!(cenobite, [:member, Acquaintance]) }
  let(:created_acquaintance) { FactoryBot.create(:acquaintance, inviter: cenobite) }
  let(:invited_acquaintance) { FactoryBot.create(:acquaintance, invitee: cenobite) }
  let(:other_acquaintance) { FactoryBot.create(:acquaintance) }

  permissions '.scope' do
    it("returns all the cenobite's acquaintances") do
      created_acquaintance
      other_acquaintance

      expect(scope.count).to eq 1
    end
  end

  permissions :index?, :create? do
    it 'grants access' do
      expect(described_class).to permit(cenobite, Acquaintance)
    end
  end

  permissions :accept? do
    it 'denies access if not part of the acquaintance' do
      expect(described_class).not_to permit(cenobite, other_acquaintance)
    end

    it 'denies access if inviter of the acquaintance' do
      expect(described_class).not_to permit(cenobite, created_acquaintance)
    end

    it 'grants access if invitee in the acquaintance' do
      expect(described_class).to permit(cenobite, invited_acquaintance)
    end
  end

  permissions :resend_request? do
    it 'denies access if not part of the acquaintance' do
      expect(described_class).not_to permit(cenobite, other_acquaintance)
    end

    it 'denies access if invitee of the acquaintance' do
      expect(described_class).not_to permit(cenobite, invited_acquaintance)
    end

    it 'grants access if inviter in the acquaintance' do
      expect(described_class).to permit(cenobite, created_acquaintance)
    end
  end

  permissions :destroy? do
    it 'denies access if not part of the acquaintance' do
      expect(described_class).not_to permit(cenobite, other_acquaintance)
    end

    it 'grants access if part of the acquaintance' do
      expect(described_class).to permit(cenobite, created_acquaintance)
    end
  end
end
