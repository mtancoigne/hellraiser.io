require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Member::RssFeedPolicy, type: :policy do
  let(:cenobite) { FactoryBot.create(:cenobite) }
  let(:scope) { Pundit.policy_scope!(cenobite, [:member, RssFeed]) }
  let(:rss_feed) do
    feed = nil
    VCR.use_cassette 'rss_feed/get_atom_feed' do
      feed = FactoryBot.create(:rss_feed, url: 'https://linuxfr.org/news.atom', cenobite: cenobite)
    end
    feed
  end
  let(:rss_feed_other) do
    feed = nil
    VCR.use_cassette 'rss_feed/get_rss_feed' do
      feed = FactoryBot.create(:rss_feed, url: 'https://korben.info/feed')
    end
    feed
  end

  permissions '.scope' do
    it('returns all the feeds') do
      rss_feed
      rss_feed_other

      expect(scope.count).to eq 2
    end
  end

  permissions :index?, :new?, :create? do
    it 'grants access' do
      expect(described_class).to permit(cenobite, RssFeed)
    end
  end

  permissions :destroy? do
    it 'denies access if rss_feed is not owned' do
      expect(described_class).not_to permit(cenobite, rss_feed_other)
    end

    it 'grants access if rss_feed is owned' do
      expect(described_class).to permit(cenobite, rss_feed)
    end
  end
end
