require 'rails_helper'

RSpec.describe Member::InvitePolicy, type: :policy do
  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:invited_cenobite) { Cenobite.invite!({ email: Faker::Internet.email }, cenobite) }

  permissions :resend?, :destroy? do
    it 'grants access when invited cenobite is not confirmed' do
      expect(described_class).to permit(cenobite, invited_cenobite)
    end

    it 'denies access when invited cenobite is confirmed' do
      invited_cenobite.update! confirmed_at: Time.current, invitation_accepted_at: Time.current
      invited_cenobite.reload
      expect(described_class).not_to permit(cenobite, invited_cenobite)
    end
  end
end
