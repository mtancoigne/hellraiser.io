require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Member::CenobitePolicy, type: :policy do
  let(:cenobite) { FactoryBot.create(:cenobite, :active, :visible) }
  let(:scope) { Pundit.policy_scope!(cenobite, [:member, Cenobite]) }
  let(:cenobite_visible) { FactoryBot.create(:cenobite, :active, :visible) }
  let(:cenobite_invisible) { FactoryBot.create(:cenobite, :active, :invisible) }
  let(:other_cenobite_invisible) { FactoryBot.create(:cenobite, :active, :invisible) }

  permissions '.scope' do
    it 'returns all visible cenobites' do
      cenobite
      cenobite_visible
      cenobite_invisible

      expect(scope.count).to eq 2
    end
  end

  permissions :index? do
    context 'when cenobite is visible' do
      it 'grants access' do
        expect(described_class).to permit(cenobite, Cenobite)
      end
    end

    context 'when cenobite is invisible' do
      it 'denies access' do
        expect(described_class).not_to permit(cenobite_invisible, Cenobite)
      end
    end
  end

  permissions :invite? do
    context 'when current cenobite is visible' do
      context 'when target cenobite is visible' do
        it 'grants access' do
          expect(described_class).to permit(cenobite, cenobite_visible)
        end
      end

      context 'when target cenobite is invisible' do
        it 'grants access' do
          expect(described_class).not_to permit(cenobite, cenobite_invisible)
        end
      end
    end

    context 'when current cenobite is invisible' do
      context 'when target cenobite is visible' do
        it 'denies access' do
          expect(described_class).not_to permit(cenobite_invisible, cenobite_visible)
        end
      end

      context 'when target cenobite is invisible' do
        it 'denies access' do
          expect(described_class).not_to permit(cenobite_invisible, other_cenobite_invisible)
        end
      end
    end
  end
end
