require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Member::LikePolicy, type: :policy do
  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:scope) { Pundit.policy_scope!(cenobite, [:member, Like]) }
  let(:like) { FactoryBot.create(:like, cenobite: cenobite) }
  let(:like_other) { FactoryBot.create(:like) }

  permissions '.scope' do
    it('returns all the owned likes') do
      like
      like_other

      expect(scope.count).to eq 1
    end
  end

  permissions :index? do
    it 'grants access' do
      expect(described_class).to permit(cenobite, Like)
    end
  end

  permissions :destroy? do
    it 'denies access if like is not owned' do
      expect(described_class).not_to permit(cenobite, like_other)
    end

    it 'grants access if like is owned' do
      expect(described_class).to permit(cenobite, like)
    end
  end
end
