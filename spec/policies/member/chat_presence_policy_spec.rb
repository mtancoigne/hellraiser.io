require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Member::ChatPresencePolicy, type: :policy do
  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:chat_participant) { FactoryBot.create(:chat_participant, cenobite: cenobite) }
  let(:scope) { Pundit.policy_scope!(cenobite, [:member, ChatPresence]) }
  let(:chat_presence) { FactoryBot.create(:chat_presence, chat_participant: chat_participant, chat_room_id: chat_participant.chat_room_id) }
  let(:chat_presence_other) { FactoryBot.create(:chat_presence) }

  permissions '.scope' do
    it('returns all the chat presences') do
      chat_presence
      chat_presence_other

      expect(scope.count).to eq 2
    end
  end

  permissions :index? do
    it 'grants access' do
      expect(described_class).to permit(cenobite, ChatPresence)
    end
  end
end
