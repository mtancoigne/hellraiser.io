require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Member::RssItemPolicy, type: :policy do
  let(:cenobite) { FactoryBot.create(:cenobite) }
  let(:scope) { Pundit.policy_scope!(cenobite, [:member, RssItem]) }
  let(:rss_item) do
    VCR.use_cassette 'rss_feed/get_rss_feed' do
      FactoryBot.create(:rss_feed, url: 'https://korben.info/feed')
    end
    RssItem.last
  end

  permissions '.scope' do
    it('returns all the items') do
      rss_item

      expect(scope.count).to eq 10
    end
  end

  permissions :index? do
    it 'grants access' do
      expect(described_class).to permit(cenobite, RssItem)
    end
  end
end
