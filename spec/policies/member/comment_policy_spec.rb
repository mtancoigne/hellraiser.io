require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Member::CommentPolicy, type: :policy do
  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:scope) { Pundit.policy_scope!(cenobite, [:member, Comment]) }
  let(:commentable) { FactoryBot.create(:motion) }
  let(:comment) { FactoryBot.create(:comment, commentable: commentable, cenobite: cenobite) }
  let(:comment_other) { FactoryBot.create(:comment, commentable: commentable) }

  permissions '.scope' do
    it('returns all the comments') do
      comment
      comment_other

      expect(scope.count).to eq 2
    end
  end

  permissions :index?, :show?, :create? do
    it 'grants access' do
      expect(described_class).to permit(cenobite, comment)
    end
  end

  permissions :edit?, :update?, :destroy? do
    it 'denies access if motion is not owned' do
      expect(described_class).not_to permit(cenobite, comment_other)
    end

    it 'grants access if motion is owned' do
      expect(described_class).to permit(cenobite, comment)
    end
  end
end
