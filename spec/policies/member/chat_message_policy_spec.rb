require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Member::ChatMessagePolicy, type: :policy do
  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:chat_participant) { FactoryBot.create(:chat_participant, cenobite: cenobite) }
  let(:scope) { Pundit.policy_scope!(cenobite, [:member, ChatMessage]) }
  let(:chat_message) { FactoryBot.create(:chat_message, chat_participant: chat_participant) }
  let(:chat_message_other) { FactoryBot.create(:chat_message) }

  permissions '.scope' do
    it('returns all the chat messages') do
      chat_message
      chat_message_other

      expect(scope.count).to eq 2
    end
  end

  permissions :index?, :create? do
    it 'grants access' do
      expect(described_class).to permit(cenobite, ChatMessage)
    end
  end

  permissions :show? do
    it 'grants access' do
      expect(described_class).to permit(cenobite, chat_message)
    end
  end

  permissions :update?, :destroy? do
    it 'denies access if chat message is not owned' do
      expect(described_class).not_to permit(cenobite, chat_message_other)
    end

    it 'grants access if chat message is owned' do
      expect(described_class).to permit(cenobite, chat_message)
    end
  end
end
