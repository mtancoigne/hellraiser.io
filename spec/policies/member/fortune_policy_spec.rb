require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Member::FortunePolicy, type: :policy do
  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:scope) { Pundit.policy_scope!(cenobite, [:member, Fortune]) }
  let(:fortune) { FactoryBot.create(:fortune, cenobite: cenobite) }
  let(:fortune_other) { FactoryBot.create(:fortune) }

  permissions '.scope' do
    it('returns all the owned fortunes') do
      fortune
      fortune_other

      expect(scope.count).to eq 1
    end
  end

  permissions :index?, :random?, :new?, :create?, :upvote?, :downvote? do
    it 'grants access to owned fortunes' do
      expect(described_class).to permit(cenobite, fortune)
    end

    it 'grants access to other fortunes' do
      expect(described_class).to permit(cenobite, fortune_other)
    end
  end

  permissions :edit?, :update?, :destroy? do
    it 'denies access if fortune is not owned' do
      expect(described_class).not_to permit(cenobite, fortune_other)
    end

    it 'grants access if fortune is owned' do
      expect(described_class).to permit(cenobite, fortune)
    end
  end
end
