require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Member::ChatRoomPolicy, type: :policy do
  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:scope) { Pundit.policy_scope!(cenobite, [:member, ChatRoom]) }
  let(:chat_room) { FactoryBot.create(:chat_room, cenobite: cenobite) }
  let(:chat_room_other) { FactoryBot.create(:chat_room) }
  let(:non_acquainted_chat_room) { FactoryBot.create(:chat_room, :with_acquaintance) }
  let(:acquainted_chat_room) do
    acquaintance = FactoryBot.create(:acquaintance, :accepted, inviter: cenobite)
    acquaintance.chat_room
  end

  permissions '.scope' do
    it('returns all the chat rooms the cenobite can see') do
      # Public
      chat_room
      chat_room_other
      # Private, accessible
      acquainted_chat_room
      # Private, non-accessible
      non_acquainted_chat_room

      expect(scope.count).to eq 3
    end
  end

  permissions :index?, :create? do
    it 'grants access' do
      expect(described_class).to permit(cenobite, ChatRoom)
    end
  end

  permissions :show? do
    context 'when the chat room is public' do
      it 'grants access' do
        expect(described_class).to permit(cenobite, chat_room)
      end
    end

    context 'when chat room is with a non-related acquaintance' do
      it 'denies access' do
        expect(described_class).not_to permit(cenobite, non_acquainted_chat_room)
      end
    end

    context 'when chat room is with a related acquaintance' do
      it 'grants access' do
        expect(described_class).to permit(cenobite, acquainted_chat_room)
      end
    end
  end

  permissions :update?, :destroy? do
    context 'when the chat room is public' do
      it 'denies access if chat room is not owned' do
        expect(described_class).not_to permit(cenobite, chat_room_other)
      end

      it 'grants access if chat room is owned' do
        expect(described_class).to permit(cenobite, chat_room)
      end
    end

    context 'when chat room is with a non-related acquaintance' do
      it 'denies access' do
        expect(described_class).not_to permit(cenobite, non_acquainted_chat_room)
      end
    end

    context 'when chat room is with a related acquaintance' do
      it 'denies access' do
        expect(described_class).not_to permit(cenobite, acquainted_chat_room)
      end
    end
  end
end
