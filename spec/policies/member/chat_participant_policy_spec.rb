require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Member::ChatParticipantPolicy, type: :policy do
  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:scope) { Pundit.policy_scope!(cenobite, [:member, ChatParticipant]) }
  let(:chat_participant_other) { FactoryBot.create(:chat_participant) }
  let(:chat_participant) { FactoryBot.create(:chat_participant, cenobite: cenobite) }

  permissions '.scope' do
    it('filters by current cenobite') do
      FactoryBot.create(:chat_participant, cenobite: cenobite)
      FactoryBot.create_list(:chat_participant, 2)

      expect(scope.count).to eq 1
    end
  end

  permissions :index? do
    it 'grants access' do
      expect(described_class).to permit(cenobite, ChatParticipant)
    end
  end

  permissions :show?, :update?, :destroy? do
    it 'denies access if chat_participant is not owned' do
      expect(described_class).not_to permit(cenobite, chat_participant_other)
    end

    it 'grants access if chat_participant is owned' do
      expect(described_class).to permit(cenobite, chat_participant)
    end
  end
end
