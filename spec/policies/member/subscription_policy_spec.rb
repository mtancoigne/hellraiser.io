require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Member::SubscriptionPolicy, type: :policy do
  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:scope) { Pundit.policy_scope!(cenobite, [:member, Subscription]) }
  let(:subscription) { FactoryBot.create(:subscription, :on_motion, cenobite: cenobite) }
  let(:subscription_other) { FactoryBot.create(:subscription, :on_motion) }

  permissions '.scope' do
    it('returns owned subscriptions') do
      subscription
      subscription_other

      expect(scope.count).to eq 1
    end
  end

  permissions :index? do
    it 'grants access' do
      expect(described_class).to permit(cenobite, subscription)
    end
  end

  permissions :destroy? do
    it 'denies access if subscription is not owned' do
      expect(described_class).not_to permit(cenobite, subscription_other)
    end

    it 'grants access if subscription is owned' do
      expect(described_class).to permit(cenobite, subscription)
    end
  end
end
