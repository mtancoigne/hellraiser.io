require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Member::EventPolicy, type: :policy do
  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:scope) { Pundit.policy_scope!(cenobite, [:member, Event]) }
  let(:event) { FactoryBot.create(:event, :visible_by_owner, cenobite: cenobite) }
  let(:event_other_member) { FactoryBot.create(:event, :visible_by_members) }
  let(:event_other_private) { FactoryBot.create(:event, :visible_by_owner) }

  permissions '.scope' do
    it('filters private events') do
      event
      event_other_member
      event_other_private

      expect(scope.count).to eq 2
    end
  end

  permissions :index?, :new?, :create? do
    it 'grants access' do
      expect(described_class).to permit(cenobite, event)
    end
  end

  permissions :show? do
    context 'when event is owned' do
      it 'grants access' do
        expect(described_class).to permit(cenobite, event)
      end
    end

    context 'when event is not owned' do
      context 'when event is visible to members' do
        it 'grants access' do
          expect(described_class).to permit(cenobite, event_other_member)
        end
      end

      context 'when event is not visible to members' do
        it 'denies access' do
          expect(described_class).not_to permit(cenobite, event_other_private)
        end
      end
    end
  end

  permissions :update?, :destroy? do
    it 'denies access if event is not owned' do
      expect(described_class).not_to permit(cenobite, event_other_member)
    end

    it 'grants access if event is owned' do
      expect(described_class).to permit(cenobite, event)
    end
  end

  permissions :attend?, :leave? do
    context 'when event is visible by members' do
      context 'when cenobite is owner' do
        context 'when event is not even started' do
          it 'grants access' do
            expect(described_class).to permit(cenobite, event)
          end
        end

        context 'when event started' do
          it 'denies access' do
            event = FactoryBot.create(:event, :visible_by_members, :started, cenobite: cenobite)
            expect(described_class).not_to permit(cenobite, event)
          end
        end

        context 'when event is over' do
          it 'denies access' do
            event = FactoryBot.create(:event, :visible_by_members, :over, cenobite: cenobite)
            expect(described_class).not_to permit(cenobite, event)
          end
        end
      end

      context 'when cenobite is not owner' do
        context 'when event is not even started' do
          it 'grants access' do
            expect(described_class).to permit(cenobite, event_other_member)
          end
        end

        context 'when event started' do
          it 'denies access' do
            event_other_member = FactoryBot.create(:event, :visible_by_members, :started)
            expect(described_class).not_to permit(cenobite, event_other_member)
          end
        end

        context 'when event is over' do
          it 'denies access' do
            event_other_member = FactoryBot.create(:event, :visible_by_members, :over)
            expect(described_class).not_to permit(cenobite, event_other_member)
          end
        end
      end
    end

    context 'when event is not visible by members' do
      context 'when cenobite is owner' do
        context 'when event is not even started' do
          it 'grants access' do
            event = FactoryBot.create(:event, :visible_by_owner, cenobite: cenobite)
            expect(described_class).to permit(cenobite, event)
          end
        end

        context 'when event started' do
          it 'denies access' do
            event = FactoryBot.create(:event, :visible_by_owner, :started, cenobite: cenobite)
            expect(described_class).not_to permit(cenobite, event)
          end
        end

        context 'when event is over' do
          it 'denies access' do
            event = FactoryBot.create(:event, :visible_by_owner, :over, cenobite: cenobite)
            expect(described_class).not_to permit(cenobite, event)
          end
        end
      end

      context 'when cenobite is not owner' do
        it 'denies access' do
          expect(described_class).not_to permit(cenobite, event_other_private)
        end
      end
    end
  end
end
