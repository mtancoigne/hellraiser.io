require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Member::PastePolicy, type: :policy do
  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:scope) { Pundit.policy_scope!(cenobite, [:member, Paste]) }
  let(:paste) { FactoryBot.create(:paste, cenobite: cenobite) }
  let(:paste_other) { FactoryBot.create(:paste) }

  permissions '.scope' do
    it('filters by current cenobite') do
      paste
      paste_other

      expect(scope.count).to eq 1
    end
  end

  permissions :index?, :new?, :create?, :show? do
    it 'grants access' do
      expect(described_class).to permit(cenobite, paste)
    end
  end

  permissions :update?, :destroy? do
    it 'denies access if paste is not owned' do
      expect(described_class).not_to permit(cenobite, paste_other)
    end

    it 'grants access if paste is owned' do
      expect(described_class).to permit(cenobite, paste)
    end
  end
end
