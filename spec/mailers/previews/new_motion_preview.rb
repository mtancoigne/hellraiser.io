# Preview all emails at http://localhost:3000/rails/mailers/comment
class NewMotionPreview < ActionMailer::Preview
  def notify_email
    NewMotionMailer.with(cenobite: Cenobite.first, motion: Motion.first).notify_email
  end
end
