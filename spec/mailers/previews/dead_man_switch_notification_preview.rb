# Preview all emails at http://localhost:3000/rails/mailers/dead_man_switch_notification
class DeadManSwitchNotificationPreview < ActionMailer::Preview
  def notify_email
    DeadManSwitchNotificationMailer.with(dead_man_switch: DeadManSwitch.first).notify_email
  end
end
