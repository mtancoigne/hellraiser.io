# Preview all emails at http://localhost:3000/rails/mailers/comment
class CommentPreview < ActionMailer::Preview
  def subscription
    motion = FactoryBot.create(:motion)
    comment = FactoryBot.create(:comment, commentable: motion)
    CommentMailer.with(comment: comment, cenobite: motion.cenobite).subscription_email
  end
end
