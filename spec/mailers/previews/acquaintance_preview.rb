# Preview all emails at http://localhost:3000/rails/mailers/acquaintance
class AcquaintancePreview < ActionMailer::Preview
  def new_request_email_inactive
    inviter = FactoryBot.create(:cenobite, :active)
    invitee = FactoryBot.create(:cenobite)

    AcquaintanceMailer.with(invitee: invitee, inviter: inviter).new_request_email
  end

  def new_request_email_active
    inviter = FactoryBot.create(:cenobite, :active)
    invitee = FactoryBot.create(:cenobite, :active)

    AcquaintanceMailer.with(invitee: invitee, inviter: inviter).new_request_email
  end
end
