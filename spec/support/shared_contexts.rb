RSpec.shared_context 'with authenticated user', shared_context: :metadata do
  before do
    @request.env['devise.mapping'] = Devise.mappings[:cenobite] # rubocop:disable RSpec/InstanceVariable
    sign_in Cenobite.find_by(email: 'user@example.com')
  end
end
