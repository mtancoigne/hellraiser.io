require 'rails_helper'

RSpec.describe '/links', type: :request do
  let(:signed_in_cenobite) { Cenobite.find_by email: 'user@example.com' }

  let(:valid_attributes) do
    FactoryBot.build(:link, cenobite_id: signed_in_cenobite.id).attributes
  end

  let(:invalid_attributes) do
    { url: '' }
  end

  before { sign_in signed_in_cenobite }

  describe 'GET #index' do
    it 'returns a success response' do
      Link.create! valid_attributes
      get links_url
      expect(response).to be_successful
    end
  end
end
