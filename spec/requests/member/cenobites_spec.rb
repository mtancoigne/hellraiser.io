require 'rails_helper'

RSpec.describe 'member/cenobites', type: :request do
  let(:signed_in_cenobite) { Cenobite.find_by email: 'user@example.com' }

  let(:valid_attributes) do
    FactoryBot.build(:motion, cenobite: signed_in_cenobite).attributes
  end

  before { sign_in signed_in_cenobite }

  describe 'GET #edit' do
    it 'returns a success response' do
      get edit_member_preferences_url
      expect(response).to be_successful
    end
  end

  describe 'GET #index' do
    context 'when visible to members' do
      it 'returns a success response' do
        signed_in_cenobite.update visible_by_members: true
        get member_cenobites_url
        expect(response).to be_successful
      end
    end

    context 'when not visible to members' do
      it 'returns an error response' do
        get member_cenobites_url
        expect(response).not_to be_successful
      end
    end
  end

  describe 'POST #invite' do
    let(:other_cenobite) { FactoryBot.create(:cenobite, :active, :visible) }

    context 'when visible to members' do
      before do
        signed_in_cenobite.update visible_by_members: true
      end

      it 'creates a new acquaintance' do
        expect do
          post invite_member_cenobite_url(other_cenobite)
        end.to change(Acquaintance, :count).by 1
      end

      context 'when JS is disabled' do
        it 'redirects to the acquaintances list' do
          post invite_member_cenobite_url(other_cenobite)
          expect(response).to redirect_to(member_acquaintances_url)
        end
      end
    end

    context 'when not visible to members' do
      it 'returns an error response' do
        post invite_member_cenobite_url(other_cenobite)
        expect(response).not_to be_successful
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) do
        { notify_new_motion: false }
      end

      it 'updates the requested member_motion' do
        put member_preferences_url, params: { cenobite: new_attributes }
        signed_in_cenobite.reload
        expect(signed_in_cenobite.notify_new_motion).to eq new_attributes[:notify_new_motion]
      end

      it 'redirects to the member_motion' do
        put member_preferences_url, params: { cenobite: new_attributes }
        expect(response).to redirect_to(edit_member_preferences_path)
      end
    end
  end
end
