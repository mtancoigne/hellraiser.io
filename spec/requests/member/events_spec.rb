require 'rails_helper'

RSpec.describe '/member/events', type: :request do
  let(:signed_in_cenobite) { Cenobite.find_by email: 'user@example.com' }

  let(:valid_attributes) do
    FactoryBot.build(:event, cenobite: signed_in_cenobite).attributes
  end

  let(:invalid_attributes) do
    { name: nil }
  end

  before { sign_in signed_in_cenobite }

  describe 'GET #index' do
    it 'returns a success response' do
      Event.create! valid_attributes
      get member_events_url
      expect(response).to be_successful
    end
  end

  describe 'GET #show' do
    it 'returns a success response' do
      event = Event.create! valid_attributes
      get member_event_url(event)
      expect(response).to be_successful
    end
  end

  describe 'GET #new' do
    it 'returns a success response' do
      get new_member_event_url
      expect(response).to be_successful
    end
  end

  describe 'GET #edit' do
    it 'returns a success response' do
      event = Event.create! valid_attributes
      get edit_member_event_url(event)
      expect(response).to be_successful
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Event' do
        expect do
          post member_events_url, params: { event: valid_attributes }
        end.to change(Event, :count).by(1)
      end

      it 'redirects to the created member_event' do
        post member_events_url, params: { event: valid_attributes }
        expect(response).to redirect_to(member_event_path(Event.last))
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post member_events_url, params: { event: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PUT #update' do
    let(:event) { Event.create! valid_attributes }

    context 'with valid params' do
      let(:new_attributes) do
        { name: 'Renamed event' }
      end

      it 'updates the requested member_event' do
        put member_event_url(event), params: { event: new_attributes }
        event.reload
        expect(event.name).to eq new_attributes[:name]
      end

      it 'redirects to the member_event' do
        put member_event_url(event), params: { event: new_attributes }
        expect(response).to redirect_to(member_event_path(Event.last))
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        put member_event_url(event), params: { event: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested member_event' do
      event = Event.create! valid_attributes
      expect do
        delete member_event_url(event)
      end.to change(Event, :count).by(-1)
    end

    it 'redirects to the member_events list' do
      event = Event.create! valid_attributes
      delete member_event_url(event)
      expect(response).to redirect_to(member_events_url)
    end
  end

  describe 'PUT #attend' do
    context 'when event is private' do
      context 'when cenobite is owner' do
        let(:event) { FactoryBot.create(:event, :visible_by_owner, cenobite: signed_in_cenobite) }

        it 'makes the cenobite join' do
          expect do
            put attend_member_event_url(event)
          end.to change(EventsAttendance, :count).by 1
        end

        it 'redirects to the event' do
          put attend_member_event_url(event)
          expect(response).to redirect_to(member_event_path(Event.last))
        end
      end

      context 'when cenobite is not owner' do
        let(:event) { FactoryBot.create(:event, :visible_by_owner) }

        it 'does not make the cenobite join' do
          expect do
            put attend_member_event_url(event)
          end.not_to change(EventsAttendance, :count)
        end
      end
    end

    context 'when event is visible by members' do
      context 'when cenobite is owner' do
        let(:event) { FactoryBot.create(:event, :visible_by_members, cenobite: signed_in_cenobite) }

        it 'makes the cenobite join' do
          expect do
            put attend_member_event_url(event)
          end.to change(EventsAttendance, :count).by 1
        end

        it 'redirects to the event' do
          put attend_member_event_url(event)
          expect(response).to redirect_to(member_event_path(Event.last))
        end
      end

      context 'when cenobite is not owner' do
        let(:event) { FactoryBot.create(:event, :visible_by_members) }

        it 'makes the cenobite join' do
          expect do
            put attend_member_event_url(event)
          end.to change(EventsAttendance, :count).by 1
        end
      end
    end
  end

  describe 'DELETE #leave' do
    context 'when event is private' do
      context 'when cenobite is owner' do
        let(:event) { FactoryBot.create(:event, :visible_by_owner, cenobite: signed_in_cenobite) }

        it 'makes the cenobite leave' do
          event.add_attendee! signed_in_cenobite
          expect do
            delete leave_member_event_url(event)
          end.to change(EventsAttendance, :count).by(-1)
        end

        it 'redirects to the event' do
          event.add_attendee! signed_in_cenobite
          delete leave_member_event_url(event)
          expect(response).to redirect_to(member_event_path(Event.last))
        end
      end
    end

    context 'when event is visible by members' do
      context 'when cenobite is owner' do
        let(:event) { FactoryBot.create(:event, :visible_by_members, cenobite: signed_in_cenobite) }

        it 'makes the cenobite leave' do
          event.add_attendee! signed_in_cenobite
          expect do
            delete leave_member_event_url(event)
          end.to change(EventsAttendance, :count).by(-1)
        end

        it 'redirects to the event' do
          event.add_attendee! signed_in_cenobite
          delete leave_member_event_url(event)
          expect(response).to redirect_to(member_event_path(Event.last))
        end
      end

      context 'when cenobite is not owner' do
        let(:event) { FactoryBot.create(:event, :visible_by_members) }

        it 'makes the cenobite leave' do
          event.add_attendee! signed_in_cenobite
          expect do
            delete leave_member_event_url(event)
          end.to change(EventsAttendance, :count).by(-1)
        end
      end
    end
  end
end
