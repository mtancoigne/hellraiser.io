require 'rails_helper'

RSpec.describe '/member/chat_rooms/:id/chat_presences', type: :request do
  let(:signed_in_cenobite) { Cenobite.find_by email: 'user@example.com' }

  let(:chat_room) { FactoryBot.create(:chat_room) }
  let(:chat_participant) { FactoryBot.create(:chat_participant, chat_room: chat_room, cenobite: signed_in_cenobite) }
  let(:chat_presence) { FactoryBot.build(:chat_presence, chat_room: chat_room, chat_participant: chat_participant) }

  let(:valid_attributes) do
    FactoryBot.build(:chat_presence, chat_participant: chat_participant, chat_room: chat_room).attributes
  end

  before { sign_in signed_in_cenobite }

  describe 'GET #index' do
    it 'returns a success response' do
      ChatPresence.create! valid_attributes
      FactoryBot.create(:chat_presence, chat_room: chat_room)
      get member_chat_room_chat_presences_url(chat_room, format: :json)
      expect(response).to be_successful
    end
  end
end
