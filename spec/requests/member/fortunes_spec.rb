require 'rails_helper'

RSpec.describe '/member/fortunes', type: :request do
  let(:signed_in_cenobite) { Cenobite.find_by email: 'user@example.com' }

  let(:valid_attributes) do
    FactoryBot.build(:fortune, cenobite: signed_in_cenobite).attributes
  end

  let(:invalid_attributes) do
    { content: '' }
  end

  before { sign_in signed_in_cenobite }

  describe 'GET #index' do
    it 'returns a success response' do
      Fortune.create! valid_attributes
      get member_fortunes_url
      expect(response).to be_successful
    end
  end

  describe 'GET #random' do
    it 'returns a success response' do
      Fortune.create! valid_attributes
      get random_member_fortunes_url
      expect(response).to be_successful
    end
  end

  describe 'GET #new' do
    it 'returns a success response' do
      get new_member_fortune_url
      expect(response).to be_successful
    end
  end

  describe 'GET #edit' do
    it 'returns a success response' do
      fortune = Fortune.create! valid_attributes
      get edit_member_fortune_url(fortune)
      expect(response).to be_successful
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Fortune' do
        expect do
          post member_fortunes_url, params: { fortune: valid_attributes }
        end.to change(Fortune, :count).by(1)
      end

      it 'redirects to the fortunes list' do
        post member_fortunes_url, params: { fortune: valid_attributes }
        expect(response).to redirect_to(member_fortunes_path)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post member_fortunes_url, params: { fortune: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) do
        { content: 'Something funny' }
      end

      it 'updates the requested member_fortune' do
        fortune = Fortune.create! valid_attributes
        put member_fortune_url(fortune), params: { fortune: new_attributes }
        fortune.reload
        expect(fortune.content).to eq new_attributes[:content]
      end

      it 'redirects to the fortunes list' do
        fortune = Fortune.create! valid_attributes
        put member_fortune_url(fortune), params: { fortune: new_attributes }
        expect(response).to redirect_to(member_fortunes_path)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        fortune = Fortune.create! valid_attributes
        put member_fortune_url(fortune), params: { fortune: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested member_fortune' do
      fortune = Fortune.create! valid_attributes
      expect do
        delete member_fortune_url(fortune)
      end.to change(Fortune, :count).by(-1)
    end

    it 'redirects to the member_fortunes list' do
      fortune = Fortune.create! valid_attributes
      delete member_fortune_url(fortune)
      expect(response).to redirect_to(member_fortunes_url)
    end
  end

  describe 'PUT #upvote' do
    it 'change the score' do
      fortune = Fortune.create! valid_attributes
      put upvote_member_fortune_url(fortune, format: :json)
      fortune.reload
      expect(fortune.score).to eq 1
    end
  end

  describe 'PUT #downvote' do
    it 'does not change the score' do
      fortune = Fortune.create! valid_attributes
      put downvote_member_fortune_url(fortune, format: :json)
      fortune.reload
      expect(fortune.score).to eq 0
    end

    it 'changes the votes count' do
      fortune = Fortune.create! valid_attributes
      put downvote_member_fortune_url(fortune, format: :json)
      fortune.reload
      expect(fortune.votes_count).to eq 1
    end
  end
end
