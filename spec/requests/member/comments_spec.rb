require 'rails_helper'

RSpec.describe '/member/xxx/:id/comments', type: :request do
  let(:signed_in_cenobite) { Cenobite.find_by email: 'user@example.com' }

  let(:commentable) { FactoryBot.create(:motion) }

  let(:valid_attributes) do
    FactoryBot.build(:comment, commentable: commentable, cenobite: signed_in_cenobite).attributes
  end

  let(:invalid_attributes) do
    { content: '' }
  end

  before { sign_in signed_in_cenobite }

  describe 'GET #index' do
    it 'returns a JSON success response' do
      Comment.create! valid_attributes
      get member_motion_comments_url(commentable, format: :json)
      expect(response).to be_successful
    end
  end

  describe 'GET #show' do
    it 'returns a JSON success response' do
      comment = Comment.create! valid_attributes
      get member_comment_url(comment, format: :json)
      expect(response).to be_successful
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Comment' do
        expect do
          post member_motion_comments_url(commentable), params: { comment: valid_attributes }
        end.to change(Comment, :count).by(1)
      end

      it 'redirects to the commentable entity' do
        post member_motion_comments_url(commentable), params: { comment: valid_attributes }
        expect(response).to redirect_to(member_motion_path(commentable))
      end
    end

    context 'with invalid params' do
      it 'redirects to the commentable entity' do
        post member_motion_comments_url(commentable), params: { comment: invalid_attributes }
        expect(response).to redirect_to(member_motion_path(commentable))
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) do
        { content: 'New content' }
      end

      it 'updates the requested member_comment' do
        comment = Comment.create! valid_attributes
        put member_comment_url(comment), params: { comment: new_attributes }
        comment.reload
        expect(comment.content).to eq new_attributes[:content]
      end

      it 'redirects to the commented entity' do
        comment = Comment.create! valid_attributes
        put member_comment_url(comment), params: { comment: new_attributes }
        expect(response).to redirect_to(member_motion_path(commentable))
      end
    end

    context 'with invalid params' do
      it 'redirects to the commented entity' do
        comment = Comment.create! valid_attributes
        put member_comment_url(comment), params: { comment: invalid_attributes }
        expect(response).to redirect_to(member_motion_path(commentable))
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested member_comment' do
      comment = Comment.create! valid_attributes
      expect do
        delete member_comment_url(comment)
      end.to change(Comment, :count).by(-1)
    end

    it 'redirects to the commented entity' do
      comment = Comment.create! valid_attributes
      delete member_comment_url(comment)
      expect(response).to redirect_to(member_motion_path(commentable))
    end
  end
end
