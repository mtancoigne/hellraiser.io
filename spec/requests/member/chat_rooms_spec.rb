require 'rails_helper'

RSpec.describe '/member/chat_rooms', type: :request do
  let(:signed_in_cenobite) { Cenobite.find_by email: 'user@example.com' }

  let(:valid_attributes) do
    FactoryBot.build(:chat_room, cenobite: signed_in_cenobite).attributes
  end

  before { sign_in signed_in_cenobite }

  describe 'GET #index' do
    it 'returns a success response' do
      ChatRoom.create! valid_attributes
      get member_chat_rooms_url(format: :json)
      expect(response).to be_successful
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new ChatRoom' do
        expect do
          post member_chat_rooms_url(format: :json), params: { chat_room: valid_attributes, chat_participant: { name: 'John' } }
        end.to change(ChatRoom, :count).by(1)
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) do
        { name: 'New name' }
      end

      it 'updates the requested member_chat_room' do
        chat_room = ChatRoom.create! valid_attributes
        put member_chat_room_url(chat_room, format: :json), params: { chat_room: new_attributes }
        chat_room.reload
        expect(chat_room.name).to eq new_attributes[:name]
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested member_chat_room' do
      chat_room = ChatRoom.create! valid_attributes
      expect do
        delete member_chat_room_url(chat_room, format: :json)
      end.to change(ChatRoom, :count).by(-1)
    end
  end
end
