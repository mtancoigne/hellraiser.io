require 'rails_helper'

RSpec.describe '/member/motions', type: :request do
  let(:signed_in_cenobite) { Cenobite.find_by email: 'user@example.com' }

  let(:valid_attributes) do
    FactoryBot.build(:motion, cenobite: signed_in_cenobite).attributes
  end

  let(:invalid_attributes) do
    { title: '' }
  end

  before { sign_in signed_in_cenobite }

  describe 'GET #index' do
    it 'returns a success response' do
      Motion.create! valid_attributes
      get member_motions_url
      expect(response).to be_successful
    end
  end

  describe 'GET #show' do
    it 'returns a success response' do
      motion = Motion.create! valid_attributes
      get member_motion_url(motion)
      expect(response).to be_successful
    end
  end

  describe 'GET #new' do
    it 'returns a success response' do
      get new_member_motion_url
      expect(response).to be_successful
    end
  end

  describe 'GET #edit' do
    it 'returns a success response' do
      motion = Motion.create! valid_attributes
      get edit_member_motion_url(motion)
      expect(response).to be_successful
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Motion' do
        expect do
          post member_motions_url, params: { motion: valid_attributes }
        end.to change(Motion, :count).by(1)
      end

      it 'redirects to the created member_motion' do
        post member_motions_url, params: { motion: valid_attributes }
        expect(response).to redirect_to(member_motion_path(Motion.last))
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post member_motions_url, params: { motion: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) do
        { title: 'New title' }
      end

      it 'updates the requested member_motion' do
        motion = Motion.create! valid_attributes
        put member_motion_url(motion), params: { motion: new_attributes }
        motion.reload
        expect(motion.title).to eq new_attributes[:title]
      end

      it 'redirects to the member_motion' do
        motion = Motion.create! valid_attributes
        put member_motion_url(motion), params: { motion: new_attributes }
        expect(response).to redirect_to(member_motion_path(Motion.last))
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        motion = Motion.create! valid_attributes
        put member_motion_url(motion), params: { motion: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested member_motion' do
      motion = Motion.create! valid_attributes
      expect do
        delete member_motion_url(motion)
      end.to change(Motion, :count).by(-1)
    end

    it 'redirects to the member_motions list' do
      motion = Motion.create! valid_attributes
      delete member_motion_url(motion)
      expect(response).to redirect_to(member_motions_url)
    end
  end

  describe 'PUT #upvote' do
    it 'change the score' do
      motion = Motion.create! valid_attributes
      put upvote_member_motion_url(motion, format: :json)
      motion.reload
      expect(motion.score).to eq 1
    end
  end

  describe 'PUT #downvote' do
    it 'does not change the score' do
      motion = Motion.create! valid_attributes
      put downvote_member_motion_url(motion, format: :json)
      motion.reload
      expect(motion.score).to eq 0
    end

    it 'changes the votes count' do
      motion = Motion.create! valid_attributes
      put downvote_member_motion_url(motion, format: :json)
      motion.reload
      expect(motion.votes_count).to eq 1
    end
  end
end
