require 'rails_helper'

RSpec.describe '/member/links', type: :request do
  let(:signed_in_cenobite) { Cenobite.find_by email: 'user@example.com' }

  let(:valid_attributes) do
    FactoryBot.build(:link, cenobite: signed_in_cenobite).attributes
  end

  let(:invalid_attributes) do
    { url: '' }
  end

  before { sign_in signed_in_cenobite }

  describe 'GET #new' do
    it 'returns a success response' do
      get new_member_link_url
      expect(response).to be_successful
    end
  end

  describe 'GET #edit' do
    it 'returns a success response' do
      link = Link.create! valid_attributes
      get edit_member_link_url(link)
      expect(response).to be_successful
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Link' do
        expect do
          post member_links_url, params: { link: valid_attributes }
        end.to change(Link, :count).by(1)
      end

      it 'redirects to the member_links list' do
        post member_links_url, params: { link: valid_attributes }
        expect(response).to redirect_to(links_url)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post member_links_url, params: { link: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) do
        { url: 'https://google.com' }
      end

      it 'updates the requested member_link' do
        link = Link.create! valid_attributes
        put member_link_url(link), params: { link: new_attributes }
        link.reload
        expect(link.url).to eq new_attributes[:url]
      end

      it 'redirects to the member_links list' do
        link = Link.create! valid_attributes
        put member_link_url(link), params: { link: new_attributes }
        expect(response).to redirect_to(links_url)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        link = Link.create! valid_attributes
        put member_link_url(link), params: { link: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested member_link' do
      link = Link.create! valid_attributes
      expect do
        delete member_link_url(link)
      end.to change(Link, :count).by(-1)
    end

    it 'redirects to the member_links list' do
      link = Link.create! valid_attributes
      delete member_link_url(link)
      expect(response).to redirect_to(links_url)
    end
  end

  describe 'PUT #toggle_like' do
    it 'adds the requested link to the current cenobite\'s favorites' do
      link = Link.create! valid_attributes
      put toggle_like_member_link_url(link, format: :json)
      link.reload
      expect(link.likes.count).to eq 1
    end
  end

  describe 'PUT #upvote' do
    it 'change the score' do
      link = Link.create! valid_attributes
      put upvote_member_link_url(link, format: :json)
      link.reload
      expect(link.score).to eq 1
    end
  end

  describe 'PUT #downvote' do
    it 'does not change the score' do
      link = Link.create! valid_attributes
      put downvote_member_link_url(link, format: :json)
      link.reload
      expect(link.score).to eq 0
    end

    it 'changes the votes count' do
      link = Link.create! valid_attributes
      put downvote_member_link_url(link, format: :json)
      link.reload
      expect(link.votes_count).to eq 1
    end
  end
end
