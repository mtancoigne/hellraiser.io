require 'rails_helper'

RSpec.describe '/member/dead_man_switches', type: :request do
  let(:signed_in_cenobite) { Cenobite.find_by email: 'user@example.com' }

  let(:valid_attributes) do
    FactoryBot.build(:dead_man_switch, cenobite: signed_in_cenobite).attributes.symbolize_keys
  end

  let(:invalid_attributes) do
    { name: '' }
  end

  before { sign_in signed_in_cenobite }

  describe 'GET #index' do
    it 'returns a success response' do
      DeadManSwitch.create! valid_attributes
      get member_dead_man_switches_url
      expect(response).to be_successful
    end
  end

  describe 'GET #show' do
    it 'returns a success response' do
      dead_man_switch = DeadManSwitch.create! valid_attributes
      get member_dead_man_switch_url(dead_man_switch)
      expect(response).to be_successful
    end
  end

  describe 'GET #new' do
    it 'returns a success response' do
      get new_member_dead_man_switch_url
      expect(response).to be_successful
    end
  end

  describe 'GET #edit' do
    it 'returns a success response' do
      dead_man_switch = DeadManSwitch.create! valid_attributes
      get edit_member_dead_man_switch_url(dead_man_switch)
      expect(response).to be_successful
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new DeadManSwitch' do
        expect do
          post member_dead_man_switches_url, params: { dead_man_switch: valid_attributes }
        end.to change(DeadManSwitch, :count).by(1)
      end

      it 'redirects to the created dead_man_switch' do
        post member_dead_man_switches_url, params: { dead_man_switch: valid_attributes }
        expect(response).to redirect_to(member_dead_man_switch_url(DeadManSwitch.last))
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post member_dead_man_switches_url, params: { dead_man_switch: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) do
        { name: 'New name' }
      end

      it 'updates the requested dead_man_switch' do
        dead_man_switch = DeadManSwitch.create! valid_attributes
        put member_dead_man_switch_url(dead_man_switch), params: { dead_man_switch: new_attributes }
        dead_man_switch.reload
        expect(dead_man_switch.name).to eq 'New name'
      end

      it 'redirects to the dead_man_switch' do
        dead_man_switch = DeadManSwitch.create! valid_attributes
        put member_dead_man_switch_url(dead_man_switch), params: { dead_man_switch: new_attributes }
        expect(response).to redirect_to(member_dead_man_switch_url(dead_man_switch))
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        dead_man_switch = DeadManSwitch.create! valid_attributes
        put member_dead_man_switch_url(dead_man_switch), params: { dead_man_switch: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested dead_man_switch' do
      dead_man_switch = DeadManSwitch.create! valid_attributes
      expect do
        delete member_dead_man_switch_url(dead_man_switch)
      end.to change(DeadManSwitch, :count).by(-1)
    end

    it 'redirects to the dead_man_switches list' do
      dead_man_switch = DeadManSwitch.create! valid_attributes
      delete member_dead_man_switch_url(dead_man_switch)
      expect(response).to redirect_to(member_dead_man_switches_url)
    end
  end

  describe 'PUT #ping' do
    it 'pings the requested dead_man_switch' do
      dead_man_switch = DeadManSwitch.create! valid_attributes
      before_update   = Time.current
      put ping_member_dead_man_switch_url(dead_man_switch)
      dead_man_switch.reload
      expect(dead_man_switch.last_pinged_at > before_update).to be_truthy
    end
  end

  describe 'PUT #ping_all' do
    it 'updates every dead_man_switch' do
      dead_man_switch = DeadManSwitch.create! valid_attributes
      before_update   = Time.current
      put ping_all_member_dead_man_switches_url
      dead_man_switch.reload
      expect(dead_man_switch.last_pinged_at > before_update).to be_truthy
    end
  end

  describe 'PUT #rearm' do
    it 'rearms the requested dead_man_switch' do
      dead_man_switch = FactoryBot.create(:dead_man_switch, :sent, cenobite: signed_in_cenobite)
      put rearm_member_dead_man_switch_url(dead_man_switch)
      dead_man_switch.reload
      expect(dead_man_switch.email_sent_at).to be_nil
    end
  end
end
