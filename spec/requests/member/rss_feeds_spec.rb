require 'rails_helper'

RSpec.describe '/member/rss_feeds', type: :request do
  let(:signed_in_cenobite) { Cenobite.find_by email: 'user@example.com' }

  let(:valid_attributes) do
    { url: 'https://korben.info/feed', cenobite_id: signed_in_cenobite.id }
  end

  let(:invalid_attributes) do
    { url: '' }
  end

  let(:cassette) { 'rss_feed/get_rss_feed' }
  let(:rss_feed) do
    rss_feed = nil
    VCR.use_cassette(cassette) do
      rss_feed = RssFeed.create! valid_attributes
    end
    rss_feed
  end

  before { sign_in signed_in_cenobite }

  describe 'GET /index' do
    it 'renders a successful response' do
      rss_feed
      get member_rss_feeds_url
      expect(response).to be_successful
    end
  end

  describe 'GET /new' do
    it 'renders a successful response' do
      get new_member_rss_feed_url
      expect(response).to be_successful
    end
  end

  describe 'GET /edit' do
    it 'render a successful response' do
      get edit_member_rss_feed_url(rss_feed)
      expect(response).to be_successful
    end
  end

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new RssFeed' do
        VCR.use_cassette(cassette) do
          expect do
            post member_rss_feeds_url, params: { rss_feed: valid_attributes }
          end.to change(RssFeed, :count).by(1)
        end
      end

      it 'redirects to the created list of feeds' do
        VCR.use_cassette(cassette) do
          post member_rss_feeds_url, params: { rss_feed: valid_attributes }
          expect(response).to redirect_to(member_rss_feeds_url)
        end
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new RssFeed' do
        expect do
          post member_rss_feeds_url, params: { rss_feed: invalid_attributes }
        end.not_to change(RssFeed, :count)
      end

      it "renders a successful response (i.e. to display the 'new' template)" do
        post member_rss_feeds_url, params: { rss_feed: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PATCH /update' do
    context 'with valid parameters' do # rubocop:disable RSpec/MultipleMemoizedHelpers
      let(:new_attributes) do
        { tag_list: 'movie, horror' }
      end

      it 'updates the requested rss feed' do
        patch member_rss_feed_url(rss_feed), params: { rss_feed: new_attributes }
        rss_feed.reload
        expect(rss_feed.tag_list).to eq %w[movie horror]
      end

      it 'redirects to the rss feeds list' do
        patch member_rss_feed_url(rss_feed), params: { rss_feed: new_attributes }
        rss_feed.reload
        expect(response).to redirect_to(member_rss_feeds_url)
      end
    end

    # NOTE: There is no test for an update with invalid parameters, as the only
    # modifiable field can be blank.
  end

  describe 'DELETE /destroy' do
    it 'destroys the requested member_rss_feed' do
      rss_feed
      expect do
        delete member_rss_feed_url(rss_feed)
      end.to change(RssFeed, :count).by(-1)
    end

    it 'redirects to the member_rss_feeds list' do
      delete member_rss_feed_url(rss_feed)
      expect(response).to redirect_to(member_rss_feeds_url)
    end
  end
end
