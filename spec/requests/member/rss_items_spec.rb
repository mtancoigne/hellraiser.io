require 'rails_helper'

RSpec.describe '/member/rss_items', type: :request do
  let(:signed_in_cenobite) { Cenobite.find_by email: 'user@example.com' }

  let(:valid_attributes) do
    { url: 'https://korben.info/feed', cenobite_id: signed_in_cenobite.id }
  end

  let(:cassette) { 'rss_feed/get_rss_feed' }
  let(:rss_feed) do
    rss_feed = nil
    VCR.use_cassette(cassette) do
      rss_feed = RssFeed.create! valid_attributes
    end
    rss_feed
  end

  before { sign_in signed_in_cenobite }

  describe 'GET /index' do
    it 'renders a successful response' do
      rss_feed # Creates items
      get member_rss_items_url
      expect(response).to be_successful
    end
  end

  describe 'PUT #toggle_like' do
    it 'adds the requested item to the current cenobite\'s favorites' do
      rss_feed # Creates items
      rss_item = RssItem.last
      put toggle_like_member_rss_item_url(rss_item, format: :json)
      rss_item.reload
      expect(rss_item.likes.count).to eq 1
    end
  end

  describe 'PUT #upvote' do
    it 'upvotes the requested item' do
      rss_feed
      rss_item = RssItem.last
      put upvote_member_rss_item_url(rss_item, format: :json)
      rss_item.reload
      expect(rss_item.score).to eq 1
    end
  end

  describe 'PUT #downvote' do
    it 'adds a vote to the requested item' do
      rss_feed
      rss_item = RssItem.last
      put downvote_member_rss_item_url(rss_item, format: :json)
      rss_item.reload
      expect(rss_item.votes_count).to eq 1
    end

    it "don't change the score" do
      rss_feed
      rss_item = RssItem.last
      put downvote_member_rss_item_url(rss_item, format: :json)
      rss_item.reload
      expect(rss_item.score).to eq 0
    end
  end
end
