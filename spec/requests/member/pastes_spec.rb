require 'rails_helper'

RSpec.describe '/member/pastes', type: :request do
  let(:signed_in_cenobite) { Cenobite.find_by email: 'user@example.com' }

  let(:valid_attributes) do
    FactoryBot.build(:paste, cenobite: signed_in_cenobite).attributes.symbolize_keys
  end

  let(:invalid_attributes) do
    { content: '' }
  end

  before { sign_in signed_in_cenobite }

  describe 'GET #index' do
    it 'returns a success response' do
      Paste.create! valid_attributes
      get member_pastes_url
      expect(response).to be_successful
    end
  end

  describe 'GET #new' do
    it 'returns a success response' do
      get new_member_paste_url
      expect(response).to be_successful
    end
  end

  describe 'GET #edit' do
    it 'returns a success response' do
      paste = Paste.create! valid_attributes
      get edit_member_paste_url(paste)
      expect(response).to be_successful
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Paste' do
        expect do
          post member_pastes_url, params: { paste: valid_attributes }
        end.to change(Paste, :count).by(1)
      end

      it 'redirects to the created paste' do
        post member_pastes_url, params: { paste: valid_attributes }
        expect(response).to redirect_to(member_paste_path(Paste.last))
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post member_pastes_url, params: { paste: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) do
        { name: 'New name' }
      end

      it 'updates the requested paste' do
        paste = Paste.create! valid_attributes
        put member_paste_url(paste), params: { paste: new_attributes }
        paste.reload
        expect(paste.name).to eq 'New name'
      end

      it 'redirects to the paste' do
        paste = Paste.create! valid_attributes
        put member_paste_url(paste), params: { paste: new_attributes }
        expect(response).to redirect_to(member_paste_path(paste))
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        paste = Paste.create! valid_attributes
        put member_paste_url(paste), params: { paste: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested paste' do
      paste = Paste.create! valid_attributes
      expect do
        delete member_paste_url(paste)
      end.to change(Paste, :count).by(-1)
    end

    it 'redirects to the pastes list' do
      paste = Paste.create! valid_attributes
      delete member_paste_url(paste)
      expect(response).to redirect_to(member_pastes_path)
    end
  end
end
