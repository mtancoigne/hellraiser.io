require 'rails_helper'

RSpec.describe '/member/subscriptions', type: :request do
  let(:signed_in_cenobite) { Cenobite.find_by email: 'user@example.com' }

  let(:valid_attributes) do
    FactoryBot.build(:subscription, :on_motion, cenobite: signed_in_cenobite).attributes
  end

  before { sign_in signed_in_cenobite }

  describe 'GET #index' do
    it 'returns a success response' do
      Subscription.create! valid_attributes
      get member_subscriptions_url
      expect(response).to be_successful
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested member_subscription' do
      subscription = Subscription.create! valid_attributes
      expect do
        delete member_subscription_url(subscription)
      end.to change(Subscription, :count).by(-1)
    end

    it 'redirects to the member_subscriptions list' do
      subscription = Subscription.create! valid_attributes
      delete member_subscription_url(subscription)
      expect(response).to redirect_to(member_subscriptions_url)
    end
  end
end
