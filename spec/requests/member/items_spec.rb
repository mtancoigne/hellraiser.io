require 'rails_helper'

RSpec.describe '/member/items', type: :request do
  let(:signed_in_cenobite) { Cenobite.find_by email: 'user@example.com' }

  let(:valid_attributes) do
    FactoryBot.build(:item, owner: signed_in_cenobite).attributes
  end

  let(:invalid_attributes) do
    { name: '' }
  end

  before { sign_in signed_in_cenobite }

  describe 'GET #index' do
    it 'returns a success response' do
      Item.create! valid_attributes
      get member_items_url
      expect(response).to be_successful
    end
  end

  describe 'GET #show' do
    it 'returns a success response' do
      item = Item.create! valid_attributes
      get member_item_url(item)
      expect(response).to be_successful
    end
  end

  describe 'GET #new' do
    it 'returns a success response' do
      get new_member_item_url
      expect(response).to be_successful
    end
  end

  describe 'GET #edit' do
    it 'returns a success response' do
      item = Item.create! valid_attributes
      get edit_member_item_url(item)
      expect(response).to be_successful
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Item' do
        expect do
          post member_items_url, params: { item: valid_attributes }
        end.to change(Item, :count).by(1)
      end

      it 'redirects to the created member_item' do
        post member_items_url, params: { item: valid_attributes }
        expect(response).to redirect_to(member_item_path(Item.last))
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post member_items_url, params: { item: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) do
        { name: 'John Connor meets Pinhead' }
      end

      it 'updates the requested member_item' do
        item = Item.create! valid_attributes
        put member_item_url(item), params: { item: new_attributes }
        item.reload
        expect(item.name).to eq new_attributes[:name]
      end

      it 'redirects to the member_item' do
        item = Item.create! valid_attributes
        put member_item_url(item), params: { item: new_attributes }
        expect(response).to redirect_to(member_item_path(Item.last))
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        item = Item.create! valid_attributes
        put member_item_url(item), params: { item: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested member_item' do
      item = Item.create! valid_attributes
      expect do
        delete member_item_url(item)
      end.to change(Item, :count).by(-1)
    end

    it 'redirects to the member_items list' do
      item = Item.create! valid_attributes
      delete member_item_url(item)
      expect(response).to redirect_to(member_items_url)
    end
  end
end
