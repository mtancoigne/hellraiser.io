require 'rails_helper'

RSpec.describe '/member/acquaintances', type: :request do
  let(:signed_in_cenobite) { Cenobite.find_by email: 'user@example.com' }
  let(:other_cenobite) { FactoryBot.create(:cenobite, :active, email: 'john@doe.com') }

  let(:valid_attributes) do
    # NOTE: here, the current cenobite is invited
    FactoryBot.build(:acquaintance, invitee: signed_in_cenobite, inviter: other_cenobite).attributes
  end

  let(:valid_payload) do
    invitee = FactoryBot.create(:cenobite, :active)
    { email: invitee.email }
  end

  let(:invalid_payload) do
    { email: 'nonexistingperson@example.com' }
  end

  before { sign_in signed_in_cenobite }

  describe 'GET #index' do
    it 'returns a success response' do
      Acquaintance.create! valid_attributes
      get member_acquaintances_url
      expect(response).to be_successful
    end
  end

  describe 'GET #new' do
    it 'returns a success response' do
      get new_member_acquaintance_url
      expect(response).to be_successful
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Acquaintance' do
        expect do
          post member_acquaintances_url, params: { acquaintance: valid_payload }
        end.to change(Acquaintance, :count).by(1)
      end

      it 'redirects to the acquaintances list' do
        post member_acquaintances_url, params: { acquaintance: valid_payload }
        expect(response).to redirect_to(member_acquaintances_url)
      end
    end

    context 'with invalid params' do
      it 'invites the cenobite' do
        expect do
          post member_acquaintances_url, params: { acquaintance: invalid_payload }
        end.to change(Cenobite, :count).by 1
      end
    end
  end

  describe 'POST #accept' do
    context 'with valid acquaintance' do
      it 'redirects to the acquaintances list' do
        acquaintance = Acquaintance.create! valid_attributes
        post accept_member_acquaintance_url(acquaintance)
        expect(response).to redirect_to(member_acquaintances_url)
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested member_acquaintance' do
      acquaintance = Acquaintance.create! valid_attributes
      expect do
        delete member_acquaintance_url(acquaintance)
      end.to change(Acquaintance, :count).by(-1)
    end

    it 'redirects to the member_acquaintances list' do
      acquaintance = Acquaintance.create! valid_attributes
      delete member_acquaintance_url(acquaintance)
      expect(response).to redirect_to(member_acquaintances_url)
    end
  end
end
