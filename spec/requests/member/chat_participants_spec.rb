require 'rails_helper'

RSpec.describe 'member/chat_participants/', type: :request do
  let(:signed_in_cenobite) { Cenobite.find_by email: 'user@example.com' }

  let(:chat_room) { FactoryBot.create(:chat_room) }

  let(:valid_attributes) do
    FactoryBot.build(:chat_participant, chat_room: chat_room, cenobite: signed_in_cenobite).attributes
  end

  before { sign_in signed_in_cenobite }

  describe 'GET #index' do
    it 'returns a success response' do
      ChatParticipant.create! valid_attributes
      get member_chat_participants_url(format: :json)
      expect(response).to be_successful
    end
  end

  describe 'GET #show' do
    it 'returns a success response' do
      chat_participant = ChatParticipant.create! valid_attributes
      get member_chat_participant_url(chat_participant, format: :json)
      expect(response).to be_successful
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) { { name: 'Paul' } }

      it 'updates the requested chat_participant' do
        chat_participant = ChatParticipant.create! valid_attributes
        put member_chat_participant_url(chat_participant, format: :json), params: { chat_participant: new_attributes }
        chat_participant.reload
        expect(chat_participant.name).to eq new_attributes[:name]
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested chat_participant' do
      chat_participant = ChatParticipant.create! valid_attributes
      expect do
        delete member_chat_participant_url(chat_participant, format: :json)
      end.to change(ChatParticipant, :count).by(-1)
    end
  end
end
