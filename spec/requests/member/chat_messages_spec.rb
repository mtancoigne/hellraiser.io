require 'rails_helper'

RSpec.describe '/member/chat_rooms/:id/chat_messages', type: :request do
  let(:signed_in_cenobite) { Cenobite.find_by email: 'user@example.com' }

  let(:chat_room) do
    FactoryBot.create(:chat_room)
  end
  let(:chat_participant) do
    FactoryBot.create(:chat_participant, chat_room: chat_room, cenobite: signed_in_cenobite)
  end
  let(:chat_presence) do
    FactoryBot.build(:chat_presence, chat_participant: chat_participant, chat_room: chat_room)
  end

  let(:valid_attributes) do
    FactoryBot.build(:chat_message, chat_participant: chat_participant, chat_room: chat_room).attributes
  end

  before { sign_in signed_in_cenobite }

  describe 'GET #index' do
    it 'returns a success response' do
      ChatMessage.create! valid_attributes
      get member_chat_room_chat_messages_url(chat_room, format: :json)
      expect(response).to be_successful
    end
  end

  describe 'GET #show' do
    it 'returns a success response' do
      chat_message = ChatMessage.create! valid_attributes
      get member_chat_room_chat_message_url(chat_room, chat_message, format: :json)
      expect(response).to be_successful
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new ChatMessage' do
        expect do
          post member_chat_room_chat_messages_url(chat_room, format: :json), params: { chat_message: valid_attributes }
        end.to change(ChatMessage, :count).by(1)
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do # rubocop:disable RSpec/MultipleMemoizedHelpers
      let(:new_attributes) { { content: 'Dude, what?' } }

      it 'updates the requested member_chat_message' do
        chat_message = ChatMessage.create! valid_attributes
        put member_chat_room_chat_message_url(chat_room, chat_message, format: :json), params: { chat_message: new_attributes }
        chat_message.reload
        expect(chat_message.content).to eq new_attributes[:content]
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested member_chat_message' do
      chat_message = ChatMessage.create! valid_attributes
      expect do
        delete member_chat_room_chat_message_url(chat_room, chat_message, format: :json)
      end.to change(ChatMessage, :count).by(-1)
    end
  end
end
