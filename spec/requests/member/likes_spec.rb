require 'rails_helper'

RSpec.describe '/member/likes', type: :request do
  let(:signed_in_cenobite) { Cenobite.find_by email: 'user@example.com' }

  let(:valid_attributes) do
    FactoryBot.build(:like, cenobite: signed_in_cenobite).attributes
  end

  before { sign_in signed_in_cenobite }

  describe 'GET #index' do
    it 'returns a success response' do
      Like.create! valid_attributes
      get member_likes_url
      expect(response).to be_successful
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested member_like' do
      like = Like.create! valid_attributes
      expect do
        delete member_like_url(like)
      end.to change(Like, :count).by(-1)
    end

    it 'redirects to the member_likes list' do
      like = Like.create! valid_attributes
      delete member_like_url(like)
      expect(response).to redirect_to(member_likes_url)
    end
  end
end
