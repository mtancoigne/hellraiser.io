require 'rails_helper'

RSpec.describe '/pastes', type: :request do
  let(:signed_in_cenobite) { Cenobite.find_by email: 'user@example.com' }

  let(:valid_attributes) do
    cenobite = FactoryBot.create(:cenobite, :active)
    FactoryBot.build(:paste, cenobite: cenobite).attributes.symbolize_keys
  end

  before { sign_in signed_in_cenobite }

  context 'with visitor' do
    describe 'GET #show' do
      it 'returns a success response' do
        paste = Paste.create! valid_attributes
        get paste_url(paste)
        expect(response).to be_successful
      end
    end

    describe 'GET #reveal' do
      it 'burns the paste' do
        paste = Paste.create! valid_attributes.merge(burn: true)
        get reveal_paste_url(paste)
        expect(Paste.count).to eq 0
      end
    end

    describe 'PUT #reveal' do
      context 'with valid password' do
        it 'displays the paste' do
          paste = Paste.create! valid_attributes.merge(password: 'password', burn: true)
          get reveal_paste_url(paste), params: { paste: { password: 'password' } }
          expect(Paste.count).to eq 0
        end
      end

      context 'with invalid password' do
        it 'displays an error' do
          paste = Paste.create! valid_attributes.merge(password: 'password', burn: true)
          get reveal_paste_url(paste), params: { paste: { password: 'wrong' } }
          expect(response).to have_http_status :unprocessable_entity
        end

        it 'does not destroys the paste' do
          paste = Paste.create! valid_attributes.merge(password: 'password', burn: true)
          get reveal_paste_url(paste), params: { paste: { password: 'wrong' } }
          expect(Paste.count).to eq 1
        end
      end
    end
  end
end
