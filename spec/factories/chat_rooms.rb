FactoryBot.define do
  factory :chat_room do
    name { Faker::Lorem.words(number: rand(1..3)).join(' ') }
    cenobite

    trait :with_acquaintance do
      name { nil }
      # association :acquaintance, factory: :acquaintance
      acquaintance
      cenobite_id { acquaintance.inviter_id }
    end
  end
end
