FactoryBot.define do
  factory :link do
    title { Faker::Lorem.sentence }
    url { Faker::Internet.url }
    description { Faker::Lorem.paragraphs(number: 2).join "\n" }
    cenobite
    # Default
    visible_by_all

    trait(:visible_by_owner) do
      visibility { 0 }
    end

    trait(:visible_by_members) do
      visibility { 1 }
    end

    trait(:visible_by_all) do
      visibility { 2 }
    end
  end
end
