FactoryBot.define do
  factory :acquaintance do
    inviter factory: [:cenobite, :active]
    invitee factory: [:cenobite, :active]
    by_email

    trait :accepted do
      after :create do |acq|
        acq.accept!
        acq.reload
        acq
      end
    end

    trait :by_email do
      invite_method { :email }
    end

    trait :by_id do
      invite_method { :id }
    end
  end
end
