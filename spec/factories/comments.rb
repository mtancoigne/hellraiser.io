FactoryBot.define do
  factory :comment do
    content { Faker::Lorem.paragraphs(number: 2).join("\n") }
    cenobite
    # Default
    on_motion

    trait :on_motion do
      commentable factory: [:motion]
    end
  end
end
