FactoryBot.define do
  factory :rss_feed do
    sequence(:url) do |n|
      %w[
        https://korben.info/feed
        https://linuxfr.org/news.atom
        https://www.youtube.com/feeds/videos.xml?user=TSPcarbonfree
      ][n - 1]
    end
    title { Faker::Lorem.sentence }
    description { Faker::Lorem.paragraph }
    link do
      uri = URI.parse url
      "#{uri.scheme}://#{uri.host}"
    end
    cenobite
  end
end
