FactoryBot.define do
  factory :chat_message do
    content { Faker::Lorem.sentence }
    chat_room
    chat_participant
  end
end
