FactoryBot.define do
  factory :dead_man_switch do
    name { Faker::Lorem.sentence }
    recipients { 'buddy@example.com' }
    content { Faker::Lorem.paragraphs(number: 3).join("\n") }
    ping_frequency { 1 }
    cenobite

    trait :expired do
      after :create do |record|
        record.update_column :expires_at, Time.zone.now - record.ping_frequency.hours # rubocop:disable Rails/SkipsModelValidations
      end
    end

    trait :sent do
      email_sent_at { Time.current }

      after :create do |record|
        record.update_column :expires_at, Time.zone.now - record.ping_frequency.hours # rubocop:disable Rails/SkipsModelValidations
      end
    end
  end
end
