FactoryBot.define do
  factory :motion do
    title { Faker::Lorem.sentence }
    content { Faker::Lorem.paragraphs(number: 2).join "\n" }
    cenobite
  end
end
