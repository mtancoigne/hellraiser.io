FactoryBot.define do
  factory :like do
    cenobite
    on_link

    trait :on_link do
      likable factory: [:link], strategy: :create
    end

    trait :on_rss_item do
      likable factory: [:rss_item], strategy: :create
    end
  end
end
