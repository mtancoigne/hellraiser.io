FactoryBot.define do
  factory :chat_presence do
    chat_room
    chat_participant
    instance_id { rand(1..1_000_000) }
  end
end
