FactoryBot.define do
  factory :cenobite do
    email { Faker::Internet.unique.email }
    password { 'password' }
    invisible

    trait :known do
      email { 'user@example.com' }
    end

    trait :active do
      invitation_accepted_at { Time.current }
    end

    trait :visible do
      visible_by_members { true }
    end

    trait :invisible do
      visible_by_members { false }
    end
  end
end
