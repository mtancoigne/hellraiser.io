FactoryBot.define do
  factory :subscription do
    cenobite
    # Default
    on_motion

    trait :on_motion do
      entity factory: [:motion], strategy: :create
    end
  end
end
