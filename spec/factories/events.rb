FactoryBot.define do
  factory :event do
    name { Faker::Lorem.sentence }
    description { Faker::Markdown.sandwich }
    cenobite
    future
    starts_at_timezone { Time.current.zone }
    ends_at_timezone { Time.current.zone }
    visible_by_members

    trait(:visible_by_owner) do
      visibility { 0 }
    end

    trait(:visible_by_members) do
      visibility { 1 }
    end

    trait :future do
      starts_at { 2.hours.from_now }
      ends_at { 3.hours.from_now }
    end

    trait :over do
      after :create do |record|
        # rubocop:disable Rails/SkipsModelValidations
        # Validation won't allow us to do that
        record.update_columns starts_at: 2.hours.ago, ends_at: 1.hour.ago
        # rubocop:enable Rails/SkipsModelValidations
      end
    end

    trait :started do
      after :create do |record|
        # rubocop:disable Rails/SkipsModelValidations
        # Validation won't allow us to do that
        record.update_columns starts_at: 1.hour.ago, ends_at: 2.hours.from_now
        # rubocop:enable Rails/SkipsModelValidations
      end
    end

    trait :with_attendee do
      after :create do |record|
        create(:events_attendance, event_id: record.id)
      end
    end
  end
end
