FactoryBot.define do
  factory :rss_item do
    title { Faker::Lorem.sentence }
    link { Faker::Internet.unique.url }
    description { Faker::Lorem.paragraph }
    published_at { Faker::Date.backward(days: 60) }
    rss_feed
  end
end
