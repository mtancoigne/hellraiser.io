FactoryBot.define do
  factory :vote do
    approve { false }
    cenobite
    # Default
    on_motion

    trait :on_motion do
      votable factory: [:motion], strategy: :create
    end
  end
end
