FactoryBot.define do
  factory :paste do
    name { Faker::Alphanumeric.alphanumeric number: 8 }
    content { Faker::Lorem.paragraphs(number: 3).join("\n") }
    cenobite

    trait :burnable do
      burn { true }
    end

    trait :protected do
      password { 'password' }
    end

    trait :code do
      content do
        <<~RUBY
          puts 'Hello world'
        RUBY
      end
      extension { 'rb' }
    end
  end
end
