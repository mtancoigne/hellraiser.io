FactoryBot.define do
  factory :chat_participant do
    chat_room
    cenobite
    name { Faker::Internet.username }
  end
end
