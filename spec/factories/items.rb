FactoryBot.define do
  factory :item do
    name { Faker::Books::CultureSeries.culture_ship }
    owner factory: [:cenobite]

    trait :borrowed do
      borrower { create(:acquaintance, :accepted, inviter_id: owner_id).invitee }
    end
  end
end
