require 'rails_helper'

RSpec.describe Cenobite, type: :model do
  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:invitee) { described_class.invite!({ email: Faker::Internet.email }, cenobite) }

  context 'when accepting an invite' do
    context 'when there is a pending acquaintance request' do
      context 'when the acquaintance request comes from the inviter' do
        it 'accepts the acquaintance' do
          acquaintance = Acquaintance.create! inviter: cenobite, invitee: invitee
          invitee.accept_invitation!
          acquaintance.reload
          expect(acquaintance).to be_accepted
        end
      end

      context 'when the acquaintance request comes from from someone else' do
        it 'does not accepts the acquaintance' do
          other_cenobite = FactoryBot.create(:cenobite, :active)
          acquaintance = Acquaintance.create! inviter: other_cenobite, invitee: invitee
          invitee.accept_invitation!
          acquaintance.reload
          expect(acquaintance).not_to be_accepted
        end
      end
    end
  end
end
