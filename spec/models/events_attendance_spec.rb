require 'rails_helper'

RSpec.describe EventsAttendance, type: :model do
  describe '.check_visibility' do
    context 'when event is private' do
      let(:event) { FactoryBot.create(:event, :visible_by_owner) }

      it 'allows creator to join' do
        attendance = described_class.new event: event, cenobite: event.cenobite

        expect(attendance).to be_valid
      end

      it 'does not allow members to join' do
        cenobite   = FactoryBot.create(:cenobite)
        attendance = described_class.new event: event, cenobite: cenobite

        expect(attendance).not_to be_valid
      end
    end

    context 'when event is visible by members' do
      let(:event) { FactoryBot.create(:event, :visible_by_members) }

      it 'allows creator to join' do
        attendance = described_class.new event: event, cenobite: event.cenobite

        expect(attendance).to be_valid
      end

      it 'allows members to join' do
        cenobite   = FactoryBot.create(:cenobite)
        attendance = described_class.new event: event, cenobite: cenobite

        expect(attendance).to be_valid
      end
    end
  end
end
