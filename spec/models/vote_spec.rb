require 'rails_helper'

RSpec.describe Vote, type: :model do
  let(:votable) { FactoryBot.create(:motion) }
  let(:cenobite) { FactoryBot.create(:cenobite, :active) }

  it "can't be voted more than once by a cenobite" do
    described_class.create approve: true, votable: votable, cenobite: cenobite

    expect do
      described_class.create! approve: true, votable: votable, cenobite: cenobite
    end.to raise_error ActiveRecord::RecordInvalid
  end
end
