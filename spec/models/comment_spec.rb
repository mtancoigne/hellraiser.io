require 'rails_helper'

RSpec.describe Comment, type: :model do
  include ActiveJob::TestHelper

  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:owned_motion) { FactoryBot.create(:motion, cenobite: cenobite) }
  let(:other_motion) { FactoryBot.create(:motion) }

  before do
    owned_motion
    other_motion
    # We get 3 subscribers (2 + author)
    FactoryBot.create_list(:subscription, 2, entity: other_motion)
  end

  context 'when owner is commenter' do
    it 'does not send email to owner' do
      expect do
        perform_enqueued_jobs do
          FactoryBot.create(:comment, commentable: owned_motion, cenobite: cenobite)
        end
      end.not_to change(ActionMailer::Base.deliveries, :count)
    end
  end

  context 'when owner is not commenter' do
    it 'sends emails to subscribers' do
      expect do
        perform_enqueued_jobs do
          FactoryBot.create(:comment, commentable: other_motion, cenobite: cenobite)
        end
      end.to change(ActionMailer::Base.deliveries, :count).by(3)
    end
  end
end
