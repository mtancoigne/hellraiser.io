require 'rails_helper'

RSpec.describe DeadManSwitch, type: :model do
  before do
    FactoryBot.create(:dead_man_switch, name: 'Active')
    FactoryBot.create(:dead_man_switch, :expired, name: 'Expired')
    FactoryBot.create(:dead_man_switch, :sent, name: 'Sent')
  end

  it 'filters unsent switches' do
    expect(described_class.unsent.count).to eq 2
  end

  it 'filters expired switches' do
    expect(described_class.expired.count).to eq 2
  end

  it 'returns chains expired and unsent scopes' do
    expect(described_class.expired.unsent.count).to eq 1
  end

  it 'returns the right expired and unsent element' do
    expect(described_class.expired.unsent.first.name).to eq 'Expired'
  end

  it 'filters pingable switches' do
    expect(described_class.pingable.count).to eq 1
  end
end
