require 'rails_helper'

RSpec.describe Motion, type: :model do
  include ActiveJob::TestHelper

  let(:motion) { FactoryBot.create(:motion) }
  let(:vote) do
    FactoryBot.create_list(:vote, 3, approve: true, votable: motion)
    FactoryBot.create_list(:vote, 1, approve: false, votable: motion)
  end

  it 'has a vote count' do
    vote
    expect(motion.score).to eq 3
  end

  it 'has a counter for total votes' do
    vote
    expect(motion.votes_count).to eq 4
  end

  context 'when vote is destroyed' do
    it 'updates counter' do
      vote
      motion.votes.first.destroy
      expect(motion.votes_count).to eq 3
    end

    it 'updates score' do
      vote
      motion.votes.first.destroy
      expect(motion.score).to eq 2
    end
  end

  context 'when create a motion' do
    let(:cenobites) do
      cenobites = FactoryBot.create_list(:cenobite, 3, :active)
      cenobites.second.update notify_new_motion: false
      cenobites
    end

    it 'sends emails people who subscribes' do
      expect do
        perform_enqueued_jobs do
          FactoryBot.create(:motion, cenobite: cenobites.first)
        end
      end.to change(ActionMailer::Base.deliveries, :count).by(2)
    end
  end

  context 'when creating motion' do
    it 'subscribes the auhtor' do
      motion
      expect(motion).to be_subscribed(motion.cenobite)
    end
  end
end
