require 'rails_helper'

RSpec.describe Link, type: :model do
  let(:link) { FactoryBot.create(:link) }

  it 'is not valid with unsupported scheme' do
    link.url = 'tel:+33678901234'
    expect(link).not_to be_valid
  end

  it 'is not valid without scheme' do
    link.url = 'invalid.url'
    expect(link).not_to be_valid
  end

  it 'is valid with a supported scheme' do
    link.url = 'https://hellraiser.io'
    expect(link).to be_valid
  end

  it 'is valid with an invalid url' do
    link.url = 'https://domain.@invalid'
    expect(link).to be_valid
  end
end
