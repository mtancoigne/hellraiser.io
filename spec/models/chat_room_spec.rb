require 'rails_helper'

RSpec.describe ChatRoom, type: :model do
  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:other_cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:acquaintance) { FactoryBot.create(:acquaintance, inviter: cenobite, invitee: other_cenobite) }

  context 'with a created acquaintance' do
    it 'selects the right rooms for given cenobite' do
      FactoryBot.create(:chat_room)
      acquaintance.accept!
      expect(described_class.accessible_by(cenobite).count).to eq 2
    end
  end

  context 'with an accepted acquaintance' do
    it 'selects the right rooms for given cenobite' do
      FactoryBot.create(:chat_room)
      acquaintance.accept!
      expect(described_class.accessible_by(other_cenobite).count).to eq 2
    end
  end
end
