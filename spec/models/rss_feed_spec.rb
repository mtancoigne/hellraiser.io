require 'rails_helper'

require 'hell_raiser/rss'

RSpec.describe RssFeed, type: :model do
  describe '.complete_channel' do
    context 'with an Atom feed' do
      it 'fills data' do # rubocop:disable RSpec/ExampleLength
        atom_feed = described_class.new url: 'https://linuxfr.org/news.atom'
        VCR.use_cassette 'rss_feed/get_atom_feed' do
          atom_feed.send :complete_channel
          expect(atom_feed.attributes).to include({
                                                    'description' => nil,
                                                    'language'    => nil,
                                                    'link'        => 'https://linuxfr.org/news',
                                                    'title'       => 'LinuxFr.org : les dépêches',
                                                    'url'         => 'https://linuxfr.org/news.atom',
                                                  })
        end
      end
    end

    context 'with a RSS feed' do
      it 'fills data' do # rubocop:disable RSpec/ExampleLength
        rss_feed = described_class.new url: 'https://korben.info/feed'
        VCR.use_cassette 'rss_feed/get_rss_feed' do
          rss_feed.send :complete_channel
          expect(rss_feed.attributes).to include({
                                                   'description' => 'Upgrade your mind',
                                                   'language'    => 'fr-FR',
                                                   'link'        => 'https://korben.info',
                                                   'title'       => 'Korben',
                                                 })
        end
      end
    end
  end
end
