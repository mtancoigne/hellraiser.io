require 'rails_helper'

RSpec.describe Item, type: :model do
  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:acquaintance) { FactoryBot.create(:acquaintance, :accepted, inviter: cenobite) }
  let(:acquaintance_pending) { FactoryBot.create(:acquaintance, invitee: cenobite) }

  let(:item) { FactoryBot.create(:item, owner: cenobite) }

  it "can't be borrowed by an unknown cenobite" do
    item.borrower = FactoryBot.create :cenobite, :active

    expect do
      item.save!
    end.to raise_error ActiveRecord::RecordInvalid
  end

  it "can't be borrowed in an unvalidated acquaintance" do
    item.borrower = acquaintance_pending.inviter

    expect do
      item.save!
    end.to raise_error ActiveRecord::RecordInvalid
  end

  it "can't be borrowed by the owner" do
    item.borrower = item.owner

    expect do
      item.save!
    end.to raise_error ActiveRecord::RecordInvalid
  end

  it 'can be borrowed by an accepted acquaintance' do
    item.borrower = acquaintance.invitee

    expect do
      item.save!
    end.not_to raise_error
  end
end
