require 'rails_helper'

RSpec.describe Like, type: :model do
  let(:likable) { FactoryBot.create(:link) }
  let(:cenobite) { FactoryBot.create(:cenobite, :active) }

  it "can't be liked more than once by a cenobite" do
    described_class.create likable: likable, cenobite: cenobite

    expect do
      described_class.create! likable: likable, cenobite: cenobite
    end.to raise_error ActiveRecord::RecordInvalid
  end
end
