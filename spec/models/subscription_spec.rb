require 'rails_helper'

RSpec.describe Subscription, type: :model do
  let(:motion) { FactoryBot.create(:motion) }
  let(:cenobite) { FactoryBot.create(:cenobite, :active) }

  it "can't subscribe more than once to something" do
    described_class.create! entity: motion, cenobite: cenobite

    expect do
      described_class.create! entity: motion, cenobite: cenobite
    end.to raise_error ActiveRecord::RecordInvalid
  end
end
