require 'rails_helper'

RSpec.describe Acquaintance, type: :model do
  include ActiveJob::TestHelper

  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:other_cenobite) { FactoryBot.create(:cenobite, :active) }

  it "can't be acquainted to itself" do
    acquaintance = described_class.new inviter: cenobite, invitee: cenobite
    expect(acquaintance).not_to be_valid
  end

  it "can't be acquainted more than one to the same invitee" do
    described_class.create inviter: cenobite, invitee: other_cenobite
    acquaintance = described_class.new inviter: cenobite, invitee: other_cenobite
    expect(acquaintance).not_to be_valid
  end

  it "can't create an inverted pair of cenobites" do
    described_class.create inviter: cenobite, invitee: other_cenobite
    acquaintance = described_class.new inviter: other_cenobite, invitee: cenobite
    expect(acquaintance).not_to be_valid
  end

  context 'when creating an acquaintance request' do
    context 'when target cenobite is not yet registered' do
      let(:inviter) { FactoryBot.create(:cenobite, :active) }
      let(:invitee) { Cenobite.invite!({ email: Faker::Internet.email }, cenobite) }

      before do
        inviter
        invitee
      end

      context 'when sender is inviter' do
        it 'does not send notification emails' do
          expect do
            perform_enqueued_jobs do
              FactoryBot.create(:acquaintance, inviter: inviter, invitee: invitee)
            end
          end.not_to change(ActionMailer::Base.deliveries, :count)
        end
      end

      context 'when sender is someone else' do
        it 'does not send notification emails' do
          expect do
            perform_enqueued_jobs do
              FactoryBot.create(:acquaintance, inviter: other_cenobite, invitee: invitee)
            end
          end.not_to change(ActionMailer::Base.deliveries, :count)
        end
      end
    end

    context 'when target cenobite is already registered and confirmed' do
      let(:inviter) { FactoryBot.create(:cenobite, :active) }
      let(:invitee) { FactoryBot.create(:cenobite, :active) }

      context 'when creating the request based on an email' do
        it 'sends notification emails' do
          expect do
            perform_enqueued_jobs do
              FactoryBot.create(:acquaintance, inviter: inviter, invitee: invitee, invite_method: :email)
            end
          end.to change(ActionMailer::Base.deliveries, :count).by(1)
        end
      end

      context 'when creating the request based on an ID' do
        context 'when inviter is not visible by members' do # rubocop:disable RSpec/NestedGroups
          it 'fails' do
            inviter      = FactoryBot.create(:cenobite, :active, :invisible)
            acquaintance = described_class.new inviter: inviter, invitee: invitee, invite_method: :id
            expect(acquaintance).not_to be_valid
          end
        end

        context 'when invitee is not visible by members' do # rubocop:disable RSpec/NestedGroups
          it 'fails' do
            invitee      = FactoryBot.create(:cenobite, :active, :invisible)
            acquaintance = described_class.new inviter: inviter, invitee: invitee, invite_method: :id
            expect(acquaintance).not_to be_valid
          end
        end

        context 'when both inviter and invitee are visible by members' do # rubocop:disable RSpec/NestedGroups
          it 'succeeds' do
            inviter      = FactoryBot.create(:cenobite, :active, :visible)
            invitee      = FactoryBot.create(:cenobite, :active, :visible)
            acquaintance = described_class.new inviter: inviter, invitee: invitee, invite_method: :id
            expect(acquaintance).to be_valid
          end
        end
      end
    end
  end

  context 'when accepting an acquaintance request' do
    it 'creates a private chat room' do
      acquaintance = FactoryBot.create(:acquaintance, inviter: cenobite, invitee: other_cenobite)
      expect do
        acquaintance.accept!
      end.to change(ChatRoom, :count).by 1
    end
  end
end
