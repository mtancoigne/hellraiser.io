require 'rails_helper'

RSpec.describe Event, type: :model do
  it 'cannot start in the past' do
    event = FactoryBot.build(:event, starts_at: 1.hour.ago)
    event.validate
    expect(event.errors[:starts_at].count).to eq 1
  end

  it 'cannot end before it starts' do
    event = FactoryBot.build(:event, starts_at: 10.hours.from_now, ends_at: 5.hours.from_now)
    event.validate
    expect(event.errors[:ends_at].count).to eq 1
  end

  it 'cannot end when it starts' do
    time  = 1.hour.from_now
    event = FactoryBot.build(:event, starts_at: time, ends_at: time)
    event.validate
    expect(event.errors[:ends_at].count).to eq 1
  end

  describe 'Timezone validation' do
    context 'when timezone is invalid' do
      it 'does not validate' do
        event = described_class.new starts_at_timezone: 'Bad timezone'
        event.validate

        expect(event.errors[:starts_at_timezone].count).to eq 1
      end

      it 'has an error set' do
        event = described_class.new starts_at_timezone: 'Bad timezone'
        event.validate

        expect(event.errors[:starts_at_timezone].first).to eq 'is not included in the list'
      end
    end

    context 'when timezone is valid' do
      it 'validates' do
        event = described_class.new starts_at_timezone: 'Europe/Berlin'
        event.validate

        expect(event.errors[:starts_at_timezone].count).to eq 0
      end
    end
  end

  describe 'scopes' do
    before do
      FactoryBot.create_list(:event, 2, :visible_by_members)
      FactoryBot.create_list(:event, 3, :visible_by_members, :over)
      FactoryBot.create_list(:event, 4, :visible_by_members, :started)
    end

    describe '->over' do
      it 'filters elements' do
        expect(described_class.over.count).to eq 3
      end
    end

    describe '->not_over' do
      it 'filters elements' do
        expect(described_class.not_over.count).to eq 6
      end
    end

    describe '->coming' do
      it 'filters elements' do
        expect(described_class.coming.count).to eq 2
      end
    end

    describe '->started' do
      it 'filters elements' do
        expect(described_class.started.count).to eq 4
      end
    end
  end

  describe '.over?' do
    context 'when event is over' do
      it 'returns true' do
        event = FactoryBot.create(:event, :over)

        expect(event).to be_over
      end
    end

    context 'when event is started' do
      it 'returns false' do
        event = FactoryBot.create(:event, :started)

        expect(event).not_to be_over
      end
    end

    context 'when event is all in the future' do
      it 'returns false' do
        event = FactoryBot.create(:event, :future)

        expect(event).not_to be_over
      end
    end
  end

  describe '.started?' do
    context 'when event is over' do
      it 'returns false' do
        event = FactoryBot.create(:event, :over)

        expect(event).not_to be_started
      end
    end

    context 'when event is started' do
      it 'returns true' do
        event = FactoryBot.create(:event, :started)

        expect(event).to be_started
      end
    end

    context 'when event is all in the future' do
      it 'returns false' do
        event = FactoryBot.create(:event, :future)

        expect(event).not_to be_started
      end
    end
  end

  describe '.not_started_yet??' do
    context 'when event is over' do
      it 'returns false' do
        event = FactoryBot.create(:event, :over)

        expect(event).not_to be_not_started_yet
      end
    end

    context 'when event is started' do
      it 'returns false' do
        event = FactoryBot.create(:event, :started)

        expect(event).not_to be_not_started_yet
      end
    end

    context 'when event is all in the future' do
      it 'returns true' do
        event = FactoryBot.create(:event, :future)

        expect(event).to be_not_started_yet
      end
    end
  end

  describe '.add_attendee!' do
    let(:event) { FactoryBot.create(:event, :visible_by_members) }
    let(:cenobite) { FactoryBot.create(:cenobite, :active) }

    context 'when cenobite has not attended before' do
      it 'creates the attendance' do
        expect do
          event.add_attendee! cenobite
        end.to change(EventsAttendance, :count).by 1
      end

      it 'returns an Attendance' do
        expect(event.add_attendee!(cenobite)).to be_an EventsAttendance
      end
    end

    context 'when cenobite has attended before' do
      before do
        event.add_attendee! cenobite
      end

      it 'does not create the attendance' do
        expect do
          event.add_attendee! cenobite
        end.not_to change(EventsAttendance, :count)
      end

      it 'returns an Attendance' do
        expect(event.add_attendee!(cenobite)).to be_an EventsAttendance
      end
    end
  end

  describe '.remove_attendee!' do
    let(:event) { FactoryBot.create(:event, :visible_by_members) }
    let(:cenobite) { FactoryBot.create(:cenobite, :active) }

    context 'when cenobite has not attended before' do
      it 'does not remove the attendance' do
        expect do
          event.remove_attendee! cenobite
        end.to raise_error ActiveRecord::RecordNotFound
      end
    end

    context 'when cenobite has attended before' do
      before do
        event.add_attendee! cenobite
      end

      it 'removes the attendance' do
        expect do
          event.remove_attendee! cenobite
        end.to change(EventsAttendance, :count).by(-1)
      end
    end
  end
end
