require 'rails_helper'

RSpec.describe ChatRoomChannel, type: :channel do
  let(:cenobite) { FactoryBot.create(:cenobite, :active) }
  let(:public_chat_room) { FactoryBot.create(:chat_room) }
  let(:private_chat_room) { FactoryBot.create(:acquaintance, :accepted).chat_room }
  let(:accessible_private_chat_room) { FactoryBot.create(:acquaintance, :accepted, inviter: cenobite).chat_room }

  before do
    stub_connection current_cenobite: cenobite
  end

  context 'when room is accessible' do
    it 'accepts subscription to public room' do
      subscribe(id: public_chat_room.id)
      expect(subscription).to be_confirmed
    end

    it 'accepts subscription to accessible private room' do
      subscribe(id: accessible_private_chat_room.id)
      expect(subscription).to be_confirmed
    end
  end

  context 'when room is inaccessible' do
    it 'rejects subscription when no room id' do
      subscribe
      expect(subscription).to be_rejected
    end

    it 'rejects subscription when room is forbidden' do
      subscribe(id: private_chat_room.id)
      expect(subscription).to be_rejected
    end
  end
end
