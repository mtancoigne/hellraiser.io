module DropzoneHelper
  # Pack "drop_zone" must have been loaded before
  def dropzone_js(zone_id:, notice_id:, field_id:, allowed_mimes: [])
    mimes   = allowed_mimes.map { |m| "'#{m}'" }.join(', ').html_safe # rubocop:disable Rails/OutputSafety
    js      = <<~JS
      new DropZone($_('#{zone_id}'),
      $_('#{notice_id}'),
      $_('#{field_id}'),
      {
        allowedMimes: [#{mimes}],
      })
    JS

    tag.script js.html_safe # rubocop:disable Rails/OutputSafety
  end
end
