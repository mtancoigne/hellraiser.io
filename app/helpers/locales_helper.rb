module LocalesHelper
  def locales_options
    [
      [I18n.t('generic.locales.english'), :en],
      [I18n.t('generic.locales.french'), :fr],
    ]
  end
end
