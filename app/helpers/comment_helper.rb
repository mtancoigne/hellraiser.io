module CommentHelper
  def new_comment
    Comment.new
  end

  def new_comment_url(entity)
    send :"member_#{entity.class.to_s.underscore}_comments_path", entity
  end
end
