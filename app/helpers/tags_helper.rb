module TagsHelper
  def tag_list(tags, css_class: 'tag', html_tag: 'span', separator: ', ')
    elements = []
    tags.each do |tag|
      elements.push content_tag(html_tag, tag, class: css_class)
    end

    elements.join(separator).html_safe # rubocop:disable Rails/OutputSafety
  end

  def links_tags_counts
    Link.tag_counts_on(:tags).order('tags.name' => :asc)
  end

  def rss_feeds_tags_counts
    RssFeed.tag_counts_on(:tags).order('tags.name' => :desc)
  end
end
