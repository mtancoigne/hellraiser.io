module CodeMirrorHelper
  # Pack "codemirror" must have been loaded before
  # @param {string} instance - Creates a variable named `instance` for later manipulation
  def cm_js(id, extension, show_lines: false, line_wrap: true, instance: nil)
    options = { lineNumbers: show_lines, lineWrapping: line_wrap }

    # rubocop:disable Rails/OutputSafety
    js = <<~JS
      document.addEventListener('DOMContentLoaded', () => {
        #{instance ? "#{instance} = " : ''}codeMirrorArea('#{id}', '#{extension}', #{raw(options.to_json)})
      })
    JS
    # rubocop:enable Rails/OutputSafety

    tag.script js.html_safe # rubocop:disable Rails/OutputSafety
  end
end
