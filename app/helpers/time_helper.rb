module TimeHelper
  def same_day?(previous_date_time, current_date_time)
    return false if previous_date_time.nil?

    date1 = Date.parse(previous_date_time.to_s)
    date2 = Date.parse(current_date_time.to_s)

    date1 == date2
  end

  def date_time_to_date(time)
    # i18n-tasks-use t('time.formats.date_only')
    l time, format: :date_only
  end

  def date_time_to_time(time)
    # i18n-tasks-use t('time.formats.time_only')
    l time, format: :time_only
  end

  def short_date(date)
    return nil if date.nil?

    I18n.l date.to_date, format: :default
  end

  def medium_datetime(date)
    return nil if date.nil?

    # i18n-tasks-use t('time.formats.medium')
    I18n.l date, format: :medium
  end
end
