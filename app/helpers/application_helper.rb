require 'hell_raiser/cenobite'

module ApplicationHelper
  def cenobite_name(entity, parent_id: nil, html_tag: :span, anonymous: true)
    color = HellRaiser::Cenobite.colors(entity, parent_id: parent_id)
    color[:name] = anonymous ? nil : entity.cenobite.name

    return cenobite_name_tag(html_tag, color) if html_tag

    color
  end

  def owner?(entity, cenobite = nil)
    cenobite ||= current_cenobite

    entity.cenobite_id == cenobite&.id
  end

  def entry_age_class(date, additional: '')
    css_class = if date > 3.days.ago
                  'fresh'
                elsif date > 2.weeks.ago
                  'stale'
                else
                  'dead'
                end
    "#{css_class} #{additional}"
  end

  def string_ellipsis(string, max, ellipsis: '…')
    string.size > max ? "#{string.first(max)}#{ellipsis}" : string
  end

  private

  def cenobite_name_tag(html_tag, color)
    content_tag html_tag,
                color[:name] || "##{color[:hex]}",
                class: 'cenobite-color',
                style: "background-color: rgb(#{color[:bg].join(', ')});color: rgb(#{color[:fg].join(', ')})"
  end

  def yes_no(value)
    return t('generic.yes') if value

    t('generic.no')
  end

  def same_cenobite?(entity_id)
    current_cenobite.id == entity_id
  end
end
