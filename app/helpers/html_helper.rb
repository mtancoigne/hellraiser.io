module HtmlHelper
  def external_link_to(name = nil, options = nil, html_options = {}, &)
    html_options.merge target: '_blank', rel: 'noreferrer noopener'

    link_to(name, options, html_options, &)
  end

  def add_modal(content, id:, variant:, title: nil)
    css_class = variant ? "modal--#{variant}" : ''
    content_for :modals, render('layouts/modal', content: content, id: id, title: title, css_class: css_class)
  end

  def show_modal_button(text, id:)
    tag.button text, type: 'button', onclick: "openModal('#{id}')"
  end

  def nbsp
    [160].pack('U*')
  end
end
