module VoteHelper
  def color_score(entity, html_tag: :span, show_zeroes: false)
    data      = { score_id: entity.id }
    content   = "#{entity.score}/#{entity.votes_count}"
    css_class = show_zeroes ? '' : ' score--hidden'

    return content_tag(html_tag, content, class: "score#{css_class}", data: data) if entity.votes_count.zero?

    middle = entity.score / entity.votes_count.to_f

    css_class = if middle > 0.5
                  'positive'
                elsif middle < 0.5
                  'negative'
                end

    content_tag(html_tag, content, class: "score score--#{css_class}", data: data)
  end

  def upvote_link(entity, string: nil, css_class: '')
    raise "Entity #{entity.class} is not votable" unless entity.respond_to? :vote_from

    vote = entity.vote_from current_cenobite
    vote_button entity, vote&.approve == true, :upvote, string, css_class: css_class
  end

  def downvote_link(entity, string: nil, css_class: '')
    raise "Entity #{entity.class} is not votable" unless entity.respond_to? :vote_from

    vote = entity.vote_from current_cenobite
    vote_button entity, vote&.approve == false, :downvote, string, css_class: css_class
  end

  private

  def vote_button(entity, disabled, type, string, css_class: '')
    path_method = :"#{type}_member_#{entity.class.to_s.underscore}_path"
    raise "We can't find a route named #{path_method}" unless respond_to? path_method

    # i18n-tasks-use t('generic.downvote')
    # i18n-tasks-use t('generic.upvote')
    string ||= t("generic.#{type}")

    button_to string,
              send(path_method, entity),
              method:   :put,
              remote:   true,
              data:     { toggle_type: type, toggle_id: entity.id },
              role:     'button',
              class:    css_class,
              disabled: disabled
  end
end
