module FavoritesHelper
  def toggle_favorite_link(entity)
    raise "Entity #{entity.class} is not likable" unless entity.respond_to? :liked?

    liked = entity.liked?(current_cenobite)

    path_method = :"toggle_like_member_#{entity.class.to_s.underscore}_path"
    raise "We can't find a route named #{path_method}" unless respond_to? path_method

    button_to liked ? t('generic.unlike') : t('generic.like'),
              send(path_method, entity),
              method: :put,
              remote: true,
              data:   { toggle_type: :favorite },
              id:     "fav-button-#{entity.id}",
              role:   'button',
              class:  liked ? 'favorite' : 'not-favorite'
  end
end
