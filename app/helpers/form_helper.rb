module FormHelper
  def markdown_help_notice(html_tag: 'div')
    link = external_link_to t('generic.markdown_help_link'), 'https://commonmark.org/help/'
    tag.send html_tag, t('generic.markdown_help_html', link: link)
  end
end
