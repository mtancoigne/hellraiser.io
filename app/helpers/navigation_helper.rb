module NavigationHelper
  def sort_link(title, field, link_options: {}, keep_params: [])
    direction   = params['order_by']&.to_sym == field && params['direction'] == 'desc' ? 'asc' : 'desc'
    link_params = extract_params(keep_params).merge(order_by: field, direction: direction)

    link_to link_params, link_options do
      if params['order_by']&.to_sym == field
        "#{direction == 'asc' ? '▼' : '▲'}#{nbsp}#{title}"
      else
        "·#{nbsp}#{title}"
      end
    end
  end

  def filter_link(title, filter, filter_key: 'filter_by', link_options: {}, keep_params: [])
    link_params = extract_params(keep_params).merge(filter_key => filter)
    link_options[:active] = :exact

    active_link_to title, link_params, link_options
  end

  private

  def extract_params(keys_to_keep)
    parameters = {}
    keys_to_keep.each do |option|
      parameters[option] = params[option] if params[option]
    end

    parameters
  end
end
