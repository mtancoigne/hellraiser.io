module ThemesHelper
  def themes_options
    [
      [I18n.t('generic.themes.default'), nil],
      [I18n.t('generic.themes.dark'), :dark],
      [I18n.t('generic.themes.light'), :light],
      [I18n.t('generic.themes.clean_dark'), :clean_dark],
      [I18n.t('generic.themes.clean_light'), :clean_light],
    ]
  end

  def application_theme
    theme = if current_cenobite&.theme.present?
              current_cenobite.theme
            else
              :dark
            end
    "theme_#{theme}"
  end
end
