module VisibilityHelper
  # i18n-tasks-use t('generic.visibility.private')
  # i18n-tasks-use t('generic.visibility.members')
  # i18n-tasks-use t('generic.visibility.public')
  STRINGS = {
    0 => 'generic.visibility.private',
    1 => 'generic.visibility.members',
    2 => 'generic.visibility.public',
  }.freeze

  def visibility_options(except: [])
    list = []
    STRINGS.keys
           .reject { |key| except.include? key }
           .each { |key| list.push [I18n.t(STRINGS[key]), key] }

    list
  end

  def visibility(level)
    I18n.t(STRINGS[level])
  end
end
