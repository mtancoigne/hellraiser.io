module AcquaintancesHelper
  def acquaintance_select(entity)
    collection_select entity.class.name.underscore,
                      :borrower_id,
                      current_cenobite.known_cenobites,
                      :id,
                      :name,
                      { include_blank: '' }
  end
end
