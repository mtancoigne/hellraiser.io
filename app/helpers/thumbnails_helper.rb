module ThumbnailsHelper
  PICTURE_SIZES = {
    default: 960,
    large:   1920,
  }.freeze

  def image_resize_properties(size = :default)
    limit = PICTURE_SIZES[size] || PICTURE_SIZES[:default]
    { resize_to_limit: [limit, limit] }
  end

  def file_urls(attachment)
    if attachment.previewable?
      preview_urls attachment
    elsif attachment.variable?
      variant_urls attachment
    else
      # Specify ActiveStorage host, as we don't have this information when using
      # this method from a view rendered in an ActionCable channel
      url = [Rails.configuration.hellraiser.default_url_options[:host], Rails.configuration.hellraiser.default_url_options[:port]].join(':')
      ActiveStorage::Current.set(host: url) do
        { file: attachment.url }
      end
    end
  end

  def attachment_attributes(attachment)
    if attachment.attached?
      file = {
        content_type: attachment.blob.content_type,
        metadata:     attachment.blob.metadata,
        file_name:    attachment.blob.filename,
        byte_size:    attachment.blob.byte_size,
      }

      file[:urls] = file_urls(attachment)

      return file
    end

    nil
  end

  def variant_urls(attachment)
    generate_images_urls attachment, :variant
  end

  def preview_urls(attachment)
    generate_images_urls attachment, :preview
  end

  def picture_url(attachment, size)
    return url_for(attachment) if size == :full

    url_for(attachment.variant(image_resize_properties(size)).processed)
  end

  def generate_images_urls(attachment, method) # rubocop:disable Metrics/AbcSize
    urls = {}

    # Specify ActiveStorage host, as we don't have this information when using
    # this method from a view rendered in an ActionCable channel
    url = [Rails.configuration.hellraiser.default_url_options[:host], Rails.configuration.hellraiser.default_url_options[:port]].join(':')
    ActiveStorage::Current.set(host: url) do
      PICTURE_SIZES.each_key { |size| urls[size] = attachment.send(method, image_resize_properties(size)).processed.url }
      urls[:file] = attachment.url
    end

    urls
  end
end
