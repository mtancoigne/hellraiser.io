module RssItemHelper
  def sanitize_content(string)
    sanitize string, tags: %w[strong em p ul li br]
  end
end
