module UrlHelper
  def chat_controller?
    controller_name == 'chat'
  end
end
