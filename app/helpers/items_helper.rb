module ItemsHelper
  def item_categories_datalist(id:)
    options = ''
    Item.distinct.pluck(:category).each do |category|
      options += tag.option category
    end

    tag.datalist options, id: id
  end
end
