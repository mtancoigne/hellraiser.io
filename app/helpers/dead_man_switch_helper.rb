module DeadManSwitchHelper
  def ping_frequency_options
    [
      [t('dead_man_switch_helper.ping_frequency_options.1h'), 1],
      [t('dead_man_switch_helper.ping_frequency_options.6h'), 6],
      [t('dead_man_switch_helper.ping_frequency_options.12h'), 12],
      [t('dead_man_switch_helper.ping_frequency_options.24h'), 24],
      [t('dead_man_switch_helper.ping_frequency_options.48h'), 48],
      [t('dead_man_switch_helper.ping_frequency_options.1w'), 24 * 7],
      [t('dead_man_switch_helper.ping_frequency_options.2w'), 24 * 14],
      [t('dead_man_switch_helper.ping_frequency_options.1m'), 24 * 31],
      [t('dead_man_switch_helper.ping_frequency_options.2m'), 24 * 61],
    ]
  end
end
