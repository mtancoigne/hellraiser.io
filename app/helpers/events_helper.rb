module EventsHelper
  def event_css_class(event)
    if event.started?
      'element--event--started'
    elsif event.over?
      'element--event--over'
    end
  end
end
