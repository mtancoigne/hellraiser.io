class PastesController < ApplicationController
  before_action :set_paste, only: [:show, :reveal]

  # GET /pastes/1
  # GET /pastes/1.json
  def show
    # Don't scope query
    @paste = Paste.find(params[:id])
  end

  # GET /pastes/1/reveal.json
  # PUT /pastes/1/reveal.json
  # PATCH /pastes/1/reveal.json
  def reveal
    @paste = Paste.find(params[:id])

    if @paste.password_digest.present?
      paste_params = params.require(:paste).permit(:password)
      return render json: { errors: { password: ['incorrect'] } }, status: :unprocessable_entity unless @paste.authenticate_password(paste_params[:password])
    end

    @paste.destroy if @paste.burn

    render json: { content: @paste.content }, status: :ok
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_paste
    @paste = policy_scope(Paste).find(params[:id])

    authorize @paste
  end
end
