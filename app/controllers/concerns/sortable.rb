# Add a SORTABLE_FIELD constant in your controller, listing all fields allowed to
# order.
# In action allowing ordering results, use `sort_query()` to get a sorting order
# hash.
# Then, merge it to your custom hashes or use it "as is".
# Ex:
#     @items = @items.sort(sort_query(:name, :asc))
module Sortable
  extend ActiveSupport::Concern

  SORTABLE_FIELDS = [].freeze

  private

  def sort_query(default_field, default_direction)
    raise 'SORTABLE_FIELDS is empty or not defined in controller' unless self.class::SORTABLE_FIELDS.count.positive?

    if sort?
      direction = params[:direction] if [:asc, :desc].include? params[:direction].to_sym
      field     = params[:order_by]
    end

    field     ||= default_field
    direction ||= default_direction

    { field => direction }
  end

  def sort?
    params[:order_by] && self.class::SORTABLE_FIELDS.include?(params[:order_by].to_sym)
  end
end
