# This concern expects the controller to have a VOTABLE_FOR constant with
# the name of the variable used in actions (e.g.: :post), and an instance
# variable setter named on the same var (e.g.: set_post)
module Votable
  extend ActiveSupport::Concern

  included do
    before_action :set_votable_entity, only: [:upvote, :downvote]
  end

  def upvote
    respond_to :json
    entity = instance_variable_get(:"@#{self.class::VOTABLE_FOR}")

    if entity.vote!(current_cenobite, true)
      render json: { score: entity.score, votes_count: entity.votes_count, vote: :upvote }, status: :ok
    else
      render json: entity.errors, status: :unprocessable_entity
    end
  end

  def downvote
    respond_to :json
    entity = instance_variable_get(:"@#{self.class::VOTABLE_FOR}")

    if entity.vote!(current_cenobite, false)
      render json: { score: entity.score, votes_count: entity.votes_count, vote: :downvote }, status: :ok
    else
      render json: entity.errors, status: :unprocessable_entity
    end
  end

  private

  def set_votable_entity
    setter = :"set_#{self.class::VOTABLE_FOR}"
    raise "Controller don't have the #{setter} method" unless private_methods.include? setter

    send setter
  end
end
