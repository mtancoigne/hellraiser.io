# This concern expects the controller to have a FAVORITABLE_FOR constant with
# the name of the variable used in actions (e.g.: :post), and an instance
# variable setter named on the same var (e.g.: set_post)
module Favoritable
  extend ActiveSupport::Concern

  included do
    before_action :set_favoritable_entity, only: [:toggle_like]
  end

  def toggle_like
    respond_to :json
    entity = instance_variable_get(:"@#{self.class::FAVORITABLE_FOR}")

    if entity.toggle_like!(current_cenobite)
      render :show, status: :ok
    else
      render json: entity.errors, status: :unprocessable_entity
    end
  end

  private

  def set_favoritable_entity
    setter = :"set_#{self.class::FAVORITABLE_FOR}"
    raise "Controller don't have the #{setter} method" unless private_methods.include? setter

    send setter
  end
end
