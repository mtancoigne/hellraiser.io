class LinksController < ApplicationController
  include Sortable

  SORTABLE_FIELDS = [:created_at, :score].freeze

  # GET links
  def index
    @links = policy_scope Link.all

    apply_filters
  end

  private

  def policy_scope(scope)
    super([:member, scope])
  end

  def apply_filters # rubocop:disable Metrics/AbcSize, Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity
    @links = @links.order(sort_query(:created_at, :desc))

    @links = @links.where(cenobite_id: current_cenobite&.id) if params[:author] == 'you'

    @links = @links.where(visibility: 0) if params[:visibility] == 'private'
    @links = @links.where(visibility: 1) if params[:visibility] == 'members'
    @links = @links.where(visibility: 2) if params[:visibility] == 'public'

    @links = @links.tagged_with(params[:tag]) if params[:tag]

    @links = @links.favorited_by(current_cenobite&.id) if params[:likes] == 'yours'
  end
end
