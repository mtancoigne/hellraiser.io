module ActiveStorage
  class BaseController < ActionController::Base # rubocop:disable Rails/ApplicationController
    before_action :check_authentication
    include ActiveStorage::SetCurrent

    protect_from_forgery with: :exception

    private

    def check_authentication
      redirect_to '/cenobites/sign_in', alert: t('.not_auth') unless current_cenobite
    end
  end
end
