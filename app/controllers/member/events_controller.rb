module Member
  class EventsController < MemberApplicationController
    before_action :set_event, only: [:show, :edit, :update, :destroy, :attend, :leave]

    # GET /member/events
    def index
      @events = if params[:only] == 'over'
                  policy_scope(Event).over.order(starts_at: :desc)
                else
                  policy_scope(Event).not_over.order(starts_at: :asc)
                end

      @events = @events.page(params[:page])
    end

    # GET /member/events/1
    def show; end

    # GET /member/events/new
    def new
      @event = Event.new
    end

    # GET /member/events/1/edit
    def edit; end

    # POST /member/events
    def create
      @event          = Event.new(event_params)
      @event.cenobite = current_cenobite

      respond_to do |format|
        if @event.save
          format.html { redirect_to member_event_path(@event), notice: I18n.t('member.events.create.success') }
          format.json { render :show, status: :created, location: member_event_path(@event) }
        else
          format.html { render :new }
          format.json { render json: @event.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /member/events/1
    def update
      # Ignore dates changes for started or past events

      respond_to do |format|
        if @event.update(event_update_params)
          format.html { redirect_to member_event_path(@event), notice: I18n.t('member.events.update.success') }
          format.json { render :show, status: :ok, location: member_event_path(@event) }
        else
          format.html { render :edit }
          format.json { render json: @event.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /member/events/1
    def destroy
      respond_to do |format|
        @event.destroy
        format.html { redirect_to member_events_url, notice: I18n.t('member.events.destroy.success') }
        format.json { head :no_content }
      end
    end

    # PUT /member/events/1/attend
    def attend
      attendee = @event.add_attendee! @current_cenobite
      respond_to do |format|
        if attendee.valid?
          format.html { redirect_to member_event_path(@event), notice: I18n.t('member.events.attend.success') }
          format.json { render :show, status: :created, location: member_event_path(@event) }
        else
          format.html { redirect_to member_event_path(@event), alert: I18n.t('member.events.leave.failure') }
          format.json { render json: attendee.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /member/events/1/leave
    def leave
      attendee = @event.remove_attendee! @current_cenobite
      respond_to do |format|
        if attendee.valid?
          format.html { redirect_to member_event_path(@event), notice: I18n.t('member.events.leave.success') }
          format.json { head :no_content }
        else
          format.html { redirect_to member_event_path(@event), alert: I18n.t('member.events.attend.failure') }
          format.json { render json: attendee.errors, status: :unprocessable_entity }
        end
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])

      authorize @event
    end

    # Parameter filter selector for 'update' action
    def event_update_params
      return started_event_params if @event.started? || @event.over?

      event_params
    end

    # Only allow a trusted parameter "white list" through.
    def event_params
      params.require(:event).permit(:name, :description, :place, :url, :starts_at, :starts_at_timezone, :ends_at, :ends_at_timezone, :visibility)
    end

    def started_event_params
      params.require(:event).permit(:name, :description, :place, :url, :visibility)
    end
  end
end
