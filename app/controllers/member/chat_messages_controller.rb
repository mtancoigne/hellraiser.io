module Member
  class ChatMessagesController < MemberApplicationController
    before_action :set_chat_message, only: [:show, :update, :destroy]
    # If user has access to the room, then standard message policies applies
    before_action :authorize_chat_room

    # GET /member/chat_messages
    def index
      @chat_messages = policy_scope ChatMessage.where(chat_room_id: params[:chat_room_id])
    end

    # GET /member/chat_messages/1
    def show; end

    # POST /member/chat_messages
    def create
      chat_participant               = ChatParticipant.find_by(cenobite_id: current_cenobite.id, chat_room_id: params[:chat_room_id])
      @chat_message                  = ChatMessage.new(chat_message_params)
      @chat_message.chat_participant = chat_participant
      @chat_message.chat_room_id     = params[:chat_room_id]

      if @chat_message.save
        render :show, status: :created
      else
        render json: @chat_message.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /member/chat_messages/1
    def update
      if @chat_message.update(chat_message_params)
        render :show, status: :ok
      else
        render json: @chat_message.errors, status: :unprocessable_entity
      end
    end

    # DELETE /member/chat_messages/1
    def destroy
      @chat_message.destroy
      head :no_content
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_chat_message
      @chat_message = ChatMessage.find_by(id: params[:id], chat_room_id: params[:chat_room_id])
      authorize @chat_message
    end

    # Only allow a trusted parameter "white list" through.
    def chat_message_params
      params.require(:chat_message).permit(:content, :file)
    end

    def authorize_chat_room
      chat_room = ChatRoom.find params[:chat_room_id]
      authorize chat_room, :access_messages?, policy_class: Member::ChatRoomPolicy
    end
  end
end
