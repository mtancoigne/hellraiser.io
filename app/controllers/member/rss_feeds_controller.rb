module Member
  class RssFeedsController < MemberApplicationController
    include Sortable

    SORTABLE_FIELDS = [:title, :last_fetched_at].freeze

    before_action :set_rss_feed, only: [:edit, :update, :destroy]

    # GET /member/rss_feeds
    def index
      @rss_feeds = policy_scope(RssFeed)
                   .order(sort_query(:title, :asc))
                   .page(params[:page])
    end

    # GET /member/rss_feeds/new
    def new
      @rss_feed = RssFeed.new
    end

    # GET /member/rss_feeds/1/edit
    def edit; end

    # POST /member/rss_feeds
    def create
      @rss_feed = RssFeed.new(rss_feed_create_params)
      @rss_feed.cenobite = current_cenobite

      respond_to do |format|
        if @rss_feed.save
          format.html { redirect_to member_rss_feeds_path, notice: I18n.t('member.rss_feeds.create.success') }
          format.json { render :show, status: :created }
        else
          format.html { render :new }
          format.json { render json: @rss_feed.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /member/links/1
    def update
      respond_to do |format|
        if @rss_feed.update(rss_feed_update_params)
          format.html { redirect_to member_rss_feeds_path, notice: I18n.t('member.rss_feeds.update.success') }
          format.json { render :show, status: :ok }
        else
          format.html { render :edit }
          format.json { render json: @rss_feed.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /member/rss_feeds/1
    def destroy
      respond_to do |format|
        @rss_feed.destroy
        format.html { redirect_to member_rss_feeds_url, notice: I18n.t('member.rss_feeds.destroy.success') }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_rss_feed
      @rss_feed = RssFeed.find(params[:id])

      authorize @rss_feed
    end

    # Only allow a trusted parameter "white list" through.
    def rss_feed_create_params
      params.require(:rss_feed).permit(:url, :tag_list)
    end

    # Don't allow change of url
    def rss_feed_update_params
      params.require(:rss_feed).permit(:tag_list)
    end
  end
end
