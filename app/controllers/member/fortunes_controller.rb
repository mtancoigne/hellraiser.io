module Member
  class FortunesController < MemberApplicationController
    include Votable
    VOTABLE_FOR = :fortune

    before_action :set_fortune, only: [:edit, :update, :destroy]

    # GET /member/fortunes
    def index
      @fortunes = policy_scope Fortune.all
    end

    # GET /member/fortunes/random
    def random
      @fortune = Fortune.random.first || Fortune.new
      authorize @fortune
    end

    # GET /member/fortunes/new
    def new
      @fortune = Fortune.new
    end

    # GET /member/fortunes/1/edit
    def edit; end

    # POST /member/fortunes
    def create
      @fortune          = Fortune.new(fortune_params)
      @fortune.cenobite = current_cenobite

      respond_to do |format|
        if @fortune.save
          format.html { redirect_to member_fortunes_url, notice: I18n.t('member.fortunes.create.success') }
          format.json { render :random, status: :created, location: random_member_fortunes_path }
        else
          format.html { render :new }
          format.json { render json: @fortune.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /member/fortunes/1
    def update
      respond_to do |format|
        if @fortune.update(fortune_params)
          format.html { redirect_to member_fortunes_url, notice: I18n.t('member.fortunes.update.success') }
          format.json { render :random, status: :ok, location: random_member_fortunes_path }
        else
          format.html { render :edit }
          format.json { render json: @fortune.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /member/fortunes/1
    def destroy
      respond_to do |format|
        @fortune.destroy
        format.html { redirect_to member_fortunes_url, notice: I18n.t('member.fortunes.destroy.success') }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_fortune
      @fortune = Fortune.find(params[:id])

      authorize @fortune
    end

    # Only allow a trusted parameter "white list" through.
    def fortune_params
      params.require(:fortune).permit(:content, :source, :votes_count, :score, :visibility)
    end
  end
end
