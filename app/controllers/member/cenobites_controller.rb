module Member
  class CenobitesController < MemberApplicationController
    include Sortable

    SORTABLE_FIELDS = [:name].freeze

    before_action :set_cenobite, only: [:edit, :update]

    def index
      authorize Cenobite
      @cenobites = policy_scope(Cenobite.all)
                   .order(sort_query(:name, :desc))
                   .page(params[:page])
    end

    def edit; end

    # POST /member/cenobites/:id/invite
    # This differs from acquaintance#create as it works on ids instead of emails.
    # Additionally, one must appear in the Bottin in order to perform this action.
    def invite # rubocop:disable Metrics/AbcSize
      invitee = Cenobite.find params[:id]

      authorize invitee
      @acquaintance = Acquaintance.new inviter: current_cenobite, invitee: invitee, invite_method: :id
      respond_to do |format|
        if @acquaintance.save
          format.html { redirect_to :member_acquaintances, notice: I18n.t('member.acquaintances.create.success') }
          format.json { render 'member/acquaintances/show', status: :created, location: member_acquaintances_path }
        else
          format.html { render :index, alert: I18n.t('generic.errors.unknown_error') }
          format.json { render json: @acquaintance.errors, status: :unprocessable_entity }
        end
      end
    end

    def update
      respond_to do |format|
        if @cenobite.update(preferences_params)
          format.html { redirect_to edit_member_preferences_url, notice: I18n.t('member.cenobites.update.success') }
          format.json { render :show, status: :ok }
        else
          format.html { render :edit }
          format.json { render json: cenobite, status: :unprocessable_entity }
        end
      end
    end

    def preferences_params
      parameters = params.require(:cenobite).permit(:email, :name, :notify_new_motion, :notify_new_acquaintance, :locale,
                                                    :theme, :use_3d_view, :password, :current_password, :password_confirmation,
                                                    :visible_by_members, :show_email_to_acquaintances)
      return parameters.except(:password, :password_confirmation) if parameters[:password].blank?

      parameters
    end

    def set_cenobite
      @cenobite = current_cenobite
      skip_authorization
    end
  end
end
