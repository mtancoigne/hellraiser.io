module Member
  class ItemsController < MemberApplicationController
    include Sortable

    SORTABLE_FIELDS = [:name, :category, :borrowed_at].freeze

    before_action :set_item, only: [:show, :edit, :update, :destroy]

    # GET /member/items
    def index
      @items = if params[:only] == 'acquaintances'
                 policy_scope(AcquaintanceItem).all
               else
                 policy_scope(OwnedItem).all
               end
      @items = @items.page(params[:page])
      @categories = @items.where.not(category: nil).distinct(:category).order(:category).pluck(:category)
      apply_filters
    end

    # GET /member/items/1
    def show; end

    # GET /member/items/new
    def new
      @item = Item.new
    end

    # GET /member/items/1/edit
    def edit; end

    # POST /member/items
    def create
      @item       = Item.new(item_params)
      @item.owner = current_cenobite

      respond_to do |format|
        if @item.save
          format.html { redirect_to member_item_path(@item), notice: I18n.t('member.items.create.success') }
          format.json { render :show, status: :created, location: member_item_path(@item) }
        else
          format.html { render :new }
          format.json { render json: @item.errors, status: :unprocessable_entity }
        end
      end
    end

    def bulk_create # rubocop:disable Metrics/AbcSize
      @items = prepare_items
      respond_to do |format|
        if Item.upsert_all @items, unique_by: [:name, :category, :owner_id] # rubocop:disable Rails/SkipsModelValidations
          format.html { redirect_to member_items_path, notice: I18n.t('member.items.bulk_create.success') }
          format.json { render :index, status: :created, location: member_items_path }
        else
          failed_items = @items.each.select(&:invalid?).map(&:name)
          format.html { redirect_to member_items_path, notice: I18n.t('member.items.bulk_create.failure', items: failed_items.join(', ')) }
          format.json { render json: { errors: failed_items }, status: :ok }
        end
      end
    end

    # PATCH/PUT /member/items/1
    def update
      respond_to do |format|
        if @item.update(item_params)
          format.html { redirect_to member_item_path(@item), notice: I18n.t('member.items.update.success') }
          format.json { render :show, status: :ok, location: member_item_path(@item) }
        else
          format.html { render :edit }
          format.json { render json: @item.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /member/items/1
    def destroy
      respond_to do |format|
        @item.destroy
        format.html { redirect_to member_items_url, notice: I18n.t('member.items.destroy.success') }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_item
      @item = Item.find(params[:id])

      authorize @item, policy_class: @item.owner_id == current_cenobite.id ? OwnedItemPolicy : AcquaintanceItemPolicy
    end

    # Only allow a trusted parameter "white list" through.
    def item_params
      params.require(:item).permit(:name, :description, :category, :borrower_id)
    end

    def prepare_items
      items_params = params.require(:items).permit(:category, :list)
      item         = { category: items_params[:category], owner_id: current_cenobite.id, created_at: Time.current, updated_at: Time.current }
      items_params[:list].split("\n").uniq.select(&:present?).map do |name|
        item.merge name: name
      end
    end

    def apply_filters
      @items = @items.order(sort_query(:name, :asc))

      @items = @items.where.not(borrower_id: nil) if params[:filter_by] == 'borrowed'

      @items = @items.where(category: params[:category]) if params[:category]
    end
  end
end
