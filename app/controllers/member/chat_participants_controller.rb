module Member
  class ChatParticipantsController < MemberApplicationController
    before_action :set_chat_participant, only: [:show, :update, :destroy]

    # GET /member/chat_participants
    def index
      @chat_participants = policy_scope ChatParticipant.all
    end

    # GET /member/chat_participants/1
    def show; end

    # PATCH/PUT /member/chat_participants/1
    def update
      if @chat_participant.update(chat_participant_params)
        render :show, status: :ok, location: member_chat_participant_path(@chat_participant)
      else
        render json: @chat_participant.errors, status: :unprocessable_entity
      end
    end

    # DELETE /member/chat_participants/1
    def destroy
      @chat_participant.destroy
      head :no_content
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_chat_participant
      @chat_participant = ChatParticipant.find(params[:id])

      authorize @chat_participant
    end

    # Only allow a trusted parameter "white list" through.
    def chat_participant_params
      params.require(:chat_participant).permit(:auto_join, :name)
    end
  end
end
