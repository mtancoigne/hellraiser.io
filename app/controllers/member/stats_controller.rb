module Member
  class StatsController < MemberApplicationController
    before_action :skip_authorization, only: [:show]

    def show
      @stats = {
        cenobites: cenobites_count,
        motions:   motions_count,
        pastes:    pastes_count,
        dms:       DeadManSwitch.count,
        links:     links_count,
        items:     items_count,
        fortunes:  fortunes_count,
      }
    end

    private

    def links_count # rubocop:disable Metrics/MethodLength, Metrics/AbcSize
      {
        total:      Link.count,
        public:     Link.where(visibility: 2).count,
        member:     Link.where(visibility: 1).count,
        private:    Link.where(visibility: 0).count,
        graph_data: [
          {
            name: I18n.t('member.stats.show.links.public'),
            data: Link.where(visibility: 2, updated_at: 1.month.ago..Time.current).group_by_day(:updated_at, format: :day_month).count,
          },
          {
            name: I18n.t('member.stats.show.links.member'),
            data: Link.where(visibility: 1, updated_at: 1.month.ago..Time.current).group_by_day(:updated_at, format: :day_month).count,
          },
        ],
      }
    end

    def motions_count # rubocop:disable Metrics/MethodLength, Metrics/AbcSize
      {
        total:          Motion.count,
        closed:         Motion.where(closed: true).count,
        open:           Motion.where(closed: false).count,
        total_comments: Comment.count,
        graph_data:     [
          {
            name: I18n.t('activerecord.models.motion', count: 2),
            data: Motion.unscoped.where(created_at: 1.month.ago..Time.current).group_by_day(:created_at, format: :day_month).count,
          },
          {
            name: I18n.t('activerecord.models.comment', count: 2),
            data: Comment.unscoped.where(created_at: 1.month.ago..Time.current).group_by_day(:created_at, format: :day_month).count,
          },
        ],
      }
    end

    def cenobites_count
      {
        total:    Cenobite.count,
        active:   Cenobite.where.not(confirmed_at: nil).count,
        inactive: Cenobite.where(confirmed_at: nil).count,
      }
    end

    def pastes_count
      {
        total:              Paste.count,
        password_protected: Paste.where.not(password_digest: nil).count,
        burnable:           Paste.where(burn: true).count,
      }
    end

    def items_count
      {
        total: Item.count,
      }
    end

    def fortunes_count
      {
        total: Fortune.count,
      }
    end
  end
end
