module Member
  class CommentsController < MemberApplicationController
    before_action :set_comment, only: [:show, :update, :destroy]

    ALLOWED_NESTING = {
      'motion_id' => 'Motion',
      'event_id'  => 'Event',
    }.freeze

    # GET /member/<commentable_type>/<commentable_id>/comments
    def index
      respond_to :json
      @comments = policy_scope Comment.where(commentable: commentable_from_params)
    end

    # GET /member/comments/1
    def show
      respond_to :json
    end

    # POST /member/comments
    def create
      prepare_comment
      commentable_path = path_from_commentable(@comment.commentable)
      respond_to do |format|
        if @comment.save
          format.html { redirect_to commentable_path, notice: I18n.t('member.comments.create.success') }
          format.json { render :show, status: :created, location: member_comment_path(@comment) }
        else
          format.html { redirect_to commentable_path, alert: I18n.t('member.comments.create.error') }
          format.json { render json: @comment.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /member/comments/1
    def update
      commentable_path = path_from_commentable(@comment.commentable)
      respond_to do |format|
        if @comment.update(comment_params)
          format.html { redirect_to commentable_path, notice: I18n.t('member.comments.update.success') }
          format.json { render :show, status: :ok, location: member_comment_path(@comment) }
        else
          format.html { redirect_to commentable_path, alert: I18n.t('member.comments.create.error') }
          format.json { render json: @comment.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /member/comments/1
    def destroy
      commentable_path = path_from_commentable(@comment.commentable)
      respond_to do |format|
        @comment.destroy
        format.html { redirect_to commentable_path, notice: I18n.t('member.comments.destroy.success') }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])

      authorize @comment
    end

    # Only allow a trusted parameter "white list" through.
    def comment_params
      params.require(:comment).permit(:content, :commentable_id, :commentable_type, :picture)
    end

    def commentable_from_params
      type = nil
      params.each_key { |key| type = ALLOWED_NESTING[key] if ALLOWED_NESTING.key?(key) }
      raise 'No valid commentable type provided' unless type

      field = nil
      params.each_key { |key| field = key if ALLOWED_NESTING.key?(key) }
      raise 'No valid commentable id provided' unless field

      type_class = type.classify.constantize
      type_class.find(params[field])
    end

    def path_from_commentable(entity)
      send :"member_#{entity.class.to_s.underscore}_path", entity
    end

    def prepare_comment
      @comment             = Comment.new(comment_params)
      @comment.cenobite    = current_cenobite
      @comment.commentable = commentable_from_params
    end
  end
end
