module Member
  class DeadManSwitchesController < MemberApplicationController
    before_action :set_dead_man_switch, only: [:show, :edit, :update, :destroy, :ping, :rearm]

    # GET /dead_man_switches
    # GET /dead_man_switches.json
    def index
      @dead_man_switches = policy_scope DeadManSwitch
    end

    def ping_all
      skip_authorization # ping_for will scope the request
      respond_to do |format|
        if DeadManSwitch.ping_for current_cenobite
          index
          format.html { redirect_to :member_dead_man_switches, notice: I18n.t('member.dead_man_switches.ping_all.success') }
          format.json { render :index, status: :ok, location: :member_dead_man_switches }
        else
          format.html { redirect_to :dead_man_switches, notice: I18n.t('member.dead_man_switches.ping_all.error') }
          format.json { render json: { error: 'Unable to ping this switch' }, status: :unprocessable_entity }
        end
      end
    end

    def ping
      respond_to do |format|
        if @dead_man_switch.ping!
          format.html { redirect_to member_dead_man_switch_path(@dead_man_switch), notice: I18n.t('member.dead_man_switches.ping.success') }
          format.json { render :show, status: :ok, location: member_dead_man_switch_path(@dead_man_switch) }
        else
          format.html { render :show, notice: I18n.t('member.dead_man_switches.ping.error') }
          format.json { render json: @dead_man_switch.errors, status: :unprocessable_entity }
        end
      end
    end

    def rearm
      respond_to do |format|
        if @dead_man_switch.rearm!
          format.html { redirect_to member_dead_man_switch_path(@dead_man_switch), notice: I18n.t('member.dead_man_switches.rearm.success') }
          format.json { render :show, status: :ok, location: member_dead_man_switch_path(@dead_man_switch) }
        else
          format.html { render :show, notice: I18n.t('member.dead_man_switches.rearm.error') }
          format.json { render json: @dead_man_switch.errors, status: :unprocessable_entity }
        end
      end
    end

    # GET /dead_man_switches/1
    # GET /dead_man_switches/1.json
    def show; end

    # GET /dead_man_switches/new
    def new
      @dead_man_switch = DeadManSwitch.new
    end

    # GET /dead_man_switches/1/edit
    def edit; end

    # POST /dead_man_switches
    # POST /dead_man_switches.json
    def create
      @dead_man_switch          = DeadManSwitch.new(dead_man_switch_params)
      @dead_man_switch.cenobite = current_cenobite

      respond_to do |format|
        if @dead_man_switch.save
          format.html { redirect_to member_dead_man_switch_path(@dead_man_switch), notice: I18n.t('member.dead_man_switches.create.success') }
          format.json { render :show, status: :created, location: member_dead_man_switch_path(@dead_man_switch) }
        else
          format.html { render :new }
          format.json { render json: @dead_man_switch.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /dead_man_switches/1
    # PATCH/PUT /dead_man_switches/1.json
    def update
      respond_to do |format|
        if @dead_man_switch.update(dead_man_switch_params)
          format.html { redirect_to member_dead_man_switch_path(@dead_man_switch), notice: I18n.t('member.dead_man_switches.update.success') }
          format.json { render :show, status: :ok, location: member_dead_man_switch_path(@dead_man_switch) }
        else
          format.html { render :edit }
          format.json { render json: @dead_man_switch.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /dead_man_switches/1
    # DELETE /dead_man_switches/1.json
    def destroy
      @dead_man_switch.destroy
      respond_to do |format|
        format.html { redirect_to :member_dead_man_switches, notice: I18n.t('member.dead_man_switches.destroy.success') }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_dead_man_switch
      @dead_man_switch = DeadManSwitch.find(params[:id])

      authorize @dead_man_switch
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def dead_man_switch_params
      params.require(:dead_man_switch).permit(:name, :recipients, :content, :ping_frequency, :update_ping)
    end
  end
end
