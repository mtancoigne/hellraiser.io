module Member
  class SubscriptionsController < MemberApplicationController
    before_action :set_subscription, only: [:destroy]

    # GET /member/subscriptions
    def index
      @subscriptions = policy_scope Subscription.all
    end

    # DELETE /member/subscriptions/1
    def destroy
      respond_to do |format|
        @subscription.destroy
        format.html { redirect_to member_subscriptions_url, notice: I18n.t('member.subscriptions.destroy.success') }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_subscription
      @subscription = Subscription.find(params[:id])

      authorize @subscription
    end

    # Only allow a trusted parameter "white list" through.
    def subscription_params
      params.require(:subscription).permit(:entity_id, :entity_type, :action)
    end
  end
end
