# frozen_string_literal: true

module Member
  class MotionsController < MemberApplicationController
    include Sortable
    SORTABLE_FIELDS = [:title, :notable_change_at, :score].freeze

    include Votable
    VOTABLE_FOR = :motion

    before_action :set_motion, only: [:show, :edit, :update, :destroy, :subscribe, :unsubscribe]

    # GET /member/motions
    def index
      @motions = policy_scope(Motion)
                 .order(sort_query(:notable_change_at, :desc))
                 .page(params[:page])
    end

    # GET /member/motions/1
    def show; end

    # GET /member/motions/new
    def new
      @motion = Motion.new
    end

    # GET /member/motions/1/edit
    def edit; end

    # POST /member/motions
    def create
      @motion          = Motion.new(motion_create_params)
      @motion.cenobite = current_cenobite

      respond_to do |format|
        if @motion.save
          format.html { redirect_to member_motion_path(@motion), notice: I18n.t('member.motions.create.success') }
          format.json { render :show, status: :created, location: member_motion_path(@motion) }
        else
          format.html { render :new }
          format.json { render json: @motion.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /member/motions/1
    def update
      respond_to do |format|
        if @motion.update(motion_update_params)
          format.html { redirect_to member_motion_path(@motion), notice: I18n.t('member.motions.update.success') }
          format.json { render :show, status: :ok, location: member_motion_path(@motion) }
        else
          format.html { render :edit }
          format.json { render json: @motion.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /member/motions/1
    def destroy
      respond_to do |format|
        @motion.destroy
        format.html { redirect_to member_motions_url, notice: I18n.t('member.motions.destroy.success') }
        format.json { head :no_content }
      end
    end

    def subscribe
      respond_to do |format|
        if @motion.subscribe!(current_cenobite)
          format.html { redirect_to member_motion_path(@motion), notice: I18n.t('member.motions.subscribe.success') }
          format.json { render :show, status: :ok, location: member_motion_path(@motion) }
        else
          format.html { redirect_to member_motion_path(@motion), alert: I18n.t('member.motions.subscribe.failure') }
          format.json { render json: @motion.errors, status: :unprocessable_entity }
        end
      end
    end

    def unsubscribe
      respond_to do |format|
        if @motion.unsubscribe!(current_cenobite)
          format.html { redirect_to member_motion_path(@motion), notice: I18n.t('member.motions.unsubscribe.success') }
          format.json { render :show, status: :ok, location: member_motion_path(@motion) }
        else
          format.html { redirect_to member_motion_path(@motion), alert: I18n.t('member.motions.unsubscribe.failure') }
          format.json { render json: @motion.errors, status: :unprocessable_entity }
        end
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_motion
      @motion = Motion.find(params[:id])

      authorize @motion
    end

    # Only allow a trusted parameter "white list" through.
    def motion_create_params
      params.require(:motion).permit(:title, :content, :closed, :picture, :anonymous)
    end

    def motion_update_params
      params.require(:motion).permit(:title, :content, :closed, :picture)
    end
  end
end
