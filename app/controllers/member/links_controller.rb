require 'open-uri'

module Member
  class LinksController < MemberApplicationController
    include Favoritable
    FAVORITABLE_FOR = :link

    include Votable
    VOTABLE_FOR = :link

    before_action :set_link, only: [:edit, :update, :destroy]

    # GET /member/links/new
    def new
      @link = Link.new
      if params[:url].present?
        url = params[:url].sub(%r{:/(?=[^/])}, '://')
        @link.url = url
        doc = scrape(url)
        if doc.present?
          @link.title = doc.css("meta[property='og:title']/@content").to_s
          @link.description = doc.css("meta[property='og:description']/@content").to_s
        end
      end
      @link
    end

    # GET /member/links/1/edit
    def edit; end

    # POST /member/links
    def create
      @link = Link.new(link_params)
      @link.cenobite = current_cenobite

      respond_to do |format|
        if @link.save
          format.html { redirect_to links_path, notice: I18n.t('member.links.create.success') }
          format.json { render 'links/show', status: :created }
        else
          format.html { render :new }
          format.json { render json: @link.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /member/links/1
    def update
      respond_to do |format|
        if @link.update(link_params)
          format.html { redirect_to links_path, notice: I18n.t('member.links.update.success') }
          format.json { render 'links/show', status: :ok }
        else
          format.html { render :edit }
          format.json { render json: @link.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /member/links/1
    def destroy
      respond_to do |format|
        @link.destroy
        format.html { redirect_to links_url, notice: I18n.t('member.links.destroy.success') }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_link
      @link = Link.find(params[:id])

      authorize @link
    end

    # Only allow a trusted parameter "white list" through.
    def link_params
      params.require(:link).permit(:title, :url, :description, :visibility, :tag_list)
    end

    # Scrape url to get content value of <meta> tag targeted by selector.
    def scrape(url)
      response = Faraday.get url
      Nokogiri::HTML(response.body).css('meta[property^="og"]')
    rescue SocketError, NoMethodError, OpenURI::HTTPError, OpenSSL::OpenSSLError, Faraday::ConnectionFailed
      nil
    end
  end
end
