module Member
  class RssItemsController < MemberApplicationController
    include Sortable
    SORTABLE_FIELDS = [:published_at, :score].freeze

    include Favoritable
    FAVORITABLE_FOR = :rss_item

    include Votable
    VOTABLE_FOR = :rss_item

    # GET /member/rss_items
    def index
      @rss_items = policy_scope(RssItem)
                   .order(sort_query(:published_at, :desc))

      apply_filters
    end

    private

    # Used by concerns
    def set_rss_item
      @rss_item = RssItem.find(params[:id])

      authorize @rss_item
    end

    def apply_filters # rubocop:disable Metrics/AbcSize
      # tag/feed/feeds are exclusive
      feed_ids = if params[:tag]
                   RssFeed.tagged_with(params[:tag]).pluck :id
                 elsif params[:feed]
                   params[:feed]
                 elsif params[:feeds] == 'yours'
                   RssFeed.where(cenobite_id: current_cenobite.id).pluck :id
                 end

      @rss_items = @rss_items.where rss_feed_id: feed_ids if feed_ids

      @rss_items = @rss_items.favorited_by(current_cenobite&.id) if params[:likes] == 'yours'

      @rss_items = @rss_items.page(params[:page])
    end
  end
end
