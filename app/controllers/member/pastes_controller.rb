module Member
  class PastesController < MemberApplicationController
    include Sortable

    SORTABLE_FIELDS = [:name, :extension].freeze

    before_action :set_paste, only: [:show, :edit, :update, :destroy]

    # GET /pastes
    # GET /pastes.json
    def index
      @pastes = policy_scope(Paste)
                .order(sort_query(:name, :asc))
    end

    # GET /pastes/1
    # GET /pastes/1.json
    def show
      # Don't scope query
      @paste = Paste.find(params[:id])
    end

    # GET /pastes/new
    def new
      @paste = Paste.new
    end

    # GET /pastes/1/edit
    def edit; end

    # POST /pastes
    # POST /pastes.json
    def create
      @paste          = Paste.new(paste_params)
      @paste.cenobite = current_cenobite

      respond_to do |format|
        if @paste.save
          format.html { redirect_to member_paste_url(@paste), notice: I18n.t('member.pastes.create.success') }
          format.json { render :show, status: :created, location: member_paste_path(@paste) }
        else
          format.html { render :new }
          format.json { render json: @paste.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /pastes/1
    # PATCH/PUT /pastes/1.json
    def update
      respond_to do |format|
        if @paste.update(paste_params)
          format.html { redirect_to member_paste_url(@paste), notice: I18n.t('member.pastes.update.success') }
          format.json { render :show, status: :ok, location: member_paste_path(@paste) }
        else
          format.html { render :edit }
          format.json { render json: @paste.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /pastes/1
    # DELETE /pastes/1.json
    def destroy
      @paste.destroy
      respond_to do |format|
        format.html { redirect_to member_pastes_url, notice: I18n.t('member.pastes.destroy.success') }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_paste
      @paste = Paste.find(params[:id])

      authorize @paste
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def paste_params
      params.require(:paste).permit(:name, :content, :burn, :password, :cenobite_id, :extension)
    end
  end
end
