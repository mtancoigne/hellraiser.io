module Member
  class AcquaintancesController < MemberApplicationController
    before_action :set_acquaintance, only: [:accept, :resend_request, :destroy]

    # GET /member/acquaintances
    def index
      @acquaintances = policy_scope Acquaintance.all
      @acquaintance = Acquaintance.new
    end

    def new
      @acquaintance = Acquaintance.new
    end

    def accept
      respond_to do |format|
        if @acquaintance.accept!
          format.html { redirect_to member_acquaintances_path, notice: I18n.t('member.acquaintances.accept.success') }
          format.json { render :show, status: :created, location: member_acquaintances_path }
        else
          format.html { redirect_to member_acquaintances_path, notice: I18n.t('member.acquaintances.accept.failure') }
          format.json { render json: @acquaintance.errors, status: :unprocessable_entity }
        end
      end
    end

    # POST /member/acquaintances
    # This differs from cenobites#invite as it works on email instead of ids.
    # Additionally, one don't need to be in the bottin to create acquaintance
    # requests from here.
    def create
      build_acquaintance

      respond_to do |format|
        if @acquaintance.save
          format.html { redirect_to member_acquaintances_path, notice: I18n.t('member.acquaintances.create.success') }
          format.json { render :show, status: :created, location: member_acquaintances_path }
        else
          format.html { render :new }
          format.json { render json: @acquaintance.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /member/acquaintances/1
    def destroy
      respond_to do |format|
        @acquaintance.destroy
        format.html { redirect_to member_acquaintances_url, notice: I18n.t('member.acquaintances.destroy.success') }
        format.json { head :no_content }
      end
    end

    def resend_request
      respond_to do |format|
        if @acquaintance.send_request_email!
          format.html { redirect_to member_acquaintances_path, notice: I18n.t('member.acquaintances.resend.success') }
          format.json { head :no_content }
        end
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_acquaintance
      @acquaintance = Acquaintance.find(params[:id])

      authorize @acquaintance
    end

    # Only allow a trusted parameter "white list" through.
    def acquaintance_params
      params.require(:acquaintance).permit(:email)
    end

    def build_acquaintance
      parameters = acquaintance_params
      invitee = Cenobite.invite!({ email: parameters[:email] }, current_cenobite)

      @acquaintance = invitee.request_acquaintance with: current_cenobite, by: :email
    end
  end
end
