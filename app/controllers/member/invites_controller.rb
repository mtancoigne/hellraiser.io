module Member
  class InvitesController < MemberApplicationController
    before_action :set_invite, only: [:resend, :destroy]

    def resend
      respond_to do |format|
        if @invite.invite!
          format.html { redirect_to member_acquaintances_path, notice: I18n.t('member.invite.resend.success') }
          format.json { head :no_content }
        end
      end
    end

    def destroy
      respond_to do |format|
        if @invite.destroy
          format.html { redirect_to member_acquaintances_path, notice: I18n.t('member.invite.destroy.success') }
          format.json { head :no_content }
        end
      end
    end

    private

    def set_invite
      @invite = policy_scope(Cenobite, policy_scope_class: InvitePolicy::Scope).find(params[:id])

      authorize @invite, policy_class: InvitePolicy
    end
  end
end
