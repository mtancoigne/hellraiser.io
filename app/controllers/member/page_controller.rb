module Member
  class PageController < MemberApplicationController
    ALLOWED_PAGES = ['changelog'].freeze

    def show
      @page = params[:id] if ALLOWED_PAGES.include? params[:id]

      raise ActiveRecord::RecordNotFound unless @page
    end
  end
end
