module Member
  class ChatRoomsController < MemberApplicationController
    before_action :set_chat_room, only: [:show, :update, :destroy]

    # GET /member/chat_rooms
    def index
      @chat_rooms = policy_scope ChatRoom.all
    end

    # GET /member/chat_rooms/1
    def show; end

    # POST /member/chat_rooms
    def create
      @chat_room = build_chat_room

      if @chat_room.save
        render :show, status: :created, location: member_chat_room_path(@chat_room)
      else
        render json: @chat_room.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /member/chat_rooms/1
    def update
      if @chat_room.update(chat_room_params)
        render :show, status: :ok, location: member_chat_room_path(@chat_room)
      else
        render json: @chat_room.errors, status: :unprocessable_entity
      end
    end

    # DELETE /member/chat_rooms/1
    def destroy
      @chat_room.destroy
      head :no_content
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_chat_room
      @chat_room = ChatRoom.find(params[:id])

      authorize @chat_room
    end

    # Only allow a trusted parameter "white list" through.
    def chat_room_params
      params.require(:chat_room).permit(:name, :description)
    end

    def chat_participant_params
      params.require(:chat_participant).permit(:name, :auto_join)
    end

    def build_chat_room
      chat_participant = ChatParticipant.new(chat_participant_params)
      chat_participant.cenobite = current_cenobite
      chat_room = ChatRoom.new(chat_room_params)
      chat_room.cenobite = current_cenobite
      chat_room.chat_participants = [chat_participant]

      chat_room
    end
  end
end
