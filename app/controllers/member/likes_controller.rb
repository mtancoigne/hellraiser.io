module Member
  class LikesController < MemberApplicationController
    include Sortable
    SORTABLE_FIELDS = [:created_at, :likable_type].freeze

    before_action :set_like, only: [:destroy]

    # GET /member/likes
    def index
      @likes = policy_scope Like
               .order(sort_query(:created_at, :desc))
               .page(params[:page])
    end

    # DELETE /member/likes/1
    def destroy
      respond_to do |format|
        @like.destroy
        format.html { redirect_to member_likes_url, notice: I18n.t('member.likes.destroy.success') }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_like
      @like = Like.find(params[:id])

      authorize @like
    end
  end
end
