module Member
  class ChatPresencesController < MemberApplicationController
    # If user has access to the room, then standard presence policies applies
    before_action :authorize_chat_room

    # GET /member/chat_presences
    def index
      @chat_presences = policy_scope(ChatPresence).unique_presences_in params[:chat_room_id]
    end

    private

    def authorize_chat_room
      chat_room = ChatRoom.find params[:chat_room_id]
      authorize chat_room, :access_messages?, policy_class: Member::ChatRoomPolicy
    end
  end
end
