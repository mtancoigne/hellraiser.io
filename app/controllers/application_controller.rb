class ApplicationController < ActionController::Base
  include Localizable
  include Pundit::Authorization

  before_action :configure_permitted_parameters, if: :devise_controller?

  rescue_from ActiveRecord::RecordNotFound, with: :error_not_found
  rescue_from ActionController::InvalidAuthenticityToken, with: :error_csrf
  rescue_from ActionController::ParameterMissing, with: :error_unprocessable
  rescue_from Pundit::NotAuthorizedError, with: :error_not_authorized

  private

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:accept_invitation, keys: [:name, :locale])
  end

  # We have cenobites
  def pundit_user
    current_cenobite
  end

  def error_fallback(exception, fallback_message, status)
    message = exception&.message || fallback_message
    respond_to do |format|
      format.json { render json: { error: message }, status: status }
      format.html { raise exception }
    end
  end

  def error_not_found(exception = nil)
    error_fallback(exception, 'Requested content was not found', :not_found)
  end

  def error_unprocessable(exception = nil)
    error_fallback(exception, 'Unprocessable entity', :unprocessable_entity)
  end

  def error_csrf(exception = nil)
    error_fallback(exception, 'Invalid CSRF token', :unprocessable_entity)
  end

  def error_not_authorized(_exception = nil)
    respond_to do |format|
      format.json { render json: { error: 'You are not authorized to perform this action' }, status: :forbidden }
      format.html do
        render file: 'public/403.html', status: :forbidden, layout: false
      end
    end
  end
end
