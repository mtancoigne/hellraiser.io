json.extract! paste,
              :id,
              :name,
              :content,
              :burn,
              :extension,
              :created_at,
              :updated_at
json.password paste.password_digest.present?
