json.extract! link,
              :id,
              :title,
              :url,
              :description,
              :visibility,
              :votes_count,
              :created_at,
              :updated_at

json.tag_list link.tags.join(', ')

json.cenobite_id(current_cenobite && same_cenobite?(link.cenobite_id) ? link.cenobite_id : nil)
json.score link.score

json.liked current_cenobite ? link.liked?(current_cenobite) : false
