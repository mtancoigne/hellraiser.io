json.extract! fortune, :id, :content, :source, :votes_count, :score, :created_at, :updated_at

json.cenobite_id same_cenobite?(fortune.cenobite_id) ? fortune.cenobite_id : nil
json.cenobite_colors cenobite_name(fortune, html_tag: nil)
json.score fortune.score
