json.extract! cenobite,
              :email,
              :locale,
              :name,
              :notify_new_acquaintance,
              :notify_new_motion,
              :theme,
              :use_3d_view
