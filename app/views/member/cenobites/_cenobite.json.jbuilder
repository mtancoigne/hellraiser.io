# Who's asking?
pending_acquaintance_with = current_cenobite.pending_acquaintance_with(cenobite)
pending_acquaintance_from = current_cenobite.pending_acquaintance_from(cenobite)
acquainted_with           = cenobite.acquainted_with?(current_cenobite)

acquaintance = acquainted_with || pending_acquaintance_with || pending_acquaintance_from
you          = cenobite.id == current_cenobite.id

# Mandatory to create acquaintance requests from IDs
json.id cenobite.id
json.name cenobite.name
json.email(acquaintance&.show_email?(cenobite) || you ? cenobite.email : nil)
