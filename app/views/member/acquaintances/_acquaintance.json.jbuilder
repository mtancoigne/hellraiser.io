json.extract! acquaintance, :id, :inviter_id, :invitee_id, :accepted_at, :created_at
invitee = acquaintance.acquainted(current_cenobite)
json.cenobite id:    (acquaintance.show_cenobite_id? ? invitee.id : nil),
              name:  (acquaintance.show_cenobite_name?(invitee) ? invitee.name : nil),
              email: (acquaintance.show_cenobite_email?(invitee) ? invitee.email : nil)
