json.extract! @stats,
              :cenobites,
              :motions,
              :pastes,
              :dms,
              :links,
              :items
