json.extract! rss_feed,
              :id,
              :url,
              :title,
              :link,
              :description,
              :language,
              :last_fetched_at,
              :created_at,
              :updated_at

json.tag_list rss_feed.tags.join(', ')

json.cenobite_id same_cenobite?(rss_feed.cenobite_id) ? rss_feed.cenobite_id : nil
