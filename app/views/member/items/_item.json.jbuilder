json.extract! item, :id, :name, :description, :category, :owner_id, :created_at, :updated_at

if same_cenobite?(item.borrower_id)
  json.borrower_id item.borrower_id
  json.borrowed_at item.borrowed_at
else
  json.borrower_id nil
  json.borrowed_at nil
end

json.borrowable item.borrowable?
