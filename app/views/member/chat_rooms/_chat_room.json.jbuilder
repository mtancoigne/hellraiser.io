json.extract! chat_room, :id, :name, :description, :acquaintance_id, :created_at, :updated_at

json.cenobite_id same_cenobite?(chat_room.cenobite_id) ? chat_room.cenobite_id : nil
