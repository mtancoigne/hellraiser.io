json.extract! chat_presence, :id, :chat_room_id, :chat_participant_id, :created_at, :updated_at

json.chat_participant id:   chat_presence.chat_participant.id,
                      name: chat_presence.chat_participant.name
