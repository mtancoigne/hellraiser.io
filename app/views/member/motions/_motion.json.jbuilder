json.extract! motion, :id, :title, :content, :closed, :votes_count, :created_at, :updated_at, :notable_change_at

json.has_picture motion.picture.attached?
json.picture attachment_attributes motion.picture

json.cenobite_id same_cenobite?(motion.cenobite_id) ? motion.cenobite_id : nil
json.cenobite_colors cenobite_name(motion, html_tag: nil, anonymous: motion.anonymous)
json.score motion.score
