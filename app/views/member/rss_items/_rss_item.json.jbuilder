json.extract! rss_item,
              :id,
              :title,
              :link,
              :rss_feed_id,
              :votes_count,
              :published_at,
              :created_at,
              :updated_at

json.description sanitize_content rss_item.description

json.score rss_item.score
json.liked current_cenobite ? rss_item.liked?(current_cenobite) : false
