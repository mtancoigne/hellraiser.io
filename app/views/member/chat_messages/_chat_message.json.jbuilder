json.extract! chat_message, :id, :content, :system, :chat_participant_id, :chat_room_id, :created_at, :updated_at

if chat_message.system
  json.chat_participant nil
else
  json.chat_participant id:   chat_message.chat_participant.id,
                        name: chat_message.chat_participant.name
end

json.file attachment_attributes chat_message.file
