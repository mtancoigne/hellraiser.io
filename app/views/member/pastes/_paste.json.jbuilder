json.extract! paste,
              :id,
              :name,
              :content,
              :burn,
              :cenobite_id,
              :extension,
              :created_at,
              :updated_at
json.password paste.password_digest.present?
