json.extract! event, :id,
              :name,
              :description,
              :place,
              :url,
              :starts_at,
              :starts_at_timezone,
              :ends_at,
              :ends_at_timezone,
              :visibility,
              :created_at,
              :updated_at

json.cenobite_id(current_cenobite && same_cenobite?(event.cenobite_id) ? event.cenobite_id : nil)

json.attendees_amount event.events_attendances.count

json.attendees event.attendees.map(&:name) if owner? event, @current_cenobite
