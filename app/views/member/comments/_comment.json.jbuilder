json.extract! comment, :id, :content, :created_at, :updated_at

json.has_picture comment.picture.attached?

json.cenobite_id same_cenobite?(comment.cenobite_id) ? comment.cenobite_id : nil

anonymous = comment.commentable.respond_to?(:anonymous) ? comment.commentable&.anonymous : false
json.cenobite_colors cenobite_name(comment, parent_id: comment.commentable_id, html_tag: nil, anonymous: anonymous)

json.picture attachment_attributes comment.picture
