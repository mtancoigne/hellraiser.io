class ChatMessage < ApplicationRecord
  validates :content, presence: true
  validates :file, with: :file_validation
  validates :chat_participant_id, presence: true, unless: :system

  belongs_to :chat_room
  belongs_to :chat_participant, optional: true

  before_validation :set_content
  after_create :update_unread_count

  # ActionCable notification needs the data to be persisted
  after_commit :ac_notify_create, on: :create
  after_commit :ac_notify_update, on: :update
  after_commit :ac_notify_destroy, on: :destroy

  has_one_attached :file, dependent: :purge_later

  private

  def ac_notify_create
    ac_notify :create
  end

  def ac_notify_update
    ac_notify :update
  end

  def ac_notify_destroy
    ac_notify :destroy
  end

  def ac_notify(action)
    ChatRoomChannel.broadcast_message action, self
  end

  def update_unread_count
    chat_room.chat_participants.reject(&:watching?).each do |participant|
      participant.increment :unread_count, 1
      participant.save!
    end
  end

  def set_content
    self.content = file.name if file.attached?
  end

  def file_validation
    return unless file.attached?

    errors.add(:file, I18n.t('activerecord.errors.file_too_big', max: Rails.configuration.hellraiser.chat[:max_upload_string])) if file.blob.byte_size > Rails.configuration.hellraiser.chat[:max_upload_size]
  end
end
