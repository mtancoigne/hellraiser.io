class Motion < ApplicationRecord
  include VotableEntity

  PICTURE_MIMES = ['image/png', 'image/jpeg', 'image/gif'].freeze

  attribute :notable_change_at, :datetime, default: -> { Time.current }
  attribute :anonymous, :boolean, default: true

  validates :title, presence: true
  validates :content, presence: true
  validate :picture_format

  belongs_to :cenobite
  has_many :comments, as: :commentable, dependent: :delete_all
  has_many :subscriptions, as: :entity, dependent: :delete_all

  has_one_attached :picture

  before_save :update_notable_change_at
  after_create :notify_creation!
  after_create :subscribe_author

  def notify_creation!
    Cenobite.where(notify_new_motion: true).where.not(id: cenobite_id).find_each do |cenobite|
      NewMotionMailer.with(cenobite: cenobite, motion: self).notify_email.deliver_later
    end
  end

  def subscription_of(cenobite)
    subscriptions.find_by(cenobite: cenobite)
  end

  alias subscribed? :subscription_of

  def subscribe!(cenobite)
    Subscription.create(entity: self, cenobite: cenobite)
  end

  def unsubscribe!(cenobite)
    subscription = subscription_of(cenobite)
    subscription&.destroy
  end

  private

  def subscribe_author
    subscribe! cenobite
  end

  def picture_format
    return unless picture.attached?

    errors.add(:avatar, I18n.t('activerecord.errors.bad_image_format', formats.join(', '))) unless PICTURE_MIMES.include? picture.blob.content_type
  end

  def update_notable_change_at
    self.notable_change_at = Time.current if changes.keys != [:closed] && !closed
  end
end
