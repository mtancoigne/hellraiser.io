class Subscription < ApplicationRecord
  validates :cenobite_id, uniqueness: { scope: [:entity_id, :entity_type], message: I18n.t('activerecord.errors.models.subscription.attribute.cenobite_id.already_subscribed') }

  belongs_to :entity, polymorphic: true
  belongs_to :cenobite
end
