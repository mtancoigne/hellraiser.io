class Cenobite < ApplicationRecord
  validates :theme, inclusion: { in: ['dark', 'light', 'clean_light', 'clean_dark', '', nil] }

  # Include default devise modules. Others available are:
  # :registerable, :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :invitable, :database_authenticatable, :recoverable, :rememberable, :validatable, :confirmable

  has_many :comments, dependent: :restrict_with_exception
  has_many :dead_man_switches, dependent: :restrict_with_exception
  has_many :votes, dependent: :restrict_with_exception
  has_many :motions, dependent: :restrict_with_exception
  has_many :pastes, dependent: :restrict_with_exception
  has_many :subscriptions, dependent: :destroy
  has_many :invitations, class_name: 'Cenobite', as: :invited_by, dependent: :nullify
  has_many :items, class_name: 'Item', foreign_key: :owner_id, inverse_of: :owner, dependent: :restrict_with_exception
  has_many :borrowed_items, class_name: 'Item', foreign_key: :borrower_id, inverse_of: :borrower, dependent: :restrict_with_exception
  has_many :fortunes, dependent: :destroy
  has_many :chat_participants, dependent: :delete_all
  has_many :rss_feeds, dependent: :delete_all
  has_many :events, dependent: :delete_all
  has_many :events_attendances, dependent: :delete_all

  # Skip confirmation email when a cenobite is created.
  # We use invitations and don't want them to receive two different emails.
  after_create :skip_confirmation_notification!
  after_invitation_accepted :accept_acquaintance
  before_destroy :destroy_acquaintances
  after_save :update_chat_names

  scope :visible, -> { where visible_by_members: true }

  def name
    attributes['name'] || "cenobite_#{id}"
  end

  def acquaintances
    Acquaintance.for_cenobite self
  end

  def known_cenobites
    ids = Acquaintance.accepted.ids_for_cenobite self
    Cenobite.where(id: ids)
  end

  def pending_invitations
    invitations.where(confirmed_at: nil)
  end

  def acquainted_with?(cenobite)
    Acquaintance.accepted.ids_for_cenobite(self).include? cenobite.id
  end

  def unread_chat_messages
    chat_participants.sum(:unread_count)
  end

  def confirmed?
    confirmed_at || invitation_accepted_at
  end

  # Checks if current entity has an acquaintance request from the given cenobite
  def pending_acquaintance_from?(cenobite)
    pending_acquaintance_from(cenobite).present?
  end

  def pending_acquaintance_from(cenobite)
    acquaintances.pending.find_by(invitee: cenobite)
  end

  # Checks if current entity created an acquaintance request for the given cenobite
  def pending_acquaintance_with?(cenobite)
    pending_acquaintance_with(cenobite).present?
  end

  def pending_acquaintance_with(cenobite)
    acquaintances.pending.find_by(inviter: cenobite)
  end

  def request_acquaintance(with:, by: :email)
    Acquaintance.new invitee: self, inviter: with, invite_method: by
  end

  private

  def destroy_acquaintances
    acquaintances.each(&:destroy)
  end

  def update_chat_names
    ChatRoom.accessible_by(self).where.not(acquaintance_id: nil).find_each do |chat_room|
      chat_room.chat_participants.find_by(cenobite_id: id)&.update! name: name
    end
  end

  def accept_acquaintance
    acquaintance = acquaintances.find_by(inviter_id: invited_by_id)
    return unless acquaintance

    acquaintance.accept!
  end
end
