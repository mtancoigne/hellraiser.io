require 'feedjira'

require 'hell_raiser/rss'

class RssFeed < ApplicationRecord
  validates :link, presence: true
  validates :title, presence: true
  validates :url, presence: true, uniqueness: true

  belongs_to :cenobite
  has_many :rss_items, dependent: :destroy

  before_validation :complete_channel
  after_create_commit :fetch_items

  acts_as_taggable

  def fetch_items
    @feed ||= HellRaiser::Rss.fetch url

    @feed.entries.each do |item|
      RssItem.create_from(item, feed: self)
    end

    update_column :last_fetched_at, Time.current # rubocop:disable Rails/SkipsModelValidations
  end

  def host
    return @host if @host

    Rails.logger.warn "Host for #{url}"

    uri = URI.parse url
    @host = uri.host
  end

  private

  def complete_channel
    # Only complete on creation
    return if id.present?

    @feed ||= HellRaiser::Rss.fetch url
  rescue HellRaiser::Rss::InvalidFeedError
    errors.add :base, I18n.t('rss_feed.complete_channel.invalid_feed')
    false
  else
    f = HellRaiser::Rss::Feed.new @feed
    self.attributes = f.to_hash
  end
end
