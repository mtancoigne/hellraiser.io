class Vote < ApplicationRecord
  validates :cenobite_id, uniqueness: { scope: [:votable_id, :votable_type], message: I18n.t('activerecord.errors.models.vote.attribute.cenobite_id.already_voted') }

  belongs_to :votable, polymorphic: true, counter_cache: true
  belongs_to :cenobite

  after_destroy :update_score
  after_save :update_score

  private

  def update_score
    votable.refresh_score
  end
end
