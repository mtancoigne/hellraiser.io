module LikableEntity
  extend ActiveSupport::Concern

  included do
    has_many :likes, as: :likable, dependent: :delete_all

    scope :favorited_by, ->(id) { joins(:likes).where('likes.cenobite_id' => id) }
  end

  def toggle_like!(cenobite)
    like = like_from cenobite

    return like.destroy if like.present?

    like = Like.new cenobite: cenobite, likable: self

    like.save!
  end

  def like_from(cenobite)
    likes.find_by(cenobite_id: cenobite.id)
  end

  def liked?(cenobite)
    like_from(cenobite).present?
  end
end
