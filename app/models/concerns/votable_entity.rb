module VotableEntity
  extend ActiveSupport::Concern

  included do
    has_many :votes, as: :votable, dependent: :delete_all
  end

  def vote!(cenobite, approve)
    vote = vote_from cenobite
    vote ||= Vote.new cenobite: cenobite, votable: self
    vote.approve = approve

    vote.save!
  end

  def vote_from(cenobite)
    votes.find_by(cenobite_id: cenobite.id)
  end

  def voted?(cenobite)
    vote_from(cenobite).present?
  end

  def refresh_score
    update_column :score, votes.where(approve: true).count # rubocop:disable Rails/SkipsModelValidations
  end
end
