class Comment < ApplicationRecord
  PICTURE_MIMES = ['image/png', 'image/jpeg', 'image/gif'].freeze

  validates :content, presence: true
  validate :picture_format
  belongs_to :commentable, polymorphic: true
  belongs_to :cenobite

  has_one_attached :picture

  after_create :notify_subscribers
  after_create :update_notable_change_at
  after_create :notify_ac_create
  after_update :notify_ac_update
  after_destroy :notify_ac_destroy

  private

  def notify_subscribers
    return unless commentable.respond_to? :subscriptions

    subscriptions = commentable.subscriptions
    return unless subscriptions

    subscriptions.each do |subscription|
      next if cenobite_id == subscription.cenobite_id

      CommentMailer.with(comment: self, cenobite: subscription.cenobite).subscription_email.deliver_later
    end
  end

  def picture_format
    return unless picture.attached?

    errors.add(:picture, I18n.t('activerecord.errors.bad_image_format', formats.join(', '))) unless PICTURE_MIMES.include? picture.blob.content_type
  end

  def update_notable_change_at
    commentable.update_column :notable_change_at, created_at if commentable.has_attribute? :notable_change_at # rubocop:disable Rails/SkipsModelValidations
  end

  def notify_ac_create
    send_ac_notice :new
  end

  def notify_ac_update
    send_ac_notice :update
  end

  def notify_ac_destroy
    send_ac_notice :destroy
  end

  def send_ac_notice(action)
    ActionCable.server.broadcast "comments_#{commentable_type}_#{commentable_id}", { action: action, id: id }
  end
end
