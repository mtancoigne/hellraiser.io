class Event < ApplicationRecord
  validates :name, presence: true
  validates :description, presence: true
  validates :starts_at, presence: true
  validates :starts_at_timezone, presence: true, inclusion: { in: TZInfo::Timezone.all_identifiers }
  validates :ends_at, presence: true
  validates :ends_at_timezone, presence: true, inclusion: { in: TZInfo::Timezone.all_identifiers }
  validates :visibility, inclusion: { in: [0, 1] }
  validate :dates, unless: -> { persisted? && (started? || over?) }

  belongs_to :cenobite
  has_many :events_attendances, dependent: :delete_all
  has_many :attendees, class_name: 'Cenobite', source: :cenobite, through: :events_attendances
  has_many :comments, as: :commentable, dependent: :delete_all

  scope :over, -> { where('ends_at < ?', Time.current) }
  scope :not_over, -> { where('ends_at >= ?', Time.current) }
  scope :coming, -> { where('starts_at >= ?', Time.current) }
  scope :started, -> { where('starts_at <= ?', Time.current).where('ends_at > ?', Time.current) }

  def private?
    visibility.zero?
  end

  def same_day?
    starts_at.to_date === ends_at.to_date # rubocop:disable Style/CaseEquality
  end

  def over?
    persisted? && ends_at < Time.current
  end

  def started?
    persisted? && starts_at <= Time.current && !over?
  end

  def not_started_yet?
    persisted? && starts_at >= Time.current
  end

  def local_starts_at
    starts_at.in_time_zone starts_at_timezone
  end

  def local_ends_at
    ends_at.in_time_zone ends_at_timezone
  end

  def add_attendee!(cenobite)
    attendance = EventsAttendance.new event: self, cenobite: cenobite
    attendance.save

    attendance
  end

  def remove_attendee!(cenobite)
    attendance = events_attendances.find_by(cenobite: cenobite)
    raise ActiveRecord::RecordNotFound unless attendance

    attendance.destroy
  end

  def attends?(cenobite)
    events_attendances.where(cenobite_id: cenobite.id).count == 1
  end

  private

  def dates
    errors.add :starts_at, I18n.t('activerecord.errors.models.event.attribute.starts_at.is_in_the_past') if starts_at&.before? Time.current
    errors.add :ends_at, I18n.t('activerecord.errors.models.event.attribute.ends_at.ends_before_it_starts') if ends_at&.before?(starts_at)
    errors.add :ends_at, I18n.t('activerecord.errors.models.event.attribute.ends_at.ends_when_it_starts') if ends_at === starts_at # rubocop:disable Style/CaseEquality
  end
end
