class EventsAttendance < ApplicationRecord
  validates :cenobite_id, uniqueness: { scope: [:event_id, :cenobite_id] }
  validate :check_visibility

  belongs_to :event
  belongs_to :cenobite

  private

  def check_visibility
    errors.add :cenobite, I18n.t('activerecord.errors.models.event.attribute.cenobite.cant_join_this') if event.private? && event.cenobite_id != cenobite_id
  end
end
