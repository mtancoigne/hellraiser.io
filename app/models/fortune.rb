class Fortune < ApplicationRecord
  include VotableEntity

  validates :content, presence: true

  belongs_to :cenobite

  scope :random, -> { order('RANDOM()') }
end
