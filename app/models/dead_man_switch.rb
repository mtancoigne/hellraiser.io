class DeadManSwitch < ApplicationRecord
  attr_writer :update_ping

  validates :name, presence: true
  validate :validator

  belongs_to :cenobite

  before_save :update_last_pinged_at

  scope :unsent, -> { where(email_sent_at: nil) }
  scope :expired, -> { where('expires_at <= ?', Time.current) }
  scope :pingable, -> { unsent.where('expires_at > ?', Time.current) }

  def expired?
    expires_at < Time.current
  end

  def sent?
    !!email_sent_at
  end

  def ping!
    update! last_pinged_at: Time.current
  end

  def rearm!
    @update_ping = true
    update! email_sent_at: nil
  end

  def recipients_list
    list = recipients&.split("\n")&.reject(&:blank?)&.map(&:chomp)
    list || []
  end

  def notify!
    raise 'Trigger date not reached' unless expired?

    DeadManSwitchNotificationMailer.with(dead_man_switch: self).notify_email.deliver
    update! email_sent_at: Time.current

    Rails.logger.info "[DMS] Email sent for ##{id}"
  end

  def self.ping_for(cenobite)
    now = Time.current
    where(cenobite: cenobite).pingable.find_each do |dms|
      dms.update! last_pinged_at: now
    end

    true
  end

  private

  def update_last_pinged_at
    update_ping         = ActiveRecord::Type::Boolean.new.cast(@update_ping)
    self.last_pinged_at = Time.current if update_ping || !id
    self.expires_at     = last_pinged_at + ping_frequency.hours
  end

  def validator
    recipients_list.each do |address|
      errors.add :recipients, I18n.t('errors.dead_man_switches.bad_email', address: address) unless URI::MailTo::EMAIL_REGEXP.match? address
    end
  end
end
