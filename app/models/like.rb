class Like < ApplicationRecord
  validates :cenobite_id, uniqueness: { scope: [:likable_id, :likable_type], message: I18n.t('activerecord.errors.models.like.attribute.cenobite_id.already_liked') }

  belongs_to :cenobite
  belongs_to :likable, polymorphic: true
end
