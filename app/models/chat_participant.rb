class ChatParticipant < ApplicationRecord
  validates :name, presence: true
  validates :chat_room_id, uniqueness: { scope: [:chat_room_id, :cenobite_id] }

  belongs_to :cenobite
  belongs_to :chat_room
  has_many :chat_messages, dependent: :delete_all
  has_many :chat_presences, dependent: :delete_all

  after_create :ac_notify_create
  after_update :ac_notify_update

  def not_watching?
    chat_presences.not_watching.count.positive?
  end

  def watching?
    chat_presences.watching.count.positive?
  end

  private

  def ac_notify_create
    ChatChannel.broadcast_to "chat_notifications_#{cenobite_id}", { action: :chat_participant_create, chat_participant: self }
  end

  def ac_notify_update
    # Unread count change, notified to participant only
    ChatChannel.broadcast_to "chat_notifications_#{cenobite_id}", { action: :chat_participant_update, chat_participant: self } if saved_change_to_unread_count?
    return unless saved_change_to_name?

    # Other updates needs to be broadcasted to other participants
    # Vars are in camelCase to fit JS strings format
    message = { string: 'js.chat.messages.system.name_changed', vars: { oldName: previous_changes['name'][0], newName: name } }

    chat_room.add_system_message message.to_json

    # Broadcast new presence
    presence = ChatPresence.find_by(chat_room_id: chat_room_id, chat_participant_id: id)
    ChatRoomChannel.broadcast_to chat_room, { action: :presence_update, presence: presence } if presence
  end
end
