class Paste < ApplicationRecord
  validates :name, presence: true
  validates :content, presence: true

  belongs_to :cenobite

  has_secure_password validations: false
end
