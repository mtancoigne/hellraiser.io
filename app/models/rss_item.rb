class RssItem < ApplicationRecord
  include VotableEntity
  include LikableEntity

  validates :title, presence: true
  validates :link, presence: true, uniqueness: true
  validates :description, presence: true

  belongs_to :rss_feed

  class << self
    def create_from(item, feed:)
      hash = HellRaiser::Rss::Entry.new(item).to_hash
      return if RssItem.where(link: hash[:link]).count.positive?

      instance = new hash
      instance.rss_feed = feed

      if instance.valid?
        instance.save!
        return instance
      end

      Rails.logger.warn "Item #{hash[:link]} could not be saved"
    end
  end
end
