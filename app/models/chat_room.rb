class ChatRoom < ApplicationRecord
  validates :name, presence: true, unless: :acquaintance_id

  belongs_to :cenobite
  belongs_to :acquaintance, optional: true
  has_many :chat_messages, dependent: :delete_all
  has_many :chat_presences, dependent: :delete_all
  has_many :chat_participants, dependent: :delete_all

  after_create :ac_notify_create
  after_update :ac_notify_update
  after_destroy :ac_notify_destroy

  scope :accessible_by, lambda { |cenobite|
    acquaintances_ids = cenobite.acquaintances.accepted.pluck :id
    where(acquaintance_id: acquaintances_ids).or(where(acquaintance_id: nil))
  }
  def accessible_by?(cenobite)
    ChatRoom.accessible_by(cenobite).pluck(:id).include? id
  end

  def enter(cenobite, instance_id)
    raise 'Not allowed to join' unless accessible_by?(cenobite)

    participant = ChatParticipant.find_by(chat_room: self, cenobite: cenobite)
    # Join with a random name
    participant ||= ChatParticipant.create!(chat_room: self, cenobite: cenobite, name: ('a'..'z').to_a.sample(8).join)

    chat_presences.push ChatPresence.new chat_participant: participant, instance_id: instance_id
  end

  def leave(cenobite, instance_id)
    participant = ChatParticipant.find_by(chat_room: self, cenobite: cenobite)
    raise 'Participant not found' unless participant

    ChatPresence.find_by(chat_room: self, chat_participant: participant, instance_id: instance_id).destroy
  end

  def self.room_accessible_by?(room_id, cenobite)
    ChatRoom.accessible_by(cenobite).pluck(:id).include? room_id
  end

  def add_system_message(content)
    chat_messages << ChatMessage.new(content: content, system: true)
  end

  private

  def ac_notify_create
    ac_notify :create
  end

  def ac_notify_update
    ac_notify :update
  end

  def ac_notify_destroy
    ac_notify :destroy
  end

  def ac_notify(action)
    ChatChannel.broadcast_to 'chat', { action: "chat_room_#{action}", chat_room: self }
  end
end
