class Item < ApplicationRecord
  validates :name, presence: true, uniqueness: { scope: [:owner_id, :category] }
  validate :borrower_validity

  belongs_to :owner, class_name: 'Cenobite'
  belongs_to :borrower, class_name: 'Cenobite', optional: true

  before_save :set_borrowed_at

  def visible_borrower?(cenobite)
    owner == cenobite || borrower&.acquainted_with?(cenobite)
  end

  def borrowable?
    borrower_id.nil?
  end

  private

  def set_borrowed_at
    if borrower_id
      self.borrowed_at = Time.current if changes.key? :borrower_id
    else
      self.borrowed_at = nil
    end
  end

  def borrower_validity
    return unless borrower_id

    errors.add(:borrower_id, I18n.t('activerecord.errors.borrower_is_you')) if borrower_id == owner_id
    errors.add(:borrower_id, I18n.t('activerecord.errors.unknown_cenobite')) unless borrower.acquainted_with? owner
  end
end
