class Acquaintance < ApplicationRecord
  enum invite_method: { email: 0, id: 1 }

  validates :invitee_id, unique_acquaintance: true, not_self_acquaintance: true, on: :create
  validates :invite_method, with: :validate_invite_method

  belongs_to :inviter, class_name: 'Cenobite'
  belongs_to :invitee, class_name: 'Cenobite'
  has_one :chat_room, dependent: :destroy

  scope :accepted, -> { where.not(accepted_at: nil) }
  scope :pending, -> { where(accepted_at: nil) }
  scope :for_cenobite, ->(cenobite) { where(inviter: cenobite).or(where(invitee: cenobite)) }
  # Returns an array of ids of the cenobite's acquaintances.
  # Use this result to filter data that should only be seen by acquaintances.
  scope :ids_for_cenobite, lambda { |cenobite|
    for_cenobite(cenobite)
      .pluck(Arel.sql("(CASE WHEN (inviter_id = #{cenobite.id}) THEN invitee_id ELSE inviter_id END) as aquainted_id"))
  }

  after_create :notify_cenobite
  after_save :create_chat_room

  def acquainted(cenobite)
    inviter_id == cenobite.id ? invitee : inviter
  end

  def accept!
    update! accepted_at: Time.current

    # Private chatroom and participants
    chat_room = ChatRoom.create! cenobite_id: inviter_id, acquaintance_id: id
    [inviter, invitee].each { |cenobite| ChatParticipant.create(chat_room: chat_room, name: cenobite.name, cenobite: cenobite, auto_join: true) }
  end

  def accepted?
    !pending?
  end

  def pending?
    accepted_at.nil?
  end

  def send_request_email!
    notify_cenobite
  end

  def show_cenobite_id?
    invite_method == 'id'
  end

  def show_cenobite_email?(cenobite)
    (invite_method == 'email' && pending?) || (accepted? && cenobite.show_email_to_acquaintances)
  end

  def show_cenobite_name?(cenobite)
    accepted? || cenobite.visible_by_members
  end

  private

  def notify_cenobite
    AcquaintanceMailer.with(invitee: invitee, inviter: inviter).new_request_email.deliver_later if invitee.notify_new_acquaintance && invitee.confirmed?
  end

  def validate_invite_method
    return if invite_method == 'email' || (invite_method == 'id' && inviter.visible_by_members && invitee.visible_by_members)

    errors.add :invitee, I18n.t('activerecord.errors.models.acquaintance.attribute.invitee.dont_want_intempestive_invites')
  end
end
