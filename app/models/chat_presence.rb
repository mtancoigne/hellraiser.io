class ChatPresence < ApplicationRecord
  belongs_to :chat_room
  belongs_to :chat_participant

  after_create :ac_notify_create
  after_destroy :ac_notify_destroy

  scope :not_watching, -> { where watching: false }
  scope :watching, -> { where watching: true }

  def as_json(options = {})
    super(options).merge(chat_participant: { id: chat_participant.id, name: chat_participant.name })
  end

  def self.unique_presences_in(chat_room_id)
    presences = []
    where(chat_room_id: chat_room_id).reject do |presence|
      next true if presences.include? presence.chat_participant_id

      presences << presence.chat_participant_id
      false
    end
  end

  private

  def remaining_instances
    ChatPresence.where(chat_participant_id: chat_participant_id, chat_room_id: chat_room_id).count
  end

  def ac_notify_create
    ac_notify :join if remaining_instances == 1
  end

  def ac_notify_destroy
    ac_notify :leave if remaining_instances.zero?
  end

  def ac_notify(action)
    ChatRoomChannel.broadcast_to chat_room, { action: "presence_#{action}", presence: self }
  end
end
