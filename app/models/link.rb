class Link < ApplicationRecord
  include VotableEntity
  include LikableEntity

  validates :title, presence: true, length: { maximum: 255 }
  validates :url, presence: true, url: true
  validates :description, presence: true, length: { minimum: 10, maximum: 255 }
  validates :visibility, inclusion: { in: [0, 1, 2] }

  belongs_to :cenobite

  acts_as_taggable
end
