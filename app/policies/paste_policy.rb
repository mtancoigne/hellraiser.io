class PastePolicy < ApplicationPolicy
  def show?
    true
  end

  def reveal?
    show?
  end
end
