module Member
  class InvitePolicy < ApplicationPolicy
    def resend?
      unconfirmed?
    end

    def destroy?
      unconfirmed?
    end

    class Scope < Scope
      def resolve
        Cenobite.where(invited_by: cenobite, invitation_accepted_at: nil)
      end
    end

    private

    def unconfirmed?
      @record.confirmed_at.nil? && @record.invitation_accepted_at.nil?
    end
  end
end
