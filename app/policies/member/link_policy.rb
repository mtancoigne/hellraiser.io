module Member
  class LinkPolicy < MemberApplicationPolicy
    def upvote?
      @cenobite.present?
    end

    def downvote?
      upvote?
    end

    def toggle_like?
      upvote?
    end

    class Scope
      attr_reader :cenobite, :scope

      def initialize(cenobite, scope)
        @cenobite = cenobite
        @scope    = scope
      end

      def resolve
        visibility = @cenobite ? 'visibility > 0' : 'visibility > 1'
        scope.where(visibility).or(scope.where(cenobite_id: @cenobite&.id))
      end
    end
  end
end
