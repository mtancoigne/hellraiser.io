# Default policies for private controllers (with no other member access)
module Member
  class PrivateApplicationPolicy < ApplicationPolicy
    attr_reader :cenobite, :record

    def index?
      @cenobite.present?
    end

    def show?
      owner?
    end

    def create?
      @cenobite.present?
    end

    def new?
      create?
    end

    def update?
      owner?
    end

    def edit?
      update?
    end

    def destroy?
      owner?
    end

    def owner?
      @record.cenobite_id == @cenobite&.id
    end

    class Scope
      attr_reader :cenobite, :scope

      def initialize(cenobite, scope)
        @cenobite = cenobite
        @scope    = scope
      end

      def resolve
        scope.where(cenobite: @cenobite)
      end
    end
  end
end
