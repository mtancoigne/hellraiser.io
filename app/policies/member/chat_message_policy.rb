module Member
  class ChatMessagePolicy < MemberApplicationPolicy
    # NOTE: we only use the MemberApplicationPolicy as the "filter" on chat room
    # is made at the controller level.

    private

    def owner?
      @record.chat_participant.cenobite == @cenobite
    end
  end
end
