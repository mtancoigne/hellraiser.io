module Member
  class OwnedItemPolicy < MemberApplicationPolicy
    class Scope
      attr_reader :cenobite, :scope

      def initialize(cenobite, scope)
        @cenobite = cenobite
        @scope    = scope
      end

      def resolve
        scope.where(owner: @cenobite)
      end
    end

    private

    def owner?
      @record.owner_id == @cenobite&.id
    end
  end
end
