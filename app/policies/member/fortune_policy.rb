module Member
  class FortunePolicy < MemberApplicationPolicy
    def upvote?
      @cenobite.present?
    end

    def downvote?
      upvote?
    end

    def random?
      show?
    end

    class Scope < Scope
      def resolve
        scope.where cenobite_id: @cenobite.id
      end
    end
  end
end
