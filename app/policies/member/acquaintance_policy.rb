module Member
  class AcquaintancePolicy < MemberApplicationPolicy
    def accept?
      invitee?
    end

    def destroy?
      inviter? || invitee?
    end

    def resend_request?
      inviter? && @record.accepted_at.nil?
    end

    private

    def invitee?
      @record.invitee_id == @cenobite.id
    end

    def inviter?
      @record.inviter_id == @cenobite.id
    end

    class Scope
      attr_reader :cenobite, :scope

      def initialize(cenobite, scope)
        @cenobite = cenobite
        @scope    = scope
      end

      def resolve
        scope.for_cenobite @cenobite
      end
    end
  end
end
