module Member
  class DeadManSwitchPolicy < PrivateApplicationPolicy
    def ping?
      owner? && !@record.expired?
    end

    def rearm?
      owner? && @record.sent?
    end

    def ping_all?
      cenobite.present?
    end
  end
end
