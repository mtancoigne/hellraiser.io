module Member
  class CenobitePolicy < ApplicationPolicy
    def index?
      @cenobite.visible_by_members
    end

    def invite?
      @record.visible_by_members && @cenobite.visible_by_members
    end

    class Scope < Scope
      def resolve
        scope.visible
      end
    end
  end
end
