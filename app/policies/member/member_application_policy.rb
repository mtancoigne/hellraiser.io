# Default policies for member controllers (with access to other members content)
module Member
  class MemberApplicationPolicy < ApplicationPolicy
    attr_reader :cenobite, :record

    def index?
      @cenobite.present?
    end

    def show?
      true
    end

    def create?
      @cenobite.present?
    end

    def new?
      create?
    end

    def update?
      owner?
    end

    def edit?
      update?
    end

    def destroy?
      owner?
    end

    class Scope
      attr_reader :cenobite, :scope

      def initialize(cenobite, scope)
        @cenobite = cenobite
        @scope    = scope
      end

      def resolve
        scope.all
      end
    end
  end
end
