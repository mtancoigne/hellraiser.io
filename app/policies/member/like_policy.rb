module Member
  class LikePolicy < MemberApplicationPolicy
    class Scope < Scope
      def resolve
        scope.where cenobite_id: @cenobite.id
      end
    end
  end
end
