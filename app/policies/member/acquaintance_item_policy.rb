module Member
  class AcquaintanceItemPolicy < ApplicationPolicy
    def index?
      true
    end

    def new?
      create?
    end

    def show?
      ids = Acquaintance.accepted.ids_for_cenobite(cenobite)
      ids.include? record.owner_id
    end

    def create?
      false
    end

    def edit?
      update?
    end

    def update?
      false
    end

    def destroy?
      false
    end

    class Scope
      attr_reader :cenobite, :scope

      def initialize(cenobite, scope)
        @cenobite = cenobite
        @scope    = scope
      end

      def resolve
        ids = Acquaintance.accepted.ids_for_cenobite(cenobite)
        scope.where(owner_id: ids)
      end
    end
  end
end
