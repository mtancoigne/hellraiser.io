module Member
  class MotionPolicy < MemberApplicationPolicy
    def upvote?
      @cenobite.present?
    end

    def downvote?
      upvote?
    end

    def subscribe?
      @cenobite.present?
    end

    def unsubscribe?
      subscribe?
    end
  end
end
