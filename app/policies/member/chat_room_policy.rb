module Member
  class ChatRoomPolicy < MemberApplicationPolicy
    def access_messages?
      @cenobite.chat_participants.where(chat_room_id: @record.id).count == 1
    end

    def show?
      super && accessible_room?
    end

    def update?
      super && !acquaintance_room?
    end

    def destroy?
      super && !acquaintance_room?
    end

    class Scope < Scope
      def resolve
        scope.accessible_by @cenobite
      end
    end

    private

    def accessible_room?
      ChatRoom.accessible_by(@cenobite).pluck(:id).include? @record.id
    end

    def acquaintance_room?
      @record.acquaintance_id != nil
    end
  end
end
