module Member
  class ChatPresencePolicy < ApplicationPolicy
    # NOTE: we only use the MemberApplicationPolicy as the "filter" on chat room
    # is made at the controller level.
  end
end
