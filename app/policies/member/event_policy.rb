module Member
  class EventPolicy < MemberApplicationPolicy
    def show?
      @record.cenobite_id == @cenobite.id || @record.visibility == 1
    end

    def edit?
      super && !@record.over?
    end

    def attend?
      (!@record.private? || owner?) && !@record.over? && !@record.started?
    end

    def leave?
      # Allow someone to leave if visibility changed
      attend? || @record.attends?(@cenobite)
    end

    class Scope < Scope
      def resolve
        # No "public" events
        scope.where('visibility = 1').or(scope.where(cenobite_id: @cenobite&.id))
      end
    end
  end
end
