module Member
  class RssItemPolicy < MemberApplicationPolicy
    def upvote?
      @cenobite.present?
    end

    def downvote?
      upvote?
    end

    def toggle_like?
      upvote?
    end

    class Scope < Scope
      def resolve
        scope.all
      end
    end
  end
end
