class ApplicationPolicy
  attr_reader :cenobite, :record

  def initialize(cenobite, record)
    @cenobite = cenobite
    @record   = record
  end

  def index?
    true
  end

  def show?
    true
  end

  def create?
    true
  end

  def new?
    true
  end

  def update?
    true
  end

  def edit?
    true
  end

  def destroy?
    true
  end

  def owner?
    @record.cenobite_id == @cenobite&.id
  end

  class Scope
    attr_reader :cenobite, :scope

    def initialize(cenobite, scope)
      @cenobite = cenobite
      @scope    = scope
    end

    def resolve
      scope.all
    end
  end
end
