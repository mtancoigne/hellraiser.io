class NewMotionMailer < ApplicationMailer
  def notify_email
    @motion = params[:motion]
    @cenobite = params[:cenobite]
    @url = member_motion_url(@motion)
    mail(subject: email_subject, to: @cenobite.email)
  end

  private

  def email_subject
    I18n.t('new_motion_mailer.notify_email.motion_subject', title: @motion.title)
  end
end
