class AcquaintanceMailer < ApplicationMailer
  def new_request_email
    @inviter = params[:inviter]
    @invitee = params[:invitee]
    @url = member_acquaintances_url

    mail subject: I18n.t('acquaintance_mailer.new_request_email.subject'),
         to:      @invitee.email
  end
end
