class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@hellraiser.io'
  layout 'mailer'
end
