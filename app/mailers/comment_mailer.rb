class CommentMailer < ApplicationMailer
  def subscription_email
    @comment     = params[:comment]
    @commentable = @comment.commentable
    cenobite     = params[:cenobite]
    @url         = entity_url

    mail(subject: email_subject, to: cenobite.email)
  end

  private

  def email_subject
    case @commentable.class.name
    when 'Motion'
      I18n.t('comment_mailer.subscription_email.motion_subject', title: @commentable.title)
    end
  end

  def entity_url
    case @commentable.class.name
    when 'Motion'
      member_motion_url(@commentable)
    end
  end
end
