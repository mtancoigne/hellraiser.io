class DeadManSwitchNotificationMailer < ApplicationMailer
  def notify_email
    @dead_man_switch = params[:dead_man_switch]

    mail(subject: I18n.t('dead_man_switch_notification_mailer.notify_email.subject'), bcc: @dead_man_switch.recipients_list)
  end
end
