class NotSelfAcquaintanceValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, _value)
    record.errors.add attribute, I18n.t('activerecord.errors.models.acquaintance.attribute.invitee.is_you') unless record.inviter_id != record.invitee_id
  end
end
