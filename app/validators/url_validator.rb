class UrlValidator < ActiveModel::EachValidator
  REGEXES = [
    'http://',
    'https://',
    'ftp://',
    'sftp://',
    'geo:',
  ].freeze
  def validate_each(record, attribute, value)
    record.errors.add attribute, (options[:message] || "does not start by #{REGEXES.join(', ')}") unless /\A^(#{REGEXES.join('|')}).{4,}\z/.match?(value)
  end
end
