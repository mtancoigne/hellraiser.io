class UniqueAcquaintanceValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, _value)
    if Acquaintance.where(invitee_id: record.invitee_id, inviter_id: record.inviter_id)
                   .or(Acquaintance.where(inviter_id: record.invitee_id, invitee_id: record.inviter_id)).count.positive?
      record.errors.add attribute, I18n.t('activerecord.errors.models.acquaintance.attribute.invitee.already_acquainted')
    end
  end
end
