module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_cenobite

    def connect
      self.current_cenobite = find_verified_cenobite
    end

    private

    def find_verified_cenobite
      if cookies.encrypted[:_hellraiser_session] &&
         cookies.encrypted[:_hellraiser_session]['warden.user.cenobite.key']
        id = cookies.encrypted[:_hellraiser_session]['warden.user.cenobite.key'][0][0]
        if (verified_cenobite = Cenobite.find(id))
          return verified_cenobite
        end
      end
      reject_unauthorized_connection
    end
  end
end
