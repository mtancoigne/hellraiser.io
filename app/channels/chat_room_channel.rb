class ChatRoomChannel < ApplicationCable::Channel
  before_subscribe :set_room

  def subscribed
    # Exit early: @room.enter creates participants
    reject && return unless @room

    stream_for @room
    @room.enter(current_cenobite, __id__)
  end

  def unsubscribed
    # "unsubscribed" is called on rejection, so room may be nil
    @room&.leave(current_cenobite, __id__)
  end

  # Called when user displays a chat room
  def watch
    ChatParticipant.find_by(chat_room: @room, cenobite: current_cenobite).update! unread_count: 0
    ChatPresence.find_by(chat_room: @room, instance_id: __id__).update! watching: true
  end

  # Called when user switches chat room
  def unwatch
    ChatPresence.find_by(chat_room: @room, instance_id: __id__).update! watching: false
  end

  class << self
    def broadcast_message(action, message)
      string_message = ApplicationController.new.render_to_string(
        template: 'member/chat_messages/_chat_message',
        format:   :json,
        locals:   { chat_message: message }
      )

      broadcast_to message.chat_room,
                   action:  "message_#{action}",
                   message: JSON.parse(string_message)
    end
  end

  private

  def set_room
    return unless ChatRoom.room_accessible_by?(params[:id], current_cenobite)

    @room = ChatRoom.find(params[:id])
  end
end
