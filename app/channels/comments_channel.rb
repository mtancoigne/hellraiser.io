class CommentsChannel < ApplicationCable::Channel
  def subscribed
    stream_from "comments_#{params[:type]}_#{params[:id]}"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
