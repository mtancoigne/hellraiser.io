class ChatChannel < ApplicationCable::Channel
  def subscribed
    stream_for 'chat'
    stream_for "chat_notifications_#{current_cenobite.id}"
  end
end
