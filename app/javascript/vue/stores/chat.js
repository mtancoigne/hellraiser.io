import Vue from 'vue/dist/vue.esm'
import VueX from 'vuex'

import ajax from '../../helpers/ajax'
import consumer from '../../channels/consumer'

Vue.use(VueX)

function handleACRoomEvent (commit, data) {
  switch (data.action) {
    case 'presence_join':
      commit('setInState', { key: 'chatPresences', value: data.presence })
      break
    case 'presence_leave':
      commit('removeInState', { key: 'chatPresences', value: data.presence })
      break
    case 'message_create':
    case 'message_update':
      commit('setInState', { key: 'chatMessages', value: data.message })
      break
    case 'message_destroy':
      commit('removeInState', { key: 'chatMessages', value: data.message })
      break
    case 'list_presences':
      commit('setPresences', data.presences)
      break
    case 'list_messages':
      commit('setMessages', data.messages)
      break
    case 'presence_update':
      commit('updatePresence', data.presence)
      break
    // Ignoring unknown actions
  }
}

function handleACInactiveRoomEvent (commit, data) {
  switch (data.action) {
    case 'message_create':
      changeIcon(true)
      break
    // Ignoring unknown actions
  }
}

function handleACChatEvent (commit, data) {
  switch (data.action) {
    case 'chat_room_create':
    case 'chat_room_update':
      commit('setInState', { key: 'chatRooms', value: data.chat_room })
      break
    case 'chat_room_destroy':
      commit('removeChatRoom', data.chat_room)
      break
    case 'chat_participant_create':
    case 'chat_participant_update':
      commit('setInState', { key: 'chatParticipants', value: data.chat_participant })
      break
    // Ignoring unknown actions
  }
}

function loadPreference () {
  const lastRoomId = localStorage.getItem('lastRoomId')
  return { lastRoomId: lastRoomId ? Number(lastRoomId) : null }
}

function savePreference ({ id }) {
  if (id) localStorage.setItem('lastRoomId', id)
}

function changeIcon (notify) {
  const icon = document.querySelector('link[rel="icon"][type="image/x-icon"]')
  icon.href = notify ? '/favicon-notify.ico' : '/favicon.ico'
}

function watchRoom (consumer) {
  consumer.perform('watch')
}

function unWatchRoom (consumer) {
  consumer.perform('unwatch')
}

export default function (cenobiteId) {
  return new VueX.Store({
    state: {
      chatRooms: [],
      activeRoom: null, // Main room object
      roomConnections: [], // List of connected rooms
      chatPresences: [],
      chatMessages: [],
      chatParticipants: [], // Information on joined rooms
      acquaintances: [],
      cenobiteId,
      // UI
      isChatFocused: true,
      // Background errors
      messagesError: false,
      presencesError: false,
      roomsError: false,
      loadingChatData: [],
    },
    mutations: {
      setChatRooms (state, data) { state.chatRooms = data },
      setActiveRoom (state, room) {
        state.activeRoom = room
        savePreference(room)
      },
      setPresences: (state, presences) => { Vue.set(state, 'chatPresences', presences) },
      updatePresence: (state, presence) => {
        const index = state.chatPresences.findIndex(p => p.chat_participant_id === presence.chat_participant_id && p.chat_room_id === presence.chat_room_id)
        if (index > -1) {
          const keys = Object.keys(presence)
          for (const entryKey of keys) state.chatPresences[index][entryKey] = presence[entryKey]
        }
      },
      setMessages: (state, messages) => { state.chatMessages = messages },
      setConnectedToRoom (state, flag) { state.connectedToRoom = flag },
      setInState: (state, { key, value }) => {
        const index = state[key].findIndex(c => c.id === value.id)
        if (index > -1) {
          const keys = Object.keys(value)
          for (const entryKey of keys) state[key][index][entryKey] = value[entryKey]
        } else {
          state[key].push(value)
        }
        // Change icon for new messages
        if (!state.isChatFocused && key === 'chatMessages') changeIcon(true)
      },
      removeInState: (state, { key, value }) => {
        const index = state[key].findIndex(c => c.id === value.id)
        if (index === -1) return
        state[key].splice(index, 1)
      },
      setIsChatFocused: (state, active) => { state.isChatFocused = active },
      setMessagesError: (state, value) => { state.messagesError = value },
      setRoomsError: (state, value) => { state.roomsError = value },
      setPresencesError: (state, value) => { state.presencesError = value },
      removeChatRoom: (state, chatRoom) => {
        if (state.activeRoom.id === chatRoom.id) {
          // Remove active room
          state.activeRoom = null
          // Remove messages
          state.chatMessages = []
          // Clear presences
          state.chatPresences = []
        }
        // Disconnect if joined
        let index = state.roomConnections.findIndex(c => c.id === chatRoom.id)
        if (index > -1) {
          state.roomConnections[index].consumer.unsubscribe()
          state.roomConnections.splice(index, 1)
        }
        // Remove chat room
        index = state.chatRooms.findIndex(c => c.id === chatRoom.id)
        if (index === -1) return
        state.chatRooms.splice(index, 1)
      },
      setChatParticipants: (state, chatParticipants) => { state.chatParticipants = chatParticipants },
      setAcquaintances: (state, acquaintances) => { state.acquaintances = acquaintances },
      setLoadingChatData: (state, type) => { state.loadingChatData.push(type) },
      stopLoadingChatData: (state, type) => {
        const index = state.loadingChatData.findIndex(c => c === type)
        if (index > -1) state.loadingChatData.splice(index, 1)
      },
    },
    actions: {
      loadChatRooms: ({ commit, dispatch, getters }) => {
        commit('setLoadingChatData', 'rooms')
        const preference = loadPreference()
        return ajax('get', '/member/chat_rooms')
          .then((data) => {
            commit('setChatRooms', data)
            if (preference.lastRoomId && getters.chatRoom(preference.lastRoomId)) return dispatch('enterRoom', preference.lastRoomId)
          })
          .catch(() => { commit('setRoomsError', true) })
          .then(() => { commit('stopLoadingChatData', 'rooms') })
      },
      saveRoom: ({ commit, dispatch }, payload) => {
        let url = '/member/chat_rooms'
        let method = 'post'
        if (payload.chat_room.id) {
          method = 'put'
          url = `${url}/${payload.chat_room.id}`
        }

        return ajax(method, url, payload)
          .then((data) => {
            commit('setInState', {
              key: 'chatRooms',
              value: data,
            })

            // Change room on creation
            if (!payload.id) return dispatch('enterRoom', data.id)
          })
      },
      destroyRoom: ({ commit }, id) => ajax('delete', `/member/chat_rooms/${id}`)
        .then(() => { commit('removeInState', { key: 'chatRooms', value: { id } }) }),
      joinRoom: ({ getters, commit, dispatch }, id) => {
        let connection = getters.roomConnection(id)
        // Chat room not found
        if (!getters.chatRoomExists(id)) return
        // Active room ?
        if (connection && getters.activeRoomId === id) return

        if (!connection) {
          connection = { id, connected: false }
          connection.consumer = consumer.subscriptions.create({
            channel: 'ChatRoomChannel',
            id,
          }, {
            connected () {
              connection.connected = true
              if (getters.activeRoomId === id) {
                dispatch('loadRoomData', id)
                if (getters.isChatFocused) watchRoom(connection.consumer)
              }
            },
            disconnected () {
              connection.connected = false
            },
            received (data) {
              // Ignoring message, this is not the active room
              if (getters.activeRoom && getters.activeRoom.id === id) {
                handleACRoomEvent(commit, data)
              } else {
                handleACInactiveRoomEvent(commit, data)
              }
            },
            // Silently ignore rejections
          })
          commit('setInState', { key: 'roomConnections', value: connection })
        }
      },
      enterRoom: ({ getters, commit, dispatch }, id) => {
        const previousRoomId = getters.activeRoomId
        dispatch('joinRoom', id)
        commit('setActiveRoom', getters.chatRoom(id))
        dispatch('loadRoomData', id)

        // Change watching state
        if (previousRoomId && previousRoomId !== id) {
          dispatch('setWatching', { id: previousRoomId, value: false })
        }
        dispatch('setWatching', { id, value: true })
      },
      setWatching: ({ getters }, options = { id: null, value: true }) => {
        let id = null
        if (options.id) id = options.id
        else id = getters.activeRoomId

        const connection = getters.roomConnection(id)
        if (options.value) watchRoom(connection.consumer, id)
        else unWatchRoom(connection.consumer, id)
      },
      loadAcquaintances: ({ commit }) => {
        commit('setLoadingChatData', 'acquaintances')
        return ajax('get', '/member/acquaintances')
          .then((data) => { commit('setAcquaintances', data) })
          .then(() => { commit('stopLoadingChatData', 'acquaintances') })
      },
      loadParticipants: ({ commit }) => {
        commit('setLoadingChatData', 'participants')
        return ajax('get', '/member/chat_participants')
          .then((data) => { commit('setChatParticipants', data) })
          .then(() => { commit('stopLoadingChatData', 'participants') })
      },
      joinChat: ({ commit, dispatch, getters }) => {
        Promise.all([
          dispatch('loadAcquaintances'),
          dispatch('loadParticipants'),
          dispatch('loadChatRooms'),
        ])
          .then(() => {
            // Connect to channel
            consumer.subscriptions.create({ channel: 'ChatChannel' }, {
              received (data) { handleACChatEvent(commit, data) },
            })

            // Connect to auto-join chat rooms
            for (const participant of getters.chatParticipants) {
              if (participant.auto_join) dispatch('joinRoom', participant.chat_room_id)
            }
          })
      },
      sendMessage ({ getters }, message) {
        const activeRoom = getters.activeRoom
        let url = `/member/chat_rooms/${activeRoom.id}/chat_messages/`
        let method = 'post'

        if (message.id) {
          url = `/member/chat_rooms/${activeRoom.id}/chat_messages/${message.id}`
          method = 'put'
        }

        return ajax(method, url, { chat_message: message })
      },
      sendFile ({ getters }, file) {
        const activeRoom = getters.activeRoom
        const form = new FormData()
        form.append('chat_message[file]', file)
        return ajax('post', `/member/chat_rooms/${activeRoom.id}/chat_messages/`, form)
      },
      setIsChatFocused ({ commit }, active) {
        if (active) {
          changeIcon(false)
        }
        commit('setIsChatFocused', active)
      },
      loadRoomData ({ getters, commit }, id) {
        const activeRoom = getters.activeRoom
        // Skip if not current room
        if (!activeRoom || activeRoom.id !== id) return

        commit('setMessagesError', false)
        commit('setPresencesError', false)
        commit('setLoadingChatData', 'messages')
        commit('setLoadingChatData', 'presences')

        ajax('get', `/member/chat_rooms/${id}/chat_messages`)
          .then((data) => { commit('setMessages', data) })
          .catch(() => commit('setMessagesError', true))
          .then(() => { commit('stopLoadingChatData', 'messages') })
        ajax('get', `/member/chat_rooms/${id}/chat_presences`)
          .then((data) => { commit('setPresences', data) })
          .catch(() => commit('setPresencesError', true))
          .then(() => { commit('stopLoadingChatData', 'presences') })
      },
      updateParticipant: ({ commit }, payload) => ajax('put', `/member/chat_participants/${payload.id}`, { chat_participant: payload })
        .then((data) => commit('setInState', { key: 'chatParticipants', value: data })),
    },
    getters: {
      // Only public chat rooms
      chatRooms: state => state.chatRooms
        .filter(c => c.acquaintance_id === null)
        .sort((a, b) => a.name.localeCompare(b.name)),
      // Rooms with acquaintances
      acquaintanceChatRooms: (state, getters) => state.chatRooms
        .filter(c => c.acquaintance_id !== null)
        .sort((a, b) => {
          const acquaintance1 = getters.acquaintance(a.acquaintance_id)
          const acquaintance2 = getters.acquaintance(b.acquaintance_id)
          if (!acquaintance1 || !acquaintance2) return 0
          return acquaintance1.cenobite.name.localeCompare(acquaintance2.cenobite.name)
        }),
      chatRoom: state => id => state.chatRooms.find(c => c.id === id),
      chatRoomExists: (state, getters) => id => !!getters.chatRoom(id),
      activeRoom: state => state.activeRoom,
      activeRoomId: state => state.activeRoom ? state.activeRoom.id : null,
      roomConnections: state => state.roomConnections,
      roomConnection: state => id => state.roomConnections.find(c => c.id === id),
      chatPresences: state => state.chatPresences.sort((a, b) => a.chat_participant.name.localeCompare(b.chat_participant.name)),
      chatMessages: state => state.chatMessages.sort((a, b) => new Date(a.created_at) - new Date(b.created_at)),
      chatMessagesByDate: (state, getters) => {
        const list = new Map()
        getters.chatMessages.forEach((message) => {
          const creationDate = new Date(message.created_at)
          const day = new Date(creationDate.getTime() - (creationDate.getTimezoneOffset() * 60000))
            .toISOString()
            .split('T')[0]
          const group = list.get(day)
          if (!group) list.set(day, [message])
          else group.push(message)
        })
        return list
      },
      lastSentMessage: (state, getters) => {
        const participant = getters.chatParticipant
        if (!participant) return null
        const lastMessages = getters.chatMessages.filter(m => m.chat_participant_id === participant.id)
        if (lastMessages.length === 0) return null
        return lastMessages[lastMessages.length - 1]
      },
      cenobiteId: state => state.cenobiteId,
      chatMessagesError: state => state.messagesError,
      chatPresencesError: state => state.presencesError,
      chatRoomsError: state => state.roomsError,
      chatParticipants: (state) => state.chatParticipants,
      chatParticipant: (state) => {
        if (!state.activeRoom) return null
        return state.chatParticipants.find(c => c.chat_room_id === state.activeRoom.id)
      },
      chatParticipantFor: state => id => state.chatParticipants.find(c => c.chat_room_id === id),
      acquaintances: state => state.acquaintances,
      acquaintance: state => id => state.acquaintances.find(a => a.id === id),
      inAcquaintanceRoom: (state, getters) => getters.activeRoom && getters.activeRoom.acquaintance_id !== null,
      activeAcquaintance: (state, getters) => getters.activeRoom ? getters.acquaintance(getters.activeRoom.acquaintance_id) : null,
      loadingChatData: state => state.loadingChatData.length > 0,
      loadingChatRooms: state => state.loadingChatData.filter(l => l === 'rooms').length > 0,
      isChatFocused: state => state.isChatFocused,
    },
  })
}
