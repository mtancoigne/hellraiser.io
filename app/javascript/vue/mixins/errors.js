import ApiError from '../views/_api_error.vue'

export default {
  components: { ApiError },
  data () {
    return {
      errors: false,
    }
  },
  methods: {
    errorHandler (request) {
      switch (request.status) {
        case 0:
          this.errors = { network: [this.$t('generic.errors.network')] }
          break
        case 404:
          this.errors = { request: [this.$t('generic.errors.not_found')] }
          break
        case 500:
          this.errors = { server: [this.$t('generic.errors.server_fault')] }
          break
        default:
          try {
            this.errors = JSON.parse(request.response)
          } catch (e) {
            if (e instanceof SyntaxError) this.errors = { server: [this.$t('generic.errors.malformed_response')] }
            else this.errors = { server: [this.$t('generic.errors.unknown_error')] }
          }
      }
    },
  },
}
