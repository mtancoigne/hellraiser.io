// Taken from
// https://github.com/Krazete/bookmarklets/blob/master/tri.js
// License unspecified

(function () {
  const tri = {
    // LIMIT; min 0, max 1, step 0.03125, default 0.125
    limit: 0.125,
    // GAP; min 0, max 512, step 32, default 128
    gap: 10,
    // SAG; min -0,25, max 1, step 0.03125, default 0
    sag: 0,
    // FOV; min: 7, max 13, step 1, default 10
    fov: 13,
    cssDynamic: document.createElement('style'),
    orientation: {
      yaw: 0,
      pitch: 0,
      roll: 0,
    },
    mouseMove: function (e) {
      tri.orientation.yaw = -Math.cos(Math.PI * e.clientX / window.innerWidth) * 180 * tri.limit
      tri.orientation.pitch = Math.cos(Math.PI * e.clientY / window.innerHeight) * 180 * tri.limit
      tri.updateBody()
    },
    gyroMove: function (e) {
      const landscape = window.innerWidth > window.innerHeight
      if (landscape) {
        tri.orientation.yaw = -(e.alpha + e.beta)
        tri.orientation.pitch = e.gamma - Math.sign(90 + Math.abs(e.beta)) * 45
      } else {
        tri.orientation.yaw = -(e.alpha + e.gamma)
        tri.orientation.pitch = e.beta - 45
      }
      tri.updateBody()
    },
    updateOrigin: function () {
      document.body.style.transformOrigin = (window.innerWidth / 2 + window.pageXOffset) + 'px ' + (window.innerHeight / 2 + window.pageYOffset) + 'px'
    },
    updateBody: function () {
      document.body.style.transform = 'perspective(' + Math.pow(2, tri.fov) + 'px) translateZ(-' + tri.gap + 'px) rotateX(' + tri.orientation.pitch + 'deg) rotateY(' + tri.orientation.yaw + 'deg)'
    },
    init: function () {
      tri.cssDynamic.innerHTML = 'html{background-color: var(--panel-bg)}'
      document.querySelector('body').appendChild(tri.cssDynamic)
      window.addEventListener('deviceorientation', tri.gyroMove)
      window.addEventListener('mousemove', tri.mouseMove)
      window.addEventListener('scroll', tri.updateOrigin)
      window.addEventListener('resize', tri.updateOrigin)
      window.scrollBy(0, 1)
    },
  }
  tri.init()
})()
