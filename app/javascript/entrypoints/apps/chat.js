import Vue from 'vue/dist/vue.esm'
import Vuebar from 'vuebar'
import store from '../../vue/stores/chat'
import i18n from '../../vue/helpers/i18n'

import ApiErrorList from '../../vue/views/api_error/_list.vue'
import ChatForm from '../../vue/views/chat/_form.vue'
import ChatMessages from '../../vue/views/chat/_messages.vue'
import ChatPanel from '../../vue/views/chat/_panel.vue'
import ChatStatus from '../../vue/views/chat/_status.vue'
import ChatRooms from '../../vue/views/chat/_rooms.vue'

import ErrorMixin from '../../vue/mixins/errors'

Vue.use(Vuebar)

window.ChatView = function (userId = null) {
  new Vue({
    el: '#chat',
    store: store(userId),
    components: { ApiErrorList, ChatForm, ChatMessages, ChatPanel, ChatRooms, ChatStatus },
    mixins: [ErrorMixin],
    i18n,
    computed: {
      activeRoom () { return this.$store.getters.activeRoom },
    },
    methods: {
      documentBlurHandler () { this.$store.dispatch('setIsChatFocused', false) },
      documentFocusHandler () { this.$store.dispatch('setIsChatFocused', true) },
      chatClickHandler () {
        const openModals = document.querySelectorAll('.modal.active').length
        if (!window.getSelection().isCollapsed || openModals > 0) return

        $_('chat-form-input').focus()
      },
      initialize () {
        this.loading = true
        this.errors = false
      },
    },
    created () {
      if (!document.hasFocus()) this.$store.dispatch('setIsChatFocused', false)
      window.addEventListener('blur', this.documentBlurHandler)
      window.addEventListener('focus', this.documentFocusHandler)

      this.initialize()
      this.$store.dispatch('joinChat')
    },
  }).$mount()
}
