import Vue from 'vue/dist/vue.esm'
import CommentForm from '../../vue/views/comments/_form.vue'
import CommentEntry from '../../vue/views/comments/_comment.vue'
import ajax from '../../helpers/ajax'
import consumer from '../../channels/consumer'
import ErrorMixin from '../../vue/mixins/errors'
import i18n from '../../vue/helpers/i18n'

window.CommentsView = function ({ entity, url, userId = null }) {
  new Vue({
    el: '#comments',
    i18n,
    components: { CommentForm, CommentEntry },
    mixins: [ErrorMixin],
    data () {
      return {
        userId,
        entityURL: url,
        comments: [],
        loading: false,
        connected: false,
        errors: false,
      }
    },
    computed: {
      sortedComments () {
        // .slice makes a copy of the array, instead of mutating the original
        return this.comments.slice(0).sort((a, b) => {
          a = new Date(a.created_at)
          b = new Date(b.created_at)
          return a < b ? -1 : a > b ? 1 : 0
        })
      },
    },
    methods: {
      set (payload) {
        const index = this.comments.findIndex(c => c.id === payload.id)
        if (index > -1) Vue.set(this.comments, index, payload)
        else this.comments.push(payload)
      },
      remove (id) {
        const index = this.comments.findIndex(c => c.id === id)
        if (index > -1) Vue.delete(this.comments, index)
      },
      loadComments () {
        ajax('get', url)
          .then((data) => { this.comments = data })
          .catch((errors) => { this.errorHandler(errors) })
          .then(() => { this.loading = false })
      },
      loadComment (id) {
        this.loading = true
        ajax('get', `/member/comments/${id}`)
          .then((data) => { this.set(data) })
          .catch((errors) => { this.errorHandler(errors) })
          .then(() => { this.loading = false })
      },
      handleNewMessage (actionCableData) {
        switch (actionCableData.action) {
          case 'new' :
          case 'update' :
            this.loadComment(actionCableData.id)
            break
          case 'destroy':
            this.remove(actionCableData.id)
            break
          // Ignoring unsupported actions
        }
      },
      connectAC () {
        const that = this
        consumer.subscriptions.create({ channel: 'CommentsChannel', type: entity.type, id: entity.id }, {
          connected () { that.connected = true },
          disconnected () { that.connected = false },
          received (data) { that.handleNewMessage(data) },
        })
      },
    },
    created () {
      this.connectAC()
      this.loadComments()
    },
  }).$mount()
}
