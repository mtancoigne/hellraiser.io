import Vue from 'vue/dist/vue.esm'

import i18n from '../../vue/helpers/i18n'

function daysInMonth (month, year) {
  return new Date(year, month + 1, 0).getDate()
}

function firstDayOfMonth (referenceDate) {
  const date = new Date(referenceDate)
  date.setDate(1)
  return date.getDay()
}

function fillMonthDays (targetDate) {
  // Don't touch the date
  const targetDateTime = new Date(targetDate)
  const days = []

  const amountOfDaysInMonth = daysInMonth(targetDateTime.getMonth(), targetDateTime.getFullYear())
  const firstWeekDay = firstDayOfMonth(targetDateTime)

  let amountToBuild = firstWeekDay + amountOfDaysInMonth
  amountToBuild = Math.ceil(amountToBuild / 7) * 7

  targetDateTime.setDate(-firstWeekDay + 1)

  // Fill days
  for (let i = 0; i < amountToBuild; i++) {
    const newDay = new Date(targetDateTime)
    newDay.setDate(newDay.getDate() + i)
    days.push(newDay)
  }

  return days
}

const EVENT = new Event('change')

window.dateTimePicker = function (targetElement, selectedDate) {
  const date = new Date(Date.parse(selectedDate))

  new Vue({
    el: targetElement,
    i18n,
    data () {
      return {
        selectedDate: date,
        today: new Date(),
        selectedHours: date.getHours(),
        selectedMinutes: date.getMinutes(),
        daysOfMonth: [],
        selectedDateIso: date.toISOString(),
        detectedTimeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
      }
    },
    methods: {
      buildMonth () {
        this.daysOfMonth = fillMonthDays(this.selectedDate)
      },
      isSameDay (firstDate, secondDate) {
        return (firstDate.getFullYear() === secondDate.getFullYear())
          && (firstDate.getMonth() === secondDate.getMonth())
          && (firstDate.getDate() === secondDate.getDate())
      },
      isSameMonth (firstDate, secondDate) {
        return (firstDate.getFullYear() === secondDate.getFullYear())
          && (firstDate.getMonth() === secondDate.getMonth())
      },
      notifyChange () {
        this.selectedDateIso = this.selectedDate.toISOString()
        this.$nextTick(() => {
          this.$refs.hiddenInput.dispatchEvent(EVENT)
        })
      },
      setDate (date) {
        const newDate = new Date(this.selectedDate)
        newDate.setMonth(date.getMonth())
        newDate.setDate(date.getDate())
        this.selectedDate = newDate
        this.notifyChange()
      },
      setHours () {
        const hours = parseInt(this.selectedHours, 10)
        const date = new Date(this.selectedDate)
        if (hours >= 0 && hours <= 23) date.setHours(hours)
        this.selectedDate = date
        this.notifyChange()
      },
      setMinutes () {
        const minutes = parseInt(this.selectedMinutes, 10)
        const date = new Date(this.selectedDate)
        if (minutes >= 0 && minutes <= 59) date.setMinutes(minutes)
        this.selectedDate = date
        this.notifyChange()
      },
      changeMonth (amount) {
        const month = this.selectedDate.getMonth()
        const date = new Date(this.selectedDate)
        date.setMonth(month + amount)
        this.selectedDate = date
        this.buildMonth()
        this.notifyChange()
      },
      dayCssClasses (date) {
        return {
          'datetime_picker__day--today': this.isSameDay(date, this.today),
          'datetime_picker__day--out_of_range': !this.isSameMonth(date, this.selectedDate),
          'datetime_picker__day--selected': this.isSameDay(date, this.selectedDate),
        }
      },
      niceDate (date) {
        return this.$d(date, 'medium')
      },
      dayName (date) {
        return this.$d(date, 'day_name')
      },
    },
    created () {
      this.buildMonth()
    },
  }).$mount()
}
