window.toggleDetails = function (id) {
  const trigger = document.querySelector(`[data-details-trigger="${id}"]`)
  if (!trigger) return false

  const newState = trigger.getAttribute('data-details-state') === 'closed'
  trigger.setAttribute('data-details-state', newState ? 'opened' : 'closed')
  trigger.innerText = newState ? '▲' : '▼'
  document.querySelectorAll(`[data-details="${id}"`).forEach(function (element) {
    element.classList.toggle('collapsed', !newState)
  })
}
