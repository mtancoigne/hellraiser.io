const dateErrorEndNoticeElt = $_('date-error-end-notice')
const dateErrorStartNoticeElt = $_('date-error-start-notice')
const submitButtonElt = $_('event-submit-button')
const startDateFieldId = 'picker-event-starts-at_selected_date'
const endDateFieldId = 'picker-event-ends-at_selected_date'

function show (element, display = 'block') {
  element.style.display = display
}

function hide (element) {
  element.style.display = 'none'
}

function enable (element) {
  element.removeAttribute('disabled')
}

function disable (element) {
  element.setAttribute('disabled', 'disabled')
}

function getDate (inputId) {
  const element = $_(inputId)
  return new Date(Date.parse(element.value))
}

function checkDates (firstFieldId, secondFieldId) {
  const firstDate = getDate(firstFieldId)
  const secondDate = getDate(secondFieldId)
  let errors = 0

  if (firstDate - new Date() <= 0) {
    errors++
    show(dateErrorStartNoticeElt)
  } else {
    hide(dateErrorStartNoticeElt)
  }

  if (firstDate - secondDate >= 0) {
    errors++
    show(dateErrorEndNoticeElt)
  } else {
    hide(dateErrorEndNoticeElt)
  }

  if (errors === 0) {
    enable(submitButtonElt)
  } else {
    disable(submitButtonElt)
  }
}

function attachChangeEvents (fieldId) {
  $_(fieldId).addEventListener('change', () => {
    checkDates(startDateFieldId, endDateFieldId)
  })
}

window.addEventListener('load', () => {
  attachChangeEvents(startDateFieldId)
  attachChangeEvents(endDateFieldId)
  checkDates(startDateFieldId, endDateFieldId)
})
