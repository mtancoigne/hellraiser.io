import MarkdownIt from 'markdown-it'
import implicitFigures from 'markdown-it-implicit-figures'
import modifyToken from 'markdown-it-modify-token'

import hljs from '../helpers/highlight'

function mdTokenModifier (token) {
  // "Secure" links to the outside world
  if (token.type === 'link_open') {
    const rx = new RegExp(`^https?://${window.location.host}(?:/.*)?$`)

    if (!rx.test(token.attrObj.href)) {
      token.attrObj.target = '_blank'
      token.attrObj.rel = 'noreferrer noopener'
    }
  }
}

function mdHighlighter (str, lang) {
  if (lang && hljs.getLanguage(lang)) {
    try {
      return hljs.highlight(lang, str).value
    } catch (__) {}
  }

  return '' // use external default escaping
}

const mdConfig = {
  linkify: true,
  typographer: true,
  highlight: mdHighlighter,
  modifyToken: mdTokenModifier,
}

const md = new MarkdownIt(mdConfig)

md.use(modifyToken)
md.use(implicitFigures, {
  // <figure data-type="image">, default: false
  dataType: true,
  // <figcaption>alternative text</figcaption>, default: false
  figcaption: false,
  // <figure tabindex="1+n">..., default: false
  tabindex: false,
  // <a href="img.png"><img src="img.png"></a>, default: false
  link: false,
})

window.markdown = function (text) {
  return md.render(text)
}

window.renderMD = function (parent, element) {
  const content = element.innerHTML
    .replace(/&gt;/g, '>')
    .replace(/&lt;/g, '<')
  parent.innerHTML = window.markdown(content)
}
