import ajax from '../helpers/ajax'
import sanitize from '../helpers/escapeHTML'

window.revealPaste = function (event, id, password, target, form, feedbackArea) {
  event.preventDefault()

  const $password = document.querySelector(`[data-target="${password}"]`)
  const $content = document.querySelector(`[data-target="${target}"]`)
  const $form = document.querySelector(`[data-target="${form}"]`)
  const $feedbackArea = document.querySelector(`[data-target="${feedbackArea}"]`)
  if ($feedbackArea) {
    $feedbackArea.classList = []
    $feedbackArea.innerHTML = ''
  }
  let payload = null

  if ($password && $password.value) {
    payload = { paste: { password: $password.value } }
  }

  const method = $password ? 'PUT' : 'GET'
  ajax(method, `${id}/reveal`, payload)
    .then((result) => {
      $form.parentElement.removeChild($form)
      $content.innerHTML = sanitize(result.content)
    })
    .catch((response) => {
      $feedbackArea.classList = 'alert alert--alert'
      switch (response.status) {
        case 0:
          $feedbackArea.innerHTML = I18n.t('js.pastes.network_error')
          break
        case 422:
          $feedbackArea.innerHTML = I18n.t('js.pastes.incorrect_password')
          break
        default:
          $feedbackArea.innerHTML = `${I18n.t('js.pastes.error')} ${sanitize(JSON.parse(response.response).error)}`
      }
    })
}
