import Rails from '@rails/ujs'
import * as ActiveStorage from '@rails/activestorage'

window.$_ = function (id) {
  return document.getElementById(id)
}

Rails.start()
ActiveStorage.start()
