function replaceElement (button, text, cssClass) {
  const replacementText = document.createElement('span')
  replacementText.innerHTML = text
  replacementText.classList.add(cssClass)
  button.replaceWith(replacementText)
}

document.body.addEventListener('ajax:success', function (e) {
  const button = e.target.querySelector('[data-toggle-type]')
  if (!button) return

  const requestId = button.getAttribute('data-request-id')

  switch (button.getAttribute('data-toggle-type')) {
    case 'create_acquaintance_request':
      replaceElement(button, I18n.t('js.acquaintances.request_sent'), 'text--success')
      break
    case 'destroy_acquaintance_request':
      if (requestId) {
        // May be a button or an input
        document.querySelector(`[data-request-id="${requestId}"][data-toggle-type="accept_acquaintance_request"]`).remove()
      }
      replaceElement(button, I18n.t('js.acquaintances.request_destroyed'), 'text--alert')
      break
    case 'accept_acquaintance_request':
      // May be a button or an input
      document.querySelector(`[data-request-id="${requestId}"][data-toggle-type="destroy_acquaintance_request"]`).remove()
      replaceElement(button, I18n.t('js.acquaintances.request_accepted'), 'text--success')
      break
  }
})
