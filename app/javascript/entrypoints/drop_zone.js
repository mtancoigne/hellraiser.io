import prettyBytes from '../helpers/pretty_bytes'

function DropZone (dropZone, noticeZone, field, options = {
  allowedMimes: [],
  multiple: false,
}) {
  const allowedMimes = options.allowedMimes || []
  const multiple = options.multiple || false
  const originalContent = noticeZone.innerHTML
  let dragging = false

  const handleFileChange = function () {
    if (checkFiles(field.files)) updateFileList()
  }

  const handleDrop = function (event) {
    event.preventDefault()
    event.stopPropagation()

    resetZone()

    const files = extractFiles(event.dataTransfer)
    if (!checkFiles(files)) return

    field.files = files
    updateFileList()
  }

  const handleDragOver = function (event) {
    event.preventDefault()
    event.stopPropagation()
    if (dragging) return

    dragging = true
    dropZone.classList.add('drop-zone--over')
    setNotice(I18n.t('js.dropzone.drop_it'))
  }

  const handleDragLeave = function (event) {
    event.preventDefault()
    event.stopPropagation()
    if (!dragging) return

    resetZone()
  }

  const resetZone = function () {
    noticeZone.innerHTML = originalContent
    dragging = false
    dropZone.classList.remove('drop-zone--over')
    dropZone.classList.remove('drop-zone--error')
  }

  const extractFiles = function (dataTransfer) {
    if (dataTransfer.files && dataTransfer.files.length > 0) {
      return dataTransfer.files
    } else {
      setNotice(I18n.t('js.dropzone.no_file_selected'), 'error')
    }
  }

  const checkFiles = function (items) {
    if (!items || items.length === 0) {
      setNotice(I18n.t('js.dropzone.no_file_dropped'), 'error')
      return false
    }

    if (!multiple && items.length > 1) {
      setNotice(I18n.t('js.dropzone.too_much_files'), 'error')
      return false
    } else {
      for (const item of items) {
        if (!checkAllowedMimes(item.type)) {
          if (multiple) {
            setNotice(I18n.t('js.dropzone.multiple_wrong_type'), 'error')
          } else {
            setNotice(I18n.t('js.dropzone.one_wrong_type'), 'error')
          }
          return false
        }
      }
    }

    return true
  }

  const checkAllowedMimes = function (type) {
    if (allowedMimes.length === 0) return true

    return allowedMimes.indexOf(type) > -1
  }

  const setNotice = function (text, type = null) {
    noticeZone.innerHTML = text
    if (type) dropZone.classList.add(`drop-zone--${type}`)
  }

  const updateFileList = function () {
    let string = ''
    if (field.files.length > 1) {
      const names = []
      for (const file of field.files) names.push(file.name)
      string = names.join(', ')
    } else {
      string = `${field.files[0].name} - ${prettyBytes(field.files[0].size)}`
    }

    resetZone()
    setNotice(string)
  }

  dropZone.addEventListener('dragover', handleDragOver)
  dropZone.addEventListener('dragleave', handleDragLeave)
  dropZone.addEventListener('drop', handleDrop)
  field.addEventListener('change', handleFileChange)
}

window.DropZone = DropZone
