import hljs from '../helpers/highlight'

window.highlight = function (target, lang) {
  const element = $_(target)
  element.innerHTML = hljs.highlight(lang, element.innerHTML).value
}
