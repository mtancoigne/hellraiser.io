function updateVoteElements (id, details) {
  const button = document.querySelector(`[data-toggle-type="${details.vote}"][data-toggle-id="${id}"]`)
  const inverseButtonAction = details.vote === 'upvote' ? 'downvote' : 'upvote'
  const inverseButton = document.querySelector(`[data-toggle-type="${inverseButtonAction}"][data-toggle-id="${id}"]`)
  const scoreElement = document.querySelector(`[data-score-id="${id}"]`)
  const score = details.score / details.votes_count

  button.setAttribute('disabled', 'disabled')
  inverseButton.removeAttribute('disabled')

  scoreElement.innerHTML = `${details.score}/${details.votes_count}`

  scoreElement.classList.remove('score--hidden')

  if (score > 0.5) {
    scoreElement.classList.remove('score--negative')
    scoreElement.classList.add('score--positive')
  } else if (score < 0.5) {
    scoreElement.classList.add('score--negative')
    scoreElement.classList.remove('score--positive')
  } else {
    scoreElement.classList.remove('score--negative')
    scoreElement.classList.remove('score--positive')
  }
}

document.body.addEventListener('ajax:success', function (e) {
  const button = e.target.querySelector('[data-toggle-type]')
  if (!button) return

  switch (button.getAttribute('data-toggle-type')) {
    case 'favorite':
      const liked = e.detail[0].liked /* eslint-disable-line no-case-declarations */
      button.classList.remove(liked ? 'unfavorite' : 'favorite')
      button.classList.add(liked ? 'favorite' : 'unfavorite')
      button.innerText = liked ? I18n.t('generic.unlike') : I18n.t('generic.like')
      break
    case 'upvote':
    case 'downvote':
      updateVoteElements(button.getAttribute('data-toggle-id'), e.detail[0])
      break
  }
})
