import * as CodeMirror from 'codemirror/lib/codemirror'

// Languages
// Must be synced with extMime variable (in this file).
import 'codemirror/mode/cmake/cmake'
import 'codemirror/mode/coffeescript/coffeescript'
import 'codemirror/mode/clike/clike' // C, C++, C#, Java, Scala
import 'codemirror/mode/css/css' // CSS, Less, SCSS
import 'codemirror/mode/diff/diff'
import 'codemirror/mode/dockerfile/dockerfile'
import 'codemirror/mode/gfm/gfm' // Github Flavored Markdown
import 'codemirror/mode/gherkin/gherkin'
import 'codemirror/mode/go/go'
import 'codemirror/mode/groovy/groovy'
import 'codemirror/mode/haml/haml'
import 'codemirror/mode/htmlmixed/htmlmixed' // HTML and other
import 'codemirror/mode/htmlembedded/htmlembedded' // Other and HTML
import 'codemirror/mode/handlebars/handlebars'
import 'codemirror/mode/haskell/haskell'
import 'codemirror/mode/javascript/javascript' // TS, JSON, ...
import 'codemirror/mode/markdown/markdown'
import 'codemirror/mode/nginx/nginx'
import 'codemirror/mode/perl/perl'
import 'codemirror/mode/php/php'
import 'codemirror/mode/puppet/puppet'
import 'codemirror/mode/python/python'
import 'codemirror/mode/r/r'
import 'codemirror/mode/ruby/ruby'
import 'codemirror/mode/rust/rust'
import 'codemirror/mode/scheme/scheme'
import 'codemirror/mode/shell/shell'
import 'codemirror/mode/twig/twig'
import 'codemirror/mode/yaml/yaml'

window.CodeMirror = CodeMirror

// Global options for Hellraiser
const baseConfiguration = {
  lineNumbers: true,
  theme: 'zenburn',
}

// Generic config per language
const languageConfig = {
  markdown: {
    mode: 'gfm',
    gitHubSpice: false,
    emoji: false,
  },
}

// File extension and mode
const extMime = {
  aspx: { mode: 'application/x-aspx' },
  c: { mode: 'text/x-c' },
  cmake: { mode: 'text/x-cmake' },
  cpp: { mode: 'text/x-c++src' },
  cs: { mode: 'application/vnd.coffeescript' },
  css: { mode: 'text/css' },
  diff: { mode: 'text/x-diff' },
  dockerfile: { mode: 'text/x-dockerfile' },
  ejs: { mode: 'application/x-ejs' },
  erb: { mode: 'application/x-erb' },
  gherkin: { mode: 'text/x-feature' },
  go: { mode: 'text/x-go' },
  groovy: { mode: 'text/x-groovy' },
  h: { mode: 'text/x-c' },
  haml: { mode: 'text/x-haml' },
  handlebars: { mode: 'text/x-handlebars-template' },
  haskell: { mode: 'text/x-haskell' },
  html: { mode: 'text/html' },
  java: { mode: 'text/x-java' },
  js: { mode: 'application/javascript' },
  json: { mode: 'application/json' },
  jsp: { mode: 'application/x-jsp' },
  less: { mode: 'text/x-less' },
  markdown: languageConfig.markdown,
  md: languageConfig.markdown,
  mdown: languageConfig.markdown,
  nginx: { mode: 'text/x-nginx-conf' },
  perl: { mode: 'text/x-perl' },
  php: { mode: 'application/x-httpd-php' },
  puppet: { mode: 'text/x-puppet' },
  python: { mode: 'text/x-python' },
  r: { mode: 'text/x-rsrc' },
  ruby: { mode: 'text/x-ruby' },
  rust: { mode: 'text/rust' },
  scheme: { mode: 'text/x-scheme' },
  scss: { mode: 'text/x-scss' },
  sh: { mode: 'application/x-sh' },
  shell: { mode: 'text/x-sh' },
  text: { mode: 'text/plain' },
  ts: { mode: 'application/typescript' },
  twig: { mode: 'text/x-twig' },
  txt: { mode: 'text/plain' },
  xml: { mode: 'application/xml' },
  yaml: { mode: 'text/yaml' },
}

// Merge configuration
function configFor (language) {
  const config = extMime[language] || { mode: language }
  return { ...baseConfiguration, ...config }
}

window.codeMirrorArea = function (id, mode, options = {}, changeCallback = null) {
  const element = $_(id)
  const cm = {
    element,
    defaults: {},
    instance: CodeMirror.fromTextArea(element),
    applyMode (mode) {
      // Set new options
      const newOptions = { ...this.defaults, ...configFor(mode), ...options }
      // Backup default config and apply new one
      for (const o of Object.keys(newOptions)) {
        if (this.defaults[o] === undefined) this.defaults[o] = this.instance.getOption(o)
        this.instance.setOption(o, newOptions[o])
      }
    },
  }

  cm.applyMode(mode)

  // This is not optimal but CodeMirror only fills the textarea when form is
  // submitted which is an issue with required fields
  const required = ['required', true].indexOf(element.getAttribute('required')) > -1
  if (typeof changeCallback === 'function') {
    cm.instance.on('change', changeCallback)
  } else if (required) {
    cm.instance.on('change', function () {
      element.value = cm.instance.getValue()
    })
  }

  return cm
}

export default CodeMirror
