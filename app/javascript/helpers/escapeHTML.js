export default function (string) {
  const tmp = document.createElement('div')
  tmp.appendChild(document.createTextNode(string))

  return tmp.innerHTML
}
