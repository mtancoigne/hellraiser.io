const units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
export default function (num, precision = 3, addSpace = true) {
  if (Math.abs(num) < 1) return num + (addSpace ? ' ' : '') + units[0]
  const exponent = Math.min(Math.floor(Math.log10(num < 0 ? -num : num) / 3), units.length - 1)
  const n = Number(((num < 0 ? -num : num) / 1000 ** exponent).toPrecision(precision))
  return (num < 0 ? '-' : '') + n + (addSpace ? ' ' : '') + units[exponent]
};
