export default {
  countDraggedFiles (event) {
    if (event.dataTransfer.items) {
      let files = 0
      for (let i = 0; i < event.dataTransfer.items.length; i++) {
        if (event.dataTransfer.items[i].kind === 'file') files++
      }

      return files
    } else {
      return event.dataTransfer.files.length
    }
  },
  extractFiles (event) {
    const files = []

    if (event.dataTransfer.items) {
      for (let i = 0; i < event.dataTransfer.items.length; i++) {
        if (event.dataTransfer.items[i].kind === 'file') {
          files.push(event.dataTransfer.items[i].getAsFile())
        }
      }
    } else {
      const files = []
      for (let i = 0; i < event.dataTransfer.files.length; i++) {
        files.push(event.dataTransfer.files[i])
      }
    }

    return files
  },
}
