import extractCSRF from './csrf_token'

export default function (method, url, data = {}) {
  if (!data) data = {} // When a null value is passed
  const token = extractCSRF()
  const request = new window.XMLHttpRequest()

  request.open(method, url, true)
  request.setRequestHeader('Accept', 'application/json')
  if ((typeof data.append) !== 'function') {
    request.setRequestHeader('Content-Type', 'application/json')
    data = JSON.stringify(data)
  }

  if (token) {
    request.setRequestHeader('X-CSRF-TOKEN', token)
  }
  return new Promise((resolve, reject) => {
    request.onload = function () {
      if (request.status >= 200 && request.status < 400) {
        resolve(request.response ? JSON.parse(request.response) : null)
      } else {
        reject(request)
      }
    }
    request.onerror = function () {
      reject(request)
    }

    request.send(data)
  })
}
