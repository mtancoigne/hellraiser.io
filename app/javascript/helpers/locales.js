export default function Locales (locales) {
  return function (string) {
    if (locales[string]) {
      return locales[string]
    } else {
      return `<span class="translation_missing">${string}</span>`
    }
  }
}
