export default {
  setCaretAt (element, position) {
    element.focus()
    if (typeof element.selectionStart === 'number') {
      element.selectionStart = element.selectionEnd = position
    }
    // Don't care about unsupported browsers
  },
}
