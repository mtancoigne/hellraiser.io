function stringToColor (str) {
  let hash = 0
  for (let i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash)
  }
  const rgb = []
  for (let i = 0; i < 3; i++) {
    const value = (hash >> (i * 8)) & 0xFF
    rgb.push(value)
  }
  return rgb
}

function lighten (rgb, amount) {
  return rgb.map((c) => {
    const newVal = c + amount
    if (newVal > 255) return 255

    return newVal
  })
}

function darken (rgb, amount) {
  return rgb.map((c) => {
    const newVal = c - amount
    if (newVal < 0) return 0

    return newVal
  })
}

function fgColor (base) {
  return lighten(base, 100)
}

function bgColor (base) {
  return darken(base, 50)
}

function baseColor (name, id) {
  return stringToColor(`${id}-${name}`)
}

export default {
  cenobiteStyle (name, id) {
    const baseColors = baseColor(name, id)
    return `color: rgb(${fgColor(baseColors).join(',')}); background: rgb(${bgColor(baseColors).join(',')})`
  },
  cenobiteMentionStyle (name, id) {
    const baseColors = baseColor(name, id)
    return `text-decoration: underline 2px rgba(${bgColor(baseColors).join(',')})`
  },
}
