export default function extractCSRF () {
  const token = document.getElementsByName('csrf-token')

  if (token.length > 0) return token[0].content

  return null
}
