Feature: Links
  As a visitor
  I want to see all the public links

  Scenario: List all the public links
    Given there are 3 member links in the system
    And there are 3 private links in the system
    And there are 3 public links in the system
    When I access the links list
    Then I see 3 links
