Given(/\AI add the link to favorites\z/) do
  click_on '☆'
end

Then(/\AI can remove the link from favorites\z/) do
  expect(find_all('button[type="submit"]', text: '★').count).to eq 1
end
