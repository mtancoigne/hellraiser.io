Given('I access the new link form') do
  step 'I access the links list'

  click_on 'New link'
end

Given('I create a link named {string}') do |name|
  fill_in 'Title', with: name
  fill_in 'Url', with: Faker::Internet.url
  fill_in 'Description', with: Faker::GreekPhilosophers.quote

  click_on 'Save'
end

Then('I see the {string} link in the list') do |name|
  expect(page).to have_content name
end

Given('I have a {string} link') do |name|
  FactoryBot.create :link, title: name, cenobite: @current_cenobite
end

And('I access the link update form for {string}') do |name|
  step 'I access the links list'

  within('.element', text: name) do
    click_on '▼'
    click_on 'Edit'
  end
end

When('I change the link name to {string}') do |name|
  fill_in 'Title', with: name

  click_on 'Save'
end

When('I destroy the {string} link') do |name|
  step 'I access the links list'

  within('.element', text: name) do
    click_on '▼'
    click_on 'Destroy'
    accept_browser_alert
  end
end

Then('I dont see the {string} link in the list') do |name|
  expect(page).to have_no_content name
end

Given(/\AI have (\d+) links?\z/) do |amount|
  FactoryBot.create_list :link, amount, cenobite: @current_cenobite
end

Given(/^there are (\d+)(?: other)?(?: (private|public|member)) links? in the system/) do |amount, visibility|
  trait = case visibility
          when 'member'
            :visible_by_members
          when 'public'
            :visible_by_all
          else
            :visible_by_owner
          end
  FactoryBot.create_list :link, amount, trait
end

When('I access the links list') do
  click_in_main_menu 'Links'
end

Then(/\AI see (\d+) links?\z/) do |amount|
  expect(find_all('.content-layout__content > .element').count).to eq amount
end

Given('I access the shortcut to create a link with {string}') do |url|
  VCR.use_cassette "member/links/visit_#{url.gsub(%r{[:/&?#]}, '_')}" do
    visit "/member/links/new?url=#{url}"
  end
end

Then('The {string} field should contain {string}') do |field, value|
  expect(page).to have_field("link_#{field}", with: /#{value}/)
end

And(/\AI filter by favorites\z/) do
  click_on 'Your favorites'
end

And(/^there are (\d+) other links in my favorites$/) do |amount|
  FactoryBot.create_list :like, amount, cenobite: @current_cenobite
end
