Given('I have favorited a link') do
  FactoryBot.create :like, :on_link, cenobite: @current_cenobite
end

When('I access my favorites list') do
  click_in_user_menu 'Favorites'
end

Then('I see {int} favorite') do |amount|
  expect(find_all('tbody > tr').count).to eq amount
end

When('I destroy the favorite') do
  click_on 'Destroy'
  accept_browser_alert
end

Then('I see a message stating I have no favorited items') do
  expect(page).to have_content 'You have no favorite for now'
end
