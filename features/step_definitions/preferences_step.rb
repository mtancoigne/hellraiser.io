Given('{string} changes his name to {string}') do |name, new_name|
  cenobite = Cenobite.find_by name: name
  cenobite.update! name: new_name
end
