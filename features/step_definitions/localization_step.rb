Given('my favourite locale is french') do
  @current_cenobite.update! locale: 'fr'
end

When('I access the home page') do
  visit '/'
end

Then('I see the site structure in {string}') do |locale|
  expectation = case locale
                when 'Français'
                  'Accueil'
                when 'English'
                  'Home'
                else
                  raise "Unsupported locale #{locale}"
                end

  expect(page).to have_content expectation
end

Given('there is one comment on the {string} motion') do |title|
  motion = Motion.find_by title: title
  FactoryBot.create :comment, commentable: motion
end

When('I access the first motion \(in french)') do
  visit("/member/motions/#{Motion.first.id}")
end

Then('I see the comments structure in french') do
  within('.element:last-child .element__title') do
    expect(current_scope).to have_content 'Par'
  end
end

When('I access the paste \(in french)') do
  visit("/pastes/#{Paste.first.id}")
end

When('I enter a wrong password') do
  fill_in 'Mot de passe', with: 'wrong'

  click_on 'Révéler'
end

Then('I see an error message in french') do
  expect(page).to have_content 'Mot de passe incorrect'
end

Given('I change my preferred locale from {string} to {string}') do |locale, new_locale|
  case locale
  when 'Français'
    label  = "Langue d'affichage"
    button = 'Sauvegarder'
  when 'English'
    label  = 'Site locale'
    button = 'save'
  else
    raise "Unsupported locale #{locale}"
  end

  visit '/member/preferences/edit'
  select new_locale, from: label

  click_on button
end
