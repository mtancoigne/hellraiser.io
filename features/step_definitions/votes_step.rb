Then(/\AI can't upvote the (?:motion|link|fortune)\z/) do
  expect(find_all('button[type="submit"][disabled="disabled"]', text: 'Coool!').count).to eq 1
end

Given(/\AI upvote the (?:motion|fortune)\z/) do
  click_on 'Coool!'
end

Then(%r{\AI see the (?:motion|fortune) with a score of (\d+/\d+)\z}) do |score|
  expect(page).to have_css '.score', text: score
end

Given(/\AI downvote the (?:motion|fortune)\z/) do
  click_on 'Laaaame!'
end

Then(/\AI can't downvote the (?:motion|fortune)\z/) do
  expect(find_all('button[type="submit"][disabled="disabled"]', text: 'Laaaame!').count).to eq 1
end
