Then('I only see {string} in the acquaintances rooms') do |name|
  within '.modal' do
    expect(current_scope).to have_css '.chat-room', text: name
    expect(find_all('.chat-room.chat-room--acquaintance').count).to eq 1
  end
end

Given('I join {string} in private') do |name|
  cenobite = Cenobite.find_by name: name
  acquaintance = Acquaintance.find_by inviter: @current_cenobite, invitee: cenobite
  @chat_room = acquaintance.chat_room

  within('.modal') do
    click_on name
  end
end

When('{string} writes {string} in our private room') do |name, message|
  cenobite = Cenobite.find_by name: name
  participant = ChatParticipant.find_by chat_room: @chat_room, cenobite: cenobite
  ChatMessage.create! chat_room: @chat_room, chat_participant: participant, content: message
end
