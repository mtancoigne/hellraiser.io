Given('I access the new DMS form') do
  step 'I access the DMS list'

  click_on 'New switch'
end

When('I create a DMS named {string}') do |name|
  fill_in 'Name', with: name
  fill_in 'Recipients', with: 'buddy@example.com'
  fill_in 'Content', with: 'Hey, my password for hellraiser.io is "1234".'

  click_on 'Save'
end

Then('I see the {string} DMS in the list') do |name|
  expect(page).to have_content name
end

Given('I have a DMS named {string}') do |name|
  FactoryBot.create :dead_man_switch, cenobite: @current_cenobite, name: name
end

Given('I access the DMS update form for {string}') do |name|
  step 'I access the DMS list'
  within('.element', text: name) do
    click_on 'Edit'
  end
end

When('I change the DMS name to {string}') do |name|
  fill_in 'Name', with: name

  click_on 'Save'
end

Given('I destroy the {string} DMS') do |name|
  step 'I access the DMS list'
  within('.element', text: name) do
    click_on 'Destroy'
  end
  accept_browser_alert
end

Then('the {string} DMS is not present in the DMS list') do |name|
  expect(page).to have_no_content name
end

Given('I display the {string} DMS') do |name|
  step 'I access the DMS list'

  click_on name
end

Then('I see the details of {string} DMS') do |name|
  expect(page).to have_content "DMS: #{name}"
end

Given('I have {int} DMS') do |amount|
  FactoryBot.create_list :dead_man_switch, amount, cenobite: @current_cenobite
end

Given('there are {int} other DMS in the system') do |amount|
  FactoryBot.create_list :dead_man_switch, amount
end

When('I access the DMS list') do
  click_in_main_menu 'Dead man switches'
end

Then('I see {int} DMS') do |amount|
  expect(find_all('.element').count).to eq amount
end

Given('I have {int} DMS which will expire in {int} minutes') do |amount, time|
  list = FactoryBot.create_list :dead_man_switch, amount, cenobite: @current_cenobite

  list.each do |dms|
    dms.update_column :expires_at, Time.current + time.minutes # rubocop:disable Rails/SkipsModelValidations
  end
end

Given('I have a {string} DMS which will expire in {int} minutes') do |name, time|
  dms = FactoryBot.create :dead_man_switch, name: name, cenobite: @current_cenobite
  dms.update_column :expires_at, Time.current + time.minutes # rubocop:disable Rails/SkipsModelValidations
end

Then('the {int} switches expire in {string}') do |amount, time_remaining_string|
  expect(find_all('.element', text: time_remaining_string).count).to eq amount
end

When('I ping the {string} DMS') do |name|
  step 'I access the DMS list'
  within('.element', text: name) do
    click_on 'Ping'
  end
end

Then('the {string} expire in {string}') do |name, time_remaining_string|
  step 'I access the DMS list'
  within('.element', text: name) do
    expect(current_scope).to have_content time_remaining_string
  end
end

Given('I have a {string} which is expired') do |name|
  dms = FactoryBot.create :dead_man_switch, name: name, cenobite: @current_cenobite
  dms.update_column :expires_at, 5.minutes.ago # rubocop:disable Rails/SkipsModelValidations
  dms.update_column :email_sent_at, Time.current # rubocop:disable Rails/SkipsModelValidations
end

When('I rearm the {string}') do |name|
  step 'I access the DMS list'
  within('.element__title', text: name) do
    click_on 'Rearm'
  end
end

Then('the {string} is rearmed') do |name|
  step 'I access the DMS list'
  within('.element', text: name) do
    expect(current_scope).to have_link 'Ping'
  end
end
