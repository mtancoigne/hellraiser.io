Given('I access the new motion form') do
  step 'I access the motions list'

  click_on 'New motion'
end

Given('I create a motion named {string}') do |name|
  fill_in 'Title', with: name
  fill_code_mirror_area_with Faker::GreekPhilosophers.quote

  click_on 'Save'
end

Then('I see the {string} motion in the list') do |name|
  expect(page).to have_content name
end

Given('I have a {string} motion') do |name|
  FactoryBot.create :motion, title: name, cenobite: @current_cenobite
end

Given('I access the motion update form for {string}') do |name|
  step 'I access the motions list'

  within('.motion-item', text: name) do
    click_on '▼'
    click_on 'Edit'
  end
end

When('I change the motion name to {string}') do |name|
  fill_in 'Title', with: name

  click_on 'Save'
end

Given('I display the {string} motion') do |name|
  step 'I access the motions list'

  click_on name
end

Then('I see the motion details') do
  expect(page).to have_content 'Motion:'
end

When('I destroy the {string} motion') do |name|
  step 'I access the motions list'

  within('.motion-item', text: name) do
    click_on '▼'
    click_on 'Destroy'
    accept_browser_alert
  end
end

Then("I don't see the {string} motion in the list") do |name|
  expect(page).to have_no_content name
end

Then('I see the closed {string} motion in the list') do |name|
  expect(page).to have_css('.closed', text: name)
end

Given('I have {int} motion') do |amount|
  FactoryBot.create_list :motion, amount, cenobite: @current_cenobite
end

Given('there are {int} other motions in the system') do |amount|
  FactoryBot.create_list :motion, amount
end

When('I access the motions list') do
  click_in_main_menu 'Motions'
end

Then('I see {int} motions') do |amount|
  expect(find_all('.motion-item').count).to eq amount
end

Given('I have a comment stating {string} on the {string} motion') do |content, name|
  motion   = FactoryBot.create :motion, title: name
  @comment = FactoryBot.create :comment, content: content, commentable: motion, cenobite: @current_cenobite
end

When('I close the {string} motion') do |name|
  step "I access the motion update form for \"#{name}\""

  check 'Closed'

  click_on 'Save'
end

Given('there is one {string} motion') do |name|
  FactoryBot.create :motion, title: name
end

Given('there is one {string} public motion') do |name|
  FactoryBot.create :motion, title: name, anonymous: false
end

When('I subscribe to it') do
  click_on 'Subscribe'
end

Then("I'm subscribed to this motion") do
  expect(page).to have_button 'Unsubscribe'
end

Given('I have a subscription for the {string} motion') do |title|
  motion = FactoryBot.create :motion, title: title
  motion.subscribe! @current_cenobite
end

When('I unsubscribe to it') do
  click_on 'Unsubscribe'
end

Then("I'm not subscribed to this motion") do
  expect(page).to have_button 'Subscribe'
end

When('someone leaves a comment on the motion') do
  sleep 3 # Wait for ActionCable connection
  FactoryBot.create :comment, commentable: Motion.last, content: 'This is a comment!'
end

Given('there is a comment on this motion') do
  FactoryBot.create :comment, commentable: Motion.last, content: 'This is a kind of comment!'
end

When("someone destroys the motion's comment") do
  sleep 3 # Wait for ActionCable connection
  Comment.last.destroy
end

When("someone updates the motion's comment") do
  sleep 3 # Wait for ActionCable connection
  Comment.last.update content: 'Hello ! This is the new content'
end
