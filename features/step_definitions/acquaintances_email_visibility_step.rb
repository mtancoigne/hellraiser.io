Given(/^"(.*)" refuses to show (?:his|her) email to (?:his|her) acquaintances$/) do |name|
  Cenobite.find_by(name: name).update(show_email_to_acquaintances: false)
end

Then(/^I see "(.*)'s" email adress in the acquaintances list$/) do |name|
  email = Cenobite.find_by(name: name).email
  expect(page).to have_content email
end

When(/^"(.*)" accepts the pending acquaintance request$/) do |name|
  cenobite = Cenobite.find_by(name: name)
  cenobite.acquaintances.last.accept!
end

Then(/^I don't see "(.*)'s" email adress in the acquaintances list$/) do |name|
  email = Cenobite.find_by(name: name).email
  expect(page).to have_no_content email
end
