Given('I access the new fortune form') do
  step 'I access the random fortune page'
  click_on 'New fortune'
end

Given('I create a fortune with content {string}') do |content|
  fill_code_mirror_area_with content

  click_on 'Save'
end

Then('I see the {string} fortune in the list') do |content|
  expect(page).to have_content content
end

Given('I have a {string} fortune') do |content|
  FactoryBot.create :fortune, content: content, cenobite: @current_cenobite
end

Given('I access the fortune update form for {string}') do |content|
  step 'I access the fortunes list'

  within('.element', text: content) do
    click_on 'Edit'
  end
end

When('I change the fortune content to {string}') do |content|
  fill_code_mirror_area_with content

  click_on 'Save'
end

Given('there is one fortune in the system') do
  FactoryBot.create :fortune
end

Given('I access the random fortune page') do
  click_in_main_menu 'Fortune'
end

Then('I see the fortune details') do
  expect(page).to have_css 'figure > blockquote'
end

When('I destroy the {string} fortune') do |content|
  within('.element', text: content) do
    click_on 'Destroy'

    accept_browser_alert
  end
end

Then("I don't see the {string} fortune in the list") do |content|
  expect(page).to have_no_content content
end

Given(/\AI have (\d+) fortunes?\z/) do |amount|
  FactoryBot.create_list :fortune, amount, cenobite: @current_cenobite
end

Given('there are {int} other fortunes in the system') do |amount|
  FactoryBot.create_list :fortune, amount
end

When('I access the fortunes list') do
  step 'I access the random fortune page'

  click_on 'Manage'
end

Then('I see {int} fortune') do |amount|
  expect(find_all('.element').count).to eq amount
end
