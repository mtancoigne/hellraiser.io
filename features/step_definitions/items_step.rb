Given('I access the items list') do
  click_in_main_menu 'Items'
end

Given('I access the new item form') do
  step 'I access the items list'

  click_on 'New item'
end

Given('I create an item named {string}') do |name|
  fill_in 'Name', with: name

  click_on 'Save'
end

Then('I see the {string} item in the list') do |name|
  expect(page).to have_content name
end

Given('I access the item bulk creation form') do
  step 'I access the new item form'

  click_on 'Add many'
end

When('I create the following items:') do |table|
  # table is a Cucumber::MultilineArgument::DataTable
  list = table.raw.pluck(0).join("\n")

  fill_in 'List', with: list

  click_on 'Save'
end

Then('I see these items in the list:') do |table|
  # table is a Cucumber::MultilineArgument::DataTable
  table.raw.each do |entry|
    expect(page).to have_content entry[0]
  end
end

Given('I have a {string} item') do |name|
  FactoryBot.create :item, name: name, owner: @current_cenobite
end

Given('I access the item update form for the {string}') do |name|
  step 'I access the items list'

  within('tr', text: name) do
    click_on 'Edit'
  end
end

When('I change the item name to {string}') do |name|
  fill_in 'Name', with: name

  click_on 'Save'
end

When('I display the {string} item') do |name|
  step 'I access the items list'

  click_on name
end

Then("I see the item's details") do
  expect(page).to have_content 'Owner'
end

When('I destroy the {string} item') do |name|
  step 'I access the items list'

  within('tr', text: name) do
    click_on 'Destroy'
    accept_browser_alert
  end
end

Then("I don't see the {string} item in the list") do |name|
  expect(page).to have_no_content name
end

Given('{string} has a {string}') do |cenobite_name, name|
  cenobite = Cenobite.find_by(name: cenobite_name)

  FactoryBot.create :item, name: name, owner: cenobite
end

When('I access the acquaintances item list') do
  step 'I access the items list'
  click_on 'Acquaintances items'
end

When('I lend the {string} to {string}') do |name, cenobite_name|
  step "I access the item update form for the \"#{name}\""

  select cenobite_name, from: 'item_borrower_id'
  click_on 'Save'
end

Then('I see that {string} borrowed the {string}') do |cenobite_name, _name|
  expect(page).to have_content "Borrower:\n#{cenobite_name}"
end
