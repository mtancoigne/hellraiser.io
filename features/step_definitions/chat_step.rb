Given('I access the chat') do
  click_in_main_menu 'Chat'
end

Given('I create the {string} room') do |name|
  within('.modal') do
    click_on 'Create one'
  end

  fill_in 'Name', with: name
  click_on 'Save'
end

Then("I'm watching the {string} room") do |name|
  expect(page).to have_css '.chat-room.chat-room--connected.active', text: name
end

Then("I'm alone in the room") do
  expect(find('.chat-roster')).to have_content "There's no one but you."
end

Given('there is a {string} chat room') do |name|
  FactoryBot.create :chat_room, name: name
end

Given(/^I'm in the "(.*)" chat room(?: too)?$/) do |name|
  step 'I access the chat'

  within('.modal') do
    click_on name
  end
  @chat_room = ChatRoom.find_by(name: name)
end

Given('{string} is in the {string} room') do |cenobite_name, chat_room_name|
  chat_room        = ChatRoom.find_by name: chat_room_name
  cenobite         = Cenobite.find_by name: cenobite_name
  chat_participant = ChatParticipant.create name: cenobite_name, cenobite: cenobite, chat_room: chat_room
  FactoryBot.create :chat_presence, chat_room: chat_room, chat_participant: chat_participant
end

When(/^I go(?: back)? to the "(.*)" room$/) do |name|
  within '.chat-rooms' do
    click_on name
  end
end

Then('I see {string} in the presences list') do |name|
  within '.chat-roster' do
    expect(current_scope).to have_content name
  end
end

When('I write {string}') do |string|
  within '.chat-form' do
    fill_in 'chat-form-input', with: string
    click_on 'Send'
  end
end

Then('I see the message saying {string}') do |message|
  expect(page).to have_css('.chat-message', text: message)
end

Then('I see no message in the room') do
  expect(page).to have_no_css '.chat-message'
end

When('{string} enters the {string} room') do |cenobite_name, room_name|
  chat_room   = ChatRoom.find_by(name: room_name)
  participant = FactoryBot.create :chat_participant, name: cenobite_name, chat_room: chat_room
  FactoryBot.create :chat_presence, chat_room: chat_room, chat_participant: participant
end

When('{string} writes {string}') do |cenobite_name, message|
  participant = FactoryBot.create :chat_participant, name: cenobite_name, chat_room: @chat_room
  ChatMessage.create chat_room: @chat_room, chat_participant: participant, content: message
end

When('{string} exits the {string} room') do |cenobite_name, room_name|
  participant = ChatParticipant.find_by name: cenobite_name
  chat_room   = ChatRoom.find_by(name: room_name)
  ChatPresence.find_by(chat_participant: participant, chat_room: chat_room).destroy
end

Then("I don't see {string} in the presences list") do |name|
  within '.chat-roster' do
    expect(current_scope).to have_no_content name
  end
end

Given('someone creates the {string} room') do |name|
  FactoryBot.create :chat_room, name: name
end

Then('I can join the {string} room') do |name|
  expect(page).to have_css('.chat-room', text: name)
end

Given('The {string} chat room owner changes the name to {string}') do |old_name, new_name|
  ChatRoom.find_by(name: old_name).update name: new_name
end

Then("I see I'm in the {string} chat room") do |name|
  expect(page).to have_css('.chat-room.active', text: name)
end

Then('the room {string} is not available anymore') do |name|
  expect(page).to have_no_css('.chat-room', text: name)
end

Given('someone destroys the {string} chat room') do |name|
  ChatRoom.find_by(name: name).destroy
end

Then("I'm asked to join another room") do
  expect(page).to have_css('.modal.active', text: 'Select a room')
end

When('{string} change her name to {string}') do |old_name, new_name|
  ChatParticipant.find_by(name: old_name).update! name: new_name
end

Given('all the rooms in which I participate are configured in auto-join') do
  ChatRoom.find_each do |room|
    ChatParticipant.create! name: @current_cenobite.name, cenobite: @current_cenobite, chat_room: room, auto_join: true
  end
end

Then("I'm connected to the {string} room") do |name|
  expect(page).to have_css '.chat-room.chat-room--connected', text: name
end

When('{string} writes {string} in the {string} room') do |participant, message, room|
  chat_room = ChatRoom.find_by(name: room)
  chat_participant = ChatParticipant.find_by name: participant
  ChatMessage.create! content: message, chat_room: chat_room, chat_participant: chat_participant
end

Then('I see the message from {string} saying {string}') do |participant, message|
  within('.chat-message', text: participant) do
    expect(current_scope).to have_content message
  end
end

Then('I see a pending message for the {string} room') do |name|
  expect(page).to have_css '.chat-room', text: "(1) #{name}"
end

When("I'm in the {string} chat room with {string} and {string}") do |chat_room_name, cenobite1, cenobite2|
  step "there is a \"#{chat_room_name}\" chat room"
  step "\"#{cenobite1}\" is in the \"#{chat_room_name}\" room"
  step "\"#{cenobite2}\" is in the \"#{chat_room_name}\" room"
  step "I'm in the \"#{chat_room_name}\" chat room"
end

Given('I joined {string} chat room in the past') do |name|
  chat_room = ChatRoom.find_by name: name
  FactoryBot.create :chat_participant, chat_room: chat_room, cenobite: @current_cenobite
end

Then('I see a pending message for the chat') do
  within '.main-menu' do
    expect(current_scope).to have_content 'Chat *1'
  end
end
