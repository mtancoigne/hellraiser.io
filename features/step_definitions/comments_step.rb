Given('I leave a comment stating {string}') do |content|
  fill_code_mirror_area_with content

  click_on 'Save'
end

Then('I see the comment {string}') do |content|
  expect(page).to have_content content
end

Given('I update this comment to {string}') do |content|
  within('.element', text: @comment.content) do
    click_on 'Edit'
    fill_code_mirror_area_with content
    click_on 'Save'
  end

  @comment.reload
end

Then('I see the new comment') do
  expect(page).to have_content @comment.content
end

Given('I destroy this comment') do
  within('.element', text: @comment.content) do
    click_on 'Destroy'
    # Wait for countdown
    sleep 2
  end
end

Then('the comment is gone') do
  expect(page).to have_no_content @comment.content
end

Then('I see the comment popping in') do
  expect(page).to have_css '.element', text: 'This is a comment!'
end

Then('I see the comment disappear') do
  expect(page).to have_no_content 'This is a kind of comment!'
end

Then('I see the updated comment replacing the old one') do
  expect(page).to have_css '.element', text: 'Hello ! This is the new content'
  expect(page).to have_no_content 'This is a kind of comment!'
end

Then('I see the real names of participants') do
  within('aside.content-layout__panel section.element--panel dl') do
    expect(current_scope).to have_content Motion.last.cenobite.name
  end

  within('.comments .element .element__title') do
    expect(current_scope).to have_content Comment.last.cenobite.name
  end
end

Then("I don't see the real names of participants") do
  within('aside.content-layout__panel section.element--panel dl') do
    expect(current_scope).to have_no_content Motion.last.cenobite.name
  end

  within('.comments .element .element__title') do
    expect(current_scope).to have_no_content Comment.last.cenobite.name
  end
end
