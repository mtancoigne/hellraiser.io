Given('I access the pastes list') do
  click_in_main_menu 'Pastes'
end

When('I create a paste named {string}') do |name|
  click_on 'New paste'

  fill_in 'Name', with: name
  fill_code_mirror_area_with 'Some fine content'

  click_on 'Save'
end

Then('I see the {string} paste in the list') do |name|
  expect(page).to have_content name
end

Given('I have a paste named {string}') do |name|
  FactoryBot.create :paste, cenobite: @current_cenobite, name: name
end

Given('I have a protected paste named {string}') do |name|
  FactoryBot.create :paste, :protected, cenobite: @current_cenobite, name: name
end

Then('the password for {string} paste is still the same') do |name|
  paste = Paste.find_by(name: name)
  expect(paste.authenticate_password('password')).to be_truthy
end

Given('I access the paste update form for {string}') do |name|
  step 'I access the pastes list'
  within('.element--paste', text: name) do
    click_on '▼'
    click_on 'Edit'
  end
end

When('I change the paste name to {string}') do |name|
  fill_in 'Name', with: name

  click_on 'Save'
end

When('I destroy the {string} paste') do |name|
  step 'I access the pastes list'
  within('.element--paste', text: name) do
    click_on '▼'
    click_on 'Destroy'
    accept_browser_alert
  end
end

Then('the {string} paste is not present in the list') do |name|
  expect(page).to have_no_content name
end

Given('I display the {string} paste') do |name|
  step 'I access the pastes list'
  click_on name
end

Then('I see the details of {string} paste') do |name|
  expect(page).to have_content "#{name}.txt"
end

Given('I have {int} pastes') do |amount|
  FactoryBot.create_list :paste, amount, cenobite: @current_cenobite
end

Given('there are {int} other pastes in the sytem') do |amount|
  FactoryBot.create_list :paste, amount
end

Then('I see {int} pastes') do |amount|
  expect(find_all('.element--paste').count).to eq amount
end

Given('there is one paste in the system') do
  @paste = FactoryBot.create :paste
end

When('I access the paste using the link') do
  visit "/pastes/#{@paste.id}"
end

Then('I see the content') do
  expect(page).to have_content @paste.content
end

Given('there is one burnable paste in the system') do
  @paste = FactoryBot.create :paste, :burnable
end

When('I display the paste content') do
  click_on 'Reveal'
end

Then('the paste is deleted') do
  expect(Paste.where(id: @paste.id).count).to eq 0
end

Given('there is one protected paste in the system') do
  @paste = FactoryBot.create :paste, :protected
end

When('I display the paste content using the password') do
  fill_in 'Password', with: 'password'
  click_on 'Reveal'
end

When('I display the paste content using a wrong password') do
  fill_in 'Password', with: 'badpass'
  click_on 'Reveal'
end

Then('I see an error message') do
  expect(page).to have_content 'Incorrect password'
end

Then("I don't see the content") do
  expect(page).to have_no_content @paste.content
end

Given('there is one protected and burnable paste in the system') do
  @paste = FactoryBot.create :paste, :burnable, :protected
end

Given('there is a Markdown paste in the system') do
  content = <<~MD
    # The title

    hello world
  MD
  @paste = FactoryBot.create :paste, extension: 'md', content: content
end

Then('I see the rendered HTML paste') do
  expect(page).to have_css 'h1', text: 'The title'
end

Given('there is a Ruby paste in the system') do
  content = <<~RB
    puts 'Hello world'
  RB
  @paste = FactoryBot.create :paste, extension: 'rb', content: content
end

Then('I see the higlighted Ruby code') do
  expect(page).to have_css '.hljs-string'
end

Given('there is a dart paste in the system') do
  content = <<~DART
    void main() {
      print('Hello, World!');
    }
  DART
  @paste = FactoryBot.create :paste, extension: 'dart', content: content
end

Then('I see the original code') do
  expect(page).to have_no_css '.hljs-string'
end
