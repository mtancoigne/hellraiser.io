Given('I access the event creation form') do
  click_in_main_menu 'Events'
  click_on 'New event'
end

Given('I create an event named {string}') do |name|
  fill_in 'Name', with: name
  fill_code_mirror_area_with 'This is a nice event'

  click_on 'Save'
end

Then('I see the {string} event in the list') do |name|
  expect(page).to have_content name
end

Given('I have a {string} event') do |string|
  FactoryBot.create :event, name: string, cenobite: @current_cenobite, starts_at: 1.day.from_now, ends_at: 2.days.from_now
end

Given('I access the event update form for {string}') do |name|
  step 'I access the events list'
  within '.element__title', text: name do
    click_on '▼'
    click_on 'Edit'
  end
end

When('I change the event name to {string}') do |new_name|
  fill_in 'Name', with: new_name

  click_on 'Save'
end

When('I destroy the {string} event') do |name|
  step 'I access the events list'

  within '.element__title', text: name do
    click_on '▼'
    click_on 'Destroy'
    accept_browser_alert
  end
end

Then('I dont see the {string} event in the list') do |name|
  expect(page).to have_no_content name
end

Given(/^I have (\d+) (past |)event$/) do |amount, past|
  if past == 'past '
    FactoryBot.create_list :event, amount, :over, cenobite: @current_cenobite
  else
    FactoryBot.create_list :event, amount, cenobite: @current_cenobite
  end
end

Given(/^there (?:are|is) (\d+) other (member|private) (upcoming|past) events? in the system$/) do |amount, visibility, past|
  visibility = visibility == 'private' ? 0 : 1
  if past == 'past'
    FactoryBot.create_list :event, amount, :over, visibility: visibility
  else
    FactoryBot.create_list :event, amount, visibility: visibility
  end
end

When('I access the events list') do
  click_in_main_menu 'Events'
end

Then('I see {int} events') do |amount|
  expect(find_all('.element').count).to eq amount
end

When('I access the past events list') do
  step 'I access the events list'
  click_on 'Past'
end

When('I display the event') do
  name = Event.first.name
  click_on name
end

Then('I joined the event successfully') do
  expect(page).to have_content 'You joined the event'
  expect(page).to have_button "I won't participate, finally"
end

Given('I joined this event') do
  Event.first.add_attendee! @current_cenobite
end

Then('I left the event successfully') do
  expect(page).to have_content 'You left this event'
  expect(page).to have_button "I'll be there too!"
end

Given(/^I created an event for (\d+):00 \((.*)\)$/) do |hour, timezone|
  start_time = Time.now.in_time_zone(timezone).change(hour: hour)
  end_time = start_time + 1.hour

  FactoryBot.create :event,
                    starts_at:          start_time,
                    starts_at_timezone: timezone,
                    ends_at:            end_time,
                    ends_at_timezone:   timezone,
                    cenobite:           @current_cenobite
end

Then('I see an event for {string}') do |hour|
  expect(page).to have_content hour
end

Given('I access the {string} event') do |name|
  step 'I access the events list'
  click_on name
end

Given('I have a comment stating {string} on the {string} event') do |string, string2|
  event = FactoryBot.create :event, :visible_by_members, name: string2, cenobite: @current_cenobite
  @comment = FactoryBot.create :comment, commentable: event, content: string, cenobite: @current_cenobite
end
