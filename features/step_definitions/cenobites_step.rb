Given(/^I have an? (visible |invisible |)account for "(.*)"$/) do |status, email|
  status = status == 'visible '
  FactoryBot.create :cenobite, :active, email: email, visible_by_members: status
end

Given("I'm authenticated with {string}") do |email|
  @current_cenobite = Cenobite.find_by email: email
  click_in_main_menu 'Sign in'

  fill_in 'Email', with: email
  fill_in 'Password', with: 'password'

  click_on 'Log in'
end

When('I logout') do
  click_in_user_menu 'Sign out'
  @current_cenobite = nil
end

When('I login as {string}') do |email|
  step "I'm authenticated with \"#{email}\""
end

Given('there is a cenobite named {string}') do |name|
  FactoryBot.create :cenobite, :active, name: name
end
