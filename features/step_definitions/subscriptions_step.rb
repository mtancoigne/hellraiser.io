When('I access the subscriptions list') do
  click_in_user_menu 'Subscriptions'
end

Then('I see {int} subscriptions') do |amount|
  expect(find_all('tbody > tr').count).to eq amount
end

Given('I subscribed to {int} motions') do |amount|
  FactoryBot.create_list :subscription, amount, :on_motion, cenobite: @current_cenobite
end

Given('I have {int} subscription') do |amount|
  FactoryBot.create_list :subscription, amount, :on_motion, cenobite: @current_cenobite
end

When('I destroy the subscription') do
  click_on 'Destroy'

  accept_browser_alert
  sleep 1
end
