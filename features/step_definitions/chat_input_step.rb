When('I type {string} and validate the autocompletion box') do |string|
  input = find_by_id 'chat-form-input'
  input.send_keys string, :tab
end

Then('I see {string} in the input box') do |string|
  expect(page).to have_field('chat-form-input', with: string)
end

When('I type {int} lines message') do |amount|
  messages = []
  amount.times { messages << 'Hello' }
  fill_in 'chat-form-input', with: messages.join("\n")
end

Then('I the chat input area has a height of {int}') do |height|
  expect(page).to have_css ".chat-form__input.chat-form__input--#{height}"
end
