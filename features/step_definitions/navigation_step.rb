When('I reload the page') do
  visit current_url
end

When('I click on {string}') do |string|
  click_on string
end
