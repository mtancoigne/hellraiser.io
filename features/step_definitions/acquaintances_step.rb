When('I access the acquaintances list') do
  click_in_user_menu 'Acquaintances'
end

When('I invite {string}') do |email|
  step 'I access the acquaintances list'

  fill_in 'Email', with: email
  click_on 'Invite'
end

Then('I see that {string} has been invited') do |email|
  within('.invitations') do
    expect(current_scope).to have_content email
  end
end

When('I add {string} in my acquaintances') do |name|
  email = Cenobite.find_by(name: name).email

  step "I invite \"#{email}\""
end

Then('I see that the request for {string} is pending') do |name|
  email = Cenobite.find_by(name: name).email
  within('tr', text: email) do
    expect(current_scope).to have_content 'Sent less than a minute ago'
  end
end

Given('{string} wants to invite me') do |name|
  cenobite = Cenobite.find_by name: name
  FactoryBot.create :acquaintance, inviter: cenobite, invitee: @current_cenobite
end

Then('I accept the request from {string}') do |name|
  step 'I access the acquaintances list'
  within('tr', text: name) do
    click_on 'Accept invite'
  end
end

Then("I'm now acquainted with {string}") do |name|
  within('tr', text: name) do
    expect(current_scope).to have_content 'Accepted less than a minute ago'
  end
end

When(/^I (cancel|refuse|revoke) the request (?:from|to) "(.*)"$/) do |action, name|
  within('tr', text: name) do
    button = action == 'revoke' ? 'Revoke' : 'Cancel'
    click_on button
  end
  accept_browser_alert
end

Then("there's no pending request from {string}") do |name|
  expect(page).to have_no_css('td', text: name)
end

Given("I'm acquainted with {string}") do |name|
  cenobite = Cenobite.find_by name: name
  FactoryBot.create :acquaintance, :accepted, inviter: @current_cenobite, invitee: cenobite
end

Given('there are the following cenobites and acquaintances:') do |table|
  # table is a Cucumber::MultilineArgument::DataTable
  table.raw.each do |row|
    cenobite = FactoryBot.create :cenobite, :active, name: row[0]
    row[1].split(', ').each do |acq|
      acquaintance = FactoryBot.create :cenobite, :active, name: acq
      FactoryBot.create :acquaintance, :accepted, inviter: cenobite, invitee: acquaintance
    end
  end
end
