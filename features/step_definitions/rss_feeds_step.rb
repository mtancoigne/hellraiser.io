Given('I access to the new RSS feed form') do
  step 'I access the feeds list'
  click_on 'New RSS feed'
end

Given('I add the feed for LinuxFr') do
  fill_in 'Feed URL', with: 'https://linuxfr.org/news.atom'

  VCR.use_cassette 'rss_feed/get_atom_feed' do
    click_on 'Save'
  end
end

Then('I see the LinuxFr feed in the list') do
  expect(page).to have_content 'LinuxFr.org : les dépêches'
end

Given('I have the LinuxFr feed') do
  VCR.use_cassette 'rss_feed/get_atom_feed' do
    RssFeed.create! url: 'https://linuxfr.org/news.atom', cenobite: @current_cenobite
  end
end

Given('I access the RSS feed update form for LinuxFr') do
  step 'I access the feeds list'
  within 'tr', text: 'LinuxFr.org : les dépêches' do
    click_on 'Edit'
  end
end

When('I change the tags for {string}') do |tags|
  fill_in 'Tags', with: tags

  click_on 'Save'
end

Then('I see the {string} tags for LinuxFr') do |tags|
  expect(find('tr', text: 'LinuxFr.org : les dépêches')).to have_content tags
end

Given('I destroy the LinuxFr feed') do
  step 'I access the feeds list'
  within 'tr', text: 'LinuxFr.org : les dépêches' do
    click_on 'Destroy'
    accept_browser_alert
  end
end

Then("I don't see the LinuxFr feed in the list") do
  expect(page).to have_no_content 'LinuxFr.org'
end

Given('I have 1 RSS feed') do
  step 'I have the LinuxFr feed'
end

Given('there is 1 other RSS feed in the system') do
  VCR.use_cassette 'rss_feed/get_rss_feed' do
    RssFeed.create! url: 'https://korben.info/feed', cenobite: FactoryBot.create(:cenobite)
  end
end

When('I access the feeds list') do
  click_in_main_menu 'Articles'
  click_on 'Manage'
end

Then('I see {int} feeds') do |amount|
  expect(find_all('tbody > tr').count).to eq amount
end
