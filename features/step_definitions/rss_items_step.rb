Given('I access the articles list') do
  click_in_main_menu 'Articles'
end

Then('I see {int} articles') do |amount|
  expect(find_all('.element--rss-item').count).to eq amount
end
