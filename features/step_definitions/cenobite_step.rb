Given('there is a cenobite named {string} which accepts to be visible') do |name|
  FactoryBot.create :cenobite, :visible, :active, name: name
end

Given("there is a cenobite named {string}, who don't care about social stuff") do |name|
  FactoryBot.create :cenobite, :invisible, :active, name: name
end

Given('I access the cenobites list') do
  click_in_main_menu 'Bottin'
end

Then('I see {string} in the list') do |name|
  expect(page).to have_css '.element', text: name
end

Then("I don't see {string}") do |name|
  expect(page).to have_no_content name
end

When('I invite {string} from the list') do |name|
  within('.element__title', text: name) do
    click_on 'Request acquaintance'
  end
end

Then('I see that the invitation for {string} has been created') do |name|
  within('.element__title', text: name) do
    expect(current_scope).to have_css '.text--success', text: 'Request sent'
  end
end

Given('{string} sent me an acquaintance request') do |name|
  cenobite = Cenobite.find_by name: name
  Acquaintance.create! inviter: cenobite, invitee: @current_cenobite, invite_method: :id
end

When('I accept the acquaintance request from {string}') do |name|
  within('.element__title', text: name) do
    click_on 'Accept invitation'
  end
end

Then('I see that the invitation for {string} has been accepted') do |name|
  within('.element__title', text: name) do
    expect(current_scope).to have_css '.text--success', text: 'Request accepted'
  end
end

When('I cancel the acquaintance request with {string}') do |name|
  within('.element__title', text: name) do
    click_on 'Cancel invitation'
  end
end

Then('I see that the invitation for {string} has been cancelled') do |name|
  within('.element__title', text: name) do
    expect(current_scope).to have_css '.text--alert', text: 'Request cancelled'
  end
end

Given('I sent an acquaintance request to {string}') do |name|
  cenobite = Cenobite.find_by name: name
  Acquaintance.create! inviter: @current_cenobite, invitee: cenobite, invite_method: :id
end
