module Helpers
  def click_in_main_menu(string)
    visit '/' unless page.current_path

    within('.main-menu') do
      click_on string
    end
  end

  def click_in_user_menu(string)
    visit '/' unless page.current_path

    within('.main-menu') do
      click_on 'You'
      click_on string
    end
  end

  def accept_browser_alert
    page.driver.browser.switch_to.alert.accept
  end

  def fill_code_mirror_area_with(content)
    execute_script <<~JS
      document.getElementsByClassName('CodeMirror')[0].CodeMirror.setValue(#{content.to_json})
    JS
  end
end

World(Helpers)
