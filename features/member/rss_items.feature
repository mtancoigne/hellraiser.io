Feature: RSS items
  As a member
  In order to read news
  I want to be able to view, vote on and favorite articles from RSS feeds

  Background:
    Given I have an account for "noxie@hellraiser.io"
    And I'm authenticated with "noxie@hellraiser.io"
    And there is 1 other RSS feed in the system

  Scenario: Display the list of feeds
    Given I access the articles list
    Then I see 20 articles
