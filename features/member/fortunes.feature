Feature: Fortune management
  As a cenobite
  In order to have a lot of fun
  I want to see and provide fortunes

  Background:
    Given I have an account for "john@ripandtear.com"
    And I'm authenticated with "john@ripandtear.com"

  Scenario: Create a fortune
    Given I access the new fortune form
    And I create a fortune with content "Is this a joke?"
    Then I see the "Is this a joke?" fortune in the list

  Scenario: Update a fortune
    Given I have a "Is this a joke?" fortune
    And I access the fortune update form for "Is this a joke?"
    When I change the fortune content to "I’m a big fan of whiteboards. I find them quite re-markable."
    Then I see the "I’m a big fan of whiteboards. I find them quite re-markable." fortune in the list

  Scenario: Display a fortune
    Given there is one fortune in the system
    And I access the random fortune page
    Then I see the fortune details

  Scenario: Destroy a fortune
    Given I have a "Let minnow what you think." fortune
    When I access the fortunes list
    And I destroy the "Let minnow what you think." fortune
    Then I don't see the "Let minnow what you think." fortune in the list

  Scenario: List all my fortunes
    Given I have 1 fortune
    Given there are 3 other fortunes in the system
    When I access the fortunes list
    Then I see 1 fortune

  Scenario: Upvote a fortune
    Given there is one fortune in the system
    And I access the random fortune page
    And I upvote the fortune
    Then I see the fortune with a score of 1/1
    And I can't upvote the fortune

  Scenario: Downvote a fortune
    Given there is one fortune in the system
    And I access the random fortune page
    And I downvote the fortune
    Then I see the fortune with a score of 0/1
    And I can't downvote the fortune
