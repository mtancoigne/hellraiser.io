Feature: Chat
  As a cenobite
  In order to speak with everyone
  I want to have a chat

  Background:
    Given I have an account for "pinhead@hellraiser.io"
    And I'm authenticated with "pinhead@hellraiser.io"
    And there is a cenobite named "Alice"
    And there is a "General" chat room

  Scenario: Create room
    Given I access the chat
    And I create the "Development" room
    Then I'm watching the "Development" room
    And I'm alone in the room

  Scenario: Change room: presences
    Given there is a "Weekend party" chat room
    And "Alice" is in the "Weekend party" room
    When I'm in the "General" chat room
    Then I'm alone in the room
    When I go to the "Weekend party" room
    Then I see "Alice" in the presences list
    When I go back to the "General" room
    Then I'm alone in the room

  Scenario: Change room: messages
    Given there is a "Weekend party" chat room
    And I'm in the "General" chat room
    And I write "Hello world"
    And I see the message saying "Hello world"
    When I go to the "Weekend party" room
    Then I see no message in the room
    When I go back to the "General" room
    Then I see the message saying "Hello world"

  Scenario: Write messages
    Given I'm in the "General" chat room
    When I write "Hello"
    Then I see the message saying "Hello"

  Scenario: Someone pops in
    Given I'm in the "General" chat room
    And I'm alone in the room
    When "John Merchant" enters the "General" room
    Then I see "John Merchant" in the presences list

  Scenario: Receive messages
    Given "Alice" is in the "General" room
    And I'm in the "General" chat room
    When "Alice" writes "Hello"
    Then I see the message saying "Hello"

  Scenario: Someone leaves
    Given "Alice" is in the "General" room
    And I'm in the "General" chat room too
    When "Alice" exits the "General" room
    Then I don't see "Alice" in the presences list

  Scenario: Someone creates a room
    Given I'm in the "General" chat room
    And someone creates the "Cube mysteries" room
    Then I can join the "Cube mysteries" room

  Scenario: Someone updates a room
    Given I'm in the "General" chat room
    And The "General" chat room owner changes the name to "AFK"
    Then I see I'm in the "AFK" chat room

  Scenario: Someone deletes a room
    Given there is a "Friends of Carlotta" chat room
    And I'm in the "General" chat room
    When someone destroys the "Friends of Carlotta" chat room
    Then the room "Friends of Carlotta" is not available anymore

  Scenario: Someone deletes a room I joined
    Given I'm in the "General" chat room
    And someone destroys the "General" chat room
    Then I'm asked to join another room

  Scenario: Someone change its name
    Given "Alice" is in the "General" room
    Given I'm in the "General" chat room
    And I see "Alice" in the presences list
    When "Alice" change her name to "Paul"
    Then I don't see "Alice" in the presences list
    And I see "Paul" in the presences list

  Scenario: Auto join rooms
    Given there is a "MOTD" chat room
    And all the rooms in which I participate are configured in auto-join
    When I access the chat
    Then I'm connected to the "General" room
    And I'm connected to the "MOTD" room

  Scenario: Unread messages: when connected
    Given there is a "Crystal Lake party" chat room
    And all the rooms in which I participate are configured in auto-join
    And "Alice" is in the "Crystal Lake party" room
    When I'm in the "General" chat room
    And "Alice" writes "Hello" in the "Crystal Lake party" room
    Then I see a pending message for the "Crystal Lake party" room

  Scenario: Unread messages: when disconnected
    Given there is a "Crystal Lake party" chat room
    And "Alice" is in the "Crystal Lake party" room
    And I joined "Crystal Lake party" chat room in the past
    When "Alice" writes "Hello" in the "Crystal Lake party" room
    And I access the chat
    Then I see a pending message for the "Crystal Lake party" room

  Scenario: Unread messages: when not in the chat
    Given there is a "Crystal Lake party" chat room
    And "Alice" is in the "Crystal Lake party" room
    And I joined "Crystal Lake party" chat room in the past
    When "Alice" writes "Hello" in the "Crystal Lake party" room
    And I access the home page
    Then I see a pending message for the chat
