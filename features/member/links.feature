Feature: Links
  As a member
  In order to share links
  I want to be able to post links and see others

  Background:
    Given I have an account for "morpheus@hellraiser.io"
    And I'm authenticated with "morpheus@hellraiser.io"

  Scenario: Create a link
    Given I access the new link form
    And I create a link named "Enter the matrix"
    Then I see the "Enter the matrix" link in the list

  Scenario: Update a link
    Given I have a "Follow the white rabbit" link
    And I access the link update form for "Follow the white rabbit"
    When I change the link name to "Wake up Neo"
    Then I see the "Wake up Neo" link in the list

  Scenario: Destroy a link
    Given I have a "There is no Spoon" link
    When I destroy the "There is no Spoon" link
    Then I dont see the "There is no Spoon" link in the list

  Scenario: List all the visible links
    Given I have 1 link
    And there are 3 other member links in the system
    And there are 3 other private links in the system
    And there are 3 other public links in the system
    When I access the links list
    Then I see 7 links

  Scenario: Prefill a link
    Given I access the shortcut to create a link with "https://github.com/"
    Then The "title" field should contain "GitHub · Build and ship software on a single, collaborative platform"
    Then The "url" field should contain "https://github.com/"
    Then The "description" field should contain "Join the world's most widely adopted"

  Scenario: Add a link to favorites
    Given I have 1 link
    And I access the links list
    And I add the link to favorites
    Then I can remove the link from favorites

  Scenario: List all links of favorites
    Given I have 1 link
    And there are 2 other links in my favorites
    When I access the links list
    And I filter by favorites
    Then I see 2 links
