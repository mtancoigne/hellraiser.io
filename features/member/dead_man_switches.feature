Feature: Dead man switches
  As a member
  In order to send emails to people if something happens to me
  I want to be able to set up and manage dead man switches

  Background:
    Given I have an account for "butterball@hellraiser.io"
    And I'm authenticated with "butterball@hellraiser.io"

  Scenario: Create a dead man switch
    Given I access the new DMS form
    When I create a DMS named "Account password"
    Then I see the "Account password" DMS in the list

  Scenario: Update a dead man switch
    Given I have a DMS named "Account password"
    And I access the DMS update form for "Account password"
    When I change the DMS name to "Keepass password"
    Then I see the "Keepass password" DMS in the list

  Scenario: Destroy a dead man switch
    Given I have a DMS named "Account password"
    And I destroy the "Account password" DMS
    Then the "Account password" DMS is not present in the DMS list

  Scenario: Show a dead man switch
    Given I have a DMS named "Account password"
    And I display the "Account password" DMS
    Then I see the details of "Account password" DMS

  Scenario: List my dead man switches
    Given I have 2 DMS
    And there are 2 other DMS in the system
    When I access the DMS list
    Then I see 2 DMS

  Scenario: Ping all switches on login
    Given I have 2 DMS which will expire in 5 minutes
    When I logout
    And I login as "butterball@hellraiser.io"
    When I access the DMS list
    Then the 2 switches expire in "1 hour"

  Scenario: Ping a switch
    Given I have a "Media email" DMS which will expire in 5 minutes
    When I ping the "Media email" DMS
    Then the "Media email" expire in "1 hour"

  Scenario: Rearm a switch
    Given I have a "Test switch" which is expired
    When I rearm the "Test switch"
    Then the "Test switch" is rearmed

