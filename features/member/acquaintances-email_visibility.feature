Feature: Email visibility
  As a member
  In order to have a bit of control on my personal data
  I want to be able to hide my email from my acquaintances

  Background:
    Given I have an account for "butterball@hellraiser.io"
    And I'm authenticated with "butterball@hellraiser.io"
    And there is a cenobite named "Alice"

  Scenario: Visible emails before acquaintance validation
    Given "Alice" refuses to show her email to her acquaintances
    When I add "Alice" in my acquaintances
    Then I see "Alice's" email adress in the acquaintances list

  Scenario: Invisible emails after acquaintance validation
    Given "Alice" refuses to show her email to her acquaintances
    When I add "Alice" in my acquaintances
    And "Alice" accepts the pending acquaintance request
    When I reload the page
    Then I don't see "Alice's" email adress in the acquaintances list
