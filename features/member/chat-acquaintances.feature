Feature: Acquaintances chat
  As a cenobite
  In order to speak with people I know
  I want to be able to speak privately with them

  Background:
    Given I have an account for "pinhead@hellraiser.io"
    And I'm authenticated with "pinhead@hellraiser.io"
    Given there are the following cenobites and acquaintances:
      | Freddy   | Loretta, Amanda |
    Given I'm acquainted with "Freddy"
    And there is a "General" chat room

  Scenario: List private chat rooms
    When I access the chat
    Then I only see "Freddy" in the acquaintances rooms

  Scenario: Name change
    Given "Freddy" changes his name to "Fred K."
    When I access the chat
    Then I only see "Fred K." in the acquaintances rooms
    And I join "Fred K." in private
    When "Fred K." writes "Nancy..." in our private room
    # Beware of punctuation in rendered Markdown
    Then I see the message from "Fred K." saying "Nancy…"
