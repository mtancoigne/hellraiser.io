Feature: Chat input
  As a cenobite
  In order to speak with everyone
  I want to have a reactive chat input

  Background:
    Given I have an account for "pinhead@hellraiser.io"
    And I'm authenticated with "pinhead@hellraiser.io"
    And there is a cenobite named "Julia"
    And there is a cenobite named "June"

  Scenario Outline: Autocompletion
    Given I'm in the "General" chat room with "Julia" and "June"
    When I type "<string>" and validate the autocompletion box
    Then I see "<result>" in the input box
    Examples:
      | string     | result       |
      | hello Ju   | hello Julia  |
      | hello\nJu  | hello\nJulia |
      | hello\nJun | hello\nJune  |

  Scenario Outline: Multiline message
    Given there is a "General" chat room
    And I'm in the "General" chat room
    When I type <amount> lines message
    Then I the chat input area has a height of <height>
    Examples:
      | amount | height |
      | 2      | 2      |
      | 4      | 4      |
      | 5      | 4      |

