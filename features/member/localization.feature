Feature: Localization
  As a member
  In order to have a better understanding of the website
  I want to see its structure translated

  Background:
    Given I have an account for "pinhead@hellraiser.io"
    And I'm authenticated with "pinhead@hellraiser.io"
    # Order matters, keep this after auth
    And my favourite locale is french

  Scenario: Test Rails translations
    When I access the home page
    Then I see the site structure in "Français"

  Scenario: Test Vue translations
    Given there is one "Check this out" motion
    And there is one comment on the "Check this out" motion
    When I access the first motion (in french)
    Then I see the comments structure in french

  # This won't test all the vanilla scripts that need a translation
  # mechanism, but is enough to see if a reference implementation is enough.
  Scenario: Tests vanilla js translations
    Given there is one protected paste in the system
    When I access the paste (in french)
    And I enter a wrong password
    Then I see an error message in french

  Scenario: Change the preferred locale
    Given I change my preferred locale from "Français" to "English"
    Then I see the site structure in "English"
