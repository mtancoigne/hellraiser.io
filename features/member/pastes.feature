Feature: Pastes
  As a member
  In order to share text with people
  I want to be able to manage pastes

  Background:
    Given I have an account for "pinhead@hellraiser.io"
    And I'm authenticated with "pinhead@hellraiser.io"

  Scenario: Create a paste
    Given I access the pastes list
    When I create a paste named "README"
    Then I see the "README" paste in the list

  Scenario: Update a paste
    Given I have a paste named "notes"
    And I access the paste update form for "notes"
    When I change the paste name to "Biography"
    Then I see the "Biography" paste in the list

  Scenario: Password protect a paste
    Given I have a protected paste named "notes"
    And I access the paste update form for "notes"
    When I change the paste name to "Biography"
    Then the password for "Biography" paste is still the same

  Scenario: Destroy a paste
    Given I have a paste named "Wicked links"
    When I destroy the "Wicked links" paste
    Then the "Wicked links" paste is not present in the list

  Scenario: Show a paste
    Given I have a paste named "Recipes"
    When I display the "Recipes" paste
    Then I see the details of "Recipes" paste

  Scenario: List my pastes
    Given I have 2 pastes
    And there are 2 other pastes in the sytem
    When I access the pastes list
    Then I see 2 pastes
