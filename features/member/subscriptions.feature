Feature: Manage subscriptions
  As an user
  In order to keep track of motions
  I want to manage my subscriptions

  Background:
    Given I have an account for "angelique@hellraiser.io"
    And I'm authenticated with "angelique@hellraiser.io"

  Scenario: List the subscriptions
    Given I subscribed to 2 motions
    When I access the subscriptions list
    Then I see 2 subscriptions

  Scenario: Destroy a subscription
    Given I have 1 subscription
    And I access the subscriptions list
    When I destroy the subscription
    Then I see 0 subscriptions
