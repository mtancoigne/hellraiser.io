Feature: RSS feeds
  As a member
  In order to follow the news and share interesting articles
  I want to be able to manage RSS feeds

  Background:
    Given I have an account for "jason@hellraiser.io"
    And I'm authenticated with "jason@hellraiser.io"

  Scenario: Add a feed
    Given I access to the new RSS feed form
    And I add the feed for LinuxFr
    Then I see the LinuxFr feed in the list

  Scenario: Update a feed
    Given I have the LinuxFr feed
    And I access the RSS feed update form for LinuxFr
    When I change the tags for "tech, opensource"
    Then I see the "tech, opensource" tags for LinuxFr

  Scenario: Destroy a feed
    Given I have the LinuxFr feed
    And I destroy the LinuxFr feed
    Then I don't see the LinuxFr feed in the list

  Scenario: List all the feeds
    Given I have 1 RSS feed
    And there is 1 other RSS feed in the system
    When I access the feeds list
    Then I see 2 feeds
