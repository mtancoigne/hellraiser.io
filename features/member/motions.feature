Feature: Motions
  As a member
  In order to discuss an plan future development
  I want to be able to post things and answer to others

  Background:
    Given I have an account for "angelique@hellraiser.io"
    And I'm authenticated with "angelique@hellraiser.io"

  Scenario: Create a motion
    Given I access the new motion form
    And I create a motion named "Meetup sunday?"
    Then I see the "Meetup sunday?" motion in the list

  Scenario: Update a motion
    Given I have a "Hellraiser party" motion
    And I access the motion update form for "Hellraiser party"
    When I change the motion name to "Friday the 13th party"
    Then I see the "Friday the 13th party" motion in the list

  Scenario: Display a motion
    Given I have a "Develop something nice" motion
    And I display the "Develop something nice" motion
    Then I see the motion details

  Scenario: Destroy a motion
    Given I have a "Spring hackathon" motion
    When I destroy the "Spring hackathon" motion
    Then I don't see the "Spring hackathon" motion in the list

  Scenario: List all the motions
    Given I have 1 motion
    Given there are 3 other motions in the system
    When I access the motions list
    Then I see 4 motions

  Scenario: Comment a motion
    Given I have a "Finish the motions feature" motion
    Given I display the "Finish the motions feature" motion
    And I leave a comment stating "I'm ok with this"
    # Beware of the quote
    Then I see the comment "I’m ok with this"

  Scenario: Edit a comment on a motion
    Given I have a comment stating "Me" on the "Who's here?" motion
    And I display the "Who's here?" motion
    When I update this comment to "Maybe"
    Then I see the new comment

  Scenario: Destroy a comment on a motion
    Given I have a comment stating "Hello" on the "How to greet someone?" motion
    And I display the "How to greet someone?" motion
    When I destroy this comment
    Then the comment is gone

  Scenario: Close a motion
    Given I have a "Spring hackathon" motion
    When I close the "Spring hackathon" motion
    And I access the motions list
    Then I see the closed "Spring hackathon" motion in the list

  Scenario: Upvote a motion
    Given there is one "Winter hackathon" motion
    And I display the "Winter hackathon" motion
    And I upvote the motion
    Then I see the motion with a score of 1/1
    And I can't upvote the motion

  Scenario: Downvote a motion
    Given there is one "Winter hackathon" motion
    And I display the "Winter hackathon" motion
    And I downvote the motion
    Then I see the motion with a score of 0/1
    And I can't downvote the motion

  Scenario: Subscribe to a motion
    Given there is one "Coding day" motion
    When I display the "Coding day" motion
    And I subscribe to it
    Then I'm subscribed to this motion

  Scenario: Unsubscribe to a motion
    Given I have a subscription for the "Coding day" motion
    When I display the "Coding day" motion
    And I unsubscribe to it
    Then I'm not subscribed to this motion

  Scenario: Someone comments a motion
    Given there is one "Coding day" motion
    When I display the "Coding day" motion
    And someone leaves a comment on the motion
    Then I see the comment popping in

  Scenario: Someone destroys a comment
    Given there is one "Coding day" motion
    And there is a comment on this motion
    When I display the "Coding day" motion
    And someone destroys the motion's comment
    Then I see the comment disappear

  Scenario: Someone updates a comment
    Given there is one "Coding day" motion
    And there is a comment on this motion
    When I display the "Coding day" motion
    And someone updates the motion's comment
    Then I see the updated comment replacing the old one

  Scenario: "Public" motion with comments
    Given there is one "Coding day" public motion
    And there is a comment on this motion
    When I display the "Coding day" motion
    Then I see the real names of participants

  Scenario: Anonymous motion with comments
    Given there is one "Coding day" motion
    And there is a comment on this motion
    When I display the "Coding day" motion
    Then I don't see the real names of participants
