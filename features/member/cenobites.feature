Feature: Cenobites
  As a member
  In order to interact with people I don't know
  I want them to have a way to contact me
  And I want a way to contact them

  Background:
    Given I have a visible account for "butterball@hellraiser.io"
    And I'm authenticated with "butterball@hellraiser.io"
    And there is a cenobite named "Alice" which accepts to be visible
    And there is a cenobite named "Chucky", who don't care about social stuff

  Scenario: Listing cenobites
    Given I access the cenobites list
    Then I see "Alice" in the list
    And I don't see "Chucky"

  Scenario: Inviting someone
    Given I access the cenobites list
    When I invite "Alice" from the list
    Then I see that the invitation for "Alice" has been created

  Scenario: Accepting an invite
    Given "Alice" sent me an acquaintance request
    Given I access the cenobites list
    When I accept the acquaintance request from "Alice"
    Then I see that the invitation for "Alice" has been accepted

  Scenario: Declining an invite
    Given "Alice" sent me an acquaintance request
    When I access the cenobites list
    And I cancel the acquaintance request with "Alice"
    Then I see that the invitation for "Alice" has been cancelled

  Scenario: Canceling an invite
    Given I sent an acquaintance request to "Alice"
    When I access the cenobites list
    And I cancel the acquaintance request with "Alice"
    Then I see that the invitation for "Alice" has been cancelled
