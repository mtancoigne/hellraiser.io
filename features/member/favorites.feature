Feature: Favorites management
  As a cenobite
  In order to find content I liked on the service
  I want to have a page that lists them

  Background:
    Given I have an account for "john@ripandtear.com"
    And I'm authenticated with "john@ripandtear.com"
    And I have favorited a link

  Scenario: List all my favorites
    When I access my favorites list
    Then I see 1 favorite

  Scenario: Destroy a favorite
    Given I access my favorites list
    When I destroy the favorite
    Then I see a message stating I have no favorited items
