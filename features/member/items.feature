Feature: Items management
  As a member
  In order to share and borrow items
  I want to be able to manage items in my possession

  Background:
    Given I have an account for "freddy@hellraiser.io"
    And I'm authenticated with "freddy@hellraiser.io"
    And there is a cenobite named "Angelique"
    And I'm acquainted with "Angelique"

  Scenario: Create an item
    Given I access the new item form
    And I create an item named "Kitchen gloves"
    Then I see the "Kitchen gloves" item in the list

  Scenario: Bulk create items
    Given I access the item bulk creation form
    When I create the following items:
      | Leather mask  |
      | Labyrinth map |
      | Voice changer |
    Then I see these items in the list:
      | Leather mask  |
      | Labyrinth map |
      | Voice changer |

  Scenario: Update an item
    Given I have a "Boiler" item
    And I access the item update form for the "Boiler"
    When I change the item name to "Large enough boiler"
    Then I see the "Large enough boiler" item in the list

  Scenario: Display an item
    Given I have a "Used knives set" item
    When I display the "Used knives set" item
    Then I see the item's details

  Scenario: Destroy an item
    Given I have a "Dream pills" item
    When I destroy the "Dream pills" item
    Then I don't see the "Dream pills" item in the list

  Scenario: Display items from my acquaintances
    Given "Angelique" has a "Leather dress"
    When I access the acquaintances item list
    Then I see the "Leather dress" item in the list

  Scenario: Lend an item
    Given I have a "Hand-sized hook" item
    When I lend the "Hand-sized hook" to "Angelique"
    Then I see that "Angelique" borrowed the "Hand-sized hook"
