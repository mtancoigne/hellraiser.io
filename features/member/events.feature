Feature: Events management
  As a cenobite
  In order to organize things to do
  I want to create and manage events
  And it's a plus if I can make private ones

  Background:
    Given I have an account for "jason.v@hellraiser.io"
    And I'm authenticated with "jason.v@hellraiser.io"

  Scenario: Create an event
    Given I access the event creation form
    And I create an event named "IRL meeting"
    Then I see the "IRL meeting" event in the list

  Scenario: Update an event
    Given I have a "Party hard" event
    And I access the event update form for "Party hard"
    When I change the event name to "Party a bit"
    Then I see the "Party a bit" event in the list

  Scenario: Destroy an event
    Given I have a "Workshop: sharpen minds" event
    When I destroy the "Workshop: sharpen minds" event
    Then I dont see the "Workshop: sharpen minds" event in the list

  Scenario: List all the upcoming events
    Given I have 1 event
    And there are 2 other member upcoming events in the system
    And there are 2 other private upcoming events in the system
    And there is 1 other member past event in the system
    And there is 1 other private past event in the system
    When I access the events list
    Then I see 3 events

  Scenario: List all the past events
    Given I have 1 past event
    And there are 2 other member upcoming events in the system
    And there are 2 other private upcoming events in the system
    And there is 1 other member past event in the system
    And there is 1 other private past event in the system
    When I access the past events list
    Then I see 2 events

  Scenario: Attend an event
    Given there is 1 other member upcoming event in the system
    When I access the events list
    And I display the event
    And I click on "I'll be there too"
    Then I joined the event successfully

  Scenario: Leave an event
    Given there is 1 other member upcoming event in the system
    And I joined this event
    When I access the events list
    And I display the event
    And I click on "I won't participate, finally"
    Then I left the event successfully

  Scenario: Check timezone for creation (server is at 2020/10/26 1:00 UTC)
    # No DST on Nairobi (UTC+3)
    Given I created an event for 17:00 (Africa/Nairobi)
    When I access the events list
    Then I see an event for "05:00 PM"

  Scenario: Comment an event
    Given I have a "IRL meeting" event
    Given I access the "IRL meeting" event
    And I leave a comment stating "Ok for me"
    Then I see the comment "Ok for me"

  Scenario: Edit a comment on an event
    Given I have a comment stating "You can count on me" on the "Fish painting session" event
    And I access the "Fish painting session" event
    When I update this comment to "Maybe"
    Then I see the new comment

  Scenario: Destroy a comment on an event
    Given I have a comment stating "This is utterly annoying" on the "Party @ home" event
    And I access the "Party @ home" event
    When I destroy this comment
    Then the comment is gone
