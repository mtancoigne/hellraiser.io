Feature: Invitations
  As a member
  In order to interact with people I know
  I want to be able to invite them

  Background:
    Given I have an account for "butterball@hellraiser.io"
    And I'm authenticated with "butterball@hellraiser.io"
    And there is a cenobite named "Alice"

  Scenario: Invite people from the outside world
    When I invite "candyman@honey.io"
    Then I see that "candyman@honey.io" has been invited

  Scenario: Invite people from inside
    When I add "Alice" in my acquaintances
    Then I see that the request for "Alice" is pending

  Scenario: Accept invitation
    Given "Alice" wants to invite me
    When I accept the request from "Alice"
    Then I'm now acquainted with "Alice"

  Scenario: Refuse invitation
    Given "Alice" wants to invite me
    And I access the acquaintances list
    When I cancel the request from "Alice"
    Then there's no pending request from "Alice"

  Scenario: Cancel invitation
    Given I add "Alice" in my acquaintances
    When I cancel the request to "Alice"
    Then there's no pending request from "Alice"

  Scenario: Revoke invitation
    Given I'm acquainted with "Alice"
    And I access the acquaintances list
    When I revoke the request from "Alice"
    Then there's no pending request from "Alice"
