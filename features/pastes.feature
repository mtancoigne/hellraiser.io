Feature: Pastes
  As a visitor
  I want to be able to access the pastes shared with me

  Scenario: Access a paste
    Given there is one paste in the system
    When I access the paste using the link
    Then I see the content

  Scenario: Access a burnable paste
    Given there is one burnable paste in the system
    When I access the paste using the link
    And I don't see the content
    And I display the paste content
    Then I see the content
    And the paste is deleted

  Scenario: Access a password-protected paste
    Given there is one protected paste in the system
    When I access the paste using the link
    And I don't see the content
    And I display the paste content using the password
    Then I see the content

  Scenario: Password error
    Given there is one protected paste in the system
    When I access the paste using the link
    And I display the paste content using a wrong password
    Then I see an error message
    And I don't see the content

  Scenario: Access a password-protected burnable paste
    Given there is one protected and burnable paste in the system
    When I access the paste using the link
    And I don't see the content
    And I display the paste content using the password
    Then I see the content
    And the paste is deleted

  Scenario: Render markdown
    Given there is a Markdown paste in the system
    When I access the paste using the link
    Then I see the rendered HTML paste

  Scenario: Render scripts
    Given there is a Ruby paste in the system
    When I access the paste using the link
    Then I see the higlighted Ruby code

  Scenario: Render unsupported scripts
    Given there is a dart paste in the system
    When I access the paste using the link
    Then I see the original code

