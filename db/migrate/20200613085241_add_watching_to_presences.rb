class AddWatchingToPresences < ActiveRecord::Migration[6.0]
  def change
    add_column :chat_presences, :watching, :boolean, null: false, default: false
  end
end
