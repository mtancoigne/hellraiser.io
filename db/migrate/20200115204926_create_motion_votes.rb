class CreateMotionVotes < ActiveRecord::Migration[6.0]
  def change
    create_table :motion_votes do |t|
      t.boolean :approve, default: false # rubocop:disable Rails/ThreeStateBooleanColumn
      t.references :motion, null: false, foreign_key: true
      t.references :cenobite, null: false, foreign_key: true

      t.timestamps
    end
  end
end
