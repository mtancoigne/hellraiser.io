class AddMotionVoteCountersInMotions < ActiveRecord::Migration[6.0]
  def change
    change_table :motions, bulk: true do |t|
      t.integer :motion_votes_count, null: false, default: 0
      t.integer :score, null: false, default: 0
    end
  end
end
