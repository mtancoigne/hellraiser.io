class CreateAcquaintances < ActiveRecord::Migration[6.0]
  def change
    create_table :acquaintances do |t|
      t.references :inviter, null: false, foreign_key: { to_table: :cenobites }
      t.references :invitee, null: false, foreign_key: { to_table: :cenobites }
      t.datetime :accepted_at

      t.timestamps
    end

    add_index :acquaintances, [:inviter_id, :invitee_id], unique: true
  end
end
