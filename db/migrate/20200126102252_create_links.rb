class CreateLinks < ActiveRecord::Migration[6.0]
  def change
    create_table :links do |t|
      t.string :title, null: false, default: nil
      t.text :url, null: false, default: nil
      t.string :description, null: false, default: nil
      t.references :cenobite, null: false, foreign_key: true

      t.timestamps
    end
  end
end
