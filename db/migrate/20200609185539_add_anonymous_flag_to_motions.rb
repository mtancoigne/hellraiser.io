class AddAnonymousFlagToMotions < ActiveRecord::Migration[6.0]
  def change
    add_column :motions, :anonymous, :boolean, null: false, default: true
  end
end
