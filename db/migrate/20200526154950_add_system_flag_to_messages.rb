class AddSystemFlagToMessages < ActiveRecord::Migration[6.0]
  def change
    add_column :chat_messages, :system, :boolean, null: false, default: false
  end
end
