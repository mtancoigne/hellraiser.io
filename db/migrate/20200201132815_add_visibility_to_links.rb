class AddVisibilityToLinks < ActiveRecord::Migration[6.0]
  def change
    add_column :links, :visibility, :integer, default: 0

    Link.find_each { |link| link.update_column :visibility, 1 } # rubocop:disable Rails/SkipsModelValidations
  end
end
