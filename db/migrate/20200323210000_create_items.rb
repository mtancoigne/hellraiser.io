class CreateItems < ActiveRecord::Migration[6.0]
  def change
    create_table :items do |t|
      t.string :name, null: false, default: nil
      t.text :description
      t.string :category
      t.references :owner, null: false, foreign_key: { to_table: :cenobites }
      t.references :borrower, null: true, foreign_key: { to_table: :cenobites }
      t.timestamp :borrowed_at

      t.timestamps

      t.index [:name, :category, :owner_id], unique: true
    end
  end
end
