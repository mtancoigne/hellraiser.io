class CreateEventsAttendances < ActiveRecord::Migration[6.0]
  def change
    create_table :events_attendances do |t|
      t.references :event, null: false, foreign_key: true
      t.references :cenobite, null: false, foreign_key: true

      t.timestamps

      t.index [:event_id, :cenobite_id], unique: true
    end
  end
end
