class CreateChatParticipants < ActiveRecord::Migration[6.0]
  def change
    create_table :chat_participants do |t|
      t.references :chat_room, null: false, foreign_key: true
      t.references :cenobite, null: false, foreign_key: true
      t.bigint :instance_id, null: false, default: nil

      t.timestamps
    end

    add_index :chat_participants, [:cenobite_id, :chat_room_id, :instance_id], name: 'cenobite_room_instance_index', unique: true
  end
end
