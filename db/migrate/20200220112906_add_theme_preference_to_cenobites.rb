class AddThemePreferenceToCenobites < ActiveRecord::Migration[6.0]
  def change
    add_column :cenobites, :theme, :string
  end
end
