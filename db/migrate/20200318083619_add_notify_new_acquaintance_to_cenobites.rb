class AddNotifyNewAcquaintanceToCenobites < ActiveRecord::Migration[6.0]
  def change
    add_column :cenobites, :notify_new_acquaintance, :boolean, default: true # rubocop:disable Rails/ThreeStateBooleanColumn
  end
end
