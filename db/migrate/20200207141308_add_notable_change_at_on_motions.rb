class AddNotableChangeAtOnMotions < ActiveRecord::Migration[6.0]
  def change
    add_column :motions, :notable_change_at, :datetime, default: -> { 'CURRENT_TIMESTAMP' }

    Motion.find_each do |motion|
      last_comment      = motion.comments.last
      notable_change_at = last_comment ? last_comment.created_at : motion.updated_at
      motion.update_column :notable_change_at, notable_change_at # rubocop:disable Rails/SkipsModelValidations
    end
  end
end
