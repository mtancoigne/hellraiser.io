class CreateRssItems < ActiveRecord::Migration[6.0]
  def change
    create_table :rss_items do |t|
      t.string :title, null: false, default: nil
      t.string :link, null: false, default: nil
      t.text :description, null: false, default: nil
      t.datetime :published_at
      t.integer :score, default: 0, null: false
      t.integer :votes_count, default: 0, null: false
      t.references :rss_feed, null: false, foreign_key: true

      t.timestamps

      t.index :link, unique: true
    end
  end
end
