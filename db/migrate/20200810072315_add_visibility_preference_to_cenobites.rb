class AddVisibilityPreferenceToCenobites < ActiveRecord::Migration[6.0]
  def change
    change_table :cenobites, bulk: true do |t|
      t.column :visible_by_members, :boolean, null: false, default: false
      t.column :show_email_to_acquaintances, :boolean, null: false, default: false
    end
  end
end
