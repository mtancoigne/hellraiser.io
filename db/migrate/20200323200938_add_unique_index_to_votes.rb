class AddUniqueIndexToVotes < ActiveRecord::Migration[6.0]
  def change
    add_index :votes, [:cenobite_id, :votable_id, :votable_type], unique: true
  end
end
