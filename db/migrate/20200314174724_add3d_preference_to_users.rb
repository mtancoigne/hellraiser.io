class Add3dPreferenceToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :cenobites, :use_3d_view, :boolean, default: false # rubocop:disable Rails/ThreeStateBooleanColumn
  end
end
