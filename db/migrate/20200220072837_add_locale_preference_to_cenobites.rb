class AddLocalePreferenceToCenobites < ActiveRecord::Migration[6.0]
  def change
    add_column :cenobites, :locale, :string
  end
end
