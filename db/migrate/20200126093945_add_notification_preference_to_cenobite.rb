class AddNotificationPreferenceToCenobite < ActiveRecord::Migration[6.0]
  def change
    change_table :cenobites, bulk: true do |t|
      t.boolean :notify_new_motion, default: true # rubocop:disable Rails/ThreeStateBooleanColumn
    end
  end
end
