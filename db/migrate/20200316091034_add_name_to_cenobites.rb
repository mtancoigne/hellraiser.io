class AddNameToCenobites < ActiveRecord::Migration[6.0]
  def up
    add_column :cenobites, :name, :string
  end
end
