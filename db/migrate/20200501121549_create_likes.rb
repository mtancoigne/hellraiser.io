class CreateLikes < ActiveRecord::Migration[6.0]
  def change
    create_table :likes do |t|
      t.references :cenobite, null: false, foreign_key: true
      t.references :likable, polymorphic: true, null: false

      t.timestamps

      t.index [:cenobite_id, :likable_id, :likable_type], unique: true
    end
  end
end
