class CreateDeadManSwitches < ActiveRecord::Migration[6.0]
  def change
    create_table :dead_man_switches do |t|
      t.string :name, null: false, default: nil
      t.text :recipients, null: false, default: nil
      t.text :content, null: false, default: nil
      t.integer :ping_frequency, null: false, default: (24 * 7)
      t.datetime :last_pinged_at
      t.datetime :expires_at
      t.datetime :email_sent_at
      t.references :cenobite, null: false, foreign_key: true

      t.timestamps
    end
  end
end
