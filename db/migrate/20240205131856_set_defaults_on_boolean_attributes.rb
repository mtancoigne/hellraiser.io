class SetDefaultsOnBooleanAttributes < ActiveRecord::Migration[7.0]
  def change
    change_column_null :pastes, :burn, false
    change_column_null :motions, :closed, false
    change_column_null :votes, :approve, false
    change_column_null :cenobites, :use_3d_view, false
    change_column_null :cenobites, :notify_new_acquaintance, false
    change_column_null :cenobites, :notify_new_motion, false
    change_column_null :chat_participants, :auto_join, false
  end
end
