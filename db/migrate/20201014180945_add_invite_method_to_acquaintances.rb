class AddInviteMethodToAcquaintances < ActiveRecord::Migration[6.0]
  def change
    add_column :acquaintances, :invite_method, :integer, default: 0
  end
end
