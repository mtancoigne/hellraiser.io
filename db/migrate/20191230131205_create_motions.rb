class CreateMotions < ActiveRecord::Migration[6.0]
  def change
    create_table :motions do |t|
      t.string :title, null: false, default: nil
      t.text :content, null: false, default: nil
      t.boolean :closed, default: false # rubocop:disable Rails/ThreeStateBooleanColumn
      t.references :cenobite, null: false, foreign_key: true

      t.timestamps
    end
  end
end
