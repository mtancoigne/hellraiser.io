class CreateEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :events do |t|
      t.string :name, null: false, default: nil
      t.text :description, null: false, default: nil
      t.string :place
      t.string :url
      t.datetime :starts_at, null: false, default: nil
      t.string :starts_at_timezone, null: false, default: 'UTC'
      t.datetime :ends_at, null: false, default: nil
      t.string :ends_at_timezone, null: false, default: 'UTC'
      t.references :cenobite, null: false, foreign_key: true
      t.integer :visibility, null: false, default: 0

      t.timestamps
    end
  end
end
