class CreateRssFeeds < ActiveRecord::Migration[6.0]
  def change
    create_table :rss_feeds do |t|
      t.string :url, null: false, default: nil
      t.string :title, null: false, default: nil
      t.string :link, null: false, default: nil
      t.string :description
      t.string :language
      t.datetime :last_fetched_at
      t.references :cenobite, null: false, foreign_key: true

      t.timestamps

      t.index :url, unique: true
    end
  end
end
