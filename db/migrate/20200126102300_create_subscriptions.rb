class CreateSubscriptions < ActiveRecord::Migration[6.0]
  def change
    create_table :subscriptions do |t|
      t.references :entity, polymorphic: true, null: false
      t.references :cenobite, null: false, foreign_key: true

      t.timestamps
    end

    Motion.find_each { |motion| motion.subscribe! motion.cenobite }
    add_index :subscriptions, [:entity_id, :entity_type, :cenobite_id], unique: true, name: 'index_on_subscriptions'
  end
end
