class CreateChatPresences < ActiveRecord::Migration[6.0]
  def change
    rename_table :chat_participants, :chat_presences

    create_table :chat_participants do |t|
      t.string :name, null: false, default: nil
      t.boolean :auto_join, default: false # rubocop:disable Rails/ThreeStateBooleanColumn
      t.references :chat_room, foreign_key: true
      t.references :cenobite, foreign_key: true

      t.timestamps
    end

    add_index :chat_participants, [:chat_room_id, :cenobite_id], unique: true

    reversible do |dir|
      dir.up do
        change_table :chat_presences, bulk: true do |t|
          t.remove :cenobite_id
          t.references :chat_participant, foreign_key: true, after: :chat_room_id
        end

        change_table :chat_messages, bulk: true do |t|
          t.remove :cenobite_id
          t.references :chat_participant, foreign_key: true, after: :chat_room_id
        end

        add_index :chat_presences, [:chat_room_id, :chat_participant_id, :instance_id], name: 'room_participant_instance_index', unique: true
      end

      dir.down do
        change_table :chat_messages, bulk: true do |t|
          t.remove :chat_participant_id
          t.references :cenobite, foreign_key: true, after: :chat_room_id
        end

        change_table :chat_presences, bulk: true do |t|
          t.remove :chat_participant_id
          t.references :cenobite, foreign_key: true, after: :chat_room_id
        end
      end
    end
  end
end
