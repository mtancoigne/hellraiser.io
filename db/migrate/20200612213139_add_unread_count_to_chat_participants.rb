class AddUnreadCountToChatParticipants < ActiveRecord::Migration[6.0]
  def change
    add_column :chat_participants, :unread_count, :integer, null: false, default: 0
  end
end
