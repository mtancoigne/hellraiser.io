class AddAcquaintancesToChatRooms < ActiveRecord::Migration[6.0]
  def change
    add_reference :chat_rooms, :acquaintance, null: true, default: nil

    reversible do |dir|
      acquaintances = execute 'SELECT * FROM acquaintances WHERE accepted_at IS NOT NULL'
      now = Time.current

      dir.up do
        change_column :chat_rooms, :name, :string, null: true, default: nil
        change_column :chat_participants, :name, :string, null: true, default: nil

        acquaintances.each do |a|
          room = execute "INSERT INTO chat_rooms (cenobite_id, acquaintance_id, created_at, updated_at) VALUES (#{a['inviter_id']}, #{a['id']}, '#{now}', '#{now}') RETURNING id"
          cenobite1 = execute "SELECT id, name FROM cenobites WHERE id = #{a['inviter_id']}"
          cenobite2 = execute "SELECT id, name FROM cenobites WHERE id = #{a['invitee_id']}"

          name1 = cenobite1.first['name'] || "cenobite_#{cenobite1.first['id']}"
          name2 = cenobite2.first['name'] || "cenobite_#{cenobite2.first['id']}"
          execute "INSERT INTO chat_participants (name, chat_room_id, cenobite_id, created_at, updated_at, auto_join) VALUES ('#{name1}', #{room.first['id']}, #{a['inviter_id']}, '#{now}', '#{now}', true)"
          execute "INSERT INTO chat_participants (name, chat_room_id, cenobite_id, created_at, updated_at, auto_join) VALUES ('#{name2}', #{room.first['id']}, #{a['invitee_id']}, '#{now}', '#{now}', true)"
        end
      end

      dir.down do
        acquaintances.each do |a|
          rooms = execute "SELECT id FROM chat_rooms WHERE acquaintance_id = #{a['id']}"
          rooms.each do |r|
            execute "DELETE FROM chat_messages WHERE chat_room_id = #{r['id']}"
            execute "DELETE FROM chat_presences WHERE chat_room_id = #{r['id']}"
            execute "DELETE FROM chat_participants WHERE chat_room_id = #{r['id']}"
            execute "DELETE FROM chat_rooms WHERE id = #{r['id']}"
          end
        end

        change_column :chat_rooms, :name, :string, null: false, default: nil
        change_column :chat_participants, :name, :string, null: false, default: nil
      end
    end
  end
end
