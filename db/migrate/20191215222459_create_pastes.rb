class CreatePastes < ActiveRecord::Migration[6.0]
  def change
    create_table :pastes, id: :uuid do |t|
      t.string :name, null: false, default: nil
      t.string :extension, null: false, default: 'txt'
      t.text :content, null: false, default: nil
      t.boolean :burn, default: false # rubocop:disable Rails/ThreeStateBooleanColumn
      t.string :password_digest
      t.references :cenobite, null: false, foreign_key: true

      t.timestamps
    end
  end
end
