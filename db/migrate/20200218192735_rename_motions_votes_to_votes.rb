class RenameMotionsVotesToVotes < ActiveRecord::Migration[6.0]
  def change
    rename_table :motion_votes, :votes

    reversible do |dir|
      dir.up do
        add_reference :votes, :votable, polymorphic: true, index: true
        execute "UPDATE votes SET votable_type = 'Motion', votable_id=motion_id"
        remove_column :votes, :motion_id
        rename_column :motions, :motion_votes_count, :votes_count
      end

      dir.down do
        rename_column :motions, :votes_count, :motion_votes_count
        add_reference :votes, :motion, foreign_key: true
        execute "UPDATE votes SET motion_id=votable_id WHERE votable_type='Motion'"
        remove_reference :votes, :votable, polymorphic: true
      end
    end
  end
end
