class CreateFortunes < ActiveRecord::Migration[6.0]
  def change
    create_table :fortunes do |t|
      t.text :content, null: false, default: nil
      t.string :source
      t.integer :votes_count, null: false, default: 0
      t.integer :score, null: false, default: 0
      t.references :cenobite, null: false, foreign_key: true

      t.timestamps
    end
  end
end
