class AddVotesToLinks < ActiveRecord::Migration[6.0]
  def change
    change_table :links, bulk: true do |t|
      t.integer :score, null: false, default: 0
      t.integer :votes_count, null: false, default: 0
    end
  end
end
