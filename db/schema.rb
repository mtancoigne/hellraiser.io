# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2024_02_05_131856) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"

  create_table "acquaintances", force: :cascade do |t|
    t.bigint "inviter_id", null: false
    t.bigint "invitee_id", null: false
    t.datetime "accepted_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "invite_method", default: 0
    t.index ["invitee_id"], name: "index_acquaintances_on_invitee_id"
    t.index ["inviter_id", "invitee_id"], name: "index_acquaintances_on_inviter_id_and_invitee_id", unique: true
    t.index ["inviter_id"], name: "index_acquaintances_on_inviter_id"
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum"
    t.datetime "created_at", precision: nil, null: false
    t.string "service_name", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "cenobites", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at", precision: nil
    t.datetime "remember_created_at", precision: nil
    t.string "confirmation_token"
    t.datetime "confirmed_at", precision: nil
    t.datetime "confirmation_sent_at", precision: nil
    t.string "unconfirmed_email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "invitation_token"
    t.datetime "invitation_created_at", precision: nil
    t.datetime "invitation_sent_at", precision: nil
    t.datetime "invitation_accepted_at", precision: nil
    t.integer "invitation_limit"
    t.string "invited_by_type"
    t.bigint "invited_by_id"
    t.integer "invitations_count", default: 0
    t.boolean "notify_new_motion", default: true, null: false
    t.string "locale"
    t.string "theme"
    t.boolean "use_3d_view", default: false, null: false
    t.string "name"
    t.boolean "notify_new_acquaintance", default: true, null: false
    t.boolean "visible_by_members", default: false, null: false
    t.boolean "show_email_to_acquaintances", default: false, null: false
    t.index ["confirmation_token"], name: "index_cenobites_on_confirmation_token", unique: true
    t.index ["email"], name: "index_cenobites_on_email", unique: true
    t.index ["invitation_token"], name: "index_cenobites_on_invitation_token", unique: true
    t.index ["invitations_count"], name: "index_cenobites_on_invitations_count"
    t.index ["invited_by_id"], name: "index_cenobites_on_invited_by_id"
    t.index ["invited_by_type", "invited_by_id"], name: "index_cenobites_on_invited_by_type_and_invited_by_id"
    t.index ["reset_password_token"], name: "index_cenobites_on_reset_password_token", unique: true
  end

  create_table "chat_messages", force: :cascade do |t|
    t.text "content", null: false
    t.bigint "chat_room_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "chat_participant_id"
    t.boolean "system", default: false, null: false
    t.index ["chat_participant_id"], name: "index_chat_messages_on_chat_participant_id"
    t.index ["chat_room_id"], name: "index_chat_messages_on_chat_room_id"
  end

  create_table "chat_participants", force: :cascade do |t|
    t.string "name"
    t.boolean "auto_join", default: false, null: false
    t.bigint "chat_room_id"
    t.bigint "cenobite_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "unread_count", default: 0, null: false
    t.index ["cenobite_id"], name: "index_chat_participants_on_cenobite_id"
    t.index ["chat_room_id", "cenobite_id"], name: "index_chat_participants_on_chat_room_id_and_cenobite_id", unique: true
    t.index ["chat_room_id"], name: "index_chat_participants_on_chat_room_id"
  end

  create_table "chat_presences", force: :cascade do |t|
    t.bigint "chat_room_id", null: false
    t.bigint "instance_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "chat_participant_id"
    t.boolean "watching", default: false, null: false
    t.index ["chat_participant_id"], name: "index_chat_presences_on_chat_participant_id"
    t.index ["chat_room_id", "chat_participant_id", "instance_id"], name: "room_participant_instance_index", unique: true
    t.index ["chat_room_id"], name: "index_chat_presences_on_chat_room_id"
  end

  create_table "chat_rooms", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.bigint "cenobite_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "acquaintance_id"
    t.index ["acquaintance_id"], name: "index_chat_rooms_on_acquaintance_id"
    t.index ["cenobite_id"], name: "index_chat_rooms_on_cenobite_id"
  end

  create_table "comments", force: :cascade do |t|
    t.text "content"
    t.string "commentable_type", null: false
    t.bigint "commentable_id", null: false
    t.bigint "cenobite_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cenobite_id"], name: "index_comments_on_cenobite_id"
    t.index ["commentable_type", "commentable_id"], name: "index_comments_on_commentable_type_and_commentable_id"
  end

  create_table "dead_man_switches", force: :cascade do |t|
    t.string "name", null: false
    t.text "recipients", null: false
    t.text "content", null: false
    t.integer "ping_frequency", default: 168, null: false
    t.datetime "last_pinged_at", precision: nil
    t.datetime "expires_at", precision: nil
    t.datetime "email_sent_at", precision: nil
    t.bigint "cenobite_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cenobite_id"], name: "index_dead_man_switches_on_cenobite_id"
  end

  create_table "events", force: :cascade do |t|
    t.string "name", null: false
    t.text "description", null: false
    t.string "place"
    t.string "url"
    t.datetime "starts_at", precision: nil, null: false
    t.string "starts_at_timezone", default: "UTC", null: false
    t.datetime "ends_at", precision: nil, null: false
    t.string "ends_at_timezone", default: "UTC", null: false
    t.bigint "cenobite_id", null: false
    t.integer "visibility", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cenobite_id"], name: "index_events_on_cenobite_id"
  end

  create_table "events_attendances", force: :cascade do |t|
    t.bigint "event_id", null: false
    t.bigint "cenobite_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cenobite_id"], name: "index_events_attendances_on_cenobite_id"
    t.index ["event_id", "cenobite_id"], name: "index_events_attendances_on_event_id_and_cenobite_id", unique: true
    t.index ["event_id"], name: "index_events_attendances_on_event_id"
  end

  create_table "fortunes", force: :cascade do |t|
    t.text "content", null: false
    t.string "source"
    t.integer "votes_count", default: 0, null: false
    t.integer "score", default: 0, null: false
    t.bigint "cenobite_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cenobite_id"], name: "index_fortunes_on_cenobite_id"
  end

  create_table "items", force: :cascade do |t|
    t.string "name", null: false
    t.text "description"
    t.string "category"
    t.bigint "owner_id", null: false
    t.bigint "borrower_id"
    t.datetime "borrowed_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["borrower_id"], name: "index_items_on_borrower_id"
    t.index ["name", "category", "owner_id"], name: "index_items_on_name_and_category_and_owner_id", unique: true
    t.index ["owner_id"], name: "index_items_on_owner_id"
  end

  create_table "likes", force: :cascade do |t|
    t.bigint "cenobite_id", null: false
    t.string "likable_type", null: false
    t.bigint "likable_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cenobite_id", "likable_id", "likable_type"], name: "index_likes_on_cenobite_id_and_likable_id_and_likable_type", unique: true
    t.index ["cenobite_id"], name: "index_likes_on_cenobite_id"
    t.index ["likable_type", "likable_id"], name: "index_likes_on_likable_type_and_likable_id"
  end

  create_table "links", force: :cascade do |t|
    t.string "title", null: false
    t.text "url", null: false
    t.string "description", null: false
    t.bigint "cenobite_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "visibility", default: 0
    t.integer "score", default: 0, null: false
    t.integer "votes_count", default: 0, null: false
    t.index ["cenobite_id"], name: "index_links_on_cenobite_id"
  end

  create_table "motions", force: :cascade do |t|
    t.string "title", null: false
    t.text "content", null: false
    t.boolean "closed", default: false, null: false
    t.bigint "cenobite_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "votes_count", default: 0, null: false
    t.integer "score", default: 0, null: false
    t.datetime "notable_change_at", precision: nil, default: -> { "CURRENT_TIMESTAMP" }
    t.boolean "anonymous", default: true, null: false
    t.index ["cenobite_id"], name: "index_motions_on_cenobite_id"
  end

  create_table "pastes", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.string "extension", default: "txt", null: false
    t.text "content", null: false
    t.boolean "burn", default: false, null: false
    t.string "password_digest"
    t.bigint "cenobite_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cenobite_id"], name: "index_pastes_on_cenobite_id"
  end

  create_table "rss_feeds", force: :cascade do |t|
    t.string "url", null: false
    t.string "title", null: false
    t.string "link", null: false
    t.string "description"
    t.string "language"
    t.datetime "last_fetched_at", precision: nil
    t.bigint "cenobite_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cenobite_id"], name: "index_rss_feeds_on_cenobite_id"
    t.index ["url"], name: "index_rss_feeds_on_url", unique: true
  end

  create_table "rss_items", force: :cascade do |t|
    t.string "title", null: false
    t.string "link", null: false
    t.text "description", null: false
    t.datetime "published_at", precision: nil
    t.integer "score", default: 0, null: false
    t.integer "votes_count", default: 0, null: false
    t.bigint "rss_feed_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["link"], name: "index_rss_items_on_link", unique: true
    t.index ["rss_feed_id"], name: "index_rss_items_on_rss_feed_id"
  end

  create_table "subscriptions", force: :cascade do |t|
    t.string "entity_type", null: false
    t.bigint "entity_id", null: false
    t.bigint "cenobite_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cenobite_id"], name: "index_subscriptions_on_cenobite_id"
    t.index ["entity_id", "entity_type", "cenobite_id"], name: "index_on_subscriptions", unique: true
    t.index ["entity_type", "entity_id"], name: "index_subscriptions_on_entity_type_and_entity_id"
  end

  create_table "taggings", id: :serial, force: :cascade do |t|
    t.integer "tag_id"
    t.string "taggable_type"
    t.integer "taggable_id"
    t.string "tagger_type"
    t.integer "tagger_id"
    t.string "context", limit: 128
    t.datetime "created_at", precision: nil
    t.index ["context"], name: "index_taggings_on_context"
    t.index ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true
    t.index ["tag_id"], name: "index_taggings_on_tag_id"
    t.index ["taggable_id", "taggable_type", "context"], name: "taggings_taggable_context_idx"
    t.index ["taggable_id", "taggable_type", "tagger_id", "context"], name: "taggings_idy"
    t.index ["taggable_id"], name: "index_taggings_on_taggable_id"
    t.index ["taggable_type"], name: "index_taggings_on_taggable_type"
    t.index ["tagger_id", "tagger_type"], name: "index_taggings_on_tagger_id_and_tagger_type"
    t.index ["tagger_id"], name: "index_taggings_on_tagger_id"
  end

  create_table "tags", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.integer "taggings_count", default: 0
    t.index ["name"], name: "index_tags_on_name", unique: true
  end

  create_table "votes", force: :cascade do |t|
    t.boolean "approve", default: false, null: false
    t.bigint "cenobite_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "votable_type"
    t.bigint "votable_id"
    t.index ["cenobite_id", "votable_id", "votable_type"], name: "index_votes_on_cenobite_id_and_votable_id_and_votable_type", unique: true
    t.index ["cenobite_id"], name: "index_votes_on_cenobite_id"
    t.index ["votable_type", "votable_id"], name: "index_votes_on_votable_type_and_votable_id"
  end

  add_foreign_key "acquaintances", "cenobites", column: "invitee_id"
  add_foreign_key "acquaintances", "cenobites", column: "inviter_id"
  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "chat_messages", "chat_participants"
  add_foreign_key "chat_messages", "chat_rooms"
  add_foreign_key "chat_participants", "cenobites"
  add_foreign_key "chat_participants", "chat_rooms"
  add_foreign_key "chat_presences", "chat_participants"
  add_foreign_key "chat_presences", "chat_rooms"
  add_foreign_key "chat_rooms", "cenobites"
  add_foreign_key "comments", "cenobites"
  add_foreign_key "dead_man_switches", "cenobites"
  add_foreign_key "events", "cenobites"
  add_foreign_key "events_attendances", "cenobites"
  add_foreign_key "events_attendances", "events"
  add_foreign_key "fortunes", "cenobites"
  add_foreign_key "items", "cenobites", column: "borrower_id"
  add_foreign_key "items", "cenobites", column: "owner_id"
  add_foreign_key "likes", "cenobites"
  add_foreign_key "links", "cenobites"
  add_foreign_key "motions", "cenobites"
  add_foreign_key "pastes", "cenobites"
  add_foreign_key "rss_feeds", "cenobites"
  add_foreign_key "rss_items", "rss_feeds"
  add_foreign_key "subscriptions", "cenobites"
  add_foreign_key "taggings", "tags"
  add_foreign_key "votes", "cenobites"
end
