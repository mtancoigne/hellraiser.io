# NOTE: on models with 'has_many :xxx, class_name: "Yyy"', there is an issue
# with Spring causing the classes to be somehow reloaded. To avoid
# "ActiveRecord::AssociationTypeMismatch", errors during seeding, we always
# recreate the cenobite instance whenever we need one.
#
# Cenobite.first
FactoryBot.create :cenobite, :known, :active
# Cenobite.second
FactoryBot.create :cenobite

# Acquaintances
# ------------------------------------------------------------------------------
FactoryBot.create :acquaintance, inviter: Cenobite.first
FactoryBot.create :acquaintance, :accepted, inviter: Cenobite.first, invitee: Cenobite.second
FactoryBot.create :acquaintance, invitee: Cenobite.first
FactoryBot.create :acquaintance, :accepted, invitee: Cenobite.first
# Unrelated acquaintances
FactoryBot.create_list :acquaintance, 2

# Dead man switches
# ------------------------------------------------------------------------------
FactoryBot.create :dead_man_switch, ping_frequency: 24, cenobite: Cenobite.first

# Events
# ------------------------------------------------------------------------------
FactoryBot.create :event, cenobite: Cenobite.first
FactoryBot.create :event, :with_attendee, cenobite: Cenobite.first
FactoryBot.create :event, starts_at: 6.hours.from_now, ends_at: 10.hours.from_now

# Fortunes
# ------------------------------------------------------------------------------
FactoryBot.create :fortune, cenobite: Cenobite.first
FactoryBot.create_list :fortune, 2

# Items
# ------------------------------------------------------------------------------
FactoryBot.create :item, name: 'How to write Ruby code', category: 'Books', owner: Cenobite.first
FactoryBot.create :item, name: 'Supercat lost in space', category: 'DVD', owner: Cenobite.first, borrower: Cenobite.second
FactoryBot.create :item, name: 'Hot to grow potatoes', category: 'Books', owner: Cenobite.second
FactoryBot.create :item, name: 'Hellraiser IV: Bloodline', owner: Cenobite.second, borrower: Cenobite.first
# Other items
FactoryBot.create :item

# Liked elements
# ------------------------------------------------------------------------------
FactoryBot.create :like, cenobite: Cenobite.first

# Links
# ------------------------------------------------------------------------------
FactoryBot.create :link, cenobite: Cenobite.first

# Motions
# ------------------------------------------------------------------------------
FactoryBot.create :motion, title: 'An anonymous motion', cenobite: Cenobite.first
FactoryBot.create :motion, title: 'A non anonymous motion', anonymous: false, cenobite: Cenobite.first
FactoryBot.create_list :motion, 2

# Pastes
# ------------------------------------------------------------------------------
FactoryBot.create :paste, cenobite: Cenobite.first
FactoryBot.create :paste, :burnable, cenobite: Cenobite.first
FactoryBot.create :paste, :protected, cenobite: Cenobite.first
FactoryBot.create :paste, :burnable, :protected, cenobite: Cenobite.first

# RSS items
# ------------------------------------------------------------------------------
feed = FactoryBot.create :rss_feed, cenobite: Cenobite.first
FactoryBot.create :rss_item, rss_feed: feed
FactoryBot.create :rss_item
