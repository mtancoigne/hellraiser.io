Styleguide::Engine.routes.draw do
  root to: 'home#index'

  get '/guide/:page', to: 'home#page', as: 'page'
  get '/guide/:page/:preview', to: 'home#preview', as: 'preview'
end
