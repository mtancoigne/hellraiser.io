module Styleguide
  module ApplicationHelper
    def render_source_html(section, page)
      content = File.read(Styleguide::Engine.root.join('app', 'views', 'styleguide', '_previews', section, "_#{page}.html.haml"))
      content.html_safe # rubocop:disable Rails/OutputSafety
    end

    def preview(section:, page:, title:, tag: 'h2', &block)
      render 'styleguide/_partials/demo', section: section, page: page, title: title, tag: tag do
        capture(&block) if block
      end
    end

    def theme_link(name, display_name)
      css_class = session[:styleguide_theme] == name ? 'active' : ''
      link_to display_name, { theme: name, method: :get }, class: css_class
    end
  end
end
