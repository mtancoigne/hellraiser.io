module Styleguide
  class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception
    before_action :set_theme

    def set_theme
      session[:styleguide_theme] ||= 'dark'
      session[:styleguide_theme] = params[:theme] if params[:theme] && %w[dark light clean_dark clean_light].include?(params[:theme])
    end
  end
end
