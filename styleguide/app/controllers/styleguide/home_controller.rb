module Styleguide
  class HomeController < ApplicationController
    layout 'styleguide/preview', only: [:preview]

    def index; end

    def page
      @view = "styleguide/_sections/#{page_view}"
    end

    def preview
      page_name = page_view

      preview_name = params[:preview]
      raise 'No preview specified' unless preview_name

      base_path = Styleguide::Engine.root.join 'app', 'views', 'styleguide', '_previews'
      view_file = File.expand_path File.join(base_path, page_name, "_#{preview_name}.html.haml")

      raise "#{view_file} is outside the allowed view path" unless view_file.match?(/^#{base_path}/)
      raise "Specified preview does not exists (#{view_file})" unless File.exist? view_file

      @view = "styleguide/_previews/#{page_name}/#{preview_name}"
    end

    private

    def page_view
      view = params[:page] || 'index'
      raise 'No page specified' unless view

      view_file = Styleguide::Engine.root.join('app', 'views', 'styleguide', '_sections', "_#{view}.html.haml")
      raise "Specified page does not exists (#{view_file})" unless File.exist? view_file

      view
    end
  end
end
