# README

## Setup

We use PostgreSQL for this project.

```sh
yarn install --check-files
bundle install
cp config/database.default.yml config/database.yml
cp config/hellraiser_defaults.yml config/hellraiser.yml
```
Edit the database and hellraiser configuration files.

Generate secrets and master key:

```
# Use config/credentials.example.yml as reference.
rails credentials:edit
```

Backup the master key and credential in some safe place if you're deploying this in production.

Run the migrations and seeds:

```
# Creates db, migrations and seeds the database
# It creates cenobite "user@example.com" with password "password"
rails db:prepare
```

## Contribution

You want to propose you own changes to Hellraiser? You're welcome!

### Workflow and what not to forget

A typical workflow when contributing can be described as follow:

1. Fork the repo (if you don't have write access to the repo).
2. Create a new branch from `master`. Name it as you want, as long as it
   makes sense.
3. Open a merge request against our master with a name starting with
   `WIP:` or `Draft:`, explaining _what_ your changes does. Opening a
   merge request early makes it easy for us to help and guide you.
4. Code code code and have fun. Push when you want.
5. Don't forget to update the changelogs when you're done (see below).
6. Remove the `WIP:` or `Draft:` prefix and assign the MR to someone who
   can review.
7. If your code is accepted, engage a party with your friends, to
   celebrate this wonderful contribution. We'd like to participate too,
   but I feel like it will be hard to do so.


### Changelogs?

We don't have a code changelog, as we rely on clean commit messages for
the development history. And there is no application versioning, so it's
useless.

But we have human readable changelogs, accessible by the website users,
written in all supported languages (in
`app/views/member/pages/changelog.*.md.erb`). If you want to contribute
to the project, you'll have to add a changelog entry for _fixes and new
features_. If you're uncomfortable with one language, just ask when you
open a merge request, we'll be happy to help.

### Commit style

We use [conventional commits] styles, as it makes searching through
commits easier.

Don't feel limited to 50 chars with the commit subject, but try to be
concise nonetheless.

Don't hesitate to add a commit body on fixes, reworks or new features:
commit logs are important and makes the history of the project
understandable by us, humans.

#### Used actions

- `feat`: This commit introduces a new feature
- `fix`: This commit fixes something broken
- `rework`: This commit only rework code and adds no feature nor fix
- `style`: This commit fixes coding style (i.e.: after a Rubocop
  upgrade)
- `chore`: This commit is a maintenance commit (i.e.: new dependency,
  upgrades, etc...)

#### Scopes

Scopes targets _what the commit touches_. They may be hard to define
sometimes, but here is a small list to get started if your commit
touches...

- ... an application feature: use the feature name (e.g.: `chat`,
  `motions`, `links`, etc...)
- ... a library: use the library path (e.g.: `hellraiser/rss`)
- ... a dependency file: use the target language (e.g.: `ruby`, `node`)

For everything else, be creative!

### Example log

```text
feat(chat): Greet people when they enter a new channel
rework(chat): Split channels code to be reusable
fix(hell_raiser/colors): Prevent background and foreground colors to be the same
style(js): Fix new ESLint errors
chore(node): Upgrade dependencies
fix(links): Display links with the right CSS class
feat(motions): Add pagination to index
chore(ruby): Add Kaminary dependency
```

## Development

### Scaffolding

Scaffolding templates were customized to fit in the project, in
`lib/templates`.

- Use `rails g scaffold Xxx ...` to generate public elements (index/show
  only)
- Once you have a model, use `rails g scaffold_controller member/Xxx
  ...` to generate protected CRUD for this model

Note that RSpec controller tests are not generated for new controllers,
you have to create them manually.

### FactoryBot - Generate entities

[FactoryBot](https://github.com/thoughtbot/factory_bot) Rails allows to
quickly build and create entities. It's useful in tests, as creating
fixtures can be a real pain.

They are located in `spec/factories`, and are available in Rails
console, RSpec and Cucumber. It's also used for development seeds.

Note that FactoryBot is not available in production.

FactoryBot is configured to create related entities by default, instead
of only building them (`FactoryBot.use_parent_strategy = false`)

#### Check the factories

A rake task can be used to test the ability to run a factory multiple
times:

```sh
rake factory_bot:lint
```

This task is executed in CI

### Faker - Fake data generation

[Faker](https://github.com/faker-ruby/faker) is used during development
to generate fake data. It should be used in new factories.

### Authentication

We use [Devise](https://github.com/plataformatec/devise) for
authentication.

These FactoryBot factories will help you during the development:

- `:cenobite` for a random user
- `:cenobite_known` for an user with email "user@example.com"

All created user are created with the `password` password unless you
specify a custom one.

### Authorization

Authorization is managed with
[Pundit](https://github.com/varvet/pundit).

Usage of `authorize xxx` and `policy_scope` are not enforced in
controllers, but in RSpec tests (check `spec/rails_helper.rb`).

### Roles namespaces

Roles have their own controller namespaces (while models have not):

- `app/controllers` is for public controllers
- `app/controllers/member` is for registered users

### Notable concerns

#### Votable

Adds vote-related methods to model. Your table will need two fields,
`votes_count` and `score` for this to work.

Check the `Motion` model and `motions` table in `db/schema.rb` to see an
implementation of these fields.

To create routes/endpoints, check the routes for motions.

Don't forget to add the parameter to the `MODELS` hash in
`VotesController` to allow votes on your new content.

### Internationalization

Internationalization is made as in "traditional" Rails applications, and
is managed with [i18n-tasks](https://github.com/glebm/i18n-tasks)

`i18n-tasks` helps to check if your translations are up-to-date.

A custom rake task exists to add missing translations, prefixed with
`TRANSLATE_ME` to find them easily, and another one adds the model
attributes in a dummy file, so they can be translated.

```sh
# Check
i18n-tasks health
# Add missing model attributes in `app/i18n/model_attributes.i18n`
rake i18n:add-models-attributes
# Add missing translations
rake i18n:add-missing
# Remove unused translations
i18n-tasks remove-unused
```

Check [config/i18n-tasks.yml](config/i18n-tasks.yml) for configuration.

A RSpec test checks for missing/unused translations, and a CI job tests
for missing model attributes translations.

#### JS internationalization

Internationalization for Javascripts components are generated by
[i18n-js](https://github.com/fnando/i18n-js) and lives in the default
translation files (`config/locales`). Only the `generic.*` and `js.*`
keys are exported. Check [config/i18n-js.yml](config/i18n-js.yml) for
configuration.

The `I18n` global variable is available to use in JS scripts.

VueJS components use `vue-i18n` for translations. Use the helper in
`app/javascript/vue/helpers/i18n.js` to initialize the module (check
`app/javascripts/packs/app/chat.js` for an example).

**Be careful about string interpolation and pluralization**; the
translation strings don't have the same format when they are used with
`vue-i18n`. Check
[Vue I18n documentation](https://kazupon.github.io/vue-i18n/guide/formatting.html)
and [Rails I18n documentation](https://guides.rubyonrails.org/i18n.html)
for differences.

Thanks to [i18n-js](http://rubygems.org/gems/i18n-js), scripts can use
the `I18n` global to translate strings.

### Haml views

This project uses [HAML](http://haml.info/) views, except for text emails, which uses ERB
templates.

To help having a consistent formatting, `haml_lint` checks files in CI.

To run it, simply execute:

```sh
haml-lint
```

### VueJS

This project uses VueJS.

The VueJS files should be organized this way:

```text
app/javascript/
├── packs/
│   ├── vue_app1
│   ├── vue_app2
│   └── ...
└── vue/
    ├── helpers/
    ├── mixins/
    ├── stores/ # VueX stores (use modules to reuse stores parts across apps)
    └── views/  # For the views, use the same organization as a Rails app:
        └── posts/
            ├── _form.vue
            ├── _post.vue
            ├── index.vue
            ├── show.vue
            └── ...
```

### Style

Stylesheets are located in `app/assets/stylesheets/` and generated the
default Rails asset pipeline. We use SCSS language.

The application global style file is
`app/assets/stylesheets/application.scss`

Application have themes (`app/assets/stylesheets/theme_*.scss`). If you
create a new one, you'll have to update the form to select one (in
`app/helpers/themes_helper.rb`).

#### Design principles

We want to give a "console" look and feel. Which means
- "every space is related to a character size". These variables are
  widely used for spacing:
- No font sizes. Play with weights, colors and spacings to make things
  stand out.
- No spacings that are not integer multiples of a character size (except
  for special cases with half heights, when boxes outlines would have
  bee made with half blocks (`▄`). Which involves half heights
  paddings... You'll see if you need it).

Some variables are widely used to create spaces:
- `$char-width`: 1 character width
- `$spacer-v`: default vertical spacing (_one character height_)
- `$spacer-h`: default horizontal spacing (_one character width_)

#### Notes

We use _CSS variables_ for colors, which means you never have to use a
color variable directly. If you need a new color, create a CSS variable
in the theme files.

We don't use CSS variables for anything else.

#### Style guide

A style guide is available in development environment, at
[/style](http://localhost:3000/style/). It's a Rails engine, its files
are located in `styleguide/`

#### Unused CSS selectors

Cucumber can be started with the `UNUSED_CSS=true` environment variable.
This will check for unused CSS selectors _after each step_.

It's not used in CI because it's slow (20+ minutes for a normal 3+
minutes run), and results should still be checked by hand, as all pages
are not visited during Cucumber runs.

### Seeds

There are 3 seeds files available in the project:

- `db/seeds_development.rb`
- `db/seeds_production.rb`
- `db/seeds.rb` for shared seeds.

When you seed the database with `rails db:seed`, shared seeds are run
first, then one of the other files is executed, depending on the
environment.

### Continuous integration

CI jobs are configured for Gitlab. Check
`[.gitlab-ci.yml](.gitlab-ci.yml)` to see the list.

### Notes

On models with `has_many :xxx, class_name: "Yyy"`, there is an issue
with Spring causing the classes to be somehow reloaded. To avoid
"ActiveRecord::AssociationTypeMismatch", when assigning an entity to a
relation, re-fetch the related entities:

```rb
class MyModel < ActiveRecord::Base
  has_many 'owner', class_name: 'Cenobite'
end

cenobite = Cenobite.last
MyModel.create title: 'Something', owner: cenobite
# Works the first time
MyModel.create title: 'Something', owner: cenobite
# ActiveRecord::AssociationTypeMismatch on next calls.

# solution:
MyModel.create title: 'Something', owner: Cenobite.first
```

**This may happen in console too**.

Another approach is to disable spring temporarily:

```
DISABLE_SPRING=true bundle exec rails db:migrate
DISABLE_SPRING=true bundle exec rails console
DISABLE_SPRING=true bundle exec rails runner xxx
# ...
```

## Deployment notes

When deploying a new version, you should clear the chat participants
list as the ActionCable connections will be reset:

```sh
rake chat:clear-participants
```

### Cron jobs

Dead man switches are triggered with the `hr:dms:send_emails` task and
needs to be run by your system.

Also, the RSS feeds needs to be fetched with the `hr:feeds:fetch` task.

Additionally, `chat:prune` cleans the chat messages and must be scheduled
too.

For convenience, the [whenever](https://github.com/javan/whenever) gem
is present in gemfile. A task list is present in `config/schedule.rb`,
and can be imported in the system with:

```shell
bundle exec whenever --user deploy --update-crontab

# For more options:
bundle exec whenever --help
```

You'll be all set with these recurring tasks.

## Testing

### Rubocop

[Rubocop](https://rubocop.org/) checks for coding standards, allowing us
to have consistent coding style in the project. Configuration is in
[.rubocop.yml](.rubocop.yml).

Enabled plugins:

- [rubocop-performance](https://docs.rubocop.org/projects/performance),
- [rubocop-rails](https://docs.rubocop.org/projects/rails/) for common
  errors in Rails projects
- [rubocop-rspec](https://github.com/rubocop-hq/rubocop-rspec)

Run it with:

```sh
bundle exec rubocop
# To fix automatically what can be fixed:
bundle exec rubocop -a
```

### RSpec

[RSpec](https://github.com/rspec/rspec) examples are in `spec/`. Run the
suite with:

```sh
bundle exec rspec
```

To debug step by step:

```sh
# Run this once
bundle exec rspec
# Then run this to replay failed examples
bundle exec rspec --only-failures
```

#### Shared contexts

As the project uses Devise for authentication, some shared contexts are
available to use in the specs:

- 'with authenticated user'
- 'with authenticated admin'

### Cucumber

[Cucumber](https://github.com/cucumber/cucumber-rails) is configured
with
[capybara-screenshot](http://github.com/mattheworiordan/capybara-screenshot),
which makes HTML and png screenshots of pages when a step fails.

By default, Cucumber will use Firefox to run the tests, but this can be
changed with the `CAPYBARA_DRIVER` environment variable:

```sh
# Default with firefox
bundle exec cucumber
# Variants
CAPYBARA_DRIVER=firefox-headless bundle exec cucumber
CAPYBARA_DRIVER=chrome bundle exec cucumber
CAPYBARA_DRIVER=chrome-headless bundle exec cucumber
```

When using Chrome/Chromium, Cucumber steps will fail on Javascript
errors.

The project uses the
[webdrivers](https://github.com/titusfortner/webdrivers) gem, which
manage the browsers respective drivers.

#### Special tags

##### time_freeze

If you need your scenario to play at a different date, tag it with `@time_freeze`.

To specify the date, you will need to alter your scenario name to include the target
date and time (it will always be UTC, check `feature/support/configuration.rb` for the
regex).

```feature
  Scenario: Check timezone for creation (server is at 2020/10/26 1:00 UTC)
  #                 Regex matches from ^                               to ^
```

This scenario will play with a server set on 2020/10/26 1:00 AM.
### Code coverage

When using RSpec or Cucumber, code coverage summary is generated in
`coverage/`. Don't hesitate to open it and check by yourself.

### Brakeman

[Brakeman](https://brakemanscanner.org/) is a "security scanner" for
common mistakes leading to security issues.

It can be launched with:

```sh
bundle exec brakeman
```

### ESLint

[ESLint](https://github.com/eslint/eslint) is configured to use Standard
configuration and some small tweaks.

Configuration for VueJS is based on the `vue/recommended` plugin.

Run ESLint with:

```sh
yarn run lint:js
# To auto-fix files:
yarn run lint:js-fix
```

### StyleLint

[Stylelint](https://github.com/stylelint/stylelint) is configured to use
the [stylelint-scss](https://github.com/kristerkari/stylelint-scss)
plugin; the
[stylelint-config-standard](https://github.com/stylelint/stylelint-config-standard)
and
[stylelint-config-recommended-scss](https://github.com/kristerkari/stylelint-config-recommended-scss)
configurations.

```sh
yarn lint:sass
```

**Note:** don't even try to auto-fix your files, auto-fixing ignores the
"disabling" directives in your files.
